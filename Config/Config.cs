﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Reflection;
using System.Web;


namespace Pantry.Config {

  public class ThisVisit {
    public static ServiceLevel ServiceLevel = ServiceLevel.Medium;
  }


  public class SiteContacts {
    public readonly static string WebmasterEmail = "kevinnbrunner@gmail.com";
    public readonly static string KevinnBrunner = "kevinnbrunner@gmail.com";
    public readonly static string JerryS = "JerrySssssssssssssssssssssssss@gmail.com";
  }




  public class SiteConfig {

    public readonly static string RestrictedPassword = "Super187";
    public readonly static string RestrictedCookieValue = "FC4BE562-50A6-42A5-9C0F-D52D5FF6AD13";

    public readonly static int ExpireURLDays = 30;

    public readonly static string CookieName = "Pantry";
    public readonly static int CookieAge = 730;

  }



  public class AppConfig {
    public readonly static string AccessDenied = "AccessDenied";
    public readonly static string Forbidden = "Forbidden";
    public readonly static string Unauthorized = "Unauthorized";

    public readonly static string AdminFeedSecret = "iW2sz6bZTLxjgHodxLzTnKAD3fiUWxZV1S0N";
    public readonly static int PasswordVersion = 1;

    public readonly static string MailServer = "";
    public readonly static bool EnableErrorLogEmail = true;


    public readonly static DateTime MaxDateTime = new DateTime(2099, 1, 1);
    public readonly static DateTime MinDateTime = new DateTime(1970, 1, 1);
    public readonly static DateTime CheckDateTime = new DateTime(1975, 6, 21);
    public readonly static DateTime AbsoluteZeroDateTime = new DateTime(1970, 1, 1);
    public readonly static DateTime TheBeginningDateTime = new DateTime(2014, 1, 1);

    public readonly static List<string> AllowedZips = new List<string>() { "32702", "32102", "32720", "32735", "32736", "32767", "32784" };

  }


  public class DataBaseConfig : ReadWebConfig {
    public readonly static string AzureConnectionString = _AzureConnectionString;
    public readonly static string SiteConnectionString = _SiteConnectionString;
    public readonly static string DevConnectionString = _DevConnectionString;
    public readonly static string SiteMembersConnectionString = _SiteMembersConnectionString;
  }


  public class ReadWebConfig {

    static ReadWebConfig() { }

    protected static string _ActiveDomain { get { return ConfigurationManager.AppSettings["ActiveDomain"]; } }
    protected static string _WebServicesBaseURL { get { return ConfigurationManager.AppSettings["WebServicesBaseURL"]; } }

    protected static string _AzureConnectionString { get { return ConfigurationManager.ConnectionStrings["AzureConnection"].ConnectionString; } }
    protected static string _SiteConnectionString { get { return ConfigurationManager.ConnectionStrings["SiteConnection"].ConnectionString; } }
    protected static string _DevConnectionString { get { return ConfigurationManager.ConnectionStrings["DevConnection"].ConnectionString; } }
    protected static string _SiteMembersConnectionString { get { return ConfigurationManager.ConnectionStrings["PantryMembers"].ConnectionString; } }

    protected static string _MailServer { get { return ConfigurationManager.AppSettings["MailServer"]; } }
    protected static bool _EnableErrorLogEmail { get { return Convert.ToBoolean(ConfigurationManager.AppSettings["EnableErrorLogEmail"]); } }

    protected static int _ExpireURLDays { get { return Convert.ToInt32(ConfigurationManager.AppSettings["ExpireURLDays"]); } }


  }


  public class AWSConfig {
    public static string Key="";
    public static string Secret = "";
    public static string PublicPath = "";
    public static string SecurePath = "";
    public static int SecureDownloadExpireMinutes = 60;
    public static string CloudFrontKeyPath = "";
    public static string CloudFrontKey = "";
    public static string CloudFrontSecure = "";

  }


  public enum ServiceLevel {
    [Description("Any")]
    Any,
    [Description("Low")]
    Low,
    [Description("Medium")]
    Medium,
    [Description("High")]
    High,
  }


}
