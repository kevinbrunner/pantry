﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pantry.Config {
  public class ErrorMessages {

    public readonly static string InvalidPostalCode = "Invalid Postal Code";

    public readonly static string AccountError = "We have a problem with this Account";
    public readonly static string AccountLockedOut = "Account locked out... please wait 5 mintues";
    public readonly static string EmailAddressPasswordMismatch = "Sorry we cant confirm your email and password";
    public readonly static string EmailAddressMismatch = "Email Addresses dont match.";
    public readonly static string PasswordFailed = "Your Password failed.";
    public readonly static string PasswordDontMatch = "Passwords dont match.";
    public readonly static string EmailAddressFormat = "This Email Address doesnt look right.";
    public readonly static string EmailAddressOnFile = "This email address is already in our files";
    public readonly static string PasswordNotStrongEnough = "Password not strong enough";

    public readonly static string WeHadProblemTryAgain = "We had a problem. Please try again...";
    public readonly static string WeHadProblem = "We had a problem...";


  }
}
