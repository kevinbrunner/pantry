﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Pantry.Config;
using Pantry.Entities;
using Pantry.Helpers;
using Pantry.Utilities;

using Dapper;


namespace Pantry.DataAccess {

  public class AdminRepository : Repository {

    //    public void Log(AdminLog adminLog) {

    //      adminLog.Admin = adminLog.Admin.SetAndTrim();
    //      adminLog.AccountID = adminLog.AccountID.SetAndTrim();
    //      adminLog.LongMessage = adminLog.LongMessage.SetAndTrim();
    //      adminLog.Message = adminLog.Message.SetAndTrim();
    //      if(adminLog.Message.Length > 1024) {
    //        adminLog.Message = adminLog.Message.Substring(0, 1023);
    //      }

    //      string sql = @"/*sql*/ --Log
    //                        INSERT INTO AdminLogs
    //                            (
    //                                AccountID
    //                              , Admin
    //                              , LogType
    //                              , Message
    //                              , LongMessage
    //                              , Updated
    //                            )
    //                          VALUES
    //                            (
    //                                @AccountID
    //                              , @Admin
    //                              , " + adminLog.LogType.ToDbQuote() + @"
    //                              , @Message
    //                              , @LongMessage
    //                              , @Updated
    //                            )
    //
    //                        ";

    //      //this.SiteConnection.Execute(sql, adminLog);

    //      using(var conn = GetDbConnection(database: Database.AdminTracing)) {
    //        conn.Open();
    //        conn.Execute(sql, adminLog);
    //      }

    //    }


    //    public void LogWarning(string accountID, string warning) {
    //      if(accountID.IsEmpty()) {
    //        accountID = "IsEmpty";
    //      }

    //      string sqlString = @"/*sql*/ --LogWarning
    //                        INSERT INTO AdminWarning (AccountID, Warning) VALUES (@AccountID, @Warning);
    //                        ";

    //      this.SiteConnection.Execute(sqlString, new { AccountID = accountID, Warning = warning });

    //      using(var conn = GetDbConnection(database: Database.AdminTracing)) {
    //        conn.Open();
    //        conn.Execute(sqlString, new { AccountID = accountID, Warning = warning });
    //      }

    //      //this.SiteConnection.Execute(sqlString, new { AccountID = accountID, Warning = warning });
    //    }


    //    public IEnumerable<CreditCard> GetAllCards() {
    //      string sqlString = @"/*sql*/ --GetAllCards
    //                        SELECT * FROM AccountCards;
    //                        ";

    //      using(var conn = GetDbConnection()) {
    //        conn.Open();
    //        IEnumerable<CreditCard> getAllCards = conn.Query<CreditCard>(sqlString);

    //        return getAllCards;
    //      }

    //      //return this.SiteConnection.Query<CreditCard>(sqlString);


    //    }


    //    public void DeleteCard(int cardID) {
    //      string sqlString = @"/*sql*/ --DeleteCard
    //                        DELETE FROM AccountCards where CardID = @CardID;
    //                        ";
    //      using(var conn = GetDbConnection()) {
    //        conn.Open();
    //        conn.Execute(sqlString, new { CardID = cardID });
    //      }

    //      //this.SiteConnection.Execute(sqlString, new { CardID = cardID });
    //    }


    //    public GeographicalLocation GetGeographicalLocation(string postalCode, string country) {
    //      string sql = @"/*sql*/ --GetGeographicalLocation
    //                        SELECT
    //                          TOP 1
    //                            *
    //                          FROM
    //                            AdminPostalStateCityLatLong
    //                          WHERE
    //                            PostalCode    = @PostalCode
    //                            AND Country   = @Country
    //        ";

    //      return this.SiteConnection.Query<GeographicalLocation>(sql, new { PostalCode = postalCode, Country = country }).FirstOrDefault();
    //    }


    //    public void TrackAdminRoleChange(string adminID, string roleName, string superAdminID, bool isAdd) {
    //      string sqlString = @"/*sql*/ --TrackAdminRoleChange
    //                        INSERT INTO
    //                          AdminRoleTracking
    //                              (
    //                                  AdminID
    //                                , SuperAdminID
    //                                , RoleName
    //                                , IsAdd
    //                                , ChangeDate
    //                              )
    //                            VALUES
    //                              (
    //                                  @AdminID
    //                                , @SuperAdminID
    //                                , @RoleName
    //                                , @IsAdd
    //                                , " + DateTime.Now.ToDbQuote() + @"
    //                              )
    //                        ";

    //      using(var conn = GetDbConnection()) {
    //        conn.Open();
    //        conn.Execute(sqlString, new { AdminID = adminID, RoleName = roleName, SuperAdminID = superAdminID, IsAdd = isAdd });
    //      }

    //    }



    //    public void SaveKeyPair(AdminKeyPairs keyPair) {

    //      DynamicParameters param = new DynamicParameters();

    //      param.Add("@AID", keyPair.AID, direction: ParameterDirection.InputOutput);
    //      param.Add("@IntID", keyPair.IntID);
    //      param.Add("@Key", keyPair.Key);
    //      param.Add("@StrID", keyPair.StrID);
    //      param.Add("@Value", keyPair.Value);
    //      param.Add("@DateValue", keyPair.DateValue);

    //      string sql = @"/*sql*/ --SaveKeyPair
    //                        IF(@AID = 0) BEGIN
    //                          INSERT
    //                            INTO
    //                              AdminKeyPairs
    //                                (
    //                                    [strID]
    //                                  , [intID]
    //                                  , [kkey]
    //                                  , [vvalue]
    //                                  , [datevalue]
    //                                )
    //                              VALUES
    //                                (
    //                                    @StrID
    //                                  , @IntID
    //                                  , @Key
    //                                  , @Value
    //                                  , @DateValue
    //                                )
    //                          SET @AID              = SCOPE_IDENTITY();
    //                        END
    //
    //                        IF(@AID > 0) BEGIN
    //                          UPDATE
    //                              AdminKeyPairs
    //                            SET
    //                                [strID]         = @StrID
    //                              , [intID]         = @IntID
    //                              , [kkey]          = @Key
    //                              , [vvalue]        = @Value
    //                              , [datevalue]        = @DateValue
    //                            WHERE
    //                              aID               = @AID
    //                        END
    //
    //                        ";

    //      this.SiteConnection.Execute(sql, param);

    //      if(keyPair.AID == 0) {
    //        keyPair.AID = param.Get<int>("@AID");
    //      }

    //    }


    //    public AdminKeyPairs GetKeyPair(AdminKeyPairs key) {
    //      string sql = @"/*sql*/ --GetKeyPair
    //                        SELECT
    //                                  [aID]         AS [AID]
    //                                , [strID]       AS [StrID]
    //                                , [intID]       AS [IntID]
    //                                , [kkey]        AS [Key]
    //                                , [vvalue]      AS [Value]
    //                                , [datevalue]   AS [DateValue]
    //                          FROM
    //                            AdminKeyPairs
    //                          WHERE
    //        ";

    //      if(key.AID > 0) {
    //        sql += @"
    //                            aid       = @AID
    //                        ";
    //      } else if(key.Key.IsNotEmpty()) {
    //        sql += @"
    //                            kkey      = @Key
    //                        ";
    //      }

    //      sql += @"
    //                          ORDER BY
    //                            aID DESC;
    //                        ";

    //      return this.SiteConnection.Query<AdminKeyPairs>(sql, key).FirstOrDefault();
    //    }




  }
}
