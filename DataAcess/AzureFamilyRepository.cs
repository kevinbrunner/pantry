﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pantry.Config;
using Pantry.Entities;
using Pantry.Utilities;

using Dapper;


namespace Pantry.DataAccess {
  public class AzureFamilyRepository : Repository {

    public void SaveFamilyHead(FamilyHead family) {
      //string sql = @"/*sql*/
      //              ";

      string sql = @"/*sql*/ --SaveFamily

                    ";

      DynamicParameters param = new DynamicParameters();

      param.Add("@FamilyID", family.FamilyID, direction: ParameterDirection.InputOutput);
      param.Add("@FirstName", family.FirstName.SetAndTrim());
      param.Add("@LastName", family.LastName.SetAndTrim());
      param.Add("@Social", family.Social.SetAndTrim());
      param.Add("@Address1", family.Address1.SetAndTrim());
      param.Add("@Address2", family.Address2.SetAndTrim());
      param.Add("@City", family.City.SetAndTrim());
      param.Add("@State", family.State.SetAndTrim());
      param.Add("@Zip", family.Zip.SetAndTrim());
      param.Add("@Phone", family.Phone.SetAndTrim());
      param.Add("@Cell", family.Cell.SetAndTrim());
      param.Add("@IsFoodStamps", family.IsFoodStamps);
      param.Add("@FoodStampsReason", family.FoodStampsReason.SetAndTrim());
      param.Add("@Updated", DateTime.Now);
      param.Add("@CreateDate", DateTime.Now);
      param.Add("@LastVisitDate", family.LastVisitDate < Config.AppConfig.MinDateTime ? Config.AppConfig.MinDateTime : family.LastVisitDate);
      param.Add("@VisitType", family.VisitType.ToString());
      param.Add("@Year", family.Year.SetAndTrim());
      param.Add("@FamilyType", family.FamilyType.ToString());


      if(family.FamilyID == 0) {
        sql += @"
                        INSERT
                          INTO
                            FAMILY
                            (
                                FirstName
                              , LastName
                              , Social
                              , Address1
                              , Address2
                              , City
                              , State
                              , Zip
                              , Phone
                              , Cell
                              , IsFoodStamps
                              , FoodStampsReason
                              , Updated
                              , CreateDate
                              , LastVisitDate
                              , VisitType
                              , Year
                              , FamilyType
                            )
                          OUTPUT INSERTED.FamilyID
                          VALUES (
                                @FirstName
                              , @LastName
                              , @Social
                              , @Address1
                              , @Address2
                              , @City
                              , @State
                              , @Zip
                              , @Phone
                              , @Cell
                              , @IsFoodStamps
                              , @FoodStampsReason
                              , @Updated
                              , @CreateDate
                              , @LastVisitDate
                              , @VisitType
                              , @Year
                              , @FamilyType
                            )

                        SET @FamilyID = SCOPE_IDENTITY();
                    ";
      } else {
        sql += @"
                        UPDATE
                            FAMILY
                          SET
                              FirstName             = @FirstName
                            , LastName              = @LastName
                            , Social                = @Social
                            , Address1              = @Address1
                            , Address2              = @Address2
                            , City                  = @City
                            , State                 = @State
                            , Zip                   = @Zip
                            , Phone                 = @Phone
                            , Cell                  = @Cell
                            , IsFoodStamps          = @IsFoodStamps
                            , FoodStampsReason      = @FoodStampsReason
                            , Updated               = @Updated
                            , LastVisitDate         = @LastVisitDate
                            , VisitType             = @VisitType
                            , Year                  = @Year
                            , FamilyType            = @FamilyType
                          WHERE
                            FamilyID        = @FamilyID
                    ";
      }

      int newFamilyID = this.AzureConnection.Query<int>(sql, param).SingleOrDefault();
      if(family.FamilyID == 0) {
        family.FamilyID = newFamilyID;
      }

    }


    public FamilyHead GetFamilyHead(int familyID) {

      string sql = @"/*sql*/ --GetFamilyHead
                        SELECT
                              FamilyID
                            , FirstName
                            , LastName
                            , Social
                            , Address1
                            , Address2
                            , City
                            , State
                            , Zip
                            , Phone
                            , Cell
                            , IsFoodStamps
                            , FoodStampsReason
                            , Updated
                            , CreateDate
                            , LastVisitDate
                            , VisitType
                            , Year
                            , FamilyType
                          FROM
                            Family
                          WHERE
                            FamilyID        = " + familyID.ToString();

      return this.AzureConnection.Query<FamilyHead>(sql).FirstOrDefault();
    }


    public void SaveFamilyMembers(List<FamilyMember> familyMembers, int familyID) {
      //string sql = @"/*sql*/ --SaveFamilyMembers
      //              ";

      foreach(FamilyMember member in familyMembers) {
        if(member.FirstName.IsEmpty()) {
          continue;
        }

        if(familyID == 0) {
          familyID = member.FamilyID;
        }
        DynamicParameters param = new DynamicParameters();

        param.Add("@id", member.id);
        param.Add("@FamilyID", familyID);
        param.Add("@Year", member.Year.SetAndTrim());
        param.Add("@CreateDate", DateTime.Now);
        param.Add("@FirstName", member.FirstName.SetAndTrim());
        param.Add("@LastName", member.LastName.SetAndTrim());
        param.Add("@FamilyRelationship", member.FamilyRelationship.ToString());
        param.Add("@IsActive", member.IsActive);
        param.Add("@Updated", DateTime.Now);
        param.Add("@Social", member.Social.SetAndTrim());

        string sql = @"/*sql*/ --SaveFamilyMembers";

        if (member.id == 0) {
          sql += @"
                        INSERT
                          INTO
                            FamilyMembers
                            (
                                FamilyID
                              , FirstName
                              , LastName
                              , Year
                              , FamilyRelationship
                              , Social
                              , IsActive
                              , Updated
                              , CreateDate
                            )
                          VALUES  (
                                @FamilyID
                              , @FirstName
                              , @LastName
                              , @Year
                              , @FamilyRelationship
                              , @Social
                              , @IsActive
                              , @Updated
                              , @CreateDate
                           )
                ";
        } else {
          sql += @"
                        UPDATE
                            FamilyMembers
                          SET
                              FamilyID              = @FamilyID
                            , FirstName             = @FirstName
                            , LastName              = @LastName
                            , Year                  = @Year
                            , FamilyRelationship    = @FamilyRelationship
                            , Social                = @Social
                            , IsActive              = @IsActive
                            , Updated               = @Updated
                          WHERE
                            ID = @ID
                        ";

        }

        this.AzureConnection.Execute(sql, param);

      }

    }


    public IEnumerable<FamilyMember> GetFamilyMembers(int familyID) {
      string sql = @"/*sql*/ --GetFamilyMembers
                        SELECT
                              id
                            , FamilyID
                            , FirstName
                            , LastName
                            , Year
                            , FamilyRelationship
                            , Social
                            , IsActive
                            , Updated
                            , CreateDate
                          FROM
                            FamilyMembers
                          WHERE
                            FamilyID      =" + familyID.ToString();



      return this.AzureConnection.Query<FamilyMember>(sql);
    }


    public FamilyData GetFamily(int familyID) {
      string sql = @"/*sql*/ --GetFamilyHead
                        SELECT
                              FamilyID
                            , FirstName
                            , LastName
                            , Social
                            , Address1
                            , Address2
                            , City
                            , State
                            , Zip
                            , Phone
                            , Cell
                            , IsFoodStamps
                            , FoodStampsReason
                            , Updated
                            , CreateDate
                            , LastVisitDate
                            , VisitType
                            , Year
                          FROM
                            Family
                          WHERE
                            FamilyID        = " + familyID.ToString();


      sql += @"
                        SELECT
                              id
                            , FamilyID
                            , FirstName
                            , LastName
                            , Year
                            , FamilyRelationship
                            , Social
                            , IsActive
                            , Updated
                            , CreateDate
                          FROM
                            FamilyMembers
                          WHERE
                            FamilyID      =" + familyID.ToString();

      sql += @"

                        SELECT
                          TOP 5
                              FamilyID
                            , CreateDate        AS LastVisitDate
                            , NumberOfMembers
                            , VisitType
                            , ServiceLevel
                            , PantryID
                          FROM
                            FamilyVisits
                          WHERE
                            FamilyID    = @FamilyID
                          ORDER BY
                            id DESC

                  ";

      FamilyData family = new FamilyData();
      FamilyHead familyHead = new FamilyHead();
      List<FamilyMember> allMembers = new List<FamilyMember>();
      List<FamilyMember> familyMembers = new List<FamilyMember>();
      List<FamilyMember> familyPickUpAssociates  = new List<FamilyMember>();
      List<LastVisit> lastVisits = new List<LastVisit>();

      using(var conn = GetDbConnection(database: Database.Azure)) {
        conn.Open();

        SqlMapper.GridReader multiQuery = conn.QueryMultiple(sql, new { FamilyID = familyID });

        familyHead = multiQuery.Read<FamilyHead>().FirstOrDefault();
        allMembers = multiQuery.Read<FamilyMember>().ToList();
        familyMembers = allMembers.Where(w => w.FamilyRelationship != FamilyRelationship.PickUp).ToList();
        familyPickUpAssociates = allMembers.Where(w => w.FamilyRelationship == FamilyRelationship.PickUp).ToList();
        lastVisits = multiQuery.Read<LastVisit>().ToList();
      }

      family.FamilyHead = familyHead;
      family.FamilyMembers = familyMembers;
      family.PickUpAssociates = familyPickUpAssociates;
      family.LastVisits = lastVisits;

      return family;
    }


    public IEnumerable<FamilyHead> GetAllHeads() {

      string sql = @"/*sql*/ --GetFamilyHead
                        SELECT
                              FamilyID
                            , FirstName
                            , LastName
                            , Social
                            , Address1
                            , Address2
                            , City
                            , State
                            , Zip
                            , Phone
                            , Cell
                            , IsFoodStamps
                            , FoodStampsReason
                            , Updated
                            , CreateDate
                            , LastVisitDate
                            , VisitType
                            , Year
                          FROM
                            Family
                    ";

      return this.AzureConnection.Query<FamilyHead>(sql);
    }


  }
}
