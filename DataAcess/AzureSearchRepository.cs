﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pantry.Entities;
using Pantry.Helpers;
using Pantry.Utilities;

using Dapper;


namespace Pantry.DataAccess {
  public class AzureSearchRepository : Repository {

    public void SaveSearchTerms(int familyID, List<string> searchTerms) {

      string sql = @"/*sql*/ --SaveSearchTerms

                        DELETE
                          FROM
                            SearchTerms
                          WHERE
                            FamilyID = " + familyID.ToString();

      foreach(string term in searchTerms) {
        if(term.IsNotEmpty()) {
          sql += @"

                        INSERT
                          INTO
                            SearchTerms
                          (
                              FamilyID
                            , Term
                          )
                          VALUES
                          (
                              " + familyID.ToString() + @"
                            , " + term.ToDbQuote() + @"
                          )

                        ";
        }
      }

      this.AzureConnection.Execute(sql);

    }





  }
}
