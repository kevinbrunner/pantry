﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Pantry.Config;
//using Pantry.Entities;
//using Pantry.Helpers;
using Pantry.Utilities;

using Dapper;


namespace Pantry.DataAccess {

  public class ConfirmCodeRepository : Repository {

//    public void SaveConfirmCode(ConfirmationCode confirmCode) {

//      if(confirmCode.cID == 0 || confirmCode.Updated < Config.AppConfig.minDateTime) {
//        confirmCode.Updated = DateTime.Now;
//      }

//      string sqlString = @"/*sql*/ --SaveConfirmCode";
//      sqlString += @"
//                        DELETE
//                          FROM
//                            AccountConfirmCodes
//                          WHERE
//                            Action                = " + confirmCode.Action.ToDbQuote() + @"
//                            AND EmailAddress      = @EmailAddress;
//
//                        IF(@cID = 0) BEGIN
//                          INSERT INTO
//                              AccountConfirmCodes
//                            (
//                                AccountID
//                              , CreateDate
//                            )
//                            VALUES
//                            (
//                                @AccountID
//                              , @CreateDate
//                            );
//
//                          SET @cID = SCOPE_IDENTITY();
//                        END
//
//                        UPDATE
//                            AccountConfirmCodes
//                          SET
//                              AccountID               = @AccountID
//                            , ConfirmCode             = @ConfirmCode
//                            , Action                  = " + confirmCode.Action.ToDbQuote() + @"
//                            , ActionParams            = @ActionParams
//                            , EmailAddress            = @EmailAddress
//                            , ExpireDate              = @ExpireDate
//                            , AssociatedAccountID     = @AssociatedAccountID
//                            , Updated                 = @Updated
//                        WHERE
//                          cID                         = @cID
//
//                        SELECT @cID
//                ";

//      using(var conn = GetDbConnection()) {
//        conn.Open();
//        confirmCode.cID = conn.Query<int>(sqlString, confirmCode).Single();
//      }
//    }


//    public ConfirmationCode GetConfirmCode(string confirmCode) {
//      string sqlString = @"/*sql*/ --GetConfirmCode";
//      sqlString += @"
//
//                        SELECT
//                              cID
//                            , AccountID
//                            , ConfirmCode
//                            , EmailAddress
//                            , ExpireDate
//                            , Action
//                            , ActionParams
//                            , AssociatedAccountID
//                            , Updated
//                            , CreateDate
//                          FROM
//                            AccountConfirmCodes
//                          WHERE
//                            ConfirmCode       = @ConfirmCode
//
//                ";

//      ConfirmationCode confirmCodeObj = null;

//      using(var conn = GetDbConnection()) {
//        conn.Open();
//        confirmCodeObj = conn.Query<ConfirmationCode>(sqlString, new { ConfirmCode = confirmCode }).FirstOrDefault();
//      }

//      return confirmCodeObj;
//    }


//    public void DeleteConfirmCode(Account account) {
//      string sqlString = @"/*sql*/ --DeleteConfirmCode1";
//      sqlString += @"
//                        DELETE FROM
//                          FROM
//                            AccountConfirmCodes
//                          WHERE
//                            AccountID         = @AccountID
//
//                ";

//      using(var conn = GetDbConnection()) {
//        conn.Open();
//        conn.Execute(sqlString, new { AccountID = account.AccountID });
//      }
//    }


//    public void DeleteConfirmCode(int codeID) {
//      string sqlString = @"/*sql*/ --DeleteConfirmCode2";
//      sqlString += @"
//                        DELETE
//                          FROM
//                            AccountConfirmCodes
//                          WHERE
//                            cID               = " + codeID.ToString();

//      using(var conn = GetDbConnection()) {
//        conn.Open();
//        conn.Execute(sqlString);
//      }
//    }


//    public void DeleteConfirmCode(string confirmCode) {
//      string sqlString = @"/*sql*/ --DeleteConfirmCode3";
//      sqlString += @"
//                        DELETE FROM
//                            AccountConfirmCodes
//                          WHERE
//                            confirmCode       = @ConfirmCode
//
//                ";

//      using(var conn = GetDbConnection()) {
//        conn.Open();
//        conn.Execute(sqlString, new { ConfirmCode = confirmCode });
//      }
//    }


//    public void DeleteConfirmCode(ConfirmCodeAction action, string identifier) {

//      string sqlString = @"/*sql*/ --DeleteConfirmCode4";
//      sqlString += @"
//                        DELETE FROM
//                            AccountConfirmCodes
//                          WHERE
//                            Action            = " + action.ToDbQuote() + @"
//                            AND (EmailAddress = @identifier OR AccountID = @Identifier)
//
//                ";

//      using(var conn = GetDbConnection()) {
//        conn.Open();
//        conn.Execute(sqlString, new { Identifier = identifier });
//      }
//    }


//    public string GetAccountIDFromJuicerToken(string token) {
//      string sql = @"/*sql*/ --GetAccountIDFromJuicerToken
//
//                        SELECT
//                            AccountID
//                          FROM
//                            AccountDownloaders
//                          WHERE
//                            Token = @Token
//                    ";

//      string accountID = "";
//      using (var conn = GetDbConnection()) {
//        conn.Open();
//        accountID = this.SiteConnection.Query<string>(sql, new { Token = token }).SingleOrDefault();
//      }

//      return accountID;
//    }


//    public IEnumerable<ConfirmationCode> GetConfirmCodes(string accountID, ConfirmCodeAction confirmCodeAction, DateTime createDate) {
//      string sql = @"/*sql*/ --GetConfirmCodes
//                        SELECT
//                              cID
//                            , AccountID
//                            , EmailAddress
//                            , ConfirmCode
//                            , ExpireDate
//                            , Action
//                            , ActionParams
//                            , AssociatedAccountID
//                            , Updated
//                          FROM
//                            AccountConfirmCodes
//                          WHERE
//                            AccountID           = @AccountID
//                            AND Action          = " + confirmCodeAction.ToDbQuote() + @"
//                            AND CreateDate      > " + createDate.ToDbQuote() + @"
//                        ";

//      IEnumerable<ConfirmationCode> confirmCodes;

//      using (var conn = GetDbConnection()) {
//        conn.Open();
//        confirmCodes = conn.Query<ConfirmationCode>(sql, new { AccountID = accountID });
//      }

//      return confirmCodes;
//    }


//    public IEnumerable<ConfirmationCode> GetInviteConfirmCodes(string accountID, DateTime createDate) {
//      string sql = @"/*sql*/ --GetConfirmCodes
//                        SELECT
//                              cID
//                            , AccountID
//                            , EmailAddress
//                            , ConfirmCode
//                            , ExpireDate
//                            , Action
//                            , ActionParams
//                            , AssociatedAccountID
//                            , Updated
//                          FROM
//                            AccountConfirmCodes
//                          WHERE
//                            AssociatedAccountID   = @AccountID
//                            AND (Action           = " + ConfirmCodeAction.SubscriptionInvitation.ToDbQuote() + @"
//                                 OR Action        = " + ConfirmCodeAction.AcceptedInvitation.ToDbQuote() + @"
//                                )
//                            AND CreateDate        >= " + createDate.ToDbQuote() + @"
//                        ";

//      IEnumerable<ConfirmationCode> confirmCodes;

//      using (var conn = GetDbConnection()) {
//        conn.Open();
//        confirmCodes = conn.Query<ConfirmationCode>(sql, new { AccountID = accountID });
//      }

//      return confirmCodes;
//    }




  }
}
