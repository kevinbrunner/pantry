﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web;

using Pantry.Config;
//using Pantry.Helpers;
//using Pantry.Entities;
using Pantry.Utilities;


namespace Pantry.DataAccess {

  public static class dbConfig {

    private static string juicyMembersString;
    private static string juicySiteConnectionString;
    private static string juicerConnectionString;
    private static string djConnectionString;
    private static string preSetsConnectionString;

    static dbConfig() {
      juicyMembersString = Config.DataBaseConfig.SiteMembersConnectionString;
      juicySiteConnectionString = Config.DataBaseConfig.SiteConnectionString;
    }

    public static string JuicyMembersString { get { return juicyMembersString; } }
    public static string JuicySiteConnectionString { get { return juicySiteConnectionString; } }
    public static string PreSetsConnectionString { get { return preSetsConnectionString; } }
    public static string JuicerConnectionString { get { return juicerConnectionString; } }
    public static string DigitalJuiceConnectionString { get { return djConnectionString; } }
  }


  public enum dbList {
    JuicySite,
    JuicyMembers
  }


  namespace sqlCmd {
    public class sqlCommand {

      private string connectionString = "";
      private string statement = "";
      private dbList db;
      private CommandType sqlCommandType_ = CommandType.StoredProcedure;
      private int timeOut_ = 0;
      private SqlCommand cmd;
      private sqlCommand() { }

      public sqlCommand(string text) {
        statement = text;
        connectionString = dbConfig.JuicySiteConnectionString;
        calcSqlCommand();
      }

      public sqlCommand(string text, dbList dbSelected) {
        statement = text;
        db = dbSelected;
        getConnectionString();
        calcSqlCommand();
      }

      public sqlCommand(string text, CommandType sqlCommandType) {
        statement = text;
        getConnectionString();
        sqlCommandType_ = sqlCommandType;
        calcSqlCommand();
      }

      public sqlCommand(string text, dbList dbSelected, CommandType sqlCommandType) {
        statement = text;
        db = dbSelected;
        getConnectionString();
        sqlCommandType_ = sqlCommandType;
        calcSqlCommand();
      }

      public sqlCommand(string text, dbList dbSelected, CommandType sqlCommandType, int timeOut) {
        statement = text;
        db = dbSelected;
        getConnectionString();
        sqlCommandType_ = sqlCommandType; ;
        timeOut_ = timeOut;
        calcSqlCommand();
      }

      private void getConnectionString() {
        switch(db) {
          case dbList.JuicySite: connectionString = dbConfig.JuicySiteConnectionString; break;
          case dbList.JuicyMembers: connectionString = dbConfig.JuicyMembersString; break;
        }
      }

      private void calcSqlCommand() {
        cmd = new SqlCommand();
        cmd.Connection = new SqlConnection(connectionString);
        string start = statement.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[0].ToLower();
        if("..select.update.delete.insert.declare./*sql*/".IndexOf(start) > 0 || sqlCommandType_ == CommandType.Text) { cmd.CommandType = CommandType.Text; } else { cmd.CommandType = CommandType.StoredProcedure; }
        if(timeOut_ > 0) { cmd.CommandTimeout = timeOut_; }
        cmd.CommandText = statement;
      }

      public SqlCommand GetTheCommand { get { return cmd; } }

    }
  }


  public static class GDA {
      static GDA() { }

      // error terms
      //Message: A network-related or instance-specific error occurred while establishing a connection to SQL Server. The server was not found or was not accessible. Verify that the instance name is correct and that SQL Server is configured to allow remote connections. (provider: Named Pipes Provider, error: 40 - Could not open a connection to SQL Server)
      //Source: .Net SqlClient Data Provider

      public static byte[] Byte_Return(SqlCommand command) {
          byte[] rawData = null;
          try {
              command.Connection.Open();
              SqlDataReader reader = command.ExecuteReader();
              if(reader.Read()) {
                  rawData = (byte[])Array.CreateInstance(typeof(byte), reader.GetInt32(0));
              }

              if(reader.NextResult() && reader.Read()) {
                  reader.GetBytes(0, 0, rawData, 0, rawData.Length);
              }
              reader.Close();
          }
          catch(Exception ex) {
              LogDBError(ex, HttpContext.Current.User.Identity.Name, GetSPandParam(command));
              throw ex;
          }
          finally { command.Connection.Close(); }
          return rawData;
      }

      public static DataTable Table_Return(SqlCommand command) {
          DataTable table = new DataTable();
          try {
              command.Connection.Open();
              SqlDataReader reader = command.ExecuteReader();
              table = new DataTable();
              table.Load(reader);
              reader.Close();
          } catch (Exception ex) { LogDBError(ex, HttpContext.Current.User.Identity.Name, GetSPandParam(command)); throw ex; }
          finally { command.Connection.Close(); }
          return table;
      }

      public static int Affected_Rows_Returned(SqlCommand command) {
          int affectedRows = -1;
          try {
              command.Connection.Open();
              affectedRows = command.ExecuteNonQuery();
          } catch (Exception ex) { LogDBError(ex, HttpContext.Current.User.Identity.Name, GetSPandParam(command)); throw ex; }
          finally { command.Connection.Close(); }
          return affectedRows;
      }

      public static string CheckSQL(SqlCommand command) {
          string value = "";
          try {
              command.Connection.Open();
              value = command.ExecuteScalar().ToString();
          }
          catch(Exception) { }
          finally { command.Connection.Close(); }
          return value;
      }

      public static void Nothing_Returned(SqlCommand command) {
          Affected_Rows_Returned(command);
      }

      public static string Variable_ReturnErrorThrow(SqlCommand command) {
          string value = "";
          try {
              command.Connection.Open();
              value = command.ExecuteScalar().ToString();
          } catch (Exception ex) { LogDBError(ex, "", GetSPandParam(command)); throw ex; }
          finally { command.Connection.Close(); }
          return value;
      }

      public static string Variable_Return(SqlCommand command) {
          string value = "";
          try {
              command.Connection.Open();
              value = command.ExecuteScalar().ToString();
          } catch (Exception ex) { LogDBError(ex, HttpContext.Current.User.Identity.Name, GetSPandParam(command)); throw ex; }
          finally { command.Connection.Close(); }
          return value;
      }

      public static string Variable_ReturnNoLog(SqlCommand command) {
          string value = "";
          try {
              command.Connection.Open();
              value = command.ExecuteScalar().ToString();
          }
          catch { value = ""; }
          finally { command.Connection.Close(); }
          return value;
      }

      public static DataSet DataSet_Return(SqlCommand command, string[] tableNames) {
          DataSet ds = new DataSet();
          try {
              command.Connection.Open();
              ds.Load(command.ExecuteReader(), LoadOption.OverwriteChanges, tableNames);
          }
          catch(Exception ex) {
            LogDBError(ex, HttpContext.Current.User.Identity.Name, GetSPandParam(command)); throw ex;
          }
          finally { command.Connection.Close(); }
          return ds;
      }

      //  GDA.DataSet_Return(comm, new string[] {""});

      public static string GetSPandParam(SqlCommand command) {
          SqlParameterCollection spc = command.Parameters;
          string paramm = "Parameters:\n\t";
          foreach(SqlParameter spp in spc) {
              paramm += spp.DbType + " : " + spp.ParameterName + " : " + spp.Value + "\n\t";
          }
          return ("\n" + command.CommandText + "\n" + paramm + "\n");
      }


      // Send error log mail
      public static void LogDBError(Exception ex, string accountID, string sqlStuff) {
        if(ex.Message.IndexOf("A network-related or instance-specific error occurred") >= 0) {
          return;
        }
        if(ex.Message.IndexOf("Timeout expired.") >= 0) {
          return;
        }
        string dateTime = DateTime.Now.ToLongDateString() + ", at "
                        + DateTime.Now.ToShortTimeString();
        string errorMessage = "Exception generated on " + dateTime;
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        errorMessage += "\n\n ID: " + accountID; // +" - IP:" + IPAddress;
        if(accountID != "") {
          errorMessage += "\n\n Name: " + accountID;
        }
        errorMessage += "\n\n SQLStuff: " + sqlStuff;
        errorMessage += "\n\n Page location: " + context.Request.RawUrl;
        errorMessage += "\n\n Message: " + ex.Message;
        errorMessage += "\n\n Source: " + ex.Source;
        errorMessage += "\n\n Method: " + ex.TargetSite;
        errorMessage += "\n\n Stack Trace: \n\n" + ex.StackTrace;
        if(AppConfig.EnableErrorLogEmail) {
          string from = "Digital Juice DB error" + "<kevinnbrunner@gmail.com>";
          string to = SiteContacts.KevinnBrunner;
          string subject = "Digital Juice DB log error " + System.Net.Dns.GetHostName().ToLower();
          string body = errorMessage;
          //Helpers.Emailing.SendMail(from, to, subject, body);
        }
      }

  }

}
