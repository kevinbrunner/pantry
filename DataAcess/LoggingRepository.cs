﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pantry.Config;
using Pantry.Entities;
using Pantry.Utilities;

using Dapper;


namespace Pantry.DataAccess {
  public class LoggingRepository : Repository {


    public void SaveVisitLog(VisitLog familyVisitLog, bool overwriteToday) {

      string sql = @"/*sql*/ --SaveVisitLog";

      if(overwriteToday) {
        sql += @"/*sql*/ --SaveVisitLog
                        DELETE
                          FROM
                            FamilyVisits
                          WHERE
                                DATEPART(DAYOFYEAR, CreateDate)   = DATEPART(DAYOFYEAR, GETDATE())
                            AND DATEPART(YEAR, CreateDate)        = DATEPART(YEAR, GETDATE())
                            AND FamilyID                          = " + familyVisitLog.FamilyID.ToString();
      }

      sql += @"
                        INSERT
                          INTO
                            FamilyVisits
                            (
                               FamilyID
                             , CreateDate
                             , NumberOfMembers
                             , VisitType
                             , ServiceLevel
                             , PantryID
                            )
                          VALUES
                           (
                               " + familyVisitLog.FamilyID.ToString() + @"
                             , " + GetDate() + @"
                             , " + familyVisitLog.NumberOfMembers.ToString() + @"
                             , " + familyVisitLog.VisitType.ToDbQuote() + @"
                             , " + familyVisitLog.ServiceLevel.ToDbQuote() + @"
                             , " + familyVisitLog.PantryID.ToString() + @"
                            )

                        UPDATE
                            FAMILY
                          SET
                            LastVisitDate         = " + DateTime.Now.ToDbQuote() + @"
                          WHERE
                            FamilyID        = " + familyVisitLog.FamilyID.ToString() + @"

                    ";

      this.SiteConnection.Execute(sql);

    }





  }
}
