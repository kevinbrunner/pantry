﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Pantry.Config;
//using Pantry.Entities;
//using Pantry.Helpers;
using Pantry.Utilities;

using Dapper;


namespace Pantry.DataAccess {

  public class MembersRepository : Repository {

//    public Account GetAccount(string accountID, string thisIPAddressUpdate, string djCustomerID, string solomonCustID, string email) {

//      if (accountID == "" && djCustomerID == "" && solomonCustID == "" && email == "") {
//        throw new Exception("Missing Search Value");
//      }

//      string sql = @"/*sql*/ --GetAccount
//
//                        /****** test harness *****/
//                        --DECLARE @AccountID varchar(36) = '3b2039ec-0a98-4926-9ee8-d5902efa12a9';
//                        /**** end test harness ***/
//                        SET NOCOUNT ON;";

//      if (thisIPAddressUpdate.IsNotEmpty() && accountID.IsNotEmpty()) {
//        sql += @"
//                        UPDATE
//                            Acnt_Accounts
//                          SET
//                            LastIpAddress = @LastIPAddressUpdate
//                          WHERE
//                            AccountID                     = @AccountID;
//                        ";
//      }

//      sql += @"
//
//                        SELECT";
//      sql += GetAccountFields();
//      sql += @"
//                          FROM
//                            Acnt_Accounts a
//                          WHERE
//                        ";
//      if (email.IsNotEmpty()) { // set where, then get the AccountID to get the card
//        sql += @"
//                            email                           = @Email;
//
//                        SELECT  @AccountID = AccountID FROM Acnt_Accounts WHERE email         = @Email;
//                        ";
//      } else if (solomonCustID.IsNotEmpty()) { // set where, then get the AccountID to get the card
//        sql += @"
//                            SolomonCustID                   = @SolomonCustID;
//
//                        SELECT  @AccountID = AccountID FROM Acnt_Accounts WHERE SolomonCustID = @SolomonCustID;
//                        ";
//      } else if (djCustomerID.IsNotEmpty()) { // set where, then get the AccountID to get the card
//        sql += @"
//                            DJCustomerID                    = @DJCustomerID;
//
//                        SELECT @AccountID = AccountID FROM Acnt_Accounts WHERE DJCustomerID   = @DJCustomerID;
//                        ";
//      } else if (accountID.IsNotEmpty()) {
//        sql += @"
//                            AccountID                       = @AccountID;
//                        ";
//      } else {
//        throw new Exception("Missing Search Value");
//      }

//      sql += @"
//                        SELECT
//                              CardID
//                            , AccountID
//                            , NameOnCard
//                            , CardX                                                           AS CardCheck
//                            , CardType
//                            , Token
//                            , FriendlyName
//                            , Organization
//                            , Address1
//                            , Address2
//                            , Address3
//                            , City
//                            , State
//                            , Zip
//                            , Country
//                            , Phone
//                            , XDate
//                            , XDateWarn
//                            , LastIP
//                            , LastUsed
//                            , IsCVV2_Verified
//                            , CardEncrypt
//                            , Salt
//                            , Updated
//                            , CardEncrtypPassword
//                            , CardStatus
//                            , FirstSix
//                            , LastFour
//                            , LastCvvCheck
//                            , RiskLevel
//                            , FirstName
//                            , LastName
//                          FROM
//                            AccountCards
//                          WHERE
//                            AccountID                       = @AccountID
//                            AND CardStatus                  = " + CardStatus.Default.ToDbQuote() + @";
//
//                          SELECT
//                                d.JuicerID                                                    AS JuicerID
//                              , d.JuicerName                                                  AS JuicerName
//                              , d.IsDefault                                                   AS IsDefault
//                            FROM
//                              AccountDownloaders d
//                            WHERE
//                              d.AccountID                   = @AccountID
//                            ORDER BY
//                              isDefault DESC;
//
//              ";


//      Account account = null;

//      using (var conn = GetDbConnection(database: Database.JuicyWorld)) {
//        conn.Open();

//        SqlMapper.GridReader multiQuery = conn.QueryMultiple(sql, new { AccountID = accountID, LastIPAddressUpdate = thisIPAddressUpdate, SolomonCustID = solomonCustID, DJCustomerID = djCustomerID, Email = email });

//        account = multiQuery.Read<Account>().FirstOrDefault();

//        if (account == null) {
//          account = new Account();
//        } else {
//          account.AboutMe = account.AboutMe ?? "";
//          account.CreditCard = multiQuery.Read<CreditCard>().FirstOrDefault();
//          if (account.CreditCard == null || account.CreditCard.CardID == 0) {
//            account.CreditCard = new CreditCard();
//            account.XDate = Config.AppConfig.minDateTime;
//            account.XDateWarn = Config.AppConfig.minDateTime;
//            account.SingleClickCardName = "";
//            account.CreditCard.AccountID = account.AccountID;
//          } else {
//            account.CreditCard.Year = account.CreditCard.XDate.Year.ToString();
//            account.CreditCard.Month = account.CreditCard.XDate.Month.ToString().PadLeft(2, '0');

//            account.XDate = account.CreditCard.XDate;
//            account.XDateWarn = account.CreditCard.XDateWarn;
//            account.CreditCard.Number = account.CreditCard.FirstSix + account.CreditCard.LastFour;
//            if (account.CreditCard.CardID == account.SingleClickCardID) {
//              account.SingleClickCardName = account.CreditCard.FriendlyName;
//            } else {
//              account.SingleClickCardName = "";
//            }
//          }
//          account.Juicers = multiQuery.Read<AccountJuicer>().ToList();
//        }

//      }

//      return account;
//    }


//    public void SaveAccount(Account account) {
//      DynamicParameters param = new DynamicParameters();

//      param.Add("@AccountID_", account.AccountID_, direction: ParameterDirection.InputOutput);
//      param.Add("@AccountID", account.AccountID);
//      param.Add("@FirstName", account.FirstName);
//      param.Add("@LastName", account.LastName);
//      param.Add("@NickName", account.NickName);
//      param.Add("@Email", account.Email);

//      if (account.PasswordSalt.IsEmpty()) {
//        account.PasswordSalt = this.GetMembershipSalt(account.AccountID);
//      }
//      param.Add("@PasswordSalt", account.PasswordSalt);
//      param.Add("@PasswordVersion", account.PasswordVersion);

//      param.Add("@AltEmail", account.AltEmail);
//      param.Add("@SendEmail", account.SendEmail);
//      param.Add("@TrustLevel", account.TrustLevel);
//      param.Add("@RewardLevel", account.RewardLevel);
//      param.Add("@CardThreshold", account.CardThreshold);
//      param.Add("@MobilePIN", account.MobilePIN);
//      param.Add("@SingleClickCardID", account.SingleClickCardID);
//      param.Add("@SubscriptionStatus", account.SubscriptionStatus.ToString());
//      param.Add("@NextRenewalDate", account.NextRenewalDate);
//      param.Add("@NextRenewalCheckDate", account.NextRenewalCheckDate);
//      param.Add("@StatusMessage", ""); //account.StatusMessage);
//      param.Add("@SubscriptionFutureStatus", account.SubscriptionFutureStatus.ToString());

    //      param.Add("@Telephone", account.Telephone.SetAndTrim());

//      param.Add("@SkinTheme", SkinningTheme.Juicer.ToString());       //param.Add("@SkinTheme", account.SkinTheme.ToString());
//      param.Add("@ImageOption", ImageOption.Opt_Black_0_0_0.ToString()); // account.ImageOption.ToString());

//      param.Add("@PageViewMode", account.PageViewMode.ToString());
    //      param.Add("@DefaultJuicerID", account.DefaultJuicerID .SetAndTrim());

//      param.Add("@DJCustomerID", account.DJCustomerID ?? String.Empty);
//      param.Add("@SolomonCustID", account.SolomonCustID ?? String.Empty);

//      param.Add("@LockedReason", account.LockedReason);
//      param.Add("@LastIpAddress", account.LastIpAddress ?? String.Empty);

//      param.Add("@GroupAccountID", String.IsNullOrWhiteSpace(account.GroupAccountID) ? account.AccountID : account.GroupAccountID);
//      param.Add("@AccountType", account.AccountType.ToString());
//      param.Add("@GroupAccountRole", account.GroupAccountRole.ToString());
//      param.Add("@HasJuicerAccess", account.HasJuicerAccess);

//      param.Add("@ActiveSince", account.ActiveSince);
//      param.Add("@HasLegacyContents", account.HasLegacyContents);

//      param.Add("@PurchaseLimit", account.PurchaseLimit);
//      param.Add("@DownloadAllotmentMB", account.DownloadAllotmentMB);
//      param.Add("@DownloadPurchaseMB", account.DownloadPurchaseMB);
//      if (account.SubscriptionItemCreditCount < 0) { account.SubscriptionItemCreditCount = 0; }
//      param.Add("@SubscriptionItemCreditCount", account.SubscriptionItemCreditCount);

//      if (account.MemberLevel == MembershipLevel.All) {
//        throw new Exception("MemberLevel");
//      }
//      param.Add("@MemberLevel", account.MemberLevel.ToString());

//      if (account.MemberType == MembershipType.All) {
//        throw new Exception("MemberType");
//      }
//      param.Add("@MemberType", account.MemberType.ToString());

//      param.Add("@HasDownloadAllotmentBeenReached", !((account.DownloadAllotmentMB + account.DownloadPurchaseMB) > 0));
//      param.Add("@StoreCredit", account.StoreCredit);

//      if (account.AvatarPath == Config.SiteConfig.AvatarPath) {
//        account.AvatarPath = null;
//        account.AvatarCDN = 1;
//      }

//      param.Add("@AvatarCDN", account.AvatarCDN);
//      param.Add("@AvatarPath", account.AvatarPath);

//      param.Add("@Country", account.Country ?? String.Empty);
//      param.Add("@IsMerged", account.IsMerged);

//      if (account.CreateDate == null || account.CreateDate < new DateTime(2014, 03, 15)) {
//        account.CreateDate = DateTime.Now;
//      }
//      param.Add("@CreateDate", account.CreateDate);

//      param.Add("@IsInternalAccount", account.IsInternalAccount);

//      param.Add("@Preferences", account.Preferences.PadRight(16, '0'));

//      //9972721
//      string sql = @"/*sql*/ --SaveAccount
//                        SET NOCOUNT ON;
//                        IF(@AccountID_ = 0) BEGIN
//                          INSERT INTO
//                            Acnt_Accounts
//                              (
//                                  AccountID
//                                , CreateDate
//                                , WasDJAccount
//                                , Country
//                              )
//                            VALUES
//                              (
//                                  @AccountID
//                                , " + DateTime.Now.ToDbQuote() + @"
//                                , 0
//                                , ''
//                              );
//                            SET @AccountID_ = SCOPE_IDENTITY();
//                        END
//
//                        UPDATE
//                            Acnt_Accounts
//                          SET
//                              FirstName                         = @FirstName
//                            , LastName                          = @LastName
//                            , NickName                          = @NickName
//                            , Email                             = @Email
//                            , PasswordSalt                      = @PasswordSalt
//                            , PasswordVersion                   = @PasswordVersion
//                            , AltEmail                          = @AltEmail
//                            , SendEmail                         = @SendEmail
//                            , TrustLevel                        = @TrustLevel
//                            , RewardLevel                       = @RewardLevel
//                            , CardThreshold                     = @CardThreshold
//                            , MobilePIN                         = @MobilePIN
//                            , SingleClickCardID                 = @SingleClickCardID
//                            , SubscriptionStatus                = @SubscriptionStatus
//                            , SubscriptionFutureStatus          = @SubscriptionFutureStatus
//                            , NextRenewalDate                   = @NextRenewalDate
//                            , NextRenewalCheckDate              = @NextRenewalCheckDate
//                            , SkinTheme                         = @SkinTheme
//                            , PageViewMode                      = @PageViewMode
//                            , ImageOption                       = @ImageOption
//                            , DefaultJuicerID                   = @DefaultJuicerID
//                            , LockedReason                      = @LockedReason
//                            , LastIpAddress                     = @LastIpAddress
//                            , GroupAccountID                    = @GroupAccountID
//                            , AccountType                       = @AccountType
//                            , GroupAccountRole                  = @GroupAccountRole
//                            , HasJuicerAccess                   = @HasJuicerAccess
//                            , ActiveSince                       = @ActiveSince
//                            , DownloadAllotmentMB               = @DownloadAllotmentMB
//                            , DownloadPurchaseMB                = @DownloadPurchaseMB
//                            , HasDownloadAllotmentBeenReached   = @HasDownloadAllotmentBeenReached
//                            , PurchaseLimit                     = @PurchaseLimit
//                            , Country                           = @Country
//                            , IsMerged                          = @IsMerged
//                            , StatusMessage                     = @StatusMessage
//                            , StoreCredit                       = @StoreCredit
//                            , AvatarPath                        = @AvatarPath
//                            , AvatarCDN                         = @AvatarCDN
//                            , MemberType                        = @MemberType
//                            , MemberLevel                       = @MemberLevel
//                            , Telephone                         = @Telephone
//                            , DJCustomerID                      = @DJCustomerID
//                            , CreateDate                        = @CreateDate
//                            , HasLegacyContents                 = @HasLegacyContents
//                            , SolomonCustID                     = @SolomonCustID
//                            , Updated                           = " + DateTime.Now.ToDbQuote() + @"
//                            , SubscriptionItemCreditCount       = @SubscriptionItemCreditCount
//                            , IsInternalAccount                 = @IsInternalAccount
//                            , Preferences                       = @Preferences
//                          WHERE
//                            AccountID_                          = @AccountID_
//                          ;
//        ";

//      this.SiteConnection.Execute(sql, param);

//      if (account.AccountID_ == 0) {
//        account.AccountID_ = param.Get<int>("@AccountID_");
//      }

//      if (account.NextRenewalDate <= Config.AppConfig.minDateTime) {
//        sql = @"/*sql*/ --NextRenewalDate = null
//
//                        UPDATE
//                            Acnt_Accounts
//                          SET
//                              NextRenewalDate                   = null
//                            , NextRenewalCheckDate              = null
//                          WHERE
//                            AccountID_                          = " + account.AccountID_.ToString() + @";
//                        ";

//        this.SiteConnection.Execute(sql);
//      }
//    }


//    public bool IsAdmin(string userName) {
//      string query = @"/*sql*/ --IsAdmin
//                        SET NOCOUNT ON;
//                        SELECT
//                            COUNT(r.UserId)
//                          FROM
//                            aspnet_UsersInRoles r
//                              INNER JOIN
//                                aspnet_Users u
//                                ON r.UserId = u.UserId
//                          WHERE
//                            u.UserName = @UserName
//                            AND RoleId IN (
//                              SELECT
//                                  RoleId
//                                FROM
//                                  aspnet_Roles
//                                WHERE
//                                  RoleName      = " + AdminRole.Admin.ToDbQuote() + @"
//                                  OR RoleName   = " + AdminRole.SuperAdmin.ToDbQuote() + @"
//                              )
//                        ";

//      return SiteMembersConnection.Query<int>(query, new { UserName = userName }).FirstOrDefault() > 0;
//    }


//    public int IsEmailAddressUniqueToken(string token, string emailAddress) {
//      string sql = @"/*sql*/ --IsEmailAddressUniqueToken
//                        SET NOCOUNT ON;
//
//                        /****** test harness *****/
//                        --DECLARE @CheckEmail   nvarchar(256) = 'kevinnbrunner@gmail.com'
//                        --DECLARE @Token        nvarchar(256) = 'baeda379-8d0a-4ea6-8b32-fd4d83119170-1369146830'
//                        /**** end test harness ****/
//
//                        DECLARE @count int = 0;
//
//                        SELECT
//                            @count = 1
//                          FROM
//                            AccountDownloaders d
//                          WHERE
//                            d.Token = @Token;
//
//                        IF(@count = 1) BEGIN
//                          SELECT
//                              @count = 2
//                            FROM
//                              Acnt_Accounts a
//                            WHERE
//                              Email = @CheckEmail
//                        END
//
//                        SELECT @count;
//
//                ";

//      int count = this.SiteConnection.Query<int>(sql, new { Token = token, CheckEmail = emailAddress.ToLower() }).First();

//      return count;
//    }


//    public bool IsUniqueEmailCheck(string emailAddress) {

//      string sql = @"/*sql*/ --IsUniqueEmailCheck
//                        SET NOCOUNT ON;
//
//                        /****** test harness *****/
//                        --DECLARE @CheckEmail nvarchar(256) = ''
//                        /**** end test harness ****/
//
//                        SELECT
//                           Email
//                          FROM
//                            Acnt_Accounts
//                          WHERE
//                            Email = @CheckEmail;
//
//        ";

//      int count = this.SiteConnection.Query<string>(sql, new { CheckEmail = emailAddress.ToLower() }).Count();

//      return count < 1;
//    }



//    public void DeleteAdminRoles(string email) {
//      this.SiteMembersConnection.Execute(@"/*sql*/ --DeleteAccount
//                        SET NOCOUNT ON;
//
//                        DECLARE @UserId uniqueidentifier;
//                        SELECT @UserId = UserID FROM [aspnet_Membership]    WHERE LoweredEmail   = @email;
//                        DELETE FROM [aspnet_UsersInRoles]                   WHERE UserId         = @UserId;
//
//              ", new { email = email });
//    }


//    public void DeleteAccount(string email) {

//      List<string> saveEmails= new List<string>();

//      saveEmails.Add("kevinnbrunner@gmail.com");
//      saveEmails.Add("kevinbrunner@gmail.com");
//      saveEmails.Add("lieslbrunner@gmail.com");
//      saveEmails.Add("threemotors@gmail.com");
//      saveEmails.Add("anil.neglur@gmail.com");
//      saveEmails.Add("viv@digitaljuice.com");
//      saveEmails.Add("brettemascarenhas@gmail.com");
//      saveEmails.Add("mamtamathew@gmail.com");
//      saveEmails.Add("brettemascarenhas.dj@gmail.com");
//      saveEmails.Add("joannedebrass.cg@gmail.com");
//      saveEmails.Add("dhebel@digitaljuice.com");
//      saveEmails.Add("dhebel@gmail.com");
//      saveEmails.Add("dhebel@djapps.com");
//      saveEmails.Add("jdawson@digitaljuice.com");
//      saveEmails.Add("jeff.earley.dj@gmail.com");
//      saveEmails.Add("jpshook.juice@gmail.com");
//      saveEmails.Add("jpshook@digitaljuice.com");
//      saveEmails.Add("shiva.kumarmv@gmail.com");

//      saveEmails.Add("yashavanthagm.cg@gmail.com");
//      saveEmails.Add("surender.singh@creativegenius.co.in");
//      saveEmails.Add("chandanckp@gmail.com");
//      saveEmails.Add("chandan.nataraj@creativegenius.co.in");
//      saveEmails.Add("chandancs.cg@gmail.com");
//      saveEmails.Add("yashavantha.gm@creativegenius.co.in");
//      saveEmails.Add("yashavanthagm.cg@gmail.com");
//      saveEmails.Add("ssanjay.cg@gmail.com");

//      if (!saveEmails.Contains(email)) {

//        string accountid = this.GetAccountOnEmail(email).AccountID;

//        if (accountid.IsNotEmpty()) {



//          List<AccountJuicer>  juicers = this.SiteConnection.Query<AccountJuicer>(@"/*sql*/ --DeleteAccount
//                          SELECT d.JuicerID AS JuicerID FROM  AccountDownloaders d WHERE d.AccountID = @AccountID;", new { AccountID = accountid }).ToList();

//          foreach (AccountJuicer aj in juicers) {
//            this.JuicerApiTrackingConnection.Execute(
//              @"/*sql*/ --DeleteAccount
//                        DELETE FROM [JuicerApiActionRequestLog]        WHERE JuicerID = @JuicerID;
//              ", new { JuicerID = aj.JuicerID });
//          }


//          this.JuicerApiTrackingConnection.Execute(
//            @"/*sql*/ --DeleteAccount
//                        SET NOCOUNT ON;
//                        DELETE FROM [AccountDownloaderSignInLog]        WHERE AccountID = @AccountID;
//              ", new { Accountid = accountid });


//          this.SiteConnection.Execute(@"/*sql*/ --DeleteAccount
//                        SET NOCOUNT ON;
//                        DECLARE @AccountID varchar(36) = '';
//                        SELECT TOP 1 @AccountID = AccountID FROM [Acnt_Accounts] WHERE Email = @email;
//                        DELETE FROM [Shop_CartItems]                    WHERE AccountID = @AccountID;
//                        DELETE FROM [Shop_OrderItems]                   WHERE AccountID = @AccountID;
//                        DELETE FROM [Shop_Orders]                       WHERE AccountID = @AccountID;
//                        DELETE FROM [Shop_OrderPaymentExceptions]       WHERE AccountID = @AccountID;
//                        DELETE FROM [Shop_OrderPayments]                WHERE AccountID = @AccountID;
//                        DELETE FROM [Shop_PaymentProcessTransactionLog] WHERE AccountID = @AccountID;
//                        DELETE FROM [Downloads_Items]                   WHERE AccountID = @AccountID;
//                        DELETE FROM [DownloadsTracker]                  WHERE AccountID = @AccountID;
//                        DELETE FROM [AccountLog]                        WHERE AccountID = @AccountID;
//                        DELETE FROM [AccountBalanceTransactionLog]      WHERE AccountID = @AccountID;
//                        DELETE FROM [AccountCards]                      WHERE AccountID = @AccountID;
//                        DELETE FROM [AccountMemberBenefits]             WHERE AccountID = @AccountID;
//                        DELETE FROM [JuicerActionLog]                   WHERE AccountID = @AccountID;
//                        DELETE FROM [JuicerCloud]                       WHERE AccountID = @AccountID;
//                        DELETE FROM [JuicerCloudDbHints]                WHERE AccountID = @AccountID;
//                        DELETE FROM [JuicerRescindedCheck]              WHERE AccountID = @AccountID;
//                        DELETE FROM [AccountDownloaders]                WHERE AccountID = @AccountID;
//                        DELETE FROM [Acnt_Accounts]                     WHERE AccountID = @AccountID;
//
//              ", new { email = email });


//          this.SiteMembersConnection.Execute(@"/*sql*/ --DeleteAccount
//                        SET NOCOUNT ON;
//
//                        DECLARE @UserId uniqueidentifier;
//                        SELECT @UserId = UserID FROM [aspnet_Membership]    WHERE LoweredEmail   = @email;
//                        DELETE FROM [aspnet_Membership]                     WHERE UserId         = @UserId;
//                        DELETE FROM [aspnet_UsersInRoles]                   WHERE UserId         = @UserId;
//                        DELETE FROM [aspnet_Users]                          WHERE UserId         = @UserId;
//
//              ", new { email = email });
//        }
//      }

//    }


//    public void UpdateCardAccountID(int cardID, string emailAddress, string accountID) {
//      string sql = @"/*sql*/ --UpdateCardAccountID
//                        SET NOCOUNT ON;
//
//                        /****** test harness *****/
//                        --DECLARE @CardID               int           = 0;
//                        --DECLARE @AccountID            varchar(36)   = '';
//                        /**** end test harness ****/
//
//                        UPDATE
//                            AccountCards
//                          SET
//                              AccountID             = @AccountID
//                            , Updated               = " + DateTime.Now.ToDbQuote() + @"
//                          WHERE
//                            CardID                  = @CardID
//                            AND AccountID           = @EmailAddress
//                          ;
//          ";

//      this.SiteConnection.Execute(sql, new { CardID = cardID, EmailAddress = emailAddress, AccountID = accountID });
//    }


//    public AccountLogEntry GetAccountLogEntry(int logID) {
//      string sql = @"SELECT LogID, AccountID, ActionType, Notes, OrderID, CreatedDate, ContentID FROM AccountLog WHERE LogID = @logID";

//      return this.SiteConnection.Query<AccountLogEntry>(sql, new { logID = logID }).SingleOrDefault();
//    }


//    public IEnumerable<AccountLogDisplay> GetJuicerLogEntries(string adminID = "", string accountID = "", JuicerActionType juicerType = JuicerActionType.All, DateTime? startDate = null, DateTime? endDate = null, int startPos = 1, int numbToReturn = 25, bool viewPrivateToo = false) {
//      return this.GetLogEntries(adminID, accountID, AccountActionType.Juicer, juicerType, startDate, endDate, startPos, numbToReturn, viewPrivateToo);
//    }


//    public IEnumerable<AccountLogDisplay> GetAccountLogEntries(string adminID = "", string accountID = "", AccountActionType actionType = AccountActionType.All, DateTime? startDate = null, DateTime? endDate = null, int startPos = 1, int numbToReturn = 25, bool viewPrivateToo = false) {
//      return this.GetLogEntries(adminID, accountID, actionType, JuicerActionType.None, startDate, endDate, startPos, numbToReturn, viewPrivateToo);
//    }


//    public IEnumerable<AccountLogDisplay> GetAccountSeverLogEntries(string adminID = "", string accountID = "", AccountActionType actionType = AccountActionType.All, DateTime? startDate = null, DateTime? endDate = null, int startPos = 1, int numbToReturn = 25, bool viewPrivateToo = false) {
//      return this.GetLogEntries(adminID, accountID, actionType, JuicerActionType.None, startDate, endDate, startPos, numbToReturn, viewPrivateToo, LogSeverity.High);
//    }


//    private IEnumerable<AccountLogDisplay> GetLogEntries(string adminID, string accountID, AccountActionType actionType, JuicerActionType juicerType, DateTime? startDate, DateTime? endDate, int startPos, int numbToReturn, bool viewPrivateToo, LogSeverity severity = LogSeverity.Any) {

//      accountID.IsValidGuidID();

//      if (adminID.IsNotEmpty() && adminID != "All") {
//        adminID.IsValidGuidID();
//      }

//      if (adminID == "All") {
//        adminID = "";
//      }

//      if (startDate == null || startDate <= Config.AppConfig.checkDateTime) {
//        startDate = Config.AppConfig.minDateTime;
//      }

//      int endPos = startPos + numbToReturn - 1;

//      string sql = @"/*sql*/ --GetAccountLogEntries
//                        /**********   test   **************/
//                        --DECLARE @AdminID          varchar(36)       = '00000001-1a5a-4ecc-a61c-12067931804b';
//                        --DECLARE @AccountID        varchar(36)       = '00000001-1a5a-4ecc-a61c-12067931804b';
//                        --DECLARE @ActionType       varchar(64)       = '';
//                        --DECLARE @StartDate        datetime          = '01/01/1975'
//                        --DECLARE @EndDate          datetime          = '01/01/2100'
//
//                        --DECLARE @StartPos         int               = 1;
//                        --DECLARE @NumbToReturn     int               = 1005;
//                        /********* end test **************/
//
//                        SET NOCOUNT ON;
//
//                        ;WITH cte AS (
//
//                          SELECT
//                                LogID                                           AS LogID
//                              , AdminID                                         AS AdminID
//                              , AccountID                                       AS AccountID
//                              , ActionType                                      AS ActionType
//                              , JuicerType                                      AS JuicerType
//                              , Notes                                           AS Notes
//                              , AdminNotes                                      AS AdminNotes
//                              , OrderID                                         AS OrderID
//                              , ContentID                                       AS ContentID
//                              , Value                                           AS Value
//                              , CreatedDate                                     AS CreatedDate
//                              , IsPrivate                                       AS IsPrivate
//                              , JuicerID                                        AS JuicerID
//                              , Severity                                        AS Severity
//                              , Row_Number() OVER(ORDER BY CreatedDate DESC)    AS RowCounted
//                            FROM
//                              AccountLog
//                            WHERE
//                              1 = 1";

//      if (accountID.IsNotEmpty()) {
//        sql += @"
//                              AND AccountID       = " + accountID.ToDbQuote() + @"
//                        ";
//      }
//      if (adminID.IsNotEmpty()) {
//        sql += @"
//                              AND AdminID         = " + adminID.ToDbQuote() + @"
//                        ";
//      }

//      if (actionType != AccountActionType.Juicer) {
//        if (actionType != AccountActionType.All) {
//          sql += @"
//                              AND ActionType      = " + actionType.ToDbQuote();
//        }
//        sql += @"
//                              AND JuicerType      != " + JuicerActionType.None.ToDbQuote();
//      }

//      if (juicerType != JuicerActionType.None) {
//        if (juicerType != JuicerActionType.All) {
//          sql += @"
//                              AND JuicerType      = " + juicerType.ToDbQuote();
//        }
//        sql += @"
//                              AND ActionType      = " + AccountActionType.Juicer.ToDbQuote() + @"
//                        ";
//      }

//      if (startDate > Config.AppConfig.minDateTime) {
//        sql += @"
//                              AND (CreatedDate BETWEEN " + startDate.ToDbQuote() + @" AND " + endDate.ToDbQuote() + @")";
//      }

//      if (!viewPrivateToo) {
//        sql += @"
//                              AND IsPrivate = 0
//                        ";
//      }

//      if (severity != LogSeverity.Any) {
//        sql += @"
//                              AND Severity  = " + severity.ToDbQuote() + @"
//                        ";
//      }

//      sql += @"
//                          )
//                          , countThem AS (
//                            SELECT COUNT(*) as TotalNumb from cte
//                          )
//                          SELECT
//                                c.LogID                 AS LogID
//                              , ISNULL(c.AdminID, '')   AS AdminID
//                              , ISNULL(a.Email, '')     AS AdminEmail
//                              , c.AccountID             AS AccountID
//                              , ISNULL(m.Email, '')     AS AccountEmail
//                              , c.ActionType            AS ActionType
//                              , c.Notes                 AS Notes
//                              , c.AdminNotes            AS AdminNotes
//                              , c.OrderID               AS OrderID
//                              , c.Value                 AS Value
//                              , c.CreatedDate           AS CreatedDate
//                              , c.IsPrivate             AS IsPrivate
//                              , c.RowCounted            AS RowCounted
//                              , t.TotalNumb             AS TotalNumb
//                              , c.JuicerType            AS JuicerType
//                              , c.JuicerID              AS JuicerID
//                              , c.ContentID             AS ContentID
//                            FROM
//                              countThem t
//                              , cte c
//                                INNER JOIN
//                                  Acnt_Accounts m
//                                  ON m.AccountID = c.AccountID
//                                LEFT OUTER JOIN
//                                  Acnt_Accounts a
//                                  ON a.AccountID = c.AdminID
//                            WHERE
//                              RowCounted BETWEEN " + startPos.ToString() + @" AND " + endPos.ToString() + @"
//                            Order BY
//                              RowCounted
//                        ;";



//      List<AccountLogDisplay> accountLogDisplay = this.SiteConnection.Query<AccountLogDisplay>(sql).ToList(); //, new { AdminID = adminID, AccountID = accountID, ActionType = actionType.ToString(), StartDate = startDate, EndDate = endDate, StartPos = startPos, NumbToReturn = numbToReturn, JuicerType = juicerType.ToString() }).ToList();

//      return accountLogDisplay;
//    }



//    public void DeleteAllAccountLogEntries(string accountID) {

//      string sql = "DELETE FROM AccountLog WHERE AccountID = @accountID";

//      SiteConnection.Execute(sql, new { accountID = accountID });

//    }



//    public void AddNewAccountLogEntry(AccountLogEntry log) {
//      if (log.LogID > 0) {
//        throw new ArgumentOutOfRangeException("LogID is greater than 0. Method only supports add new.");
//      }

//      DynamicParameters param = new DynamicParameters();

//      param.Add("@LogID", log.LogID, direction: ParameterDirection.InputOutput); // ParameterDirection.ReturnValue);
//      param.Add("@AdminID", log.AdminID);
//      param.Add("@AccountID", log.AccountID);
//      param.Add("@ActionType", log.ActionTypeS);
//      param.Add("@Notes", log.Notes ?? "");
//      param.Add("@AdminNotes", log.AdminNotes ?? "");
//      param.Add("@OrderID", log.OrderID);
//      param.Add("@Value", log.Value);
//      param.Add("@ContentID", log.ContentID);
//      param.Add("@IsPrivate", log.IsPrivate);
//      param.Add("@JuicerType", log.JuicerType.ToString());
//      param.Add("@JuicerID", log.JuicerID);
//      param.Add("@CreatedDate", log.CreatedDate);
//      param.Add("@Severity", log.Severity.ToString());

//      string sql = @"/*sql*/ --AddNewAccountLogEntry
//                        SET NOCOUNT ON;
//
//                        /****** test harness *****/
//                          --DECLARE @LogID            bigint          = 0 OUTPUT;
//                          --DECLARE @AdminID          varchar(36)     = '';
//                          --DECLARE @AccountID        varchar(36)     = '';
//                          --DECLARE @JuicerID         varchar(36)     = '';
//                          --DECLARE @ActionType       varchar(64)     = '';
//                          --DECLARE @JuicerType       varchar(64)     = '';
//                          --DECLARE @Notes            text            = '';
//                          --DECLARE @AdminNotes       text            = '';
//                          --DECLARE @OrderID          int             = 0;
//                          --DECLARE @Value            decimal(9, 2)   = 0;
//                          /**** end test harness ****/
//
//                        SET NOCOUNT ON;
//
//                        INSERT INTO
//                          AccountLog
//                          (
//                              AdminID
//                            , AccountID
//                            , JuicerID
//                            , ActionType
//                            , JuicerType
//                            , Notes
//                            , AdminNotes
//                            , OrderID
//                            , Value
//                            , ContentID
//                            , IsPrivate
//                            , CreatedDate
//                            , Severity
//                          )
//                          VALUES
//                          (
//                              @AdminID
//                            , @AccountID
//                            , @JuicerID
//                            , @ActionType
//                            , @JuicerType
//                            , @Notes
//                            , @AdminNotes
//                            , @OrderID
//                            , @Value
//                            , @ContentID
//                            , @IsPrivate
//                            , @CreatedDate
//                            , @Severity
//                          )
//
//                          SET @LogID = Cast(SCOPE_IDENTITY() as bigint);
//
//                  ";

//      this.SiteConnection.Execute(sql, param);

//      if (log.LogID == 0) {
//        log.LogID = param.Get<long>("@LogID");
//      }
//    }




//    public List<SiteAdminWithRole> SiteAdminsWithRoles() {
//      List<SiteAdminWithRole> SiteAdminsWithRoles = new List<SiteAdminWithRole>();

//      string sql1 = @"/*sql*/ --SiteAdminsWithRoles
//                        SET NOCOUNT ON;
//                        SELECT
//                              Email
//                            , UserName
//                            , RoleName
//                            , r.UserId
//                          FROM
//                            aspnet_UsersInRoles r
//                              INNER JOIN
//                                aspnet_Users u
//                                ON u.UserId = r.UserId
//                              INNER JOIN
//                                aspnet_Membership m
//                                ON m.UserId = r.UserId
//                              INNER JOIN
//                                aspnet_Roles rs
//                                ON rs.RoleId = r.RoleId
//                          ORDER BY
//                            Email;
//                      ";

//      return this.SiteMembersConnection.Query<SiteAdminWithRole>(sql1).ToList();
//    }


//    public IEnumerable<AccountJuicer> GetJuicers(string accountID = "", string juicerID = "", bool unLockedOnly = true) {
//      if (accountID.IsEmpty() && juicerID.IsEmpty()) {
//        throw new ArgumentNullException("need an account and or juicer");
//      }

//      string sql = @"/*sql*/ --GetJuicer
//                        SET NOCOUNT ON;
//
//                        SELECT
//                              JuicerID_
//                            , JuicerID
//                            , AccountID
//                            , JuicerName
//                            , MAC_Address
//                            , Updated
//                            , IsDefault
//                            , IsLocked
//                            , LastRescindIDCheck
//                            , IsLive
//                            , Token
//                            , Build
//                            , LastLoginDate
//                            , ISNULL(HostOS, '')        AS HostOS
//                            , WorkBenchBuild
//                          FROM
//                            AccountDownloaders
//                          WHERE ";

//      if (!juicerID.IsEmpty()) {
//        sql += @"
//                            JuicerID          = @JuicerID";
//        if (!accountID.IsEmpty()) {
//          sql += @"
//                            AND AccountID     = @AccountID";
//        }
//      } else {
//        sql += @"
//                            AccountID         = @AccountID";
//      }

//      if (unLockedOnly) {
//        sql += @"           AND IsLocked = 0";
//      }

//      return this.SiteConnection.Query<AccountJuicer>(sql, new { AccountID = accountID, JuicerID = juicerID });
//    }


//    public IEnumerable<AccountJuicer> GetAccountDeskTopJuicers(string accountID, Int64 buildAtInt) {
//      string sql = @"/*sql*/ --GetDeskTopoJuicers
//                        SELECT
//                              JuicerID_
//                            , JuicerID
//                            , AccountID
//                            , JuicerName
//                            , MAC_Address
//                            , Updated
//                            , IsDefault
//                            , IsLocked
//                            , LastRescindIDCheck
//                            , IsLive
//                            , Token
//                            , Build
//                            , LastLoginDate
//                            , ISNULL(HostOS, '')      AS HostOS
//                            , WorkBenchBuild
//                          FROM
//                            AccountDownloaders
//                          WHERE
//                            AccountID     = @AccountID
//                            AND IsLocked  = 0
//                        ";
//      //AND (
//      //  HostOS     LIKE 'Windows%'
//      //  OR HostOS  LIKE 'Mac OSX%'
//      //)

//      List<AccountJuicer> accountJuicers = this.SiteConnection.Query<AccountJuicer>(sql, new { AccountID = accountID }).ToList();
//      accountJuicers = accountJuicers.Where(w => ((w.HostOS.ToLower().StartsWith("windows") || w.HostOS.ToLower().StartsWith("mac osx")) && (Convert.ToInt64(w.Build.Replace(".", "")) >= buildAtInt))).ToList();

//      return accountJuicers;
//    }


//    public void UpdateJuicers(string accountID, string juicerID, bool lockIt, bool clearIt) {

//      if (clearIt) {
//        this.SaveDefaultJuicer(accountID, "");
//      }

//      string sql = @"/*sql*/ --UpdateJuicers
//                        SET NOCOUNT ON;
//
//                        UPDATE
//                            AccountDownloaders
//                          SET
//                            IsLocked = @LockIt
//                          WHERE
//                            JuicerID        = @JuicerID
//                            AND AccountID   = @AccountID;
//
//                        SELECT
//                            MAC_Address
//                          FROM
//                            AccountDownloaders
//                          WHERE
//                            JuicerID        = @JuicerID
//                            AND AccountID   = @AccountID;
//
//                    ";

//      string macAddress = this.SiteConnection.Query<string>(sql, new { AccountID = accountID, JuicerID = juicerID, LockIt = lockIt }).FirstOrDefault();

//      this.SaveBannedAddress(accountID, macAddress, DateTime.Now.AddYears(10));
//    }


//    public void SaveDefaultJuicer(string accountID, string juicerID) {

//      string sql = @"/*sql*/ --SaveDefaultJuicer
//                        SET NOCOUNT ON;
//
//                        UPDATE
//                            Acnt_Accounts
//                          SET
//                            DefaultJuicerID = @JuicerID
//                          WHERE
//                            AccountID = @AccountID;
//
//                        UPDATE
//                            AccountDownloaders
//                          SET
//                            IsDefault = 0
//                          WHERE
//                            AccountID = @AccountID;
//
//                        UPDATE
//                            AccountDownloaders
//                          SET
//                            IsDefault = 1
//                          WHERE
//                            JuicerID  = @JuicerID;
//                  ";

//      this.SiteConnection.Execute(sql, new { AccountID = accountID, JuicerID = juicerID });
//    }


//    public void SaveLiveJuicer(string accountID, string juicerID, string juicerBuild, string workBenchBuild) {

//      string sql = @"/*sql*/ --SaveLiveJuicer
//                        SET NOCOUNT ON;
//
//                        UPDATE
//                            AccountDownloaders
//                          SET
//                            IsLive    = 0
//                          WHERE
//                            AccountID = @AccountID;
//
//                        UPDATE
//                            AccountDownloaders
//                          SET
//                              IsLive          = 1
//                            , Build           = @JuicerBuild
//                            , WorkBenchBuild  = @WorkBenchBuild
//                          WHERE
//                            JuicerID  = @JuicerID;
//                  ";

//      this.SiteConnection.Execute(sql, new { AccountID = accountID, JuicerID = juicerID, JuicerBuild = juicerBuild, WorkBenchBuild = workBenchBuild });
//    }


//    public bool DeletePurchaseHistory(string accountID) {
//      string sql = @"/*sql*/ --DeletePurchaseHistory
//                        SET NOCOUNT ON;
//                        /****** test harness *****/
//                        --DECLARE @AccountID varchar(36) = '5d2146b1-3799-4458-96e6-0ee15e1fe2fe';
//                        /****** test harness *****/
//
//                        DELETE FROM Shop_CartItems                    WHERE AccountID = @AccountID;
//                        DELETE FROM Shop_Orders                       WHERE AccountID = @AccountID;
//                        DELETE FROM Shop_OrderItems                   WHERE AccountID = @AccountID;
//                        DELETE FROM Shop_OrderPayments                WHERE AccountID = @AccountID;
//                        DELETE FROM Shop_OrderPaymentExceptions       WHERE AccountID = @AccountID;
//                        DELETE FROM Shop_PaymentProcessTransactionLog WHERE AccountID = @AccountID;
//
//                        DELETE FROM Downloads_Items                   WHERE AccountID = @AccountID;
//                        DELETE FROM DownloadsTracker                  WHERE AccountID = @AccountID;
//                        DELETE FROM DownloadsPerJuicer                WHERE JuicerID IN (SELECT JuicerID FROM AccountDownloaders WHERE AccountID = @AccountID)
//
//                    ";

//      this.SiteConnection.Execute(sql, new { AccountID = accountID });

//      return true;
//    }


//    public MemberOrdersAndJuicers GetMemberOrdersAndJuicers(string accountID) {

//      string sql = @"/*sql*/ --GetMemberOrdersAndJuicers
//                        SET NOCOUNT ON;
//
//                        SELECT TOP 5
//                              PaymentStatus
//                            , OrderType
//                            , OrderID
//                            , OrderDate
//                            , GrandTotal
//                            , IPAddress
//                          FROM
//                            Shop_Orders
//                          WHERE
//                            AccountID = @AccountID
//                          ORDER BY
//                            OrderID DESC;
//
//                        SELECT
//                              d.JuicerID
//                            , d.JuicerName
//                            , d.IsDefault
//                            , d.IsLive
//                            , d.IsLocked
//                            , d.HostOS
//                            , d.Build
//                          FROM
//                            AccountDownloaders d
//                          WHERE
//                            d.AccountID = @AccountID
//                          ORDER BY
//                            isDefault DESC;
//
//                    ";



//      List<StringPairs> ConfirmableInfo = new List<StringPairs>();

//      var multiQuery = this.SiteConnection.QueryMultiple(sql, new { AccountID = accountID });

//      MemberOrdersAndJuicers memberOrdersAndJuicers = new MemberOrdersAndJuicers();

//      List<OrderInfoLite> orderInfoLiteList  = multiQuery.Read<OrderInfoLite>().ToList();
//      List<AccountJuicer> juicerList = multiQuery.Read<AccountJuicer>().ToList();

//      memberOrdersAndJuicers.OrderInfoLiteList = orderInfoLiteList;
//      memberOrdersAndJuicers.JuicerInfoLiteList = juicerList;

//      return memberOrdersAndJuicers;
//    }


//    public List<StringPairs> GetMembersEmailsLike(string memberEmail, string lastName, bool subscriptionsOnly) {
//      if (memberEmail.IsNotEmpty()) {
//        memberEmail = memberEmail.Replace("*", "");
//        memberEmail += "%";
//        lastName = "";
//      } else {
//        lastName = lastName.Replace("*", "");
//        lastName += "%";
//        memberEmail = "";
//      }

//      List<StringPairs> stringPairs = new List<StringPairs>();

//      string sql = @"/*sql*/ --GetMembersEmailsLike
//                        SET NOCOUNT ON;
//
//                        SELECT
//                              FirstName + ' ' + LastName  AS [Key]
//                            , Email                       AS [Value]
//                          FROM
//                            Acnt_Accounts
//                          WHERE ";
//      if (memberEmail.IsNotEmpty()) {
//        sql += @"
//                            email     LIKE @MemberEmail
//                        ";
//      } else {
//        sql += @"
//                            LastName  LIKE @LastName
//                        ";
//      }

//      if (subscriptionsOnly) {
//        sql += @"
//                            AND NOT (
//                              SubscriptionStatus = " + SubscriptionStatusType.DJTransferWait.ToDbQuote() + @"
//                              OR SubscriptionStatus = " + SubscriptionStatusType.ImportedLegacy.ToDbQuote() + @"
//                            )
//                        ";
//      }

//      sql += @"
//                          ORDER BY
//                            LastName, FirstName;
//                    ";

//      stringPairs = this.SiteConnection.Query<StringPairs>(sql, new { MemberEmail = memberEmail, LastName = lastName }).ToList();

//      return stringPairs;
//    }


//    public void SetImpersonating(string adminAccountID, string memberAccountID) {
//      string sql = @"/*sql*/ --SetImpersonating
//                        SET NOCOUNT ON;
//                        UPDATE Acnt_Accounts SET Impersonating = @MemberAccountID WHERE AccountID = @AdminAccountID;
//                    ";

//      this.SiteConnection.Execute(sql, new { AdminAccountID = adminAccountID, MemberAccountID = memberAccountID });
//    }


//    private bool UpdateStoreCredit(string accountID, decimal creditValue) {
//      string sql = @"/*sql*/ --UpdateStoreCredit
//                        UPDATE Acnt_Accounts SET StoreCredit = ISNULL(StoreCredit, 0) + @StoreCredit WHERE AccountID = @AccountID;
//                    ";
//      return this.SiteConnection.Execute(sql, new { AccountID = accountID, StoreCredit = creditValue }) == 1 ? true : false;
//    }


//    public bool SaveToStoreCredit(string accountID, decimal creditValue) {
//      if (creditValue < 0) {
//        throw new ArgumentOutOfRangeException("must be a positive value");
//      }

//      return this.UpdateStoreCredit(accountID, creditValue);
//    }


//    public bool RedeemeStoreCredit(string accountID, decimal creditValue) {
//      if (creditValue < 0) {
//        throw new ArgumentOutOfRangeException("this is a credit... still must be a positive value");
//      }

//      bool gotDone = false;
//      if (this.GetStoreCredit(accountID) >= creditValue) {
//        creditValue = creditValue * -1;
//        gotDone = this.UpdateStoreCredit(accountID, creditValue);
//      }

//      return gotDone;
//    }


//    private decimal GetStoreCredit(string accountID) {
//      string sql = @"/*sql*/ --GetStoreCredit
//                        SET NOCOUNT ON;
//
//                        SELECT ISNULL(StoreCredit, 0) AS StoreCredit FROM Acnt_Accounts WHERE AccountID = @AccountID;
//                    ";

//      return this.SiteConnection.Query<decimal>(sql, new { AccountID = accountID }).FirstOrDefault();
//    }


//    public bool LogAccountBalanceTransaction(string accountID, decimal amount, int orderID, DateTime createDate, string reason) {

//      string sql = @"/*sql*/ --LogAccountBalanceTransaction
//                        SET NOCOUNT ON;
//
//                        INSERT INTO
//                          AccountBalanceTransactionLog
//                          (
//                              AccountID
//                            , Amount
//                            , OrderID
//                            , Updated
//                            , Reason
//                          )
//                          VALUES
//                          (
//                              @AccountID
//                            , @Amount
//                            , @OrderID
//                            , @Updated
//                            , @Reason
//                          )
//                        ;";

//      return this.SiteConnection.Execute(sql, new {
//        AccountID = accountID,
//        OrderID = orderID,
//        Amount = amount,
//        Updated = createDate,
//        Reason = reason
//      }) == 1;
//    }


//    public SubscriptionStatusType GetSubscriptionStatusOnSolomonID(string solomonID) {
//      string sql = @"/*sql*/ --GetSubscriptionStatus
//
//                        /****** test harness *****/
//                        --DECLARE @solomonID   varchar(60) = '';
//                        /**** end test harness ***/
//                        SET NOCOUNT ON;
//
//                        SELECT TOP 1
//                            ISNULL(SubscriptionStatus, 'All') AS SubscriptionStatus
//                          FROM
//                            Acnt_Accounts
//                          WHERE
//                            solomoncustid = @SolomonID
//                          ;
//              ";


//      SubscriptionStatusType subscriptionStatusType = this.SiteConnection.Query<SubscriptionStatusType>(sql, new { SolomonID = solomonID.Trim() }).FirstOrDefault();

//      return subscriptionStatusType;
//    }


//    public IEnumerable<Account> GetNonAcceptedWaitListInviteAccounts() {

//      string sql="";

//      sql += @"
//
//                        SELECT";
//      sql += GetAccountFields();
//      sql += @"
//                          FROM
//                            Acnt_Accounts a
//                          WHERE
//                            SubscriptionStatus          = " + SubscriptionStatusType.WaitListPending.ToDbQuote() + @"
//                          ORDER BY
//                            CreateDate ASC
//                        ";

//      return this.SiteConnection.Query<Account>(sql);
//    }


//    public Account GetAccountOnEmail(string emailAddress) {

//      string sql = @"/*sql*/ --GetAccountOnEmail
//
//                        /****** test harness *****/
//                        --DECLARE @EmailAddress   varchar(60) = '';
//                        /**** end test harness ***/
//                        SET NOCOUNT ON;
//
//                        SELECT TOP 1
//                            AccountID
//                          FROM
//                            Acnt_Accounts
//                          WHERE
//                            Email = @EmailAddress
//                          ORDER BY
//                            Updated DESC
//                          ;
//              ";


//      string accountID = this.SiteConnection.Query<string>(sql, new { EmailAddress = emailAddress }).FirstOrDefault();

//      Account account = null;

//      if (accountID != null && accountID.Length == 36) {
//        account = this.GetAccount(accountID, "", "", "", "");
//      }

//      return account;
//    }


//    public void SaveJuicer(AccountJuicer juicer) {

//      juicer.Updated = DateTime.Now;

//      if (juicer.JuicerID_ == 0) {
//        juicer.JuicerID = Guid.NewGuid().ToString();
//      }

//      DynamicParameters param = new DynamicParameters();

//      param.Add("@JuicerID_", juicer.JuicerID_, direction: ParameterDirection.InputOutput);
//      param.Add("@JuicerID", juicer.JuicerID);
//      param.Add("@AccountID", juicer.AccountID);
//      param.Add("@JuicerName", juicer.JuicerName);
//      param.Add("@Mac_Address", juicer.MAC_Address);
//      param.Add("@Updated", juicer.Updated);
//      param.Add("@IsDefault", juicer.IsDefault);
//      param.Add("@IsLocked", juicer.IsLocked);
//      param.Add("@LastRescindIDCheck", juicer.LastRescindIDCheck);
//      param.Add("@IsLive", juicer.IsLive);
//      param.Add("@Token", juicer.Token ?? "");
//      param.Add("@Build", juicer.Build);
//      param.Add("@LastLoginDate", juicer.LastLoginDate < AppConfig.minDateTime ? juicer.Updated : juicer.LastLoginDate);
//      param.Add("@HostOS", juicer.HostOS ?? "");

//      string sql = @"/*sql*/ --SaveJuicer
//                        SET NOCOUNT ON;
//
//                        IF(@JuicerID_ = 0) BEGIN
//
//                          INSERT INTO
//                            AccountDownloaders
//                            (
//                                JuicerID
//                              , AccountID
//                              , Updated
//                            )
//                            VALUES
//                            (
//                                @JuicerID
//                              , @AccountID
//                              , @Updated
//                            );
//
//                          SET @JuicerID_ = SCOPE_IDENTITY();
//
//                        END
//
//                        UPDATE
//                            AccountDownloaders
//                          SET
//                              JuicerName                                                            = @JuicerName
//                            , MAC_Address                                                           = @Mac_Address
//                            , Updated                                                               = @Updated
//                            , IsDefault                                                             = @IsDefault
//                            , IsLocked                                                              = @IsLocked
//                            , LastRescindIDCheck                                                    = @LastRescindIDCheck
//                            , IsLive                                                                = @IsLive
//                            , Token                                                                 = @Token
//                            , Build                                                                 = @Build
//                            , LastLoginDate                                                         = @LastLoginDate
//                            , HostOS                                                                = @HostOS
//                          WHERE
//                            JuicerID                = @JuicerID;
//
//                      ";

//      this.SiteConnection.Execute(sql, param);

//      if (juicer.JuicerID_ == 0) {
//        juicer.JuicerID_ = param.Get<int>("@JuicerID_");
//      }
//    }


//    public void ClearRescindedList(string accountID) {
//      string sql = @"/*sql*/ --ClearRescindedList
//                        SET NOCOUNT ON;
//
//                        DELETE FROM JuicerRescindedCheck WHERE AccountID = @AccountID;
//
//                    ";

//      this.SiteConnection.Execute(sql, new { AccountID = accountID });
//    }


//    public void SaveBannedAddress(string accountID, string address, DateTime? restoreDate = null) {
//      if (!String.IsNullOrWhiteSpace(address)) {
//        DateTime uDate = DateTime.Now;

//        string sql = @"/*sql*/ --SaveBannedAddress
//                        SET NOCOUNT ON;
//
//                        INSERT INTO
//                          AdminBannedAddresses
//                          (
//                              AccountID
//                            , Address
//                            , BanStatus
//                            , ReleaseDate
//                            , Updated
//                          )
//                          VALUES
//                          (
//                              @AccountID
//                            , @Address
//                            , " + AddressBanStatus.Banned.ToDbQuote();

//        if (restoreDate == null) {
//          sql += @"         , " + DateTime.Now.AddDays(90).ToDbQuote() + @"
//                        ";
//        } else {
//          sql += @"         , " + restoreDate.ToDbQuote() + @"
//                        ";
//        }

//        sql += @"
//                            , " + uDate.ToDbQuote() + @"
//                          )
//
//                    ";

//        this.SiteConnection.Execute(sql, new { AccountID = accountID, Address = address });
//      }
//    }


//    public void RemoveBannedAddress(string address) {
//      if (!String.IsNullOrWhiteSpace(address)) {
//        DateTime uDate = DateTime.Now;
//        DateTime rDate = uDate.AddDays(90);

//        string sql = @"/*sql*/ --RemoveBannedAddress => Address
//                        SET NOCOUNT ON;
//
//                        UPDATE
//                            AdminBannedAddresses
//                          SET
//                              BanStatus = " + AddressBanStatus.Restored.ToDbQuote() + @"
//                            , Updated   = " + DateTime.Now.ToDbQuote() + @"
//                          WHERE
//                            Address     = @Address
//                    ";

//        this.SiteConnection.Execute(sql, new { Address = address });
//      }
//    }


//    public bool CheckIfBannedAddress(string address) {

//      string sql = @"/*sql*/ --CheckIfBannedAddress
//                        SET NOCOUNT ON;
//
//                        SELECT
//                            COUNT(*)
//                          FROM
//                            AdminBannedAddresses
//                          WHERE
//                            Address       = @Address
//                            AND BanStatus = " + AddressBanStatus.Banned.ToDbQuote() + @"
//                    ";

//      return this.SiteConnection.Query<int>(sql, new { Address = address }).FirstOrDefault() > 0;
//    }


//    public void RestoreBannedAddress(string address) {
//      string sql = @"/*sql*/ --RestoreAddress
//                        SET NOCOUNT ON;
//
//                        UPDATE
//                            AdminBannedAddresses
//                          SET
//                              BanStatus = " + AddressBanStatus.Restored.ToDbQuote() + @"
//                            , Updated   = " + DateTime.Now.ToDbQuote() + @"
//                          WHERE
//                            Address     = @Address
//                    ";

//      this.SiteConnection.Execute(sql, new { Address = address });
//    }


//    public void RestoreBannedAccountAddresses(string accountID) {
//      string sql = @"/*sql*/ --RestoreAddress
//                        SET NOCOUNT ON;
//
//                        UPDATE
//                            AdminBannedAddresses
//                          SET
//                              BanStatus = " + AddressBanStatus.Restored.ToDbQuote() + @"
//                            , Updated   = " + DateTime.Now.ToDbQuote() + @"
//                          WHERE
//                            AccountID = @AccountID
//                    ";

//      this.SiteConnection.Execute(sql, new { AccountID = accountID });
//    }


//    public void RestoreAddress(DateTime dateTimeNow) {
//      string sql = @"/*sql*/ --RestoreAddress
//                        SET NOCOUNT ON;
//
//                        UPDATE
//                            AdminBannedAddresses
//                          SET
//                              BanStatus = " + AddressBanStatus.RestoredByDate.ToDbQuote() + @"
//                            , Updated   = " + DateTime.Now.ToDbQuote() + @"
//                          WHERE
//                            ReleaseDate < @DateTimeNow
//                    ";

//      this.SiteConnection.Execute(sql, new { DateTimeNow = dateTimeNow });
//    }


//    public bool DoesAccountHaveLegacyPurchases(string accountID) {

//      string sql = @"/*sql*/ --DoesAccountHaveLegacyPurchases
//                        SET NOCOUNT ON;
//
//                        SELECT
//                            COUNT(*)
//                          FROM
//                            Site_ContentAssociations a
//                            INNER JOIN
//                              Site_Content2 c
//                              ON a.ContentID = c.ContentID
//                          WHERE
//                            c.AccountID           = @AccountID
//                            AND c.ContentType     = " + ContentType.ListHeader.ToDbQuote() + @"
//                            AND c.ContentSubType  = " + ListType.CustomerLegacyProducts.ToDbQuote() + @"
//                    ";

//      return this.SiteConnection.Query<int>(sql, new { AccountID = accountID }).FirstOrDefault() > 0;
//    }


//    public ListItem GetAccountLegacyProduct(string accountID, int contentID) {
//      return GetAccountLegacyProducts(accountID: accountID, contentID: contentID).FirstOrDefault();
//    }


//    public IEnumerable<ListItem> GetAccountLegacyProducts(string accountID) {
//      return GetAccountLegacyProducts(accountID: accountID, contentID: 0);
//    }


//    private IEnumerable<ListItem> GetAccountLegacyProducts(string accountID, int contentID = 0) {

//      if (contentID < 1) {
//        contentID = 0;
//      }

//      string sql = @"/*sql*/ --GetAccountLegacyProducts
//                        SET NOCOUNT ON;
//
//                        /****** test harness *****/
//                        --DECLARE @AccountID varchar(36) = 'dd2e678c-dc1c-43ee-b394-fc2c5c203b87';
//                        /****** test harness *****/
//
//                        ;WITH cte2 AS (
//                              SELECT
//                                  c.AssocContentID  AS ContentID
//                                FROM
//                                  Site_ContentAssociations c
//                                  WHERE c.ContentID IN (
//                                    SELECT
//                                        a.AssocContentID
//                                      FROM
//                                        Site_ContentAssociations a
//                                      WHERE
//                                        a.ContentID IN (
//                                          SELECT
//                                            ContentID
//                                          FROM
//                                            Site_Content2
//                                          WHERE
//                                            AccountID           = @AccountID
//                                            AND ContentType     = " + ContentType.ListHeader.ToDbQuote() + @"
//                                            AND ContentSubType  = " + ListType.CustomerLegacyProducts.ToDbQuote() + @"
//                                        )
//                                  )
//                              UNION ALL
//                              SELECT
//                                  AssocContentID    AS ContentID
//                                FROM
//                                  Site_ContentAssociations a
//                                    INNER JOIN
//                                      Site_Content2 j
//                                      ON j.ContentID = a.ContentID
//                                    INNER JOIN
//                                      cte2 k
//                                      ON k.ContentID = a.ContentID
//                                WHERE
//                                  j.ContentType       = " + ContentType.ListHeader.ToDbQuote() + @"
//                        )
//                        SELECT
//                            c.ContentID
//                          , c.ContentType
//                          , c.ContentSubType
//                          , c.FriendlyTitle
//                          , c.Title
//                          , c.SuperTitle
//                          , c.SubTitle
//                          , c.SubTitle2
//                          , c.Price
//                          , c.CategoryID
//                          , c.Category
//                          , c.SubCategoryID
//                          , c.SubCategory
//                          , c.SortOrder
//                          , c.StatusType
//                          , c.[Description]
//                          , c.Link
//                          , (
//                              SELECT
//                                TOP 1
//                                  e.CDN
//                                FROM
//                                  Site_Elements2 e
//                                WHERE
//                                  e.ContentID       = c.ContentID
//                                  AND e.ImageType   = " + ImageType.Thumb.ToDbQuote() + @"
//                                  AND e.StatusType  = " + StatusType.Active.ToDbQuote() + @"
//                            )                                                                           AS CDN
//                          , (
//                              SELECT
//                                TOP 1
//                                  e.Path
//                                FROM
//                                  Site_Elements2 e
//                                WHERE
//                                  e.ContentID       = c.ContentID
//                                  AND e.ImageType   = " + ImageType.Thumb.ToDbQuote() + @"
//                                  AND e.StatusType  = " + StatusType.Active.ToDbQuote() + @"
//                            )                                                                           AS Path
//                        FROM
//                            Site_Content2 c
//                          WHERE
//                            ContentID IN (
//                              SELECT ContentID FROM cte2
//                            )
//                    ";
//      if (contentID > 0) {
//        sql += @"
//                            AND c.ContentID = " + contentID.ToString() + @"
//                    ";
//      } else {
//        sql += @"
//                          ORDER BY
//                            ContentID
//                    ";
//      }


//      return this.SiteConnection.Query<ListItem>(sql, new { AccountID = accountID });
//    }


//    public bool TransferAccountLegacyProducts(string fromAccountID, string toAccountID) {
//      string sql = @"/*sql*/ --TransferAccountLegacyProducts
//                        UPDATE
//                            Site_Content2
//                          SET
//                            AccountID           = @ToAccountID
//                          WHERE
//                            AccountID           = @FromAccountID
//                            AND ContentType     = " + ContentType.ListHeader.ToDbQuote() + @"
//                            AND ContentSubType  = " + ListType.CustomerLegacyProducts.ToDbQuote() + @"
//
//                  ";

//      this.SiteConnection.Execute(sql, new { FromAccountID = fromAccountID, ToAccountID = toAccountID });

//      return true;
//    }


//    public bool TransferAccountOrderItems(string fromAccountID, string toAccountID) {
//      string sql=@"/*sql*/ --TransferAccountOrderItems
//
//                        /****** test harness *****/
//                        --DECLARE @ToAccountID varchar(36) = '000000jp-5d06-4d7a-96fc-d97690a62157';
//                        --DECLARE @FromAccountID varchar(36) = '000000jp-5d06-4d7a-96fc-d97690a62157';
//                        /****** test harness *****/
//
//                        BEGIN TRANSACTION [T1];
//
//                        BEGIN TRY
//                          UPDATE
//                              Shop_OrderItems
//                            SET
//                              GroupAccountID      = @ToAccountID
//                            WHERE
//                              GroupAccountID      = @FromAccountID
//
//                          UPDATE
//                              Shop_Orders
//                            SET
//                                GroupAccountID    = @ToAccountID
//                              , Notes             = 'Transfered from: ' + @FromAccountID + ', ' + Notes
//                            WHERE
//                              GroupAccountID      = @FromAccountID
//
//                            SELECT @@RowCount;
//                        END TRY
//                        BEGIN CATCH
//                          IF (@@TRANCOUNT > 0) BEGIN
//                            ROLLBACK TRANSACTION;
//                            SELECT -1;
//                          END
//
//                        END CATCH;
//
//                        IF (@@TRANCOUNT > 0) BEGIN
//                          COMMIT TRANSACTION [T1];
//                        END
//
//                  ";

//      int rowsAffected = this.SiteConnection.Query<int>(sql, new { FromAccountID = fromAccountID, ToAccountID = toAccountID }).Single();

//      return rowsAffected >= 0; ;
//    }


//    public IEnumerable<DownloadableItem> GetAccountLegacyProductVolumes(string accountID, int lastContentID) {

//      string sql = @"/*sql*/ --GetAccountLegacyProductVolumes
//                        SET NOCOUNT ON;
//                        /****** test harness *****/
//                        --DECLARE @AccountID varchar(36) = '000000jp-5d06-4d7a-96fc-d97690a62157';
//                        /****** test harness *****/
//
//                        SELECT
//                              c.ContentID                                         AS ContentID
//                            , " + ProductTypeNames.LegacyVolume.ToDbQuote() + @"  AS ProductType
//                            , c.ContentSubType                                    AS ProductSubType
//                            , c.Title                                             AS ProductName
//                            , c.SuperTitle                                        AS SuperTitle
//                            , c.SubTitle                                          AS SubTitle
//                            , c.SubTitle2                                         AS SubTitle2
//                            , c.FileXML                                           AS FileXML
//                            , 0                                                   AS ItemID
//                            , a.id                                                AS OrderItemID
//                            , NULL                                                AS OrderDate
//                            , 0                                                   AS OrderID
//                            , ''                                                  AS ActivationKey
//                            , 0                                                   AS DownloadComplete
//                            , 'zip'                                               AS FileType
//                            , 0                                                   AS IsExcluded
//                            , ISNULL((
//                                SELECT
//                                  TOP 1
//                                    e.filesize
//                                  FROM
//                                    Site_Elements2 e
//                                  WHERE
//                                    e.ContentID       = c.ContentID
//                                    AND e.ImageType   = " + ImageType.Source.ToDbQuote() + @"
//                                    AND e.StatusType  = " + StatusType.Active.ToDbQuote() + @"
//                              ), 0)                                                                           AS FileSizeBytes
//                            , ISNULL((
//                                SELECT
//                                  TOP 1
//                                    e.CheckSum
//                                  FROM
//                                    Site_Elements2 e
//                                  WHERE
//                                    e.ContentID       = c.ContentID
//                                    AND e.ImageType   = " + ImageType.Source.ToDbQuote() + @"
//                                    AND e.StatusType  = " + StatusType.Active.ToDbQuote() + @"
//                              ), '')                                                                          AS CheckSum
//                            , ISNULL((
//                                SELECT
//                                  TOP 1
//                                    e.CDN
//                                  FROM
//                                    Site_Elements2 e
//                                  WHERE
//                                    e.ContentID       = c.ContentID
//                                    AND e.ImageType   = " + ImageType.Thumb.ToDbQuote() + @"
//                                    AND e.StatusType  = " + StatusType.Active.ToDbQuote() + @"
//                              ), '')                                                                          AS Image1cdn
//                            , ISNULL((
//                                SELECT
//                                  TOP 1
//                                    e.Path
//                                  FROM
//                                    Site_Elements2 e
//                                  WHERE
//                                    e.ContentID       = c.ContentID
//                                    AND e.ImageType   = " + ImageType.Thumb.ToDbQuote() + @"
//                                    AND e.StatusType  = " + StatusType.Active.ToDbQuote() + @"
//                              ), '')                                                                          AS Image1Path
//                          FROM
//                            Site_ContentAssociations a
//                              INNER JOIN
//                                Site_Content2 c
//                                ON c.ContentID = a.AssocContentID
//                          WHERE
//                            a.ContentID IN (
//                              SELECT
//                                ContentID
//                              FROM
//                                Site_Content2  s
//                              WHERE
//                                AccountID           = @AccountID
//                                AND ContentType     = " + ContentType.ListHeader.ToDbQuote() + @"
//                                AND ContentSubType  = " + ListType.CustomerLegacyProducts.ToDbQuote() + @"
//
//                            )
//                            AND a.id > @LastContentID
//                    ";

//      return this.SiteConnection.Query<DownloadableItem>(sql, new { AccountID = accountID, LastContentID = lastContentID });
//    }


//    public IEnumerable<ListItem> GetAccountProducts(string accountID) {

//      string sql = @"/*sql*/ --GetAccountProducts
//                        /****** test harness *****/
//                        --DECLARE @AccountID varchar(36) = '3b2039ec-0a98-4926-9ee8-d5902efa12a9';
//                        /**** end test harness ***/
//
//                        SELECT
//                              c.ContentID
//                            , c.ContentType
//                            , c.ContentSubType
//                            , c.FriendlyTitle
//                            , c.Title
//                            , c.SuperTitle
//                            , c.SubTitle
//                            , c.SubTitle2
//                            , c.Price
//                            , c.CategoryID
//                            , c.Category
//                            , c.SubCategoryID
//                            , c.SubCategory
//                            , c.SortOrder
//                            , c.StatusType
//                            , c.[Description]
//                            , c.Link
//                            , (
//                                SELECT
//                                  TOP 1
//                                    e.CDN
//                                  FROM
//                                    Site_Elements2 e
//                                  WHERE
//                                    e.ContentID       = c.ContentID
//                                    AND e.ImageType   = " + ImageType.Thumb.ToDbQuote() + @"
//                                    AND e.StatusType  = " + StatusType.Active.ToDbQuote() + @"
//                              )                                                                           AS CDN
//                            , (
//                                SELECT
//                                  TOP 1
//                                    e.Path
//                                  FROM
//                                    Site_Elements2 e
//                                  WHERE
//                                    e.ContentID       = c.ContentID
//                                    AND e.ImageType   = " + ImageType.Thumb.ToDbQuote() + @"
//                                    AND e.StatusType  = " + StatusType.Active.ToDbQuote() + @"
//                              )                                                                           AS Path
//                            --, e.CDN
//                            --, e.Path
//                          FROM
//                            Site_Content2 c
//                          WHERE
//                            EXISTS (
//                              SELECT
//                                1
//                                  FROM
//                                    Shop_OrderItems i
//                                  WHERE
//                                    i.ContentID = c.ContentID
//                                    AND EXISTS (
//                                      SELECT
//                                          1
//                                        FROM
//                                          Shop_Orders o
//                                        WHERE
//                                          o.OrderID           = i.OrderID
//                                          AND o.AccountID     = @AccountID
//                                          AND o.PaymentStatus != " + PaymentStatus.PaymentFailure.ToDbQuote() + @"
//                                    )
//                              )
//                          ORDER BY c.Title ASC;";

//      List<PageContent> rawContents = SiteConnection.Query<PageContent>(sql, new { AccountID = accountID }).ToList();

//      List<ListItem> items = new List<ListItem>();

//      foreach (PageContent c in rawContents) {
//        ListItem i = new ListItem();
//        i.ContentID = (int)c.ContentID;
//        i.ContentType = (ContentType)c.ContentType;
//        i.ContentSubType = c.ContentSubType;
//        i.FriendlyTitle = c.FriendlyTitle;
//        i.Title = c.Title;
//        i.SuperTitle = c.SuperTitle;
//        i.SubTitle = c.SubTitle;
//        i.SubTitle2 = c.SubTitle2;
//        i.Price = c.Price;
//        i.CategoryID = c.CategoryID;
//        i.Category = c.Category;
//        i.SubCategoryID = c.SubCategoryID;
//        i.SubCategory = c.SubCategory;
//        i.SortOrder = c.SortOrder;
//        if (c.CDN != null) {
//          i.ThumbPath = SiteConfig.GetCDN(c.CDN) + c.Path;
//        }
//        i.StatusType = c.StatusType;
//        i.Description = c.Description;
//        i.Link = c.Link;

//        items.Add(i);
//      }

//      return items;
//    }


//    public void SaveQuickLink(QuickLink ql) {
//      string sql = @"/*sql*/ --SaveQuickLink
//
//                        INSERT INTO AccountQuickLinks
//                            (
//                                AccountID
//                              , URL
//                              , PageTitle
//                              , IsHidden
//                              , Updated
//                            )
//                          VALUES
//                            (
//                                @AccountID
//                              , @URL
//                              , @PageTitle
//                              , 0
//                              , @Updated
//                            )
//
//                          ";

//      this.SiteConnection.Execute(sql, ql);
//    }


//    public IEnumerable<QuickLink> GetQuickLinks(string accountID, int quickLinkID, bool isHidden = false) {
//      if (quickLinkID < 0) {
//        quickLinkID = 0;
//      }


//      string sql = @"/*sql*/ --GetQuickLinks
//                        SELECT
//                              QuickLinkID
//                            , AccountID
//                            , PageTitle
//                            , URL
//                            , Updated
//                          FROM
//                            AccountQuickLinks
//                          WHERE
//                            AccountID         = @AccountID
//                            AND (@QuickLinkID = 0 OR QuickLinkID > @QuickLinkID)";

//      if (!isHidden) {
//        sql += @"
//                            AND IsHidden      = 0
//                          ";
//      }

//      return this.SiteConnection.Query<QuickLink>(sql, new { AccountID = accountID, QuickLinkID = quickLinkID });
//    }


//    public void DeleteQuickLink(string accountID, int quickLinkID) {
//      string sql = @"/*sql*/ --DeleteQuickLink
//                        UPDATE
//                            AccountQuickLinks
//                          SET
//                            IsHidden = 1
//                          WHERE
//                            AccountID         = @AccountID
//                            AND QuickLinkID   = @QuickLinkID
//
//                          ";

//      this.SiteConnection.Execute(sql, new { AccountID = accountID, QuickLinkID = quickLinkID });
//    }


//    private string GetMultiUserAccountMembersSql(string returnObject, string callingMethod) {
//      string sql = @"/*sql*/ --" + callingMethod + @"
//                        SELECT";
//      sql += GetAccountFields();
//      sql += @"
//                            , ISNULL(c.XDateWarn, " + GetMinDate() + @")  AS XDateWarn
//                            , ISNULL(c.XDate, " + GetMinDate() + @")      AS XDate
//                            , ISNULL(CASE WHEN c.CardID = a.SingleClickCardID THEN c.FriendlyName ELSE '' END, '') AS SingleClickCardName
//                        ";

//      if (returnObject == "MultiUserAccountMember") {
//        sql += @"
//                            , g.PurchaseAllotment
//                            , g.DownloadAllotment
//                            , g.GroupMemberSince
//                            , g.GroupUpdated
//                            , g.GroupRole
//                        ";
//      }

//      sql += @"
//                          FROM
//                            Acnt_Accounts a
//                        ";
//      if (returnObject == "MultiUserAccountMember") {
//        sql += @"
//                              INNER JOIN
//                                AccountGroupsMembers g
//                                ON g.AccountID = a.AccountID
//                        ";
//      }

//      sql += @"
//                              LEFT OUTER JOIN
//                                AccountCards c
//                                ON c.AccountID = a.GroupAccountID AND c.CardStatus = " + CardStatus.Default.ToDbQuote() + @"
//                          WHERE
//                        ";

//      if (callingMethod == "GetMultiUserAccountMember") {
//        sql += @"
//                            a.AccountID = @AccountID;
//                          ";
//      } else {
//        sql += @"
//                            a.GroupAccountID = @GroupAccountID;
//                          ";
//      }

//      return sql;
//    }


//    public IEnumerable<Account> GetAccountMembersMultiUsers(string groupAccountID) {
//      string sql = GetMultiUserAccountMembersSql("Account", "GetAccountMembersMultiUser");

//      return this.SiteConnection.Query<Account>(sql, new { GroupAccountID = groupAccountID });
//    }


//    public IEnumerable<MultiUserAccountMember> GetMultiUserAccountMembers(string groupAccountID) {
//      string sql = GetMultiUserAccountMembersSql("MultiUserAccountMember", "GetMultiUserAccountMembers");

//      return this.SiteConnection.Query<MultiUserAccountMember>(sql, new { GroupAccountID = groupAccountID });
//    }


//    public MultiUserAccountMember GetMultiUserAccountMember(string accountID) {
//      string sql = GetMultiUserAccountMembersSql("MultiUserAccountMember", "GetMultiUserAccountMember");

//      return this.SiteConnection.Query<MultiUserAccountMember>(sql, new { AccountID = accountID }).FirstOrDefault();
//    }


//    public MembershipPasswordInfo GetMembershipPasswordInfo(string accountID) {
//      string sql = @"/*sql*/ --GetMembershipPasswordInfo
//                        DECLARE @UserId uniqueidentifier = NULL;
//
//                        SELECT
//                            @UserId = u.UserId
//                          FROM
//                            aspnet_Users u
//                              INNER JOIN
//                              aspnet_Membership m
//                              ON m.UserID         = u.UserID
//                          WHERE
//                            LoweredUserName       = LOWER(@AccountID);
//
//                        SELECT
//                              PasswordFormat
//                            , PasswordSalt
//                            , Password
//                            , @AccountID          AS AccountID
//                          FROM
//                              aspnet_Membership
//                          WHERE
//                            UserId                = @UserId;
//
//                          ";

//      return this.SiteMembersConnection.Query<MembershipPasswordInfo>(sql, new { AccountID = accountID }).FirstOrDefault();
//    }


//    public void SaveMembershipPasswordInfo(MembershipPasswordInfo membershipPasswordInfo) {
//      string sql = @"/*sql*/ --SaveMembershipPasswordInfo
//                        DECLARE @UserId uniqueidentifier = NULL;
//
//                        SELECT
//                            @UserId = u.UserId
//                          FROM
//                            aspnet_Users u
//                              INNER JOIN
//                              aspnet_Membership m
//                              ON m.UserID         = u.UserID
//                          WHERE
//                            LoweredUserName       = LOWER(@AccountID);
//
//                        UPDATE
//                            aspnet_Membership
//                          SET
//                              PasswordFormat  = @PasswordFormat
//                            , PasswordSalt    = @PasswordSalt
//                            , Password        = @Password
//                          WHERE
//                            UserId                = @UserId
//
//
//                          ";

//      this.SiteMembersConnection.Execute(sql, membershipPasswordInfo);

//    }


//    //    public void SaveMembershipPasswordInfo(string accountID, string newPassword, string passwordSalt, int passwordFormat) {

//    //      string sql = @"/*sql*/ --SaveMembershipPasswordInfo
//    //                        DECLARE @UserId uniqueidentifier = NULL;
//    //
//    //                        SELECT
//    //                            @UserId = u.UserId
//    //                          FROM
//    //                              aspnet_Membership m
//    //                            , aspnet_Users u
//    //                          WHERE
//    //                            LoweredUserName   = LOWER(@UserName)
//    //                            AND u.UserId      = m.UserId
//    //
//    //                        UPDATE
//    //                            aspnet_Membership
//    //                          SET
//    //                              PasswordFormat  = @PasswordFormat
//    //                            , PasswordSalt    = @PasswordSalt
//    //                            , Password        = @NewPassword
//    //                          WHERE
//    //                            UserId                = @UserId
//    //
//    //
//    //                          ";

//    //      //this.SiteMembersConnection.Execute(sql, new { UserName = accountID, NewPassword = newPassword, PasswordSalt = passwordSalt, PasswordFormat = passwordVersion });

//    //    }



//    public IEnumerable<SubscriptionRenewalItem> GetSubscriptionItems(string accountID, List<SubscriptionStatusType> statusTypes, DateTime? nextRenewalDate = null, DateTime? nextRenewalCheckDate = null, SubscriptionItem subscriptionItem = SubscriptionItem.All) {
//      string sql = @"/*sql*/ --GetSubscriptionItems
//                        SET NOCOUNT ON;
//
//                        SELECT
//                              s.SubscriptionItemID
//                            , s.AccountID
//                            , s.SubscriptionItem
//                            , s.ContentID
//                            , s.NextRenewalDate
//                            , s.NextRenewalCheckDate
//                            , s.ActiveSince
//                            , s.SubscriptionStatus
//                            , c.Price
//                            , c.Title AS ProductName
//                            , s.Updated
//                            , s.SubscriptionFutureStatus
//                          FROM
//                            AccountSubscriptions s
//                            INNER JOIN
//                              Site_Content2 c
//                              ON c.ContentID = s.ContentID
//                          WHERE
//                            s.AccountID               = @AccountID";

//      if (nextRenewalDate != null) {
//        sql += @"
//                            AND NextRenewalDate       >= " + nextRenewalDate.ToDbQuote() + Environment.NewLine;
//      }

//      if (nextRenewalCheckDate != null) {
//        sql += @"
//                            AND NextRenewalCheckDate  >= " + nextRenewalCheckDate.ToDbQuote() + Environment.NewLine;
//      }

//      if (!statusTypes.Contains(SubscriptionStatusType.All)) {
//        sql += @" AND (" + Environment.NewLine;
//        for (int i = 0; i < statusTypes.Count; i++) {
//          if (i > 0) {
//            sql += @" OR " + Environment.NewLine;
//          }
//          sql += @"
//                                    SubscriptionStatus      = " + statusTypes[i].ToDbQuote() + Environment.NewLine;
//        }
//        sql += @")" + Environment.NewLine;
//      }

//      if (subscriptionItem != SubscriptionItem.All) {
//        sql += @"
//                                    AND SubscriptionItem    = " + subscriptionItem.ToDbQuote() + Environment.NewLine;
//      }
//      sql += @"
//                                  ORDER BY
//                                    NextRenewalCheckDate DESC " + Environment.NewLine;

//      return this.SiteConnection.Query<SubscriptionRenewalItem>(sql, new { AccountID = accountID });

//    }


//    public void SaveSubscriptionItem(SubscriptionRenewalItem subscriptionRenewalItem) {

//      subscriptionRenewalItem.Updated = DateTime.Now;

//      subscriptionRenewalItem.NextRenewalDate = subscriptionRenewalItem.NextRenewalDate.Date;
//      string sql = @"/*sql*/ --SaveSubscriptionItem
//                        SET NOCOUNT ON;";

//      if (subscriptionRenewalItem.SubscriptionItemID == 0) {
//        sql += @"
//                        INSERT
//                          INTO
//                            AccountSubscriptions
//                            (
//                                AccountID
//                              , ContentID
//                              , SubscriptionItem
//                              , NextRenewalDate
//                              , ActiveSince
//                              , SubscriptionStatus
//                              , SubscriptionFutureStatus
//                              , NextRenewalCheckDate
//                              , Updated
//                            )
//                            VALUES
//                            (
//                                @AccountID
//                              , @ContentID
//                              , " + subscriptionRenewalItem.SubscriptionItem.ToDbQuote() + @"
//                              , @NextRenewalDate
//                              , @ActiveSince
//                              , " + subscriptionRenewalItem.SubscriptionStatus.ToDbQuote() + @"
//                              , " + subscriptionRenewalItem.SubscriptionFutureStatus.ToDbQuote() + @"
//                              , @NextRenewalCheckDate
//                              , @Updated
//                            )
//                            ";
//      } else {
//        sql += @"
//                        UPDATE
//                            AccountSubscriptions
//                          SET
//                              Updated                       = @Updated
//                            , SubscriptionItem              = " + subscriptionRenewalItem.SubscriptionItem.ToDbQuote() + @"
//                            , NextRenewalDate               = @NextRenewalDate
//                            , SubscriptionStatus            = " + subscriptionRenewalItem.SubscriptionStatus.ToDbQuote() + @"
//                            , SubscriptionFutureStatus      = " + subscriptionRenewalItem.SubscriptionFutureStatus.ToDbQuote() + @"
//                            , NextRenewalCheckDate          = @NextRenewalCheckDate
//                            --, AccountID                   = @AccountID
//                            --, ContentID                   = @ContentID
//                            --, ActiveSince                 = @ActiveSince
//                          WHERE
//                            SubscriptionItemID                = @SubscriptionItemID
//
//                            ";
//      }

//      try {
//        this.SiteConnection.Execute(sql, subscriptionRenewalItem);
//      }
//      catch (Exception ex) {
//        string msg = ex.Message;
//      }

//      if (subscriptionRenewalItem.NextRenewalDate <= Config.AppConfig.minDateTime) {
//        sql = @"/*sql*/ -- NextRenewalDate = null
//                        UPDATE
//                            AccountSubscriptions
//                          SET
//                              NextRenewalDate               = null
//                            , NextRenewalCheckDate          = null
//                          WHERE
//                            SubscriptionItemID              = " + subscriptionRenewalItem.SubscriptionItemID.ToString() + @";
//                        ";

//        this.SiteConnection.Execute(sql);
//      }

//    }


//    public void InsertToAccountSubscriptionStatusLog(Account account, SubscriptionItem subscriptionItem, SubscriptionStatusType subscriptionStatus) {

//      DynamicParameters param = new DynamicParameters();

//      param.Add("@AccountID", account.AccountID);
//      param.Add("@SubscriptionStatus", subscriptionStatus.ToString());
//      param.Add("@Country", account.Country);
//      param.Add("@WasDJAccount", account.WasDJAccount);
//      param.Add("@ActivityDate", DateTime.Now);
//      param.Add("@subscriptionItem", subscriptionItem.ToString());

//      string sql = @"/*sql*/ --InsertToAccountSubscriptionStatusLog
//                        INSERT
//                          INTO
//                            AccountSubscriptionStatusLog
//                              (
//                                  AccountID
//                                , SubscriptionStatus
//                                , Country
//                                , WasDJAccount
//                                , ActivityDate
//                                , SubscriptionItem
//                              )
//                          VALUES
//                             (
//                                  @AccountID
//                                , @SubscriptionStatus
//                                , @Country
//                                , @WasDJAccount
//                                , @ActivityDate
//                                , @SubscriptionItem
//                             )
//
//                    ";

//      this.SiteConnection.Execute(sql, param);
//    }


//    public string GetAccountIDFromJuicerToken(string token) {
//      string sql = @"/*sql*/ --GetAccountIDFromJuicerToken
//
//                        SELECT
//                            AccountID
//                          FROM
//                            AccountDownloaders
//                          WHERE
//                            Token = @Token
//                    ";

//      return this.SiteConnection.Query<string>(sql, new { Token = token }).SingleOrDefault();

//    }


//    public CreditCardBankInfo GetCreditCardBankInfo(int cardBin) {
//      string sql = @"/*sql*/ --GetCreditCardBankInfo
//
//                        SELECT
//                              id
//                            , Bin
//                            , Country
//                            , Brand
//                            , CardType
//                            , Bank
//                            , Phone
//                            , RiskLevel
//                            , CardCategory
//                          FROM
//                            AdminCreditCardIIN
//                          WHERE
//                            Bin = " + cardBin.ToString();

//      CreditCardBankInfo creditCardBankInfo = this.SiteConnection.Query<CreditCardBankInfo>(sql).FirstOrDefault();

//      if (creditCardBankInfo == null) {
//        creditCardBankInfo = new CreditCardBankInfo();
//      }

//      return creditCardBankInfo;

//    }


//    public CreditCardBankInfo SaveCreditCardBankInfo(CreditCardBankInfo creditCardBankInfo) {
//      string sql = @"/*sql*/ --SaveCreditCardBankInfo ";
//      if (creditCardBankInfo.id == 0) {
//        sql += @"
//                        INSERT INTO
//                          AdminCreditCardIIN
//                            VALUES (
//                                @Bin
//                              , @Country
//                              , @Brand
//                              , @Bank
//                              , @Phone
//                              , " + DateTime.Now.ToDbQuote() + @"
//                              , @RiskLevel
//                              , @CardCategory
//                              , @CardType
//                            )
//
//                        ";
//      } else {

//        sql += @"
//                        UPDATE
//                          AdminCreditCardIIN
//                            SET
//                                Country           = @Country
//                              , Brand             = @Brand
//                              , CardType          = @CardType
//                              , Bank              = @Bank
//                              , Phone             = @Phone
//                              , RiskLevel         = @RiskLevel
//                              , CardCategory      = @CardCategory
//                              , Updated           = " + DateTime.Now.ToDbQuote() + @"
//                          FROM
//                            AdminCreditCardIIN
//                          WHERE
//                            Bin                   = @Bin;";
//      }

//      sql += @"
//                        SELECT * FROM AdminCreditCardIIN WHERE Bin = @Bin;
//                        ";

//      return creditCardBankInfo = this.SiteConnection.Query<CreditCardBankInfo>(sql, creditCardBankInfo).FirstOrDefault();

//    }


//    public bool CheckMembershipDatabaseConnection() {

//      string sql = @"/*sql*/ --CheckMembershipDatabaseConnection
//                        SELECT
//                          TOP 1
//                            Email
//                          FROM
//                            aspnet_Membership
//                          WHERE
//                            LEN(Email) > 0
//                      ";

//      return this.SiteMembersConnection.Query<string>(sql).Single().IsNotEmpty();
//    }

//    public Account GetAccountIdOnEmailForFoundingMembers(string emailAddress) {
//      string sql = @"/*sql*/ --GetAccountOnEmail
//
//                        /****** test harness *****/
//                        --DECLARE @EmailAddress   varchar(60) = '';
//                        /**** end test harness ***/
//                        SET NOCOUNT ON;
//
//                        SELECT TOP 1
//                            AccountID
//                            ,SolomonCustID
//                          FROM
//                            Acnt_Accounts
//                          WHERE
//                            AltEmail = @EmailAddress
//                          ORDER BY
//                            Updated DESC
//                          ;
//              ";


//      return this.SiteConnection.Query<Account>(sql, new { EmailAddress = emailAddress }).FirstOrDefault();
//    }


//    private string GetAccountFields() {
//      string sqlList = @"
//                              a.AccountID
//                            , a.AccountID_
//                            , a.FirstName
//                            , a.LastName
//                            , a.NickName
//                            , a.Email
//                            , a.PasswordSalt
//                            , a.PasswordVersion
//                            , a.AltEmail
//                            , a.SendEmail
//                            , a.TrustLevel
//                            , a.RewardLevel
//                            , a.CardThreshold
//                            , a.MobilePIN
//                            , a.SingleClickCardID
//                            , a.SubscriptionStatus
//                            , a.SubscriptionFutureStatus
//                            , a.NextRenewalDate
//                            , a.NextRenewalCheckDate
//                            , 'Juicer'                      AS SkinTheme
//                            , a.PageViewMode
//                            , a.ImageOption
//                            , a.DefaultJuicerID
//                            , a.LockedReason
//                            , a.LastIpAddress
//                            , a.GroupAccountID
//                            , a.AccountType
//                            , a.GroupAccountRole
//                            , a.HasJuicerAccess
//                            , a.ActiveSince
//                            , a.DownloadAllotmentMB
//                            , a.DownloadPurchaseMB
//                            , a.HasDownloadAllotmentBeenReached
//                            , a.PurchaseLimit
//                            , a.Country
//                            , a.IsMerged
//                            , a.StatusMessage
//                            , a.StoreCredit
//                            , a.AvatarCDN
//                            , a.MemberType
//                            , a.MemberLevel
//                            , a.Telephone
//                            , a.DJCustomerID
//                            , a.CreateDate
//                            , a.HasLegacyContents
//                            , a.SolomonCustID
//                            , a.Updated
//                            , a.SubscriptionItemCreditCount
//                            , a.Impersonating
//                            , ISNULL(a.AvatarPath, " + SiteConfig.AvatarPath.ToDbQuote() + @") AS AvatarPath
//                            , a.IsInternalAccount
//                            , a.Preferences
//                        ";

//      return sqlList;
//    }


//    public IEnumerable<Account> GetAccountsFoundingMembers(string subscriptionStatus = "") {
//      string sql="/*sql*/ --GetAccountsFoundingMembers";
//      sql += @"
//
//                        SELECT";
//      sql += GetAccountFields();
//      sql += @"
//                          FROM
//                            Acnt_Accounts a
//                          WHERE";

//      if (subscriptionStatus.IsNotEmpty()) {
//        sql += @"
//                            a.SubscriptionStatus = " + subscriptionStatus.ToDbQuote() + @"
//                        ";
//      } else {
//        sql += @"
//                            a.SubscriptionStatus IN (" + SubscriptionStatusType.Active.ToDbQuote() + "," + SubscriptionStatusType.Pending.ToDbQuote() + "," + SubscriptionStatusType.WaitListPending.ToDbQuote() + "," + SubscriptionStatusType.Cancelled.ToDbQuote() + @")
//                        ";
//      }


//      return this.SiteConnection.Query<Account>(sql);

//    }


//    public IEnumerable<Account> GetAccountsWaitListPendingNotEmailed() {
//      string sql="/*sql*/ --GetAccountsWaitListPendingNotEmailed";
//      sql += @"
//
//                        SELECT";
//      sql += GetAccountFields();
//      sql += @"
//                          FROM
//                            Acnt_Accounts a
//                          WHERE
//                            a.SubscriptionStatus  = " + SubscriptionStatusType.WaitListPending.ToDbQuote() + @"
//                            AND SendEmail         = 0
//                        ";


//      return this.SiteConnection.Query<Account>(sql);
//    }


//    public IEnumerable<Account> GetAccounts(SubscriptionStatusType status) {
//      string sql = "/*sql*/ --GetAccountsFoundingMembers";
//      sql += @"
//
//                        SELECT";
//      sql += GetAccountFields();
//      sql += @"
//                          FROM
//                            Acnt_Accounts a
//                          WHERE
//                            a.SubscriptionStatus = @status
//                        ";


//      return this.SiteConnection.Query<Account>(sql, new { status = status.ToString() });

//    }


//    public IEnumerable<Account> GetAccounts(SubscriptionStatusType status, MembershipLevel level = MembershipLevel.All) {
//      string sql = "/*sql*/ --GetAccountsFoundingMembers";
//      sql += @"
//
//                        SELECT";
//      sql += GetAccountFields();
//      sql += @"
//                          FROM
//                            Acnt_Accounts a
//                          WHERE
//                            a.SubscriptionStatus = @status
//                        ";

//      if (level != MembershipLevel.All) {
//        sql += @"
//                          AND a.MemberLevel = @level
//                        ";
//      }

//      return this.SiteConnection.Query<Account>(sql, new { status = status.ToString(), level = level.ToString() });

//    }



//    public void ClearMyHistory(string accountID, List<string> juicerIDs) {
//      string sql = @"/*sql*/ --ClearMyHistory
//
//                        DELETE FROM Shop_CartItems                          WHERE AccountID = @AccountID;
//                        DELETE FROM Shop_OrderItems                         WHERE AccountID = @AccountID;
//                        DELETE FROM Shop_Orders                             WHERE AccountID = @AccountID;
//                        DELETE FROM Shop_OrderPaymentExceptions             WHERE AccountID = @AccountID;
//                        DELETE FROM Shop_OrderPayments                      WHERE AccountID = @AccountID;
//                        DELETE FROM Shop_PaymentProcessTransactionLog       WHERE AccountID = @AccountID;
//                        DELETE FROM Downloads_Items                         WHERE AccountID = @AccountID;
//                        DELETE FROM DownloadsTracker                        WHERE AccountID = @AccountID;
//                        DELETE FROM AccountLog                              WHERE AccountID = @AccountID;
//                        DELETE FROM AccountBalanceTransactionLog            WHERE AccountID = @AccountID;
//                        DELETE FROM AccountPurchasesProtoTracker            WHERE AccountID = @AccountID;
//                        DELETE FROM DownloadIDPer                           WHERE JuicerID int @JuicerIDs;
//
//                        ";

//      this.SiteConnection.Execute(sql, new { AccountID = accountID, JuicerIDs = juicerIDs });

//    }


//    public int GetMaxFoundingMemberNumber() {
//      string sql = @"/*sql*/ --GetMaxFoundingMemberNumber
//
//                        SELECT Max(RewardLevel) as MaxNumber FROM Acnt_Accounts;
//                        ";

//      return this.SiteConnection.Query<int>(sql).Single();
//    }


//    public IEnumerable<Account> GetNewFoundingMembers(int hours) {
//      string sql = "/*sql*/ --GetAccountsFoundingMembers";
//      sql += @"
//
//                        SELECT";
//      sql += GetAccountFields();
//      sql += @"
//                          FROM
//                            Acnt_Accounts a
//                          WHERE
//                            CAST(CreateDate AS Date)  >= " + DateTime.Now.AddHours(hours * -1).Date.ToDbQuote() + @"
//                            AND Updated               >= " + DateTime.Now.AddHours(hours * -1).ToDbQuote() + @"
//                            AND MemberLevel           = " + MembershipLevel.FoundingMember.ToDbQuote() + @"
//                        ";

//      return this.SiteConnection.Query<Account>(sql);

//    }


//    public IEnumerable<AccountDownloadLog> GetAccountDownloadLog(string accountID, DateTime startDate, DateTime endDate, int startPos, int numbToReturn) {

//      int endPos = startPos + numbToReturn - 1;

//      string sql = @"/*sql*/ --GetAccountDownloadLog
//                        /**********   test   **************/
//                        --DECLARE @AdminID          varchar(36)       = '00000001-1a5a-4ecc-a61c-12067931804b';
//                        --DECLARE @AccountID        varchar(36)       = '00000001-1a5a-4ecc-a61c-12067931804b';
//                        --DECLARE @ActionType       varchar(64)       = '';
//                        --DECLARE @StartDate        datetime          = '01/01/1975'
//                        --DECLARE @EndDate          datetime          = '01/01/2100'
//
//                        --DECLARE @StartPos         int               = 1;
//                        --DECLARE @NumbToReturn     int               = 1005;
//                        /********* end test **************/
//
//                        SET NOCOUNT ON;
//
//                        ;WITH cte AS (
//                          SELECT
//                                DownloadID
//                              , AccountID
//                              , ContentID
//                              , TotalDownloadCount
//                              , LastDownloadCompleteDate
//                              , Rescinded
//                              , RescindedMessage
//                              , FirstDownloadCompleted
//                              , Updated
//                              , GroupAccountID
//                              , StartDownloadTrackingCount
//                              , CreateDate
//                              , DownloadAccountingType
//                              , Row_Number() OVER(ORDER BY Updated DESC)    AS RowCounted
//                            FROM
//                              Downloads_Items
//                            WHERE
//                              AccountID       = " + accountID.ToDbQuote() + @"
//                              AND (Updated BETWEEN " + startDate.ToDbQuote() + @" AND " + endDate.ToDbQuote() + @")
//                              AND ContentID > 300
//                        ";

//      sql += @"
//                          )
//                          , countThem AS (
//                            SELECT COUNT(*) as TotalNumb from cte
//                          )
//                          SELECT
//                                c.DownloadID
//                              , c.AccountID
//                              , c.ContentID
//                              , c.TotalDownloadCount
//                              , c.LastDownloadCompleteDate
//                              , c.Rescinded
//                              , c.RescindedMessage
//                              , c.FirstDownloadCompleted
//                              , c.Updated
//                              , c.GroupAccountID
//                              , c.StartDownloadTrackingCount
//                              , c.CreateDate
//                              , c.DownloadAccountingType
//                              , t.TotalNumb
//                              , s.Title
//                              , s.SubTitle
//                              , s.Category
//                              , s.SubCategory
//                              , c.RowCounted
//                            FROM
//                              countThem t
//                              , cte c
//                                INNER JOIN
//                                  Site_Content2 s
//                                  ON s.ContentID = c.ContentID
//                            WHERE
//                              RowCounted BETWEEN " + startPos.ToString() + @" AND " + endPos.ToString() + @"
//                            Order BY
//                              RowCounted
//                        ;";

//      List<AccountDownloadLog> accountDownloadLog = this.SiteConnection.Query<AccountDownloadLog>(sql).ToList();

//      return accountDownloadLog;
//    }


//    public IEnumerable<string> GetWorkbenchJabberStatus(List<string> juicerIDs) {

//      string juicerIDList = String.Join(",", juicerIDs);
//      juicerIDList = "'" + juicerIDList.Replace(",", "','") + "'";

//      string sql = @"/*sql*/ --GetWorkbenchJabberStatus
//                        SELECT
//                              username
//                          FROM -- [JuicyOpenFire].[dbo].[userStatus]
//                            userStatus
//                          WHERE
//                            username      IN (" + juicerIDList + @")
//                            AND online    = 1
//                            AND resource  = 'Workbench'
//                        ";

//      return this.OpenfireConnection.Query<string>(sql);
//    }



  }
}
