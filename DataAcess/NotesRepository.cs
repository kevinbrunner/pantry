﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pantry.Config;
using Pantry.Entities;
using Pantry.Utilities;

using Dapper;


namespace Pantry.DataAccess {
  public class NotesRepository : Repository {

    public void SaveNote(Note note) {
      //string sql = @"/*sql*/
      //              ";

      string sql = @"/*sql*/ --SaveNote

                    ";

      DynamicParameters param = new DynamicParameters();

      param.Add("@ID", note.id, direction: ParameterDirection.InputOutput);
      param.Add("@FamilyID", note.FamilyID);
      param.Add("@Body", note.Body.SetAndTrim());
      param.Add("@CreateDate", DateTime.Now);
      param.Add("@EnteredBy", note.EnteredBy.SetAndTrim());
      param.Add("@NoteType", note.NoteType.ToString());
      param.Add("@Title", note.Title.SetAndTrim());
      param.Add("@TransferredTo", note.TransferredTo.SetAndTrim());
      param.Add("@TransferDate", note.TransferDate);


      if(note.id == 0) {
        sql += @"
                        INSERT
                          INTO
                            Notes
                            (
                                FamilyID
                              , CreateDate
                              , EnteredBy
                              , Title
                              , Body
                              , TransferredTo
                              , TransferDate
                              , NoteType
                            )
                          OUTPUT INSERTED.id
                          VALUES (
                                @FamilyID
                              , @CreateDate
                              , @EnteredBy
                              , @Title
                              , @Body
                              , @TransferredTo
                              , @TransferDate
                              , @NoteType
                            )

                        SET @id = SCOPE_IDENTITY();
                          ";
      } else {
        sql += @"
                        UPDATE
                            FAMILY
                          SET
                                EnteredBy           = @EnteredBy
                              , Title               = @Title
                              , Body                = @Body
                              , TransferredTo       = @TransferredTo
                              , TransferDate        = @TransferDate
                              , NoteType            = @NoteType
                          WHERE
                            id                      = @id
                          ";
      }

      int newID = this.SiteConnection.Query<int>(sql, param).SingleOrDefault();
      if(note.id == 0) {
        note.id = newID;
      }

    }


    public IEnumerable<Note> GetNotes(int familyID, NoteType noteType) {

      string sql = @"/*sql*/ --GetNotes
                        SELECT
                                id
                              , FamilyID
                              , CreateDate
                              , EnteredBy
                              , Title
                              , Body
                              , TransferredTo
                              , TransferDate
                              , NoteType
                          FROM
                            Notes
                          WHERE
                            FamilyID      = " + familyID.ToString();

      if(noteType != NoteType.All) {
        sql += @"
                            AND NoteType = " + noteType.ToDbQuote();
      }

      sql += @"
                          ORDER BY
                            id DESC";

      return this.SiteConnection.Query<Note>(sql);
    }



  }
}
