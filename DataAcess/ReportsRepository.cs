﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pantry.Config;
using Pantry.Entities;
using Pantry.Utilities;

using Dapper;


namespace Pantry.DataAccess {
  public class ReportsRepository : Repository {


    public VisitCounts GetVisitCount(DateTime startDate, DateTime endDate) {

      string sql = @"/*sql*/ --GetVisitCount
                        ;WITH fCount AS (
                          SELECT
                              COUNT(*) AS FamiliesCount
                            FROM
                              FamilyVisits
                            WHERE
                              CreateDate BETWEEN @StartDate AND @EndDate
                        )
                        , tCount AS (
                          SELECT
                              SUM(ISNULL(NumberOfMembers, 0))  AS FamilyMembersCount
                            FROM
                              FamilyVisits
                            WHERE
                              CreateDate BETWEEN @StartDate AND @EndDate
                        )
                        SELECT
                              FamiliesCount
                            , FamilyMembersCount
                            , @StartDate
                            , @EndDate
                          FROM fCount, tCount
                        ";

      VisitCounts visitCounts = this.SiteConnection.Query<VisitCounts>(sql, new { StartDate = startDate, EndDate = endDate }).FirstOrDefault();

      return visitCounts;
    }


    public IEnumerable<DailyDetailedReport> GetDetailedReport(DateTime startDate, DateTime endDate) {

      string sql = @"/*sql*/ --GetVisitDetails
                        SELECT
                              f.FirstName + ' ' + f.LastName  AS Name
                            , v.NumberOfMembers               AS NumberOfMembers
                            , v.CreateDate                    AS CreateDate
                            , v.VisitType                     AS VisitType
                            , v.ServiceLevel                  AS ServiceLevel
                          FROM
                            FamilyVisits v
                            INNER JOIN family f on v.FamilyID = f.FamilyID
                          WHERE
                            v.CreateDate BETWEEN @StartDate AND @EndDate
                          ORDER BY
                            v.CreateDate DESC
                  ";

      IEnumerable<DailyDetailedReport> details = this.SiteConnection.Query<DailyDetailedReport>(sql, new { StartDate = startDate, EndDate = endDate });

      return details;
    }



  }
}
