﻿using System;
using System.ComponentModel.Composition;
using System.Data;
using System.Data.SqlClient;

using Pantry.Utilities;

using System.Data.Common;


namespace Pantry.DataAccess {
  public class Repository : IDisposable {

    //protected static void SetIdentity<T>(IDbConnection connection, Action<T> setId) {
    //  dynamic identity = connection.Query("SELECT @@IDENTITY AS Id").Single();
    //  T newId = (T)identity.Id;
    //  setId(newId);
    //}

    public Repository() {
    }


    [ThreadStatic]
    private static IDbConnection siteConnection = null;

    [Export("SiteConnection")]
    public IDbConnection SiteConnection {
      get {

        if (siteConnection == null) {
          //siteConnection = new SqlConnection(Config.DataBaseConfig.SiteConnectionString);
          //siteConnection.Open();

          DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.SqlClient");
          siteConnection = factory.CreateConnection();
          siteConnection.ConnectionString = Config.DataBaseConfig.SiteConnectionString;
          //siteConnection.ConnectionString = Config.DataBaseConfig.DevConnectionString;
          siteConnection.Open();

        }
        return siteConnection;

      }
    }


    [ThreadStatic]
    private static IDbConnection azureConnection = null;

    [Export("AzureConnection")]
    public IDbConnection AzureConnection {
      get {

        if (azureConnection == null) {
          //siteConnection = new SqlConnection(Config.DataBaseConfig.SiteConnectionString);
          //siteConnection.Open();

          DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.SqlClient");
          azureConnection = factory.CreateConnection();
          azureConnection.ConnectionString = Config.DataBaseConfig.AzureConnectionString;
          azureConnection.Open();

        }
        return azureConnection;

      }
    }


    [ThreadStatic]
    private static IDbConnection siteMembersConnection = null;

    [Export("SiteMembersConnection")]
    public IDbConnection SiteMembersConnection {
      get {

        if(siteMembersConnection == null) {
          DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.SqlClient");
          siteMembersConnection = factory.CreateConnection();
          siteMembersConnection.ConnectionString = Config.DataBaseConfig.SiteMembersConnectionString;
          siteMembersConnection.Open();
        }

        return siteMembersConnection;

      }
    }


    //[ThreadStatic]
    //private static IDbConnection elmahConnection = null;

    //[Export("ElmahConnection")]
    //public IDbConnection ElmahConnection {
    //  get {

    //    if (elmahConnection == null) {
    //      DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.SqlClient");
    //      elmahConnection = factory.CreateConnection();
    //      elmahConnection.ConnectionString = Config.DataBaseConfig.ElmahConnection;
    //      elmahConnection.Open();
    //    }

    //    return elmahConnection;

    //  }
    //}




    //Please use the below method to do your data calls

    /*

     using (var conn = GetDbConnection()) {
       conn.Open();

       <insert dapper statement here> (eg - conn.Query<int>();)
     }

     Note:  You do not have to explicitly close the connection as it will be closed even if there is a exception.

     If you need to handle certain exceptions, you can also do like this, but above method is preferred.

     var conn = GetDbConnection();

     try {
      conn.Open();
       <insert dapper statement here> (eg - conn.Query<int>();)
     }
     catch() {
      //do something here.
     }
     finally {
      conn.Destroy();  //this will close and dispose!
     }


     */

    public IDbConnection GetDbConnection() {
      return GetDbConnection(Database.Pantry);
    }


    public IDbConnection GetDbConnection(Database database) {

      string connectionString = String.Empty;

      switch(database) {
        case Database.Pantry:
          connectionString = Config.DataBaseConfig.SiteConnectionString;
          //connectionString = Config.DataBaseConfig.DevConnectionString;
          break;
        case Database.Azure:
          connectionString = Config.DataBaseConfig.AzureConnectionString;
          //connectionString = Config.DataBaseConfig.DevConnectionString;
          break;
        case Database.PantryMembers:
          connectionString = Config.DataBaseConfig.SiteMembersConnectionString;
          break;
      }

      DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.SqlClient");
      DbConnection dbConnection = factory.CreateConnection();
      dbConnection.ConnectionString = connectionString;

      //return new SqlConnection(connectionString)

      return dbConnection;
    }



    public void Dispose() {

      if(siteConnection != null) {
        siteConnection.Dispose();
        siteConnection = null;
      }

      if(siteMembersConnection != null) {
        siteMembersConnection.Dispose();
        siteMembersConnection = null;
      }

    }


    public static string MinDate() {
      return "Convert(datetime,'1970-01-01') ";
    }


    public static string GetDate() {
      return GetDate(DateTime.Now);
    }


    public static string GetDate(DateTime now) {
      return now.ToDbQuote();
    }


    public static string GetMinDate() {
      return (Config.AppConfig.MinDateTime).ToDbQuote();
    }

  }


  public enum Database {
    Pantry,
    PantryMembers,
    Azure,
  }




}

// SET @ID = SCOPE_IDENTITY();

//OUTPUT INSERTED.CrashReportID
//VALUES

//ALTER TABLE {TABLENAME}
//ADD {COLUMNNAME} {TYPE} {NULL|NOT NULL}
//CONSTRAINT {CONSTRAINT_NAME} DEFAULT {DEFAULT_VALUE}
//[WITH VALUES]
