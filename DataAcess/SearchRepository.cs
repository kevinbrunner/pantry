﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pantry.Entities;
using Pantry.Helpers;
using Pantry.Utilities;

using Dapper;


namespace Pantry.DataAccess {
  public class SearchRepository : Repository {

    public void SaveSearchTerms(int familyID, List<string> searchTerms) {

      string sql = @"/*sql*/ --SaveSearchTerms

                        DELETE
                          FROM
                            SearchTerms
                          WHERE
                            FamilyID = " + familyID.ToString();

      foreach(string term in searchTerms) {
        if(term.IsNotEmpty()) {
          sql += @"

                        INSERT
                          INTO
                            SearchTerms
                          (
                              FamilyID
                            , Term
                          )
                          VALUES
                          (
                              " + familyID.ToString() + @"
                            , " + term.ToDbQuote() + @"
                          )

                              ";
        }
      }

      this.SiteConnection.Execute(sql);

    }



    public IEnumerable<SearchData> SearchOnTerms(List<string> searchTerms) {

      DynamicParameters param = new DynamicParameters();
      List<string> sTerms = new List<string>();
      for(int i = 0; i < 10; i++) {
        sTerms.Add("");
      }

      int j = 0;
      foreach(string sTerm in searchTerms) {
        if(sTerm.IsNotEmpty()) {
          sTerms[j] = sTerm;
          j += 1;
        }
      }

      if(searchTerms.Count == 1 && searchTerms[0].EndsWith("*")) {
        sTerms[0] = sTerms[0].Replace("*", "%");
      }

      param.Add("@term0", sTerms[0]);
      param.Add("@term1", sTerms[1]);
      param.Add("@term2", sTerms[2]);
      param.Add("@term3", sTerms[3]);
      param.Add("@term4", sTerms[4]);
      param.Add("@term5", sTerms[5]);
      param.Add("@term6", sTerms[6]);
      param.Add("@term7", sTerms[7]);
      param.Add("@term8", sTerms[8]);
      param.Add("@term9", sTerms[9]);

      string sql = @"

                        ;WITH cte AS (
                          SELECT
                              t.FamilyID
                              ,COUNT(*) AS counted
                            FROM
                              SearchTerms t
                                INNER JOIN Family f
                                ON f.FamilyID = t.FamilyID
                            WHERE ";

      j = 0;
      if(searchTerms.Count == 1 && sTerms[0].EndsWith("%")) {
        sql += @"   term  like @term" + j.ToString();
      } else {
        for(int i = 0; i < searchTerms.Count; i++) {
          if(searchTerms[i].IsNotEmpty()) {
            if(i > 0) {
              sql += " OR ";
            }
            sql += @"   term  = @term" + j.ToString();
            j += 1;
          }
        }
      }

      sql += @"
                          GROUP BY t.FamilyID
                        )
                        SELECT
                              f.FamilyID
                            , f.FirstName
                            , f.LastName
                            , f.Social
                            , f.Address1
                            , f.City
                            , f.Zip
                            , f.Phone
                            , f.Cell
                            , CONVERT(varchar(20), (SELECT MAX(CreateDate) FROM FamilyVisits WHERE FamilyID = f.FamilyID), 102) AS LastVisit
                          FROM
                            cte c
                              INNER JOIN
                                Family f
                                ON f.FamilyID = c.FamilyID
                            ";

      List < SearchData > sd = this.SiteConnection.Query<SearchData>(sql, param).ToList();


      return this.SiteConnection.Query<SearchData>(sql, param);
    }


    public IEnumerable<SearchData> SearchForSocial(string social) {
      string sql = @"/*sql*/--SearchForSocial
                        SELECT
                              FamilyID
                            , FirstName
                            , LastName
                            , Social
                            , Address1
                            , City
                            , Zip
                            , Phone
                            , LastVisitDate  AS LastVisit
                          FROM
                            Family
                          WHERE
                            Social = @Social
                ";

      return this.SiteConnection.Query<SearchData>(sql, new { Social = social });
    }


    public IEnumerable<SearchData> SearchForPhone(string phone) {

      string sql = @"/*sql*/--SearchForPhone
                        SELECT
                              FamilyID
                            , FirstName
                            , LastName
                            , Social
                            , Address1
                            , City
                            , Zip
                            , Phone
                            , LastVisitDate  AS LastVisit
                          FROM
                            Family
                          WHERE
                ";

      if(phone.Length == 4 || phone.Length == 7) {
        phone = "%" + phone;
        sql += @"
                            Phone LIKE @Phone
                        ";
      } else {
        sql += @"
                            Phone = @Phone
                        ";
      }

      return this.SiteConnection.Query<SearchData>(sql, new { Phone = phone });
    }



  }
}
