﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pantry;
using Pantry.DataAccess;
using Pantry.Entities;
using Pantry.Services;

namespace Pantry.DataTransfer {
  class Program {
    static void Main(string[] args) {

      AzureFamilyService azureFamilyService = new AzureFamilyService();
      FamilyService familyService = new FamilyService();
      LoggingService loggingService = new LoggingService();
      NotesService notesService = new NotesService();
      AzureSearchRepository SearchRepo = new AzureSearchRepository();
  
  
  
      List<FamilyHead> heads = familyService.GetAllHeads().ToList();
      int familyID = 0;
      foreach (FamilyHead head in heads) {
  
        FamilyData familyData = familyService.GetFamily(head.FamilyID);
        familyID = head.FamilyID;
        FamilyHead aHead = (FamilyHead)head.Clone();
        aHead.FamilyID = 0;
        azureFamilyService.SaveFamilyHead(aHead);
        familyID = aHead.FamilyID;
  
        foreach (FamilyMember m in familyData.FamilyMembers) {
          m.FamilyID = familyID;
        }
  
        azureFamilyService.SaveFamilyMembers(familyData.FamilyMembers);
  
        List<string> sTerms = new List<string>();
        sTerms.Add(aHead.Address1);
        sTerms.Add(aHead.Cell);
        sTerms.Add(aHead.City);
        sTerms.Add(aHead.FirstName);
        sTerms.Add(aHead.LastName);
        sTerms.Add(aHead.Phone);
        sTerms.Add(aHead.Social);
  
        SearchRepo.SaveSearchTerms(familyID, sTerms);
  
  
  
  
      }

      




    }
  }
}
