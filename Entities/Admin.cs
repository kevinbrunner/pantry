﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Pantry.Entities {
  public class AdminKeyPairs {
    public AdminKeyPairs() {
    }

    public int AID { get; set; }
    public string StrID { get; set; }
    public int IntID { get; set; }
    public string Key { get; set; }
    public string Value { get; set; }
    public DateTime DateValue { get; set; }
  }


  public class CheckedValueService {
    public CheckedValueService() {
      this.OnClick = false;
      this.IsBold = false;
      this.IsIPAddress = false;
      this.IsAddress = false;
      this.Key = "";
      this.Value = "";
      this.Text = "";
      this.ErrorText = "";
    }

    public CheckedValueService(string key, string value, bool isBold = false, bool isLink = false) {
      this.Key = key;
      this.Value = value;
      this.OnClick = isLink;
      this.IsBold = isBold;
      this.Text = "";
      this.ErrorText = "";
    }

    public bool OnClick { get; set; }
    public bool IsBold { get; set; }

    public string Text { get; set; }
    public string Key { get; set; }
    public string Value { get; set; }
    public string ErrorText { get; set; }
    public bool IsIPAddress { get; set; }
    public bool IsAddress { get; set; }

  }


}
