﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pantry.Entities {
  public class AmazonUploadParameters {
    public AmazonUploadParameters() { }

    public AmazonUploadParameters(
      string s3Folder,
      string s3FileName,
      string s3FileExtn,
      string redirectUrl,
      string bucketName,
      string accessMode,
      int minutes
    ) {
      this.S3Folder = s3Folder;
      this.S3FileName = s3FileName;
      this.S3FileExtn = s3FileExtn;
      this.RedirectUrl = redirectUrl;
      this.BucketName = bucketName;
      this.AccessMode = accessMode;
      this.Minutes = minutes;
    }

    public string S3Folder { get; set; }
    public string S3FileName { get; set; }
    public string S3FileExtn { get; set; }
    public string RedirectUrl { get; set; }
    public string BucketName { get; set; }
    public string AccessMode { get; set; }
    public int Minutes { get; set; }

  }


  public class AwsFolder {
    public AwsFolder() { }

    public AwsFolder(string bucketName, string awsPrefix) { }

    public string BucketName { get; set; }
    public string AwsPrefix { get; set; }
    public List<AwsObject> FileObjects   { get; set; }
    public List<AwsObject> FolderObjects { get; set; }
  }

  public class AwsObject {
    public AwsObject() { 
    }

    
    public AwsObject(string folderName) {
      this.FolderName = folderName;
    }

    public string FolderName { get; set; }
    public string Filename { get; set; }
    public string Hash { get; set; }
    public Int64 Size { get; set; }
    public DateTime LastModified { get; set; }
  }


}
