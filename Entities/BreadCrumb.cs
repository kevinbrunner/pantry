﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Pantry.Entities {
  public class BreadCrumb {
    public BreadCrumb() { }

    public BreadCrumb(string text, string href) {
      this.Text = text;
      this.Href = href;
    }

    public BreadCrumb(string text, WebIcons.WebApplicationIcons crumbIcon = WebIcons.WebApplicationIcons.noIcon) {
      this.Href = "";
      this.Text = text;
      this.CrumbIcon = crumbIcon;
    }

    public string Href { get; set; }
    public string Text { get; set; }
    public WebIcons.WebApplicationIcons CrumbIcon { get; set; }

  }


}
