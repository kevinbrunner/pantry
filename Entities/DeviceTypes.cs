using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;


namespace Pantry.Entities {

  public enum DeviceType {
    Desktop,
    Tablet,
    Phone
  }

}
