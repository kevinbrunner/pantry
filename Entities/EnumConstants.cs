﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pantry.Entities {

  public enum VisitType {
    Any,
    Grocery,
    BreadAndProduce,
  }


  public enum FamilyType {
    Regular,
    ShutIn,
    ChurchAttendee,
    Veteran,
    Homeless,
    RecoveryVillage,
    Undocumented,
  }


  public enum ServiceDay { 
    Wednesday,
    Saturday,
  }


}
