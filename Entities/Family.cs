﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pantry.Config;

namespace Pantry.Entities {

  public class FamilyData : ICloneable {
    public object Clone() {
      return this.MemberwiseClone();
    }

    public FamilyData() {
      this.FamilyMembers = new List<FamilyMember>();
      this.PickUpAssociates = new List<FamilyMember>();
      this.Count = 0;
    }

    public FamilyHead FamilyHead { get; set; }
    public List<FamilyMember> FamilyMembers { get; set; }
    public List<FamilyMember> PickUpAssociates { get; set; }
    public List<LastVisit> LastVisits { get; set; }
    public int Count { get; set; }
  }


  public class FamilyHead : ICloneable {
    public object Clone() {
      return this.MemberwiseClone();
    }
    
    public FamilyHead() {
      this.CreateDate = DateTime.Now.Date;
    }

    public int FamilyID { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Social { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string City { get; set; }
    public string State { get; set; }
    public string Zip { get; set; }
    public string Phone { get; set; }
    public string Cell { get; set; }
    public string Email { get; set; }
    public bool IsFoodStamps { get; set; }
    public string FoodStampsReason { get; set; }
    public DateTime Updated { get; set; }
    public DateTime CreateDate { get; set; }
    public DateTime LastVisitDate { get; set; }
    public string Year { get; set; }
    public VisitType VisitType { get; set; }
    public FamilyType FamilyType { get; set; }
    public string Interviewer { get; set; }
    public IdType IdType { get; set; }
    public string CriticalNote { get; set; }
  }


  public class FamilyMember : ICloneable {
    public object Clone() {
      return this.MemberwiseClone();
    }
    
    public FamilyMember() {
      this.FamilyRelationship = Entities.FamilyRelationship.Child;
      this.Year = "UKWN";
      this.IdType = Entities.IdType.UKN;
    }

    public FamilyMember(FamilyRelationship pickup) {
      this.FamilyRelationship = pickup;
      this.Year = "UKWN";
    }

    public int id { get; set; }
    public int FamilyID { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public FamilyRelationship FamilyRelationship { get; set; }
    public DateTime Updated { get; set; }
    public DateTime CreateDate { get; set; }
    public bool IsActive { get; set; }
    public string Year { get; set; }
    public string Social { get; set; }
    public IdType IdType { get; set; }
  }


  public class FamilyVisit {
    public FamilyVisit() { }

    public int id { get; set; }
    public int FamilyID { get; set; }
    public DateTime CreateDate { get; set; }
    public int NumberOfMembers { get; set; }
    public VisitType VisitType { get; set; }
    public ServiceLevel ServiceLevel { get; set; }
  }


  public enum FamilyRelationship {
    [Description("All")]
    All,

    [Description("Child")]
    Child,

    [Description("Spouse")]
    Spouse,

    [Description("Adult Child")]
    AdultChild,

    [Description("Grand Child")]
    GrandChild,

    [Description("Parent")]
    Parent,

    [Description("Grand Parent")]
    GrandParent,

    [Description("Aunt or Uncle")]
    AuntUncle,

    [Description("Niece or Nephew")]
    NieceNephew,

    [Description("Sister or Brother")]
    SisterBrother,

    [Description("Friend")]
    Friend,

    [Description("PickUp")]
    PickUp,
  
  }


  public enum IdType {
    [Description("ID Type")]
    NONE,
    [Description("Social Security")]
    SS,
    [Description("Drivers License")]
    DL,
    [Description("State ID")]
    SID,
    [Description("Birth Certificate")]
    BC,
    [Description("Self")]
    Self,
    [Description("Other")]
    OTR,
    [Description("Unknown")]
    UKN,
    [Description("Green Card")]
    GRNC,
    [Description("Bill")]
    BILL,
    [Description("Stil Required")]
    REQD,
    [Description("Credit Card")]
    CARD,
    [Description("Naturalization Card")]
    NAT,
    [Description("Pass Port")]
    PASS,
    [Description("Insurance Card")]
    INS,
    [Description("Mail with Address")]
    MAIL,

  }

}

