﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pantry.Entities {
  public class WebIcons {
    public enum WebApplicationIcons {

      [Description("")]
      noIcon,
      [Description("fa fa-adjust")]
      fa_adjust,
      [Description("fa fa-anchor")]
      fa_anchor,
      [Description("fa fa-archive")]
      fa_archive,
      [Description("fa fa-arrows")]
      fa_arrows,
      [Description("fa fa-arrows-h")]
      fa_arrows_h,
      [Description("fa fa-arrows-v")]
      fa_arrows_v,
      [Description("fa fa-asterisk")]
      fa_asterisk,
      [Description("fa fa-ban")]
      fa_ban,
      [Description("fa fa-bar-chart-o")]
      fa_bar_chart_o,
      [Description("fa fa-barcode")]
      fa_barcode,
      [Description("fa fa-bars")]
      fa_bars,
      [Description("fa fa-beer")]
      fa_beer,
      [Description("fa fa-bell")]
      fa_bell,
      [Description("fa fa-bell-o")]
      fa_bell_o,
      [Description("fa fa-bolt")]
      fa_bolt,
      [Description("fa fa-book")]
      fa_book,
      [Description("fa fa-bookmark")]
      fa_bookmark,
      [Description("fa fa-bookmark-o")]
      fa_bookmark_o,
      [Description("fa fa-briefcase")]
      fa_briefcase,
      [Description("fa fa-bug")]
      fa_bug,
      [Description("fa fa-building-o")]
      fa_building_o,
      [Description("fa fa-bullhorn")]
      fa_bullhorn,
      [Description("fa fa-bullseye")]
      fa_bullseye,
      [Description("fa fa-calendar")]
      fa_calendar,
      [Description("fa fa-calendar-o")]
      fa_calendar_o,
      [Description("fa fa-camera")]
      fa_camera,
      [Description("fa fa-camera-retro")]
      fa_camera_retro,
      [Description("fa fa-caret-square-o-down")]
      fa_caret_square_o_down,
      [Description("fa fa-caret-square-o-left")]
      fa_caret_square_o_left,
      [Description("fa fa-caret-square-o-right")]
      fa_caret_square_o_right,
      [Description("fa fa-caret-square-o-up")]
      fa_caret_square_o_up,
      [Description("fa fa-certificate")]
      fa_certificate,
      [Description("fa fa-check")]
      fa_check,
      [Description("fa fa-check-circle")]
      fa_check_circle,
      [Description("fa fa-check-circle-o")]
      fa_check_circle_o,
      [Description("fa fa-check-square")]
      fa_check_square,
      [Description("fa fa-check-square-o")]
      fa_check_square_o,
      [Description("fa fa-circle")]
      fa_circle,
      [Description("fa fa-circle-o")]
      fa_circle_o,
      [Description("fa fa-clock-o")]
      fa_clock_o,
      [Description("fa fa-cloud")]
      fa_cloud,
      [Description("fa fa-cloud-download")]
      fa_cloud_download,
      [Description("fa fa-cloud-upload")]
      fa_cloud_upload,
      [Description("fa fa-code")]
      fa_code,
      [Description("fa fa-code-fork")]
      fa_code_fork,
      [Description("fa fa-coffee")]
      fa_coffee,
      [Description("fa fa-cog")]
      fa_cog,
      [Description("fa fa-cogs")]
      fa_cogs,
      [Description("fa fa-comment")]
      fa_comment,
      [Description("fa fa-comment-o")]
      fa_comment_o,
      [Description("fa fa-comments")]
      fa_comments,
      [Description("fa fa-comments-o")]
      fa_comments_o,
      [Description("fa fa-compass")]
      fa_compass,
      [Description("fa fa-credit-card")]
      fa_credit_card,
      [Description("fa fa-crop")]
      fa_crop,
      [Description("fa fa-crosshairs")]
      fa_crosshairs,
      [Description("fa fa-cutlery")]
      fa_cutlery,
      [Description("fa fa-dashboard")]
      fa_dashboard,
      [Description("fa fa-desktop")]
      fa_desktop,
      [Description("fa fa-dot-circle-o")]
      fa_dot_circle_o,
      [Description("fa fa-download")]
      fa_download,
      [Description("fa fa-edit")]
      fa_edit,
      [Description("fa fa-ellipsis-h")]
      fa_ellipsis_h,
      [Description("fa fa-ellipsis-v")]
      fa_ellipsis_v,
      [Description("fa fa-envelope")]
      fa_envelope,
      [Description("fa fa-envelope-o")]
      fa_envelope_o,
      [Description("fa fa-eraser")]
      fa_eraser,
      [Description("fa fa-exchange")]
      fa_exchange,
      [Description("fa fa-exclamation")]
      fa_exclamation,
      [Description("fa fa-exclamation-circle")]
      fa_exclamation_circle,
      [Description("fa fa-exclamation-triangle")]
      fa_exclamation_triangle,
      [Description("fa fa-external-link")]
      fa_external_link,
      [Description("fa fa-external-link-square")]
      fa_external_link_square,
      [Description("fa fa-eye")]
      fa_eye,
      [Description("fa fa-eye-slash")]
      fa_eye_slash,
      [Description("fa fa-female")]
      fa_female,
      [Description("fa fa-fighter-jet")]
      fa_fighter_jet,
      [Description("fa fa-film")]
      fa_film,
      [Description("fa fa-filter")]
      fa_filter,
      [Description("fa fa-fire")]
      fa_fire,
      [Description("fa fa-fire-extinguisher")]
      fa_fire_extinguisher,
      [Description("fa fa-flag")]
      fa_flag,
      [Description("fa fa-flag-checkered")]
      fa_flag_checkered,
      [Description("fa fa-flag-o")]
      fa_flag_o,
      [Description("fa fa-flash")]
      fa_flash,
      [Description("fa fa-flask")]
      fa_flask,
      [Description("fa fa-folder")]
      fa_folder,
      [Description("fa fa-folder-o")]
      fa_folder_o,
      [Description("fa fa-folder-open")]
      fa_folder_open,
      [Description("fa fa-folder-open-o")]
      fa_folder_open_o,
      [Description("fa fa-frown-o")]
      fa_frown_o,
      [Description("fa fa-gamepad")]
      fa_gamepad,
      [Description("fa fa-gavel")]
      fa_gavel,
      [Description("fa fa-gear")]
      fa_gear,
      [Description("fa fa-gears")]
      fa_gears,
      [Description("fa fa-gift")]
      fa_gift,
      [Description("fa fa-glass")]
      fa_glass,
      [Description("fa fa-globe")]
      fa_globe,
      [Description("fa fa-group")]
      fa_group,
      [Description("fa fa-hdd-o")]
      fa_hdd_o,
      [Description("fa fa-headphones")]
      fa_headphones,
      [Description("fa fa-heart")]
      fa_heart,
      [Description("fa fa-heart-o")]
      fa_heart_o,
      [Description("fa fa-home")]
      fa_home,
      [Description("fa fa-inbox")]
      fa_inbox,
      [Description("fa fa-info")]
      fa_info,
      [Description("fa fa-info-circle")]
      fa_info_circle,
      [Description("fa fa-key")]
      fa_key,
      [Description("fa fa-keyboard-o")]
      fa_keyboard_o,
      [Description("fa fa-laptop")]
      fa_laptop,
      [Description("fa fa-leaf")]
      fa_leaf,
      [Description("fa fa-legal")]
      fa_legal,
      [Description("fa fa-lemon-o")]
      fa_lemon_o,
      [Description("fa fa-level-down")]
      fa_level_down,
      [Description("fa fa-level-up")]
      fa_level_up,
      [Description("fa fa-lightbulb-o")]
      fa_lightbulb_o,
      [Description("fa fa-location-arrow")]
      fa_location_arrow,
      [Description("fa fa-lock")]
      fa_lock,
      [Description("fa fa-magic")]
      fa_magic,
      [Description("fa fa-magnet")]
      fa_magnet,
      [Description("fa fa-mail-forward")]
      fa_mail_forward,
      [Description("fa fa-mail-reply")]
      fa_mail_reply,
      [Description("fa fa-mail-reply-all")]
      fa_mail_reply_all,
      [Description("fa fa-male")]
      fa_male,
      [Description("fa fa-map-marker")]
      fa_map_marker,
      [Description("fa fa-meh-o")]
      fa_meh_o,
      [Description("fa fa-microphone")]
      fa_microphone,
      [Description("fa fa-microphone-slash")]
      fa_microphone_slash,
      [Description("fa fa-minus")]
      fa_minus,
      [Description("fa fa-minus-circle")]
      fa_minus_circle,
      [Description("fa fa-minus-square")]
      fa_minus_square,
      [Description("fa fa-minus-square-o")]
      fa_minus_square_o,
      [Description("fa fa-mobile")]
      fa_mobile,
      [Description("fa fa-mobile-phone")]
      fa_mobile_phone,
      [Description("fa fa-money")]
      fa_money,
      [Description("fa fa-moon-o")]
      fa_moon_o,
      [Description("fa fa-music")]
      fa_music,
      [Description("fa fa-pencil")]
      fa_pencil,
      [Description("fa fa-pencil-square")]
      fa_pencil_square,
      [Description("fa fa-pencil-square-o")]
      fa_pencil_square_o,
      [Description("fa fa-phone")]
      fa_phone,
      [Description("fa fa-phone-square")]
      fa_phone_square,
      [Description("fa fa-picture-o")]
      fa_picture_o,
      [Description("fa fa-plane")]
      fa_plane,
      [Description("fa fa-plus")]
      fa_plus,
      [Description("fa fa-plus-circle")]
      fa_plus_circle,
      [Description("fa fa-plus-square")]
      fa_plus_square,
      [Description("fa fa-plus-square-o")]
      fa_plus_square_o,
      [Description("fa fa-power-off")]
      fa_power_off,
      [Description("fa fa-print")]
      fa_print,
      [Description("fa fa-puzzle-piece")]
      fa_puzzle_piece,
      [Description("fa fa-qrcode")]
      fa_qrcode,
      [Description("fa fa-question")]
      fa_question,
      [Description("fa fa-question-circle")]
      fa_question_circle,
      [Description("fa fa-quote-left")]
      fa_quote_left,
      [Description("fa fa-quote-right")]
      fa_quote_right,
      [Description("fa fa-random")]
      fa_random,
      [Description("fa fa-refresh")]
      fa_refresh,
      [Description("fa fa-reply")]
      fa_reply,
      [Description("fa fa-reply-all")]
      fa_reply_all,
      [Description("fa fa-retweet")]
      fa_retweet,
      [Description("fa fa-road")]
      fa_road,
      [Description("fa fa-rocket")]
      fa_rocket,
      [Description("fa fa-rss")]
      fa_rss,
      [Description("fa fa-rss-square")]
      fa_rss_square,
      [Description("fa fa-search")]
      fa_search,
      [Description("fa fa-search-minus")]
      fa_search_minus,
      [Description("fa fa-search-plus")]
      fa_search_plus,
      [Description("fa fa-share")]
      fa_share,
      [Description("fa fa-share-square")]
      fa_share_square,
      [Description("fa fa-share-square-o")]
      fa_share_square_o,
      [Description("fa fa-shield")]
      fa_shield,
      [Description("fa fa-shopping-cart")]
      fa_shopping_cart,
      [Description("fa fa-sign-in")]
      fa_sign_in,
      [Description("fa fa-sign-out")]
      fa_sign_out,
      [Description("fa fa-signal")]
      fa_signal,
      [Description("fa fa-sitemap")]
      fa_sitemap,
      [Description("fa fa-smile-o")]
      fa_smile_o,
      [Description("fa fa-sort")]
      fa_sort,
      [Description("fa fa-sort-alpha-asc")]
      fa_sort_alpha_asc,
      [Description("fa fa-sort-alpha-desc")]
      fa_sort_alpha_desc,
      [Description("fa fa-sort-amount-asc")]
      fa_sort_amount_asc,
      [Description("fa fa-sort-amount-desc")]
      fa_sort_amount_desc,
      [Description("fa fa-sort-asc")]
      fa_sort_asc,
      [Description("fa fa-sort-desc")]
      fa_sort_desc,
      [Description("fa fa-sort-down")]
      fa_sort_down,
      [Description("fa fa-sort-numeric-asc")]
      fa_sort_numeric_asc,
      [Description("fa fa-sort-numeric-desc")]
      fa_sort_numeric_desc,
      [Description("fa fa-sort-up")]
      fa_sort_up,
      [Description("fa fa-spinner")]
      fa_spinner,
      [Description("fa fa-square")]
      fa_square,
      [Description("fa fa-square-o")]
      fa_square_o,
      [Description("fa fa-star")]
      fa_star,
      [Description("fa fa-star-half")]
      fa_star_half,
      [Description("fa fa-star-half-empty")]
      fa_star_half_empty,
      [Description("fa fa-star-half-full")]
      fa_star_half_full,
      [Description("fa fa-star-half-o")]
      fa_star_half_o,
      [Description("fa fa-star-o")]
      fa_star_o,
      [Description("fa fa-subscript")]
      fa_subscript,
      [Description("fa fa-suitcase")]
      fa_suitcase,
      [Description("fa fa-sun-o")]
      fa_sun_o,
      [Description("fa fa-superscript")]
      fa_superscript,
      [Description("fa fa-tablet")]
      fa_tablet,
      [Description("fa fa-tachometer")]
      fa_tachometer,
      [Description("fa fa-tag")]
      fa_tag,
      [Description("fa fa-tags")]
      fa_tags,
      [Description("fa fa-tasks")]
      fa_tasks,
      [Description("fa fa-terminal")]
      fa_terminal,
      [Description("fa fa-thumb-tack")]
      fa_thumb_tack,
      [Description("fa fa-thumbs-down")]
      fa_thumbs_down,
      [Description("fa fa-thumbs-o-down")]
      fa_thumbs_o_down,
      [Description("fa fa-thumbs-o-up")]
      fa_thumbs_o_up,
      [Description("fa fa-thumbs-up")]
      fa_thumbs_up,
      [Description("fa fa-ticket")]
      fa_ticket,
      [Description("fa fa-times")]
      fa_times,
      [Description("fa fa-times-circle")]
      fa_times_circle,
      [Description("fa fa-times-circle-o")]
      fa_times_circle_o,
      [Description("fa fa-tint")]
      fa_tint,
      [Description("fa fa-toggle-down")]
      fa_toggle_down,
      [Description("fa fa-toggle-left")]
      fa_toggle_left,
      [Description("fa fa-toggle-right")]
      fa_toggle_right,
      [Description("fa fa-toggle-up")]
      fa_toggle_up,
      [Description("fa fa-trash-o")]
      fa_trash_o,
      [Description("fa fa-trophy")]
      fa_trophy,
      [Description("fa fa-truck")]
      fa_truck,
      [Description("fa fa-umbrella")]
      fa_umbrella,
      [Description("fa fa-unlock")]
      fa_unlock,
      [Description("fa fa-unlock-alt")]
      fa_unlock_alt,
      [Description("fa fa-unsorted")]
      fa_unsorted,
      [Description("fa fa-upload")]
      fa_upload,
      [Description("fa fa-user")]
      fa_user,
      [Description("fa fa-users")]
      fa_users,
      [Description("fa fa-video-camera")]
      fa_video_camera,
      [Description("fa fa-volume-down")]
      fa_volume_down,
      [Description("fa fa-volume-off")]
      fa_volume_off,
      [Description("fa fa-volume-up")]
      fa_volume_up,
      [Description("fa fa-warning")]
      fa_warning,
      [Description("fa fa-wheelchair")]
      fa_wheelchair,
      [Description("fa fa-wrench")]
      fa_wrench,
    }

    public enum FormControlIcons {
      [Description("fa fa-check-square")]
      fa_check_square,
      [Description("fa fa-check-square-o")]
      fa_check_square_o,
      [Description("fa fa-circle")]
      fa_circle,
      [Description("fa fa-circle-o")]
      fa_circle_o,
      [Description("fa fa-dot-circle-o")]
      fa_dot_circle_o,
      [Description("fa fa-minus-square")]
      fa_minus_square,
      [Description("fa fa-minus-square-o")]
      fa_minus_square_o,
      [Description("fa fa-plus-square")]
      fa_plus_square,
      [Description("fa fa-plus-square-o")]
      fa_plus_square_o,
      [Description("fa fa-square")]
      fa_square,
      [Description("fa fa-square-o")]
      fa_square_o,
    }

    public enum CurrencyIcons {
      [Description("fa fa-bitcoin")]
      fa_bitcoin,
      [Description("fa fa-btc")]
      fa_btc,
      [Description("fa fa-cny")]
      fa_cny,
      [Description("fa fa-dollar")]
      fa_dollar,
      [Description("fa fa-eur")]
      fa_eur,
      [Description("fa fa-euro")]
      fa_euro,
      [Description("fa fa-gbp")]
      fa_gbp,
      [Description("fa fa-inr")]
      fa_inr,
      [Description("fa fa-jpy")]
      fa_jpy,
      [Description("fa fa-krw")]
      fa_krw,
      [Description("fa fa-money")]
      fa_money,
      [Description("fa fa-rmb")]
      fa_rmb,
      [Description("fa fa-rouble")]
      fa_rouble,
      [Description("fa fa-rub")]
      fa_rub,
      [Description("fa fa-ruble")]
      fa_ruble,
      [Description("fa fa-rupee")]
      fa_rupee,
      [Description("fa fa-try")]
      fa_try,
      [Description("fa fa-turkish-lira")]
      fa_turkish_lira,
      [Description("fa fa-usd")]
      fa_usd,
      [Description("fa fa-won")]
      fa_won,
      [Description("fa fa-yen")]
      fa_yen,
    }

    public enum TextEditorIcons {
      [Description("fa fa-align-center")]
      fa_align_center,
      [Description("fa fa-align-justify")]
      fa_align_justify,
      [Description("fa fa-align-left")]
      fa_align_left,
      [Description("fa fa-align-right")]
      fa_align_right,
      [Description("fa fa-bold")]
      fa_bold,
      [Description("fa fa-chain")]
      fa_chain,
      [Description("fa fa-chain-broken")]
      fa_chain_broken,
      [Description("fa fa-clipboard")]
      fa_clipboard,
      [Description("fa fa-columns")]
      fa_columns,
      [Description("fa fa-copy")]
      fa_copy,
      [Description("fa fa-cut")]
      fa_cut,
      [Description("fa fa-dedent")]
      fa_dedent,
      [Description("fa fa-eraser")]
      fa_eraser,
      [Description("fa fa-file")]
      fa_file,
      [Description("fa fa-file-o")]
      fa_file_o,
      [Description("fa fa-file-text")]
      fa_file_text,
      [Description("fa fa-file-text-o")]
      fa_file_text_o,
      [Description("fa fa-files-o")]
      fa_files_o,
      [Description("fa fa-floppy-o")]
      fa_floppy_o,
      [Description("fa fa-font")]
      fa_font,
      [Description("fa fa-indent")]
      fa_indent,
      [Description("fa fa-italic")]
      fa_italic,
      [Description("fa fa-link")]
      fa_link,
      [Description("fa fa-list")]
      fa_list,
      [Description("fa fa-list-alt")]
      fa_list_alt,
      [Description("fa fa-list-ol")]
      fa_list_ol,
      [Description("fa fa-list-ul")]
      fa_list_ul,
      [Description("fa fa-outdent")]
      fa_outdent,
      [Description("fa fa-paperclip")]
      fa_paperclip,
      [Description("fa fa-paste")]
      fa_paste,
      [Description("fa fa-repeat")]
      fa_repeat,
      [Description("fa fa-rotate-left")]
      fa_rotate_left,
      [Description("fa fa-rotate-right")]
      fa_rotate_right,
      [Description("fa fa-save")]
      fa_save,
      [Description("fa fa-scissors")]
      fa_scissors,
      [Description("fa fa-strikethrough")]
      fa_strikethrough,
      [Description("fa fa-table")]
      fa_table,
      [Description("fa fa-text-height")]
      fa_text_height,
      [Description("fa fa-text-width")]
      fa_text_width,
      [Description("fa fa-th")]
      fa_th,
      [Description("fa fa-th-large")]
      fa_th_large,
      [Description("fa fa-th-list")]
      fa_th_list,
      [Description("fa fa-underline")]
      fa_underline,
      [Description("fa fa-undo")]
      fa_undo,
      [Description("fa fa-unlink")]
      fa_unlink,
    }

    public enum DirectionalIcons {
      [Description("fa fa-angle-double-down")]
      fa_angle_double_down,
      [Description("fa fa-angle-double-left")]
      fa_angle_double_left,
      [Description("fa fa-angle-double-right")]
      fa_angle_double_right,
      [Description("fa fa-angle-double-up")]
      fa_angle_double_up,
      [Description("fa fa-angle-down")]
      fa_angle_down,
      [Description("fa fa-angle-left")]
      fa_angle_left,
      [Description("fa fa-angle-right")]
      fa_angle_right,
      [Description("fa fa-angle-up")]
      fa_angle_up,
      [Description("fa fa-arrow-circle-down")]
      fa_arrow_circle_down,
      [Description("fa fa-arrow-circle-left")]
      fa_arrow_circle_left,
      [Description("fa fa-arrow-circle-o-down")]
      fa_arrow_circle_o_down,
      [Description("fa fa-arrow-circle-o-left")]
      fa_arrow_circle_o_left,
      [Description("fa fa-arrow-circle-o-right")]
      fa_arrow_circle_o_right,
      [Description("fa fa-arrow-circle-o-up")]
      fa_arrow_circle_o_up,
      [Description("fa fa-arrow-circle-right")]
      fa_arrow_circle_right,
      [Description("fa fa-arrow-circle-up")]
      fa_arrow_circle_up,
      [Description("fa fa-arrow-down")]
      fa_arrow_down,
      [Description("fa fa-arrow-left")]
      fa_arrow_left,
      [Description("fa fa-arrow-right")]
      fa_arrow_right,
      [Description("fa fa-arrow-up")]
      fa_arrow_up,
      [Description("fa fa-arrows")]
      fa_arrows,
      [Description("fa fa-arrows-alt")]
      fa_arrows_alt,
      [Description("fa fa-arrows-h")]
      fa_arrows_h,
      [Description("fa fa-arrows-v")]
      fa_arrows_v,
      [Description("fa fa-caret-down")]
      fa_caret_down,
      [Description("fa fa-caret-left")]
      fa_caret_left,
      [Description("fa fa-caret-right")]
      fa_caret_right,
      [Description("fa fa-caret-square-o-down")]
      fa_caret_square_o_down,
      [Description("fa fa-caret-square-o-left")]
      fa_caret_square_o_left,
      [Description("fa fa-caret-square-o-right")]
      fa_caret_square_o_right,
      [Description("fa fa-caret-square-o-up")]
      fa_caret_square_o_up,
      [Description("fa fa-caret-up")]
      fa_caret_up,
      [Description("fa fa-chevron-circle-down")]
      fa_chevron_circle_down,
      [Description("fa fa-chevron-circle-left")]
      fa_chevron_circle_left,
      [Description("fa fa-chevron-circle-right")]
      fa_chevron_circle_right,
      [Description("fa fa-chevron-circle-up")]
      fa_chevron_circle_up,
      [Description("fa fa-chevron-down")]
      fa_chevron_down,
      [Description("fa fa-chevron-left")]
      fa_chevron_left,
      [Description("fa fa-chevron-right")]
      fa_chevron_right,
      [Description("fa fa-chevron-up")]
      fa_chevron_up,
      [Description("fa fa-hand-o-down")]
      fa_hand_o_down,
      [Description("fa fa-hand-o-left")]
      fa_hand_o_left,
      [Description("fa fa-hand-o-right")]
      fa_hand_o_right,
      [Description("fa fa-hand-o-up")]
      fa_hand_o_up,
      [Description("fa fa-long-arrow-down")]
      fa_long_arrow_down,
      [Description("fa fa-long-arrow-left")]
      fa_long_arrow_left,
      [Description("fa fa-long-arrow-right")]
      fa_long_arrow_right,
      [Description("fa fa-long-arrow-up")]
      fa_long_arrow_up,
      [Description("fa fa-toggle-down")]
      fa_toggle_down,
      [Description("fa fa-toggle-left")]
      fa_toggle_left,
      [Description("fa fa-toggle-right")]
      fa_toggle_right,
      [Description("fa fa-toggle-up")]
      fa_toggle_up,
    }

    public enum VideoPlayerIcons {
      [Description("fa fa-arrows-alt")]
      fa_arrows_alt,
      [Description("fa fa-backward")]
      fa_backward,
      [Description("fa fa-compress")]
      fa_compress,
      [Description("fa fa-eject")]
      fa_eject,
      [Description("fa fa-expand")]
      fa_expand,
      [Description("fa fa-fast-backward")]
      fa_fast_backward,
      [Description("fa fa-fast-forward")]
      fa_fast_forward,
      [Description("fa fa-forward")]
      fa_forward,
      [Description("fa fa-pause")]
      fa_pause,
      [Description("fa fa-play")]
      fa_play,
      [Description("fa fa-play-circle")]
      fa_play_circle,
      [Description("fa fa-play-circle-o")]
      fa_play_circle_o,
      [Description("fa fa-step-backward")]
      fa_step_backward,
      [Description("fa fa-step-forward")]
      fa_step_forward,
      [Description("fa fa-stop")]
      fa_stop,
      [Description("fa fa-youtube-play")]
      fa_youtube_play,
    }

    public enum BrandIcons {

      [Description("fa fa-adn")]
      fa_adn,
      [Description("fa fa-android")]
      fa_android,
      [Description("fa fa-apple")]
      fa_apple,
      [Description("fa fa-bitbucket")]
      fa_bitbucket,
      [Description("fa fa-bitbucket-square")]
      fa_bitbucket_square,
      [Description("fa fa-bitcoin")]
      fa_bitcoin,
      [Description("fa fa-btc")]
      fa_btc,
      [Description("fa fa-css3")]
      fa_css3,
      [Description("fa fa-dribbble")]
      fa_dribbble,
      [Description("fa fa-dropbox")]
      fa_dropbox,
      [Description("fa fa-facebook")]
      fa_facebook,
      [Description("fa fa-facebook-square")]
      fa_facebook_square,
      [Description("fa fa-flickr")]
      fa_flickr,
      [Description("fa fa-foursquare")]
      fa_foursquare,
      [Description("fa fa-github")]
      fa_github,
      [Description("fa fa-github-alt")]
      fa_github_alt,
      [Description("fa fa-github-square")]
      fa_github_square,
      [Description("fa fa-gittip")]
      fa_gittip,
      [Description("fa fa-google-plus")]
      fa_google_plus,
      [Description("fa fa-google-plus-square")]
      fa_google_plus_square,
      [Description("fa fa-html5")]
      fa_html5,
      [Description("fa fa-instagram")]
      fa_instagram,
      [Description("fa fa-linkedin")]
      fa_linkedin,
      [Description("fa fa-linkedin-square")]
      fa_linkedin_square,
      [Description("fa fa-linux")]
      fa_linux,
      [Description("fa fa-maxcdn")]
      fa_maxcdn,
      [Description("fa fa-pagelines")]
      fa_pagelines,
      [Description("fa fa-pinterest")]
      fa_pinterest,
      [Description("fa fa-pinterest-square")]
      fa_pinterest_square,
      [Description("fa fa-renren")]
      fa_renren,
      [Description("fa fa-skype")]
      fa_skype,
      [Description("fa fa-stack-exchange")]
      fa_stack_exchange,
      [Description("fa fa-stack-overflow")]
      fa_stack_overflow,
      [Description("fa fa-trello")]
      fa_trello,
      [Description("fa fa-tumblr")]
      fa_tumblr,
      [Description("fa fa-tumblr-square")]
      fa_tumblr_square,
      [Description("fa fa-twitter")]
      fa_twitter,
      [Description("fa fa-twitter-square")]
      fa_twitter_square,
      [Description("fa fa-vimeo-square")]
      fa_vimeo_square,
      [Description("fa fa-vk")]
      fa_vk,
      [Description("fa fa-weibo")]
      fa_weibo,
      [Description("fa fa-windows")]
      fa_windows,
      [Description("fa fa-xing")]
      fa_xing,
      [Description("fa fa-xing-square")]
      fa_xing_square,
      [Description("fa fa-youtube")]
      fa_youtube,
      [Description("fa fa-youtube-play")]
      fa_youtube_play,
      [Description("fa fa-youtube-square")]
      fa_youtube_square,
    }

    public enum MedicalIcons {
      [Description("fa fa-ambulance")]
      fa_ambulance,
      [Description("fa fa-h-square")]
      fa_h_square,
      [Description("fa fa-hospital-o")]
      fa_hospital_o,
      [Description("fa fa-medkit")]
      fa_medkit,
      [Description("fa fa-plus-square")]
      fa_plus_square,
      [Description("fa fa-stethoscope")]
      fa_stethoscope,
      [Description("fa fa-user-md")]
      fa_user_md,
      [Description("fa fa-wheelchair")]
      fa_wheelchair,
    }

  }
}
