﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pantry.Config;


namespace Pantry.Entities {
  public class LastVisit {
    public LastVisit() {
    }

    public int PantryID { get; set; }
    public int FamilyID { get; set; }
    public DateTime LastVisitDate { get; set; }
    public VisitType VisitType { get; set; }
    public ServiceLevel ServiceLevel { get; set; }
    public int NumberOfMembers { get; set; }

  }

}
