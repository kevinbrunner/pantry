﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pantry.Entities {
  public class Note {
    public Note() { }

    public int id { get; set; }
    public int FamilyID { get; set; }
    public DateTime CreateDate { get; set; }
    public string EnteredBy { get; set; }
    public string Title { get; set; }
    public string Body { get; set; }
    public string TransferredTo { get; set; }
    public DateTime TransferDate { get; set; }
    public NoteType NoteType { get; set; }

  }


  public enum NoteType {
    All,
    Normal,
    Critical,
    Spiritual,
  }


}
