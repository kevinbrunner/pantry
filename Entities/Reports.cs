﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pantry.Config;


namespace Pantry.Entities {

  public class DailyDetailedReport {
    public DailyDetailedReport() {
    }

    public string Name { get; set; }
    public int NumberOfMembers { get; set; }
    public DateTime CreateDate { get; set; }
    public VisitType VisitType { get; set; }
    public ServiceLevel ServiceLevel { get; set; }

  }


}
