﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pantry.Entities {
  public class SearchData {
    public SearchData() { }

    public int FamilyID { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Social { get; set; }
    public string Address1 { get; set; }
    public string City { get; set; }
    public string Zip { get; set; }
    public string Phone { get; set; }
    public string LastVisit { get; set; }
    
  }


}
