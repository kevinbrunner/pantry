﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pantry.Entities {
  public class SearchItem {
    public SearchItem() { }

    public int id { get; set; }
    public int FamilyID { get; set; }
    public string Term { get; set; }

  }
}
