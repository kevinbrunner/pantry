﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Pantry.Entities {
  public enum RandomStringType {
    Alphanumeric,
    AlphaOnly,
    NumericOnly,
    CouponSafe,
    AlphanumericAndSymbols
  }

}
