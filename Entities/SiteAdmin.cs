﻿using System;
using System.Collections.Generic;
using System.ComponentModel;


namespace Pantry.Entities {

  public class SiteAdmin {
    public SiteAdmin() { }

    public SiteAdmin(string email, string userName) {
      this.Email = email;
      this.UserName = userName;
    }

    //public SiteAdmin(string email, string userName, List<string> roles, List<string> roleDescriptions) {
    //  this.Email = email;
    //  this.UserName = userName;
    //  this.Roles = roles;
    //  this.RoleDescriptions = roleDescriptions;
    //}

    public string Email { get; set; }
    public string UserName { get; set; }
    public List<string> Roles { get; set; }
    public List<string> RoleDescriptions { get; set; }
    public List<AdminRole> AdminRoleList { get; set; }
    public List<AdminHasRole> RoleList { get; set; }
    public int ID { get; set; }

  }


  public class SiteAdminWithRole : SiteAdmin {
    public SiteAdminWithRole() { }

    public string RoleName { get; set; }

  }


  public class AdminHasRole {
    public AdminHasRole() { }

    public AdminHasRole(AdminRole adminRole, string roleDescription, bool isTrue) {
      this.RoleName = adminRole.ToString();
      this.Role = adminRole;
      this.IsInRole = isTrue;
      this.IsInRoleInit = isTrue;
      this.RoleDescription = roleDescription;
    }

    public string RoleName { get; set; }
    public string RoleDescription { get; set; }
    public AdminRole Role { get; set; }
    public bool IsInRole { get; set; }
    public bool IsInRoleInit { get; set; }

  }


  public enum AdminRole {
    [Description("Super Admin")]
    SuperAdmin,

    [Description("Admin")]
    Admin,

    [Description("Major Admin")]
    MajorAdmin,

    [Description("Basic")]
    Basic,
  }


  public enum PasswordStrength {
    Fail,
    Poor,
    Okay,
    Good,
    Better
  }

}
