﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pantry.Entities {
  public class VisitCounts {

    public VisitCounts() { }

    public VisitCounts(DateTime startDate, DateTime endDate) {
      this.StartDate = startDate;
      this.EndDate = endDate;
    }

    public int FamiliesCount { get; set; }
    public int FamilyMembersCount { get; set; }

    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }


  }


}
