﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pantry.Config;


namespace Pantry.Entities {

  public class VisitLog {
    public VisitLog() {
    }

    public int PantryID { get; set; }
    public int FamilyID { get; set; }
    public DateTime CreateDate { get; set; }
    public int NumberOfMembers { get; set; }
    public VisitType VisitType { get; set; }
    public ServiceLevel ServiceLevel { get; set; }

  }


}
