﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Xml;
using System.Xml.Linq;

using Amazon.CloudFront;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
//using Amazon.S3.Util;
using Amazon.S3.IO;

using Pantry.Config;
using Pantry.Entities;
using Pantry.Helpers;
using Pantry.Utilities;
using Amazon.S3;

namespace Pantry.Helpers {

  public class AmazonS3Helper {

    private static string ConstructPolicy(string bucket, string expirationDate, string acl, string key, string redirect, string contentType) {
      StringBuilder policy = new StringBuilder();

      policy.Append("{");
      policy.Append("\"expiration\":\"");
      policy.Append(expirationDate);
      policy.Append("\",");
      policy.Append("\"conditions\":[");
      policy.Append("{\"bucket\":\"");
      policy.Append(bucket);
      policy.Append("\"}");
      policy.Append(",{\"acl\":\"");
      policy.Append(acl);
      policy.Append("\"}");
      policy.Append(",[\"eq\", \"$key\", \"");
      policy.Append(key);
      policy.Append("\"]");
      policy.Append(",[\"starts-with\", \"$Content-Type\", \"" + contentType + "\"]");
      if (!String.IsNullOrEmpty(redirect)) {
        policy.Append(",{\"redirect\":\"");
        policy.Append(redirect);
        policy.Append("\"}");
      }

      policy.Append("]");
      policy.Append("}");

      return Convert.ToBase64String(Encoding.UTF8.GetBytes(policy.ToString()));
    }


    public static string CreateSignature(string UnsignedPolicy) {
      HMACSHA1 sigHash = new HMACSHA1(System.Text.Encoding.UTF8.GetBytes(AWSConfig.Secret));
      byte[] bytPolicy = System.Text.Encoding.UTF8.GetBytes(UnsignedPolicy);
      byte[] bytSignature = sigHash.ComputeHash(bytPolicy);

      return Convert.ToBase64String(bytSignature);
    }


    public static AmazonUploadParameters AwsUploadParameters(
      string s3Folder
      , string s3FileName
      , string s3FileExtn
      , string redirectUrl
      , string bucketName
      , string accessMode
      , int minutes = 3600
    ) {

      if (minutes < 1) { minutes = 60; } // this "60" should be some config value!!!

      if (s3FileExtn.IsEmpty()) {
        s3FileExtn = s3FileName.Substring(s3FileName.LastIndexOf("."));
      }

      if (!s3Folder.EndsWith("/")) {
        s3Folder = s3Folder + "/";
      }

      string key = s3Folder + new FileInfo(s3FileName).Name;
      if (!key.EndsWith("." + s3FileExtn)) {
        key = key + "." + s3FileExtn;
      }

      string contentType = Helpers.ContentMimeType.MimeType(s3FileExtn);

      accessMode = accessMode.ToLower();
      if (!(
        accessMode == AwsAccessMode.Private.ToLower()
        || accessMode == AwsAccessMode.PublicRead.ToLower()
        || accessMode == "Public-Read".ToLower()
        || accessMode == AwsAccessMode.AuthenticatedRead.ToLower()
        || accessMode == AwsAccessMode.PublicReadWrite.ToLower()
      )) {
        accessMode = AwsAccessMode.Private.ToLower();
      } // Amazon.S3.Model.S3CannedACL does not work here...

      string policy = AmazonS3Helper.ConstructPolicy(
        bucketName
        , (DateTime.UtcNow.Add(new TimeSpan(days: 0, hours: 0, minutes: minutes, seconds: 0))).ToString("s") + ".000Z"
        , accessMode
        , key
        , redirectUrl
        , contentType
      );

      string signature = AmazonS3Helper.CreateSignature(policy);

      AmazonUploadParameters awsParams = new AmazonUploadParameters();

      awsParams.key = key;
      awsParams.acl = accessMode;
      awsParams.contentType = contentType;
      awsParams.AWSAccessKeyId = AWSConfig.Key;
      awsParams.policy = policy;
      awsParams.signature = signature;
      awsParams.redirect = redirectUrl;
      awsParams.bucket = "http://" + bucketName;

      return awsParams;
    }


    public static string cURLCommand(
      string s3Folder
      , string s3FileName
      , string s3FileExtn
      , string redirectUrl
      , string bucketName
      , string accessMode
      , int minutes = 3600
      , string localFileAndPath = ""
    ) {

      AmazonUploadParameters awsParams = AwsUploadParameters(
        s3Folder
        , s3FileName
        , s3FileExtn
        , redirectUrl
        , bucketName
        , accessMode
        , minutes
      );

      StringBuilder cURL = new StringBuilder();
      cURL.Append("curl -L -v ");

      //cURL.Append("-X POST ");

      cURL.Append(" -F \"key=");
      cURL.Append(awsParams.key);
      cURL.Append("\" ");

      cURL.Append(" -F \"acl=");
      cURL.Append(accessMode);
      cURL.Append("\" ");

      cURL.Append(" -F \"content-type=");
      cURL.Append(awsParams.contentType);
      cURL.Append("\" ");

      cURL.Append(" -F \"AWSAccessKeyId=");
      cURL.Append(awsParams.AWSAccessKeyId);
      cURL.Append("\" ");

      cURL.Append(" -F \"policy=");
      cURL.Append(awsParams.policy);
      cURL.Append("\" ");

      cURL.Append(" -F \"signature=");
      cURL.Append(awsParams.signature);
      cURL.Append("\" ");

      if (!awsParams.redirect.IsEmpty()) {
        cURL.Append(" -F \"redirect=");
        cURL.Append(awsParams.redirect);
        cURL.Append("\" ");
      }

      //cURL.Append(" -F \"bucket=");
      //cURL.Append(bucketName);
      //cURL.Append("\" ");

      if (!localFileAndPath.IsEmpty()) {
        cURL.Append(" -F \"file=@");
        cURL.Append(localFileAndPath);
        cURL.Append("\" ");
      }

      cURL.Append("http://" + bucketName);

      return cURL.ToString();
    }


    private static List<XElement> CreateHtmlFormInputsXElements(
      string s3Folder
      , string s3FileName
      , string s3FileExtn
      , string redirectUrl
      , string bucketName
      , string accessMode
      , int minutes = 60
    ) {
      AmazonUploadParameters awsParams = new AmazonUploadParameters(s3Folder, s3FileName, s3FileExtn, redirectUrl, bucketName, accessMode, minutes);

      List<XElement> formElements = new List<XElement>();

      formElements.Add(new XElement("input", null, new XAttribute("type", "hidden"), new XAttribute("name", "key"), new XAttribute("value", awsParams.key)));
      formElements.Add(new XElement("input", null, new XAttribute("type", "hidden"), new XAttribute("name", "acl"), new XAttribute("value", awsParams.acl)));
      formElements.Add(new XElement("input", null, new XAttribute("type", "hidden"), new XAttribute("name", "content-type"), new XAttribute("value", awsParams.contentType)));
      formElements.Add(new XElement("input", null, new XAttribute("type", "hidden"), new XAttribute("name", "AWSAccessKeyId"), new XAttribute("value", awsParams.AWSAccessKeyId)));
      formElements.Add(new XElement("input", null, new XAttribute("type", "hidden"), new XAttribute("name", "policy"), new XAttribute("value", awsParams.policy)));
      formElements.Add(new XElement("input", null, new XAttribute("type", "hidden"), new XAttribute("name", "signature"), new XAttribute("value", awsParams.signature)));

      if (redirectUrl != "") {
        formElements.Add(new XElement("input", null, new XAttribute("type", "hidden"), new XAttribute("name", "redirect"), new XAttribute("value", awsParams.redirect)));
      }

      return formElements;
    }


    public static XElement CreateHtmlForm(
      string s3Folder
      , string s3FileName
      , string s3FileExtn
      , string redirectUrl
      , string bucketName
      , string accessMode
      , int minutes = 60
    ) {
      if (minutes < 1) { minutes = 60; } // 60 should be some config value!!!

      if (!s3Folder.EndsWith("/")) {
        s3Folder = s3Folder + "/";
      }

      XElement htmlForm = new XElement(
          "form"
          , new XAttribute("action", "http://s3.amazonaws.com/" + bucketName)
          , new XAttribute("method", "post")
          , new XAttribute("enctype", "multipart/form-data")
        );

      foreach (XElement x in CreateHtmlFormInputsXElements(s3Folder, s3FileName, s3FileExtn, redirectUrl, bucketName, accessMode, minutes)) {
        htmlForm.Add(x);
      }

      htmlForm.Add(new XElement("input", null, new XAttribute("name", "file"), new XAttribute("type", "file")));
      htmlForm.Add(new XElement("input", null, new XAttribute("name", "submit"), new XAttribute("value", "Upload"), new XAttribute("type", "submit")));

      return htmlForm;
    }


    /// <summary>
    /// Creates HTML for AWS S3 Upload HTTP Post
    /// </summary>
    /// <param name="s3Folder">Tells the path to which the file has to be copied : eg: animations/animated_backgrounds/thumb/</param>
    /// <param name="s3FileName"></param>
    /// <param name="s3FileExtn"></param>
    /// <param name="redirectUrl"></param>
    /// <param name="bucketName">This tells which bucket we are uploading the items into</param>
    /// <param name="accessMode">Tells the S3CannedAcl.. i.e who all can access this content (we use mainly one of the two 1- "private" 2-"public-read" AwsAccessMode.PublicRead </param>
    /// <returns></returns>
    public static string CreateHtmlFormInputs(
      string s3Folder
      , string s3FileName
      , string s3FileExtn
      , string redirectUrl
      , string bucketName
      , string accessMode
      , int minutes = 0
    ) {
      return String.Join(Environment.NewLine, CreateHtmlFormInputsXElements(s3Folder, s3FileName, s3FileExtn, redirectUrl, bucketName, accessMode, minutes));
    }


    public static bool IsFilePresent(string filePath, bool secureBucket = false) {

      GetObjectMetadataResponse response = GetResponse(filePath, secureBucket);

      return response == null ? false : true;
    }


    public static bool IsFilePresent(string filePath, string imageType) {
      bool secureBucket = false;
      if (imageType == ImageType.Source.ToString() || imageType == ImageType.LargeImage.ToString()) {
        secureBucket = true;
      }

      return IsFilePresent(filePath, secureBucket);
    }


    public static bool IsFilePresent(string filePath, int cdn) {
      bool secureBucket = false;
      switch (cdn) {
        case 1:
          secureBucket = false;
          break;
        case 2:
          secureBucket = true;
          break;
        default:
          throw new ArgumentException("CDN not AWS");
      }

      return IsFilePresent(filePath, secureBucket);
    }


    public static bool IsFilePresent(string filePath, ImageType imageType) {

      bool secureBucket = false;
      if (imageType == ImageType.Source || imageType == ImageType.LargeImage) {
        secureBucket = true;
      }

      return S3md5(filePath, secureBucket).IsEmpty() ? false : true;
    }


    public static string S3md5(string filePath, int cdn) {
      bool secureBucket = false;
      switch (cdn) {
        case 1:
          secureBucket = false;
          break;
        case 2:
          secureBucket = true;
          break;
        default:
          throw new ArgumentException("CDN not AWS");
      }

      return S3md5(filePath, secureBucket);
    }


    public static string S3md5(string filePath, ImageType imageType) {
      bool secureBucket = false;
      if (imageType == ImageType.Source || imageType == ImageType.LargeImage) {
        secureBucket = true;
      }

      return S3md5(filePath, secureBucket);
    }


    public static string S3md5(string filePath, bool secureBucket = false) {
      string eTag = "";

      GetObjectMetadataResponse response = GetResponse(filePath, secureBucket);

      if (response != null) {
        eTag = response.ETag.Replace("\"", "");
      }

      return eTag;
    }


    public static GetObjectMetadataResponse GetResponse(string filePath, bool secureBucket) {
      //filePath = "film_and_video_clips/video_clips/Source/13329_92bca751-082d-467e-b868-cd10e08587a1.mov.zip";
      GetObjectMetadataResponse response = new GetObjectMetadataResponse();

      try {
        string bucketName = AWSConfig.PublicPath;

        if (secureBucket) {
          bucketName = AWSConfig.SecurePath;
        }

        Amazon.S3.AmazonS3Config config = new Amazon.S3.AmazonS3Config();
        config.UserAgent = "webmaster";

        response = new Amazon.S3.AmazonS3Client(AWSConfig.Key, AWSConfig.Secret, config)
          .GetObjectMetadata(new GetObjectMetadataRequest()
          .WithBucketName(bucketName)
          .WithKey(filePath));

      }
      catch {
        return null;
      }

      return response;
    }


    public static string GenerateS3Url(string key, int cdn, int expireMinutes = 0) {

      if (key.StartsWith("/")) { key = key.TrimStart('/'); }
      if (expireMinutes < 1) {
        expireMinutes = AWSConfig.SecureDownloadExpireMinutes;
      }

      string bucket = AWSConfig.PublicPath;

      if (cdn == 2) {
        bucket = AWSConfig.SecurePath;
      }

      if (key.Contains("Source")) {
        bucket = AWSConfig.SecurePath;
      }


      Amazon.S3.AmazonS3Client s3Client = new Amazon.S3.AmazonS3Client(AWSConfig.Key, AWSConfig.Secret);

      //string key = "animations/animated_backgrounds/containerthumbs/280.jpg";
      //http://s3.public.Pantry.com/animations/animated_backgrounds/containerthumbs/280.jpg

      GetPreSignedUrlRequest request = new GetPreSignedUrlRequest()
        .WithBucketName(bucket)
        .WithKey(key)
        .WithExpires(DateTime.Now.AddMinutes(expireMinutes))
        .WithProtocol(Protocol.HTTP);

      return s3Client.GetPreSignedURL(request);
    }


    public bool DoesFolderExist(string folder, string bucketName) {
      Amazon.S3.AmazonS3Client s3Client = new Amazon.S3.AmazonS3Client(AWSConfig.Key, AWSConfig.Secret);
      bool rBool = false;
      try {
        GetObjectMetadataResponse response = s3Client.GetObjectMetadata(new GetObjectMetadataRequest()
           .WithBucketName(bucketName)
           .WithKey(folder));

        rBool = true;
      }
      catch (Amazon.S3.AmazonS3Exception ex) {
        if (ex.StatusCode != System.Net.HttpStatusCode.NotFound) {
          throw;
        }
      }
      return rBool;
    }


    public static bool DeleteObject(S3FileInfo FileParams) {
      Amazon.S3.AmazonS3Client s3Client = new Amazon.S3.AmazonS3Client(AWSConfig.Key, AWSConfig.Secret);
      bool rBool = false;
      try {
        s3Client.DeleteObject(new Amazon.S3.Model.DeleteObjectRequest() {
          BucketName = FileParams.BucketName,
          Key = Path.Combine(FileParams.S3Folder, FileParams.FileName).Replace("\\", "/")
        });

        rBool = true;
      }
      catch (Amazon.S3.AmazonS3Exception ex) {
        if (ex.StatusCode != System.Net.HttpStatusCode.NotFound) {
          throw;
        }
      }

      return rBool;
    }

    public static bool DeleteMultipleObjects(string BucketName, List<string> AllKeys) {
      Amazon.S3.AmazonS3Client s3Client = new Amazon.S3.AmazonS3Client(AWSConfig.Key, AWSConfig.Secret);

      bool rBool = false;

      List<KeyVersion> allKeys = new List<KeyVersion>();
      foreach (var fileKey in AllKeys) {
        KeyVersion keyData = new KeyVersion(fileKey);
        allKeys.Add(keyData);
      }

      try {
        s3Client.DeleteObjects(new Amazon.S3.Model.DeleteObjectsRequest() {
          BucketName = BucketName,
          Keys = allKeys
        });

        rBool = true;
      }
      catch (Amazon.S3.AmazonS3Exception ex) {
        if (ex.StatusCode != System.Net.HttpStatusCode.NotFound) {
          throw;
        }
      }

      return rBool;
    }

    public static bool CopyObject(S3FileInfo SrcFileParams, S3FileInfo DesFileParams1) {

      Amazon.S3.AmazonS3Client s3Client = new Amazon.S3.AmazonS3Client(AWSConfig.Key, AWSConfig.Secret);
      bool rBool = false;
      try {
        s3Client.CopyObject(new Amazon.S3.Model.CopyObjectRequest() {

          SourceBucket = SrcFileParams.BucketName,
          SourceKey = Path.Combine(SrcFileParams.S3Folder, SrcFileParams.FileName).Replace("\\", "/"),
          DestinationBucket = DesFileParams1.BucketName,
          DestinationKey = Path.Combine(DesFileParams1.S3Folder, DesFileParams1.FileName).Replace("\\", "/")

        });

        rBool = true;
      }
      catch (Amazon.S3.AmazonS3Exception ex) {
        if (ex.StatusCode != System.Net.HttpStatusCode.NotFound) {
          throw;
        }
      }
      return rBool;
    }


    public static bool UploadFile(S3FileInfo FileParams) {
      S3CannedACL ca = S3CannedACL.PublicRead;
      TransferUtility transferUtility = new TransferUtility(AWSConfig.Key, AWSConfig.Secret);

      bool hasUploaded = false;

      try {
        FileInfo fi = new FileInfo(FileParams.FileName);
        string contentType = Helpers.ContentMimeType.MimeType(fi.Extension);

        transferUtility.S3Client.PutBucket(new PutBucketRequest().WithBucketName(FileParams.BucketName));
        TransferUtilityUploadRequest request = new TransferUtilityUploadRequest()
               .WithFilePath(FileParams.LocalFilePath)
               .WithCannedACL(ca)
               .WithStorageClass(S3StorageClass.Standard)
               .WithKey(Path.Combine(FileParams.S3Folder, fi.Name).Replace("\\", "/"))
               .WithBucketName(FileParams.BucketName);
        transferUtility.Upload(request);

        System.GC.Collect();
        System.GC.WaitForPendingFinalizers();

        hasUploaded = true;
      }
      catch (Exception ex) {
      }
      finally {
        transferUtility.Dispose();
      }
      return hasUploaded;
    }

    public static bool DownloadFile(S3FileInfo FileParams) {

      S3FileInfo FileParams_= new S3FileInfo();


      TransferUtility transferUtility = new TransferUtility(AWSConfig.Key, AWSConfig.Secret);
      bool hasDownloaded = false;


      try {

        transferUtility.S3Client.PutBucket(new PutBucketRequest().WithBucketName(FileParams.BucketName));
        TransferUtilityDownloadRequest request = new TransferUtilityDownloadRequest()
               .WithBucketName(FileParams.BucketName)
               .WithKey(Path.Combine(FileParams.S3Folder, FileParams.FileName).Replace("\\", "/"))
               .WithFilePath(FileParams.LocalFilePath);

        transferUtility.Download(request);

        System.GC.Collect();
        System.GC.WaitForPendingFinalizers();

        hasDownloaded = true;
      }
      catch (Exception ex) {
        hasDownloaded = false;
      }
      finally {
        transferUtility.Dispose();
      }

      return hasDownloaded;
    }


    public static List<string> GetFolderList() {
      List<string> listOfFolders = new List<string>();
      ListObjectsRequest request = null;
      Amazon.S3.AmazonS3Client s3Client = null;

      string awsDelim = @"/";
      string awsPrefix = @"";
      string bucketName = AWSConfig.PublicPath;

      bool useAwsDelim = false;
      bool useAwsPrefix = false;

      if (!String.IsNullOrWhiteSpace(awsPrefix)) {
        useAwsPrefix = true;
      }

      if (!String.IsNullOrWhiteSpace(awsDelim)) {
        useAwsDelim = true;
      }

      try {
        s3Client = new Amazon.S3.AmazonS3Client(AWSConfig.Key, AWSConfig.Secret);

        request = new ListObjectsRequest();

        request.WithBucketName(bucketName);
        if (useAwsDelim) { request.WithDelimiter(awsDelim); }
        if (useAwsPrefix) { request.WithPrefix(awsPrefix); }

        ListObjectsResponse response = s3Client.ListObjects(request);

        listOfFolders = response.CommonPrefixes;

        System.GC.Collect();
        System.GC.WaitForPendingFinalizers();
      }
      catch (Exception e) {
        throw new Exception(e.Message);
      }
      finally {
        s3Client.Dispose();
      }

      return listOfFolders;
    }


    public static AwsFolder GetAwsFolderInfo(string bucketName, string folderName, bool allSubFolders) {
      bool useAwsDelim = true;
      bool useAwsPrefix = true;

      string awsPrefix = folderName + @"/";
      string awsDelim = @"/";
      awsDelim = @"/"; // immediate subdirectories
      if (allSubFolders) {
        awsDelim = @""; // all subdirectories
      }

      if (String.IsNullOrWhiteSpace(bucketName)) { bucketName = AWSConfig.PublicPath; }
      if (String.IsNullOrWhiteSpace(awsPrefix) || awsPrefix == "/") { useAwsPrefix = false; awsPrefix = ""; }
      if (String.IsNullOrWhiteSpace(awsDelim)) { useAwsDelim = false; }

      AwsFolder awsf = new AwsFolder(bucketName, awsPrefix);
      List<AwsObject> awsFileObjects   = new List<AwsObject>();
      List<AwsObject> awsFolderObjects = new List<AwsObject>();
      AwsObject awsObject = new AwsObject();

      ListObjectsRequest request = new ListObjectsRequest();
      ListObjectsResponse response = null;

      try {
        Amazon.S3.AmazonS3Client s3Client = new Amazon.S3.AmazonS3Client(AWSConfig.Key, AWSConfig.Secret);

        request.WithBucketName(bucketName);
        if (useAwsDelim) { request.WithDelimiter(awsDelim); }
        if (useAwsPrefix) { request.WithPrefix(awsPrefix); }

        do {
          response = s3Client.ListObjects(request);

          List<S3Object> s3Object = response.S3Objects;

          foreach (S3Object s3o in s3Object) {
            awsObject = new AwsObject();
            awsObject.Filename = s3o.Key;
            if (!String.IsNullOrWhiteSpace(awsPrefix)) {
              awsObject.Filename = awsObject.Filename.Replace(awsPrefix, "");
            }
            if (awsObject.Filename.IndexOf("_$folder$") > 0) {
              continue;
            }
            awsObject.Hash = s3o.ETag;
            awsObject.Size = s3o.Size;
            awsObject.LastModified = Convert.ToDateTime(s3o.LastModified);

            awsFileObjects.Add(awsObject);
          }

          foreach (string folder in response.CommonPrefixes) {
            awsFileObjects.Add(new AwsObject(folderName: folder));
          }

          if (response.IsTruncated) {
            request.Marker = response.NextMarker;
          } else {
            request = null;
          }
        } while (request != null);

        awsf.FileObjects = awsFileObjects;
        awsf.FolderObjects = awsFolderObjects;

        System.GC.Collect();
        System.GC.WaitForPendingFinalizers();
      }
      catch (Exception ex) {
        throw new Exception(ex.Message);
      }
      finally {
        response.Dispose();
      }

      return awsf;
    }


    public static string GenerateEitherPreSignedUrl(string filePath, Int64 fileSize, int cdn) {
      string signedUrl = "";

      signedUrl = GenerateS3Url(filePath, 60, cdn);

      return signedUrl;
    }


    public static string GenerateCloudFrontPreSignedUrlForCurl(string filePath, Int64 fileSize, int cdn) {
      return GenerateCloudFrontPreSignedUrl(filePath, fileSize, cdn, true);
    }


    public static string GenerateCloudFrontPreSignedUrl(string filePath, Int64 fileSize, int cdn) {
      return GenerateCloudFrontPreSignedUrl(filePath, fileSize, cdn, false);
    }


    public static string GenerateCloudFrontPreSignedUrl(string filePath, Int64 fileSize, int cdn, bool isForCurl) {

      //Origin Access ID: E3ANB93CEBPCPH
      //S3 Canonical User Id: 9a181e14c8644721a987ab415fc76acc3546864370ec34e74f2b12780743843d6123c77116323b0dc2f4ebea9d4ccdb5

      /* Requirements:
       * Private S3 bucket with cloudfront distibution setup
       * Origin Access Identity user setup on the cloudfront distribution (create new or use existing)
       * Same Origin Access Identity must be assigned read permissions on the objects in the S3 bucket
       * On AWS account page, you need to go the Key Pair tab and create a new key pair and download the private key (.pem) or use existing private key (do NOT lose!)
       * Use OpenSSLkey.exe to convert the .pem file to a XML file that is required by .NET
       * CURRENTLY NOT WORKING WITH FILES WITH SPACES!
       */

      if (!filePath.StartsWith("/")) { filePath = "/" + filePath; }

      DateTime expirationDate = DateTime.Now.AddMinutes(AWSConfig.SecureDownloadExpireMinutes);
      string signedUrl = "";

      string pathToPrivateKey = (AppDomain.CurrentDomain.BaseDirectory + AWSConfig.CloudFrontKeyPath).Replace(@"\\", @"\");

      string privateKeyPairId = AWSConfig.CloudFrontKey;
      string resourceUrl = AWSConfig.CloudFrontSecure + filePath.Replace(" ", "%20");

      TimeSpan timeSpanInterval = expirationDate.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

      double expireEpoch = Math.Floor(timeSpanInterval.TotalSeconds);

      string strPolicy = String.Format(@"{{""Statement"":[{{""Resource"":""{0}"",""Condition"":{{""DateLessThan"":{{""AWS:EpochTime"":{1}}}}}}}]}}", resourceUrl, expireEpoch);

      byte[] bufferPolicy = Encoding.ASCII.GetBytes(strPolicy);

      using (SHA1CryptoServiceProvider cryptoSHA1 = new SHA1CryptoServiceProvider()) {
        bufferPolicy = cryptoSHA1.ComputeHash(bufferPolicy);

        RSACryptoServiceProvider providerRSA = new RSACryptoServiceProvider();

        XmlDocument xmlPrivateKey = new XmlDocument();
        xmlPrivateKey.Load(pathToPrivateKey);
        providerRSA.FromXmlString(xmlPrivateKey.InnerXml);

        RSAPKCS1SignatureFormatter rsaFormatter = new RSAPKCS1SignatureFormatter(providerRSA);
        rsaFormatter.SetHashAlgorithm("SHA1");

        byte[] signedPolicyHash = rsaFormatter.CreateSignature(bufferPolicy);

        string strSignedPolicy = System.Convert.ToBase64String(signedPolicyHash);

        if (isForCurl) {
          strSignedPolicy = strSignedPolicy.Replace('+', '-').Replace('=', '_').Replace('/', '~');
        }

        signedUrl = resourceUrl + "?Expires=" + expireEpoch.ToString() + "&Signature=" + strSignedPolicy + "&Key-Pair-Id=" + privateKeyPairId;

      }

      return signedUrl;
    }

  }
}
