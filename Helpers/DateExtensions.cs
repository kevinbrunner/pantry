﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pantry.Helpers {
  public static class DayExtensions {

    /// <summary>
    /// Normalizes the current Hour
    /// </summary>
    /// <param name="current">The current date</param>
    /// <returns></returns>
    public static DateTime NormalizeHour(this DateTime current, int mod) {
      if(mod > 24) { mod = 24; }
      if(mod % 24 == 0) {
        current = new DateTime(current.Year, current.Month, current.Day, 0, 0, 0);
      } else {
        current = new DateTime(current.Year, current.Month, current.Day, current.Hour, 0, 0);
        switch(mod) {
          case 1:
            mod = 0;
            break;
          case 2:
          case 3:
          case 4:
          case 6:
          case 12:
            if(current.Hour == 0) {
              mod = 0;
            } else {
              if(current.Hour > mod) {
                int cmod = current.Hour % mod;
                mod = cmod == 0 ? 0 : mod - cmod;
              } else {
                mod = mod - current.Hour;
              }
            }
            break;
        }
        current = current.AddHours(mod * -1);
      }

      return current;
    }


    /// <summary>
    /// Gets a DateTime representing the first day in the current month
    /// </summary>
    /// <param name="current">The current date</param>
    /// <returns></returns>
    public static DateTime First(this DateTime current) {
      current = current.Midnight();
      DateTime first = current.AddDays(1 - current.Day);
      return first;
    }


    /// <summary>
    /// Gets a DateTime representing the first specified day in the current month
    /// </summary>
    /// <param name="current">The current day</param>
    /// <param name="dayOfWeek">The current day of week</param>
    /// <returns></returns>
    public static DateTime First(this DateTime current, DayOfWeek dayOfWeek) {
      current = current.Midnight();
      DateTime first = current.First();

      if(first.DayOfWeek != dayOfWeek) {
        first = first.Next(dayOfWeek);
      }

      return first;
    }


    /// <summary>
    /// Gets a DateTime representing the last day in the current month
    /// </summary>
    /// <param name="current">The current date</param>
    /// <returns></returns>
    public static DateTime Last(this DateTime current) {
      current = current.Midnight();
      int daysInMonth = DateTime.DaysInMonth(current.Year, current.Month);

      DateTime last = current.First().AddDays(daysInMonth - 1);
      return last;
    }


    /// <summary>
    /// Gets a DateTime representing the last specified day in the current month
    /// </summary>
    /// <param name="current">The current date</param>
    /// <param name="dayOfWeek">The current day of week</param>
    /// <returns></returns>
    public static DateTime Last(this DateTime current, DayOfWeek dayOfWeek) {
      current = current.Midnight();
      DateTime last = current.Last();

      last = last.AddDays(Math.Abs(dayOfWeek - last.DayOfWeek) * -1);
      return last;
    }


    /// <summary>
    /// Gets a DateTime representing the first date following the current date which falls on the given day of the week
    /// </summary>
    /// <param name="current">The current date</param>
    /// <param name="dayOfWeek">The day of week for the next date to get</param>
    public static DateTime Next(this DateTime current, DayOfWeek dayOfWeek) {
      current = current.Midnight();
      int offsetDays = dayOfWeek - current.DayOfWeek;

      if(offsetDays <= 0) {
        offsetDays += 7;
      }

      DateTime result = current.AddDays(offsetDays);
      return result;
    }


    /// <summary>
    /// Gets a DateTime representing the first date of the quarter
    /// </summary>
    /// <param name="current">The current date</param>
    /// <param name="quarterBack">The number of quarters to go back</param>
    public static DateTime BeginningOfQuarter(this DateTime current, int quarterBack = 0) {
      current = current.Midnight();
      int month = current.Month;
      int quarterMonth = 1;

      if(month > 3) {
        quarterMonth = 4;
      }

      if(month > 6) {
        quarterMonth = 7;
      }

      if(month > 9) {
        quarterMonth = 10;
      }

      DateTime result = new DateTime(current.Year, quarterMonth, 1);

      if(quarterBack > 0) {
        result = result.AddMonths(quarterBack * -3);
      }

      return result;
    }


    /// <summary>
    /// Gets a DateTime representing the first date of the Quarter
    /// </summary>
    /// <param name="current">The current date</param>
    /// <param name="quarterBack">The number of quarters to go back</param>
    public static DateTime BeginningOfYear(this DateTime current, int yearBack = 0) {

      DateTime result = new DateTime(current.Year, 1, 1);

      if(yearBack > 0) {
        result = result.AddYears(yearBack * -1);
      }

      return result;
    }


    /// <summary>
    /// Gets a DateTime representing the first date of the Week
    /// </summary>
    /// <param name="current">The current date</param>
    /// <param name="quarterBack">The number of quarters to go back</param>
    public static DateTime BeginningOfWeek(this DateTime current) {

      int days = 0;

      DayOfWeek dow = current.DayOfWeek;
      switch(dow) {
        case DayOfWeek.Monday:
          days = 0;
          break;
        case DayOfWeek.Tuesday:
          days = -1;
          break;
        case DayOfWeek.Wednesday:
          days = -2;
          break;
        case DayOfWeek.Thursday:
          days = -3;
          break;
        case DayOfWeek.Friday:
          days = -4;
          break;
        case DayOfWeek.Saturday:
          days = -5;
          break;
        case DayOfWeek.Sunday:
          days = -6;
          break;
      }

      DateTime result = current.Date.AddDays(days);

      return result;
    }






  }

}
