﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

using Pantry.Config;
using Pantry.Entities;
using Pantry.Utilities;


namespace Pantry.Helpers {
  public class Emailing {
    // Generic method for sending emails

    public static void SendMail(string from, string to, string subject, string body) {

      string html = "this is an html message";
      string text = "this is a text message";
      //string subject = "this is the subject";
      //string from = Config.SiteContacts.KevinnBrunner;
      //string to = Config.SiteContacts.KevinnBrunner;

      //the Html part
      AlternateView htmlView = AlternateView.CreateAlternateViewFromString(html, null, "text/html");

      //the Plain Text part
      AlternateView plainView = AlternateView.CreateAlternateViewFromString(text, null, "text/plain");

      MailMessage message = new MailMessage();
      SmtpClient client = new SmtpClient();
      message.From = new MailAddress(from);
      message.Sender = new MailAddress(from);
      message.To.Add(to);
      message.Subject = subject;
      message.Headers.Add("X-dbvalue", "this is a customer value");

      message.IsBodyHtml = true;
      message.BodyEncoding = Encoding.Default;
      message.Body = html;

      message.AlternateViews.Add(plainView);
      message.AlternateViews.Add(htmlView);

      client.Send(message);
      client.Dispose();
    }


  }
}
