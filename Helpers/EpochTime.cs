﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Pantry.Utilities;


namespace Pantry.Helpers {
  public class EpochTime {

    public static double GetEpochTime(DateTime dateTime) {
      return (dateTime.ToUniversalTime().Ticks - 621355968000000000) / 10000000;
    }

    public static long GetEpochTime() {
      return (DateTime.Now.ToUniversalTime().Ticks - 621355968000000000);
    }

    public static double GetWithHours(int Hours) {
      return GetEpochTime(DateTime.Now.AddHours(Hours));
    }


    public static bool IsConfirmCodeExpired(string codeWithEpoch, bool checkIsGuid = false) {
      bool isExpired = true;

      if (!codeWithEpoch.IsEmpty()) {
        string[] epochSplit = codeWithEpoch.Split(new char[] { '_', ';', ':', '|', '-', ' ' }, StringSplitOptions.RemoveEmptyEntries);
        if (epochSplit.Length > 1) {

          if (checkIsGuid) {
            if (!(String.Join("-", epochSplit.Take(epochSplit.Length - 1))).ValidateGuidID()) {
              return true;
            }
          }

          long checkEpoch = 0;
          if (long.TryParse(epochSplit[epochSplit.Length - 1], out checkEpoch)) {
            isExpired = IsLessThanNow(checkEpoch);
          }
        }
      }

      return isExpired;
    }


    public static DateTime ConfirmCodeExpireDate(string codeWithEpoch) {
      DateTime expireDate = Config.AppConfig.MinDateTime;

      if (!codeWithEpoch.IsEmpty()) {
        string[] epochSplit = codeWithEpoch.Split(new char[] { '_', ';', ':', '|', '-', ' ' }, StringSplitOptions.RemoveEmptyEntries);
        if (epochSplit.Length > 1) {

          long checkEpoch = 0;
          if (long.TryParse(epochSplit[epochSplit.Length - 1], out checkEpoch)) {
            expireDate = FromEpochTime(checkEpoch);
          }
        }
      }

      return expireDate;
    }


    public static string CreateExpiringConfirmCode(DateTime dateTime) {
      return Guid.NewGuid().ToString() + "-" + GetEpochTime(dateTime).ToString();
    }


    public static string CreateExpiringConfirmCode(int hours) {
      return Guid.NewGuid().ToString() + "-" + Helpers.EpochTime.GetWithHours(hours);
    }


    public static bool IsLessThanNow(double epoch) {
      bool returnMe = false;
      if (epoch < GetWithHours(0)) {
        returnMe = true;
      }

      return returnMe;
    }


    public static DateTime FromEpochTime(long epochTime) {
      var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
      return epoch.AddSeconds(epochTime);
    }


    public static long ToEpochTime(DateTime? date = null) {
      if (date == null) { date = DateTime.Now; }
      var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
      return Convert.ToInt64(((DateTime)date - epoch).TotalSeconds);
    }


    public static string GetDateTimeStamp() {
      char zero = '0';
      DateTime now = DateTime.Now;
      string datetimeStamp = now.Year.ToString() + now.Month.ToString().PadLeft(2, zero) + now.Day.ToString().PadLeft(2, zero) + now.Hour.ToString().PadLeft(2, zero) + now.Minute.ToString().PadLeft(2, zero) + now.Second.ToString().PadLeft(2, zero) + now.Millisecond.ToString().PadLeft(3, zero);

      return datetimeStamp;

    }

  }
}

