﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

using Pantry.Entities;
using Pantry.Utilities;

namespace Pantry.Helpers {
  public static partial class StringHelperExtensions {


    public static T ParseEnum<T>(this string value) {
      return (T)Enum.Parse(typeof(T), value, true);
    }


  }


  public static class XElementExtension {

    public static List<XElement> Distinct(this List<XElement> xeList) {
      List<XElement> distinctElements = new List<XElement>();

      foreach(var xe in xeList) {
        if(!distinctElements.Any(s => AreTheSame(s.Attributes().ToList(), xe.Attributes().ToList()))) {
          distinctElements.Add(xe);
        }
      }
      return distinctElements;
    }

    public static bool AreTheSame(this IEnumerable<XAttribute> attrubutetoCheck, IEnumerable<XAttribute> thePassedAttribute) {
      bool areTheSame = true;
      foreach(var item in attrubutetoCheck) {
        var theAttrib = thePassedAttribute.Where(a => a.Name == item.Name).SingleOrDefault();

        if(theAttrib == null) {
          areTheSame = false;
          break;
        } else if(item.Value != theAttrib.Value) {
          areTheSame = false;
          break;
        }
      }
      return areTheSame;
    }

    public static List<XElement> Intersect(this List<XElement> xElementToCheckWith, List<XElement> thePassedXelement) {
      List<XElement> result = new List<XElement>();
      foreach(var item in thePassedXelement) {
        foreach(var elts in xElementToCheckWith) {
          if(AreTheSame(elts.Attributes().ToList(), item.Attributes().ToList())) {
            result.Add(item);
          }
        }
      }
      return result;
    }

    public static List<XElement> Intersect(this List<XElement> xElementToCheckWith, List<XElement> thePassedXelement, string attributeToCheck) {
      List<XElement> result = new List<XElement>();
      foreach(var item in thePassedXelement) {
        foreach(var elts in xElementToCheckWith) {
          if(elts.Attributes().ToList().AreTheSame(item.Attributes().ToList(), attributeToCheck)) {
            result.Add(item);
          }
        }
      }
      return result;
    }

    //public static bool Contain(this List<XElement> theListToBeCheck, XElement itemToCheck) {
    //  bool contains = false;
    //  contains = theListToBeCheck.Any(s => AreTheSame(s.Attributes().ToList(), itemToCheck.Attributes().ToList()));
    //  return contains;
    //}

    public static bool Contain(this IEnumerable<XElement> theListToBeCheck, XElement itemToCheck) {

      bool contains = false;
      contains = theListToBeCheck.Any(s => AreTheSame(s.Attributes().ToList(), itemToCheck.Attributes().ToList()));
      return contains;
    }

    private static bool AreTheSame(this List<XAttribute> attrubutetoCheck, List<XAttribute> thePassedAttribute, string attributeName) {
      bool areTheSame = true;

      var theAttribute = attrubutetoCheck.Where(s => s.Name == attributeName).FirstOrDefault();
      string value = "";
      if(theAttribute != null) {
        value = theAttribute.Value;
      }

      var itemsAttribute = thePassedAttribute.Where(s => s.Name == attributeName).FirstOrDefault();

      if(itemsAttribute != null) {


        if(value == itemsAttribute.Value) {
          areTheSame = true;
        } else {
          areTheSame = false;
        }
      } else {
        areTheSame = false;
      }

      return areTheSame;
    }


    /// <summary>
    /// A NOTIN B
    /// </summary>
    /// <param name="listA"></param>
    /// <param name="listB"></param>
    /// <param name="attributes"></param>
    /// <returns></returns>
    public static void NotIN(this IEnumerable<XElement> listA, IEnumerable<XElement> listB, params string[] attributes) {
      var itemsPresent = new List<XElement>();
      var itemsNotPresent = new List<XElement>();
      foreach(var a in listA) {
        var attribs = a.Attributes();
        var item = new List<XElement>();
        item.AddRange(listB.Where(s => attribs.ToList().AreTheSame(s.Attributes().ToList())));
        itemsPresent.AddRange(item);

        //foreach(var i in item) {
        //  listB.Where(s => s == i).Remove();
        //}
      }

      listA.ToList().Where(s => !itemsPresent.Contain(s)).Remove();
      // return listA.Except(itemsPresent);
    }


    /// <summary>
    /// A NOTIN B
    /// </summary>
    /// <param name="listA"></param>
    /// <param name="listB"></param>
    /// <param name="attributes"></param>
    /// <returns></returns>
    public static bool NotIN(this XElement item, IEnumerable<XElement> listB, params string[] attributes) {
      bool isPresent = false;
      isPresent = listB.Contains(item);
      return isPresent;
    }
  }


  public static class ListExtensions {

    public static List<T> NotIn<T>(this List<T> A, List<T> B) {
      return A.Except(B).ToList();
    }
  }


}
