﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Pantry.Utilities;

namespace Pantry.Helpers.Security {

  /// <summary>
  /// Password Tool Kit created by Ian Harris
  /// harro84@yahoo.com.au
  /// v2.0.0.0
  /// </summary>
  public static class PasswordTools {

    public static string CalcDigitalJuiceSalt(string djCustomerID) {
      string customerID = djCustomerID.Replace("{", "").Replace("}", "").Replace("-", "");
      string cid1 = "";

      for (int i = 1; i < 13; ++i) {
        cid1 += customerID.Substring(customerID.Length - i, 1);
      }

      return cid1;
    }

    public static string CalcDigitalJuicePassword(string djCustomerID, int djSortID, string password) {

      StringBuilder hashedPassword = new StringBuilder();

      if(djCustomerID.Length > 20) {
        string customerID = djCustomerID.Replace("{", "").Replace("}", "").Replace("-", "");
        string cid1 = "";

        for(int i = 1; i < 13; ++i) {
          cid1 += customerID.Substring(customerID.Length - i, 1);
        }

        //int cid1Length = cid1.Length;
        //string password1 = sortid + password + cid1;
        //password1 = Authorization.ComputeHexHash(password1);

        byte[] plainTextBytes = System.Text.Encoding.UTF8.GetBytes(djSortID.ToString() + password + cid1);

        HashAlgorithm hash = new SHA1Managed();

        // Compute hash value of our plain text.
        byte[] hashBytes = hash.ComputeHash(plainTextBytes);

        // Convert result into a HEX string.
        for(int i = 0; i < hashBytes.Length; i++) {
          hashedPassword.Append(hashBytes[i].ToString("X2"));
        }
      }

      return hashedPassword.ToString();

    }
    
    /// <summary>
    /// The default character length to create salt strings
    /// </summary>
    public const Int32 cDefaultSaltLength = 64;

    /// <summary>
    /// The default iteration count for key stretching
    /// </summary>
    public const Int32 cDefaultIterationCount = 5000;

    /// <summary>
    /// The minimum size in characters the password hashing function will allow for a salt string, salt must be always greater than 8 for PBKDF2 key derivitation to function
    /// </summary>
    public const Int32 cMinSaltLength = 8;

    /// <summary>
    /// The key length used in the PBKDF2 derive bytes and matches the output of the underlying HMACSHA512 psuedo random function
    /// </summary>
    public const Int32 cKeyLength = 64;

    /// <summary>
    /// A default password policy provided for use if you are unsure what to make your own PasswordPolicy
    /// </summary>
    public static PasswordPolicy cDefaultPasswordPolicy = new PasswordPolicy(1, 1, 2, 6, Int32.MaxValue);

    /// <summary>
    /// Below are regular expressions used for password to policy comparrisons
    /// </summary>
    private const string cNumbersRegex = "[\\d]";
    private const string cUppercaseRegex = "[A-Z]";
    private const string cNonAlphaNumericRegex = "[^0-9a-zA-Z]";

    /// <summary>
    /// A PasswordPolicy defines min and max password length and also minimum amount of Uppercase, Non-Alpanum and Numerics to be present in the password string
    /// </summary>
    public struct PasswordPolicy {
      private int aForceXUpperCase;
      private int aForceXNonAlphaNumeric;
      private int aForceXNumeric;
      private int aPasswordMinLength;
      private int aPasswordMaxLength;

      /// <summary>
      /// Creates a new PasswordPolicy Struct
      /// </summary>
      /// <param name="XUpper">Forces at least this number of Uppercase characters</param>
      /// <param name="XNonAlphaNumeric">Forces at least this number of Special characters</param>
      /// <param name="XNumeric">Forces at least this number of Numeric characters</param>
      /// <param name="MinLength">Forces at least this number of characters</param>
      /// <param name="MaxLength">Forces at most this number of characters</param>
      public PasswordPolicy(int XUpper, int XNonAlphaNumeric, int XNumeric, int MinLength, int MaxLength) {
        aForceXUpperCase = XUpper;
        aForceXNonAlphaNumeric = XNonAlphaNumeric;
        aForceXNumeric = XNumeric;
        aPasswordMinLength = MinLength;
        aPasswordMaxLength = MaxLength;
      }

      public int ForceXUpperCase {
        get {
          return aForceXUpperCase;
        }
      }

      public int ForceXNonAlphaNumeric {
        get {
          return aForceXNonAlphaNumeric;
        }
      }

      public int ForceXNumeric {
        get {
          return aForceXNumeric;
        }
      }

      public int PasswordMinLength {
        get {
          return aPasswordMinLength;
        }
      }

      public int PasswordMaxLength {
        get {
          return aPasswordMaxLength;
        }
      }
    }


    public static string ClassicPasswordToHash(string password, string salt) {
      byte[] bytes = Encoding.Unicode.GetBytes(password);
      byte[] src = Convert.FromBase64String(salt);
      byte[] dst = new byte[src.Length + bytes.Length];
      byte[] inArray = null;
      Buffer.BlockCopy(src, 0, dst, 0, src.Length);
      Buffer.BlockCopy(bytes, 0, dst, src.Length, bytes.Length);

      System.Security.Cryptography.HashAlgorithm algorithm = System.Security.Cryptography.HashAlgorithm.Create("SHA1");
      inArray = algorithm.ComputeHash(dst);

      return Convert.ToBase64String(inArray);
    }



    /// <summary>
    /// Convert a String into a SecureString and attempt to remove the original String from RAM
    /// </summary>
    /// <param name="ToConvert">This is the String to convert into a SecureString, please note that String ToConvert will be Overwritten with the same length of *'s to try remove it from RAM so after conversion it can be used no more and the SecureString must be used instead</param>
    /// <returns>A SecureString which resides in memory in an encrypted state</returns>
    public static SecureString StringToSecureString(ref string ToConvert) {
      SecureString outputSecureString = new SecureString();
      string OverwriteString = String.Empty;

      foreach(Char c in ToConvert) {
        outputSecureString.AppendChar(c);
      }

      //Overwrite original String to try clear it from RAM (Note may still reside in Paged and Hiberfil)
      for(int i = 0; i < ToConvert.Length; i++) {
        OverwriteString += "*";
      }

      ToConvert = OverwriteString;

      return outputSecureString;
    }


    /// <summary>
    /// Crypto Randomly generates a byte array that can be used safely as salt
    /// </summary>
    /// <param name="SaltStringLength">Length of the generated byte array</param>
    /// <returns>Bast64 string encoded Byte Array to be used as Salt</returns>
    public static string GetRandomSaltAsBase64String(int saltLength) {
      return Convert.ToBase64String(pGenerateRandomSalt(saltLength));
    }

    /// <summary>
    /// Crypto Randomly generates a Byte Array that can be used safely as salt
    /// </summary>
    /// <returns>Bast64 string encoded Byte Array to be used as Salt</returns>
    public static string GetRandomSaltAsBase64String() {
      return Convert.ToBase64String(pGenerateRandomSalt(cDefaultSaltLength));
    }

    /// <summary>
    /// Crypto Randomly generates a byte array that can be used safely as salt
    /// </summary>
    /// <param name="SaltStringLength">Length of the generated byte array</param>
    /// <returns>A Byte Array to be used as Salt</returns>
    public static Byte[] GetRandomSalt(int saltLength) {
      return pGenerateRandomSalt(saltLength);
    }


    /// <summary>
    /// Crypto Randomly generates a Byte Array that can be used safely as salt
    /// </summary>
    /// <returns>A Byte Array to be used as Salt</returns>
    public static Byte[] GetRandomSalt() {
      return pGenerateRandomSalt(cDefaultSaltLength);
    }


    /// <summary>
    /// Crypto Randomly generates a Byte Array that can be used safely as salt
    /// </summary>
    /// <returns>A Byte Array to be used as Salt</returns>
    public static string GetRandomSaltString(int saltLength = 64) {
      if(saltLength < 64) {
        throw new ArgumentOutOfRangeException("salt length too short");
      }
      return Convert.ToBase64String(pGenerateRandomSalt(cDefaultSaltLength));
      //return GetRandomSalt(saltLength).HashBytesToHexString();
    }


    /// <summary>
    ///  Input a password and a hash value in bytes and it uses PBKDF2 HMACSHA512 to hash the password and compare it to the supplied hash, uses PWDTK default iterations (cDefaultIterationCount)
    /// </summary>
    /// <param name="Password">The text password to be hashed for comparrison</param>
    /// <param name="Salt">The salt to be added to the  password pre hash</param>
    /// <param name="Hash">The existing hash byte array you have stored for comparison</param>
    /// <returns>True if Password matches Hash else returns  false</returns>
    public static bool ComparePasswordToHash(Byte[] Salt, string Password, Byte[] Hash) {
      if(pPasswordToHash(Salt, StringToUTF8Bytes(Password), cDefaultIterationCount).SequenceEqual(Hash)) {
        return true;
      }

      return false;
    }


    /// <summary>
    ///  Input a password and a hash value in bytes and it uses PBKDF2 HMACSHA512 to hash the password and compare it to the supplied hash
    /// </summary>
    /// <param name="Password">The text password to be hashed for comparrison</param>
    /// <param name="Salt">The salt to be added to the  password pre hash</param>
    /// <param name="Hash">The existing hash byte array you have stored for comparison</param>
    /// <param name="IterationCount">The number of times you have specified to hash the password for key stretching</param>
    /// <returns>True if Password matches Hash else returns  false</returns>
    public static bool ComparePasswordToHash(Byte[] Salt, string Password, Byte[] Hash, Int32 IterationCount) {
      if(pPasswordToHash(Salt, StringToUTF8Bytes(Password), IterationCount).SequenceEqual(Hash)) {
        return true;
      }

      return false;
    }


    /// <summary>
    ///  Converts Salt + Password into a Hash using PWDTK defined default iterations (cDefaultIterationCount)
    /// </summary>
    /// <param name="Salt">The salt to add infront of the password before processing the hash (Anti-Rainbow Table tactic)</param>
    /// <param name="Password">The password used to compute the hash</param>
    /// <returns>The Hash value of the salt + password as a Byte Array</returns>
    public static string PasswordToHashAsBase64String(string password, string salt, int version, bool isClassicHash = false) {
      string hashedPassword = password;
      bool justDoIt = true;

      if(justDoIt) {

        if(password.IsEmpty()) { throw new ArgumentOutOfRangeException("password"); }

        if(salt.IsEmpty() || salt.Length < 4) { throw new ArgumentOutOfRangeException("salt"); }

        if(isClassicHash || salt.Length < 30) {
          version = 2;
        }

        switch(version) {
          case 0:
            hashedPassword = ClassicPasswordToHash(password, salt);
            break;

          case 1:
            hashedPassword = ClassicPasswordToHash(password, salt); // DJ hash
            break;

          case 2:
            hashedPassword = ClassicPasswordToHash(password, salt);
            break;

          case 3:
            hashedPassword = PasswordToHashAsBase64String3(salt, password);
            break;

          default:
            throw new NotImplementedException("salt");
        }
      }

      return hashedPassword;
    }


    /// <summary>
    ///  Converts Salt + Password into a Hash Converted to Base64 string
    /// </summary>
    /// <param name="Salt">The salt to add infront of the password before processing the hash (Anti-Rainbow Table tactic)</param>
    /// <param name="Password">The password used to compute the hash</param>
    /// <param name="IterationCount">Repeat the PBKDF2 dunction this many times (Anti-Rainbow Table tactic), higher value = more CPU usage which is better defence against cracking</param>
    /// <returns>The Hash value of the salt + password as a Byte Array Converted to Base64 string</returns>
    public static string PasswordToHashAsBase64String3(string salt, string password) {
      byte[] salt1 = Convert.FromBase64String(salt);

      pCheckSaltCompliance(salt1);

      return Convert.ToBase64String(pPasswordToHash(salt1, StringToUTF8Bytes(password), 16000));
    }


    /// <summary>
    ///  Converts Salt + Password into a Hash Converted to Base64 string
    /// </summary>
    /// <param name="Salt">The salt to add infront of the password before processing the hash (Anti-Rainbow Table tactic)</param>
    /// <param name="Password">The password used to compute the hash</param>
    /// <param name="IterationCount">Repeat the PBKDF2 dunction this many times (Anti-Rainbow Table tactic), higher value = more CPU usage which is better defence against cracking</param>
    /// <returns>The Hash value of the salt + password as a Byte Array Converted to Base64 string</returns>
    public static string PasswordToHashAsBase64String4(string salt, string password) {
      byte[] salt1 = Convert.FromBase64String(salt);

      pCheckSaltCompliance(salt1);

      return Convert.ToBase64String(pPasswordToHash(salt1, StringToUTF8Bytes(password), 64000));
    }


    /// <summary>
    ///  Converts Salt + Password into a Hash using PWDTK defined default iterations (cDefaultIterationCount)
    /// </summary>
    /// <param name="Salt">The salt to add infront of the password before processing the hash (Anti-Rainbow Table tactic)</param>
    /// <param name="Password">The password used to compute the hash</param>
    /// <returns>The Hash value of the salt + password as a Byte Array</returns>
    public static Byte[] PasswordToHash(Byte[] Salt, string Password) {
      pCheckSaltCompliance(Salt);

      return pPasswordToHash(Salt, StringToUTF8Bytes(Password), cDefaultIterationCount);
    }


    /// <summary>
    ///  Converts Salt + Password into a Hash
    /// </summary>
    /// <param name="Salt">The salt to add infront of the password before processing the hash (Anti-Rainbow Table tactic)</param>
    /// <param name="Password">The password used to compute the hash</param>
    /// <param name="IterationCount">Repeat the PBKDF2 dunction this many times (Anti-Rainbow Table tactic), higher value = more CPU usage which is better defence against cracking</param>
    /// <returns>The Hash value of the salt + password as a Byte Array</returns>
    public static Byte[] PasswordToHash(Byte[] Salt, string Password, Int32 IterationCount) {
      pCheckSaltCompliance(Salt);

      return pPasswordToHash(Salt, StringToUTF8Bytes(Password), IterationCount);
    }


    /// <summary>
    /// Converts the Byte array Hash into a Human Friendly HEX String
    /// </summary>
    /// <param name="Hash">The Hash value to convert</param>
    /// <returns>A HEX String representation of the Hash value</returns>
    public static string HashBytesToHexString(this Byte[] Hash) {
      return new SoapHexBinary(Hash).ToString();
    }


    /// <summary>
    /// Converts the Hash Hex String into a Byte[] for computational processing
    /// </summary>
    /// <param name="HashHexString">The Hash Hex String to convert back to bytes</param>
    /// <returns>Esentially reverses the HashToHexString function, turns the String back into Bytes</returns>
    public static Byte[] HashHexStringToBytes(this string HashHexString) {
      return SoapHexBinary.Parse(HashHexString).Value;
    }


    /// <summary>
    /// Tests the password for compliance against the supplied password policy
    /// </summary>
    /// <param name="Password">The password to test for compliance</param>
    /// <param name="PwdPolicy">The PasswordPolicy that we are testing that the Password complies with</param>
    /// <returns>True for Password Compliance with the Policy</returns>
    public static bool TryPasswordPolicyCompliance(string Password, PasswordPolicy PwdPolicy) {
      bool isCompliant = true;

      try {
        pCheckPasswordPolicyCompliance(Password, PwdPolicy);
      }
      catch {
        isCompliant = false;
      }

      return isCompliant;
    }


    /// <summary>
    /// Tests the password for compliance against the supplied password policy
    /// </summary>
    /// <param name="Password">The password to test for compliance</param>
    /// <param name="PwdPolicy">The PasswordPolicy that we are testing that the Password complies with</param>
    /// <param name="PwdPolicyException">The exception that will contain why the Password does not meet the PasswordPolicy</param>
    /// <returns>True for Password Compliance with the Policy</returns>
    public static bool TryPasswordPolicyCompliance(string Password, PasswordPolicy PwdPolicy, ref PasswordPolicyException PwdPolicyException) {
      bool isCompliant = true;

      try {
        pCheckPasswordPolicyCompliance(Password, PwdPolicy);
      }
      catch(PasswordPolicyException ex) {
        PwdPolicyException = ex;
        isCompliant = false;
      }

      return isCompliant;
    }


    /// <summary>
    /// Converts String to UTF8 friendly Byte Array
    /// </summary>
    /// <param name="stringToConvert">String to convert to Byte Array</param>
    /// <returns>A UTF8 decoded string as Byte Array</returns>
    public static Byte[] StringToUTF8Bytes(string stringToConvert) {
      return new UTF8Encoding(false).GetBytes(stringToConvert);
    }


    /// <summary>
    /// Converts UTF8 friendly Byte Array to String
    /// </summary>
    /// <param name="bytesToConvert">Byte Array to convert to String</param>
    /// <returns>A UTF8 encoded Byte Array as String</returns>
    public static string UTF8BytesToString(Byte[] bytesToConvert) {
      return new UTF8Encoding(false).GetString(bytesToConvert);
    }


    /// <summary>
    /// Converts UTF8 friendly Byte Array to String
    /// </summary>
    /// <param name="bytesToConvert">Byte Array to convert to String</param>
    /// <returns>A UTF8 encoded Byte Array as String</returns>
    public static string UTF8BytesToStringE(this Byte[] bytesToConvert) {
      return new UTF8Encoding(false).GetString(bytesToConvert);
    }



    private static void pCheckPasswordPolicyCompliance(string Password, PasswordPolicy PwdPolicy) {
      if(new Regex(cNumbersRegex).Matches(Password).Count < PwdPolicy.ForceXNumeric) {
        throw new PasswordPolicyException("The password must contain " + PwdPolicy.ForceXNumeric.ToString() + " numeric [0-9] characters");
      }

      if(new Regex(cNonAlphaNumericRegex).Matches(Password).Count < PwdPolicy.ForceXNonAlphaNumeric) {
        throw new PasswordPolicyException("The password must contain " + PwdPolicy.ForceXNonAlphaNumeric.ToString() + " special characters");
      }

      if(new Regex(cUppercaseRegex).Matches(Password).Count < PwdPolicy.ForceXUpperCase) {
        throw new PasswordPolicyException("The password must contain " + PwdPolicy.ForceXUpperCase.ToString() + " uppercase characters");
      }

      if(Password.Length < PwdPolicy.PasswordMinLength) {
        throw new PasswordPolicyException("The password does not have a length of at least " + PwdPolicy.PasswordMinLength.ToString() + " characters");
      }

      if(Password.Length > PwdPolicy.PasswordMaxLength) {
        throw new PasswordPolicyException("The password is longer than " + PwdPolicy.PasswordMaxLength.ToString() + " characters");
      }
    }


    private static Byte[] pGenerateRandomSalt(Int32 SaltLength) {
      Byte[] Salt = new byte[SaltLength];
      RandomNumberGenerator.Create().GetBytes(Salt);
      return Salt;
    }


    private static void pCheckSaltCompliance(Byte[] Salt) {
      if(Salt.Length < cMinSaltLength) {
        throw new SaltTooShortException("The supplied salt is too short, it must be at least " + cMinSaltLength.ToString() + " bytes long as defined by cDefaultMinSaltStringLength");
      }
    }


    private static byte[] pPasswordToHash(Byte[] Salt, Byte[] Password, Int32 IterationCount) {
      return new PBKDF2_HMACSHA512(Password, Salt, IterationCount).GetDerivedKeyBytes_PBKDF2_HMACSHA512(cKeyLength);
    }


  }


  public class SaltTooShortException : Exception {
    public SaltTooShortException(string Message)
      : base(Message) {

    }
  }

  public class PasswordPolicyException : Exception {
    public PasswordPolicyException(string Message)
      : base(Message) {

    }
  }



  /// <summary>
  /// Implementation of the Rfc2898 PBKDF2 specification located here http://www.ietf.org/rfc/rfc2898.txt using HMACSHA512 as the underlying PRF
  /// Created by Ian Harris
  /// harro84@yahoo.com.au
  /// v1.0.0.0
  /// </summary>
  public class PBKDF2_HMACSHA512 {

    private HMACSHA512 HMACSHA512Obj;
    private Int32 hLen;
    private Byte[] P;
    private Byte[] S;
    private Int32 c;
    private Int32 dkLen;



    //Minimum rcommended itereations in Rfc2898
    public const int cMinIterations = 1;
    //Minimum recommended salt length in Rfc2898
    public const int cMinSaltLength = 4;


    /// <summary>
    /// Rfc2898 constructor to create Rfc2898 object ready to perform Rfc2898 functionality
    /// </summary>
    /// <param name="password">The Password to be hashed and is also the HMAC key</param>
    /// <param name="salt">Salt to be concatenated with the password</param>
    /// <param name="iterations">Number of iterations to perform HMACSHA Hashing for PBKDF2</param>
    public PBKDF2_HMACSHA512(Byte[] password, Byte[] salt, Int32 iterations) {
      if(iterations < cMinIterations) {
        throw new IterationsLessThanRecommended();
      }

      if(salt.Length < cMinSaltLength) {
        throw new SaltLessThanRecommended();
      }

      HMACSHA512Obj = new HMACSHA512(password);
      hLen = HMACSHA512Obj.HashSize / 8;
      P = password;
      S = salt;
      c = iterations;
    }


    /// <summary>
    /// Rfc2898 constructor to create Rfc2898 object ready to perform Rfc2898 functionality
    /// </summary>
    /// <param name="password">The Password to be hashed and is also the HMAC key</param>
    /// <param name="salt">Salt to be concatenated with the password</param>
    /// <param name="iterations">Number of iterations to perform HMACSHA Hashing for PBKDF2</param>
    public PBKDF2_HMACSHA512(string password, Byte[] salt, Int32 iterations)
      : this(new UTF8Encoding(false).GetBytes(password), salt, iterations) {

    }


    /// <summary>
    /// Rfc2898 constructor to create Rfc2898 object ready to perform Rfc2898 functionality
    /// </summary>
    /// <param name="password">The Password to be hashed and is also the HMAC key</param>
    /// <param name="salt">Salt to be concatenated with the password</param>
    /// <param name="iterations">Number of iterations to perform HMACSHA Hashing for PBKDF2</param>
    public PBKDF2_HMACSHA512(string password, string salt, Int32 iterations)
      : this(new UTF8Encoding(false).GetBytes(password), new UTF8Encoding(false).GetBytes(salt), iterations) {

    }



    /// <summary>
    /// Derive Key Bytes using PBKDF2 specification listed in Rfc2898 and HMACSHA512 as the underlying PRF (Psuedo Random Function)
    /// </summary>
    /// <param name="keyLength">Length in Bytes of Derived Key</param>
    /// <returns>Derived Key</returns>
    public Byte[] GetDerivedKeyBytes_PBKDF2_HMACSHA512(Int32 keyLength) {
      //no need to throw exception for dkLen too long as per spec because dkLen cannot be larger than Int32.MaxValue so not worth the overhead to check
      dkLen = keyLength;

      Double l = Math.Ceiling((Double)dkLen / hLen);

      Byte[] FinalBlock = new Byte[0];

      for(Int32 i = 1; i <= l; i++) {
        //Concatenate each block from F into the final block (T_1..T_l)
        FinalBlock = pMergeByteArrays(FinalBlock, F(P, S, c, i));
      }

      //returning DK note r not used as dkLen bytes of the final concatenated block returned rather than <0...r-1> substring of final intermediate block + prior blocks as per spec
      return FinalBlock.Take(dkLen).ToArray();

    }


    /// <summary>
    /// A static publicly exposed version of GetDerivedKeyBytes_PBKDF2_HMACSHA512 which matches the exact specification in Rfc2898 PBKDF2 using HMACSHA512
    /// </summary>
    /// <param name="P">Password passed as a Byte Array</param>
    /// <param name="S">Salt passed as a Byte Array</param>
    /// <param name="c">Iterations to perform the underlying PRF over</param>
    /// <param name="dkLen">Length of Bytes to return, an AES 256 key wold require 32 Bytes</param>
    /// <returns>Derived Key in Byte Array form ready for use by chosen encryption function</returns>
    public static Byte[] PBKDF2(Byte[] P, Byte[] S, Int32 c, Int32 dkLen) {
      PBKDF2_HMACSHA512 rfcObj = new PBKDF2_HMACSHA512(P, S, c);
      return rfcObj.GetDerivedKeyBytes_PBKDF2_HMACSHA512(dkLen);
    }


    //Main Function F as defined in Rfc2898 PBKDF2 spec
    private Byte[] F(Byte[] P, Byte[] S, Int32 c, Int32 i) {

      //Salt and Block number Int(i) concatenated as per spec
      Byte[] Si = pMergeByteArrays(S, INT(i));

      //Initial hash (U_1) using password and salt concatenated with Int(i) as per spec
      Byte[] temp = PRF(P, Si);

      //Output block filled with initial hash value or U_1 as per spec
      Byte[] U_c = temp;

      for(Int32 C = 1; C < c; C++) {
        //rehashing the password using the previous hash value as salt as per spec
        temp = PRF(P, temp);

        for(Int32 j = 0; j < temp.Length; j++) {
          //xor each byte of the each hash block with each byte of the output block as per spec
          U_c[j] ^= temp[j];
        }
      }

      //return a T_i block for concatenation to create the final block as per spec
      return U_c;
    }


    //PRF function as defined in Rfc2898 PBKDF2 spec
    private Byte[] PRF(Byte[] P, Byte[] S) {
      //HMACSHA512 Hashing, better than the HMACSHA1 in Microsofts implementation ;)
      return HMACSHA512Obj.ComputeHash(pMergeByteArrays(P, S));
    }


    //This method returns the 4 octet encoded Int32 with most significant bit first as per spec
    private Byte[] INT(Int32 i) {
      Byte[] I = BitConverter.GetBytes(i);

      //Make sure most significant bit is first
      if(BitConverter.IsLittleEndian) {
        Array.Reverse(I);
      }

      return I;
    }


    //Merge two arrays into a new array
    private Byte[] pMergeByteArrays(Byte[] source1, Byte[] source2) {
      //Most efficient way to merge two arrays this according to http://stackoverflow.com/questions/415291/best-way-to-combine-two-or-more-byte-arrays-in-c-sharp
      Byte[] Buffer = new Byte[source1.Length + source2.Length];
      System.Buffer.BlockCopy(source1, 0, Buffer, 0, source1.Length);
      System.Buffer.BlockCopy(source2, 0, Buffer, source1.Length, source2.Length);

      return Buffer;
    }


  }


  public class IterationsLessThanRecommended : Exception {
    public IterationsLessThanRecommended()
      : base("Iteration count is less than the 1000 recommended in Rfc2898") {

    }
  }


  public class SaltLessThanRecommended : Exception {
    public SaltLessThanRecommended()
      : base("Salt is less than the 8 byte size recommended in Rfc2898") {

    }
  }



}
