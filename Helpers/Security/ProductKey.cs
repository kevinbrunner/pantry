﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

using Pantry.Helpers;
using Pantry.Utilities;


namespace Pantry.Helpers.Security.ProductKeys {
  public enum AlphaMode {
    alpha,
    numeric,
    mixed
  }

  public class CreateAKey {
    private int len_ = 20;
    private AlphaMode alphaMode_ = AlphaMode.mixed;
    private int dashPlacement_ = 5;
    private string createdKey_ = "";


    /// <summary>
    /// Creates alpha-numeric ID 4 sets of 5 separated with dashes
    /// </summary>
    public CreateAKey() {
      CreateNewKey();
    }

    public string Key {
      get {
        return createdKey_;
      }
    }


    /// <summary>
    /// returns an ID with the specified length
    /// </summary>
    /// <param name="len"></param>
    public CreateAKey(int len) {
      len_ = len;
      CreateNewKey();
    }

    
    /// <summary>
    /// returns an ID with the specified length, AlphaMode 
    /// </summary>
    /// <param name="len"></param>
    /// <param name="alphaMode "></param>
    public CreateAKey(int len, AlphaMode alphaMode) {
      len_ = len;
      alphaMode_ = alphaMode;
      CreateNewKey();
    }


    /// <summary>
    /// returns an ID with the specified length, AlphaMode, dashPlace is the int placement of a "-"
    /// </summary>
    /// <param name="len"></param>
    /// <param name="alphaMode "></param>
    /// <param name="dashPlace"></param>
    public CreateAKey(int len, AlphaMode alphaMode, int dashPlace) {
      len_ = len;
      alphaMode_ = alphaMode;
      dashPlacement_ = dashPlace;
      CreateNewKey();
    }


    private void CreateNewKey() {
      if(len_ < 1) { len_ = 5; }
      if(dashPlacement_ < 0 || dashPlacement_ > len_) { dashPlacement_ = 0; }

      createdKey_ = "";
      Random rand = new Random(DateTime.Now.Millisecond);
      double randNumb = 0;
      int tablecheck = 0;
      StringBuilder t = new StringBuilder();
      int j = 0;

      do {
        for(int i = 0; i < len_; ++i) { // create a random alphanumric string, get decimal part
          randNumb = rand.NextDouble();
          j = 0;

          switch(alphaMode_) {
            case AlphaMode.alpha:
              do { 
                j = Convert.ToInt32(((randNumb * 26) + 65));
                randNumb = rand.NextDouble();
              } while(j == 73 || j == 79); // not "I" or "O"
              t.Append(Convert.ToChar(j));
              break;

            case AlphaMode.numeric:
              t.Append(Math.Floor(randNumb * 10)); // create a random number
              break;

            case AlphaMode.mixed:
              if(randNumb > 0.33) { // 2/3 of time make an alpha. 
                do { 
                  j = Convert.ToInt32(((randNumb * 26) + 65));
                  randNumb = rand.NextDouble();
                } while(j == 73 || j == 79); // not "I" or "O"
                t.Append(Convert.ToChar(j));
              }  else { // 1/3 of time make a number
               t.Append(Math.Floor(randNumb * 10)); 
              } 
              break;

          }
        }

        createdKey_ = t.ToString();
        t = new StringBuilder();

        if(dashPlacement_ > 0) {
          for(int i = 0; i < createdKey_.Length; ++i) {
            if(i % dashPlacement_ == 0) { t.Append("-"); } // add a dash every "HasDash" characters
            t.Append(createdKey_.Substring(i, 1));
          }
          createdKey_ = t.ToString().Substring(1);
        }

      }
      while(tablecheck != 0);

    }


  }


  public class ProductKey {
    private int len;
    private int isAlphaNum;
    private int dashPlacement_;
    private string emailAddress_;
    private string contentID_;
    private string createdKey_;
    private Nullable<bool> verifiedKey;


    public string CreateKey {
      get {
        if(createdKey_.IsEmpty()) {
          throw new NullReferenceException();
        }
        return createdKey_;
      }
    }


    public bool VerifyKey {
      get {
        if(verifiedKey == null) {
          throw new NullReferenceException();
        }
        return (bool)verifiedKey;
      }
    }


    /// <summary>
    /// Verifies Key against ContentID and EmailAddress
    /// </summary>
    /// <param name="Name"></param>
    /// <param name="key"></param>
    public ProductKey(string emailAddress, string contentID, string key) {
      len = 20;
      isAlphaNum = 2;
      createdKey_ = key.ToUpper().Trim();
      emailAddress_ = emailAddress.ToLower().Trim();
      contentID_ = contentID.ToUpper().Trim();

      if(createdKey_.Length < 29) {
        throw new ArgumentException("Key incorrect length");
      }

      if(emailAddress_.IsEmpty()) {
        throw new ArgumentException("Email Address required");
      }

      if(contentID_.IsEmpty()) {
        throw new ArgumentException("ContentID required");
      }

      VerifyNameKey();
    }


    /// <summary>
    /// Creates default key from name, 20 AlphaNumerics, dashes every 5 characters
    /// </summary>
    /// <param name="Name"></param>
    public ProductKey(string EmailAddress, string ContentID) {
      isAlphaNum = 2;
      emailAddress_ = EmailAddress.ToLower();
      contentID_ = ContentID.ToUpper();

      if(emailAddress_.IsEmpty()) {
        throw new ArgumentException("Email Address required");
      }

      if(contentID_.IsEmpty()) {
        throw new ArgumentException("ContentID required");
      }

      CreateNewNameKey();
    }


    private void CreateNewNameKey() {
      string appName = contentID_ + "-" + ShaMd5Hasher.ComputeMD5HexHash(emailAddress_);
      len = 20; // must use 20 in order to "check sum"
      dashPlacement_ = 5; // must use 5 in order to "check sum"
      int keyValue = 0;

      for(int i = 0; i < appName.Length; ++i) {
        keyValue += Convert.ToInt16(Convert.ToChar((appName.Substring(i, 1))));
      } // sum of ascii character values of the ActivationString

      CreateAKey key = new CreateAKey(len, AlphaMode.mixed, dashPlacement_);
      string partKey = key.Key + "-";

      int genValue = CreateCheckSumValue(partKey, appName);

      // the 'value' of the first part of the code times the 'value' of the program ActivationString, limited to 5 charachters. 
      // "83416" is to make sure the result is at least 5 chars.
      createdKey_ = partKey + (Convert.ToString((genValue * keyValue)) + "83416").Substring(0, 5);
    }


    private void VerifyNameKey() {
      string key = createdKey_;

      string[] keySplit = key.Split('-');
      string appName = contentID_ + "-" + ShaMd5Hasher.ComputeMD5HexHash(emailAddress_);

      int keyValue = 0;
      string keyCheckSum = "";

      for(int i = 0; i < appName.Length; i++) {
        keyValue += Convert.ToInt32(Convert.ToChar((appName.Substring(i, 1))));
      } // sum of ascii character values of the ActivationString


      if(keySplit[4].Length >= 5) {
        keyCheckSum = keySplit[4].Substring(0, 5);
      }
      keySplit[4] = ""; // trim off the authorization part of the key

      string partKey = String.Join("-", keySplit);

      int genValue = CreateCheckSumValue(partKey, appName);

      string calcCheckSum = (Convert.ToString(keyValue * genValue) + "83416").Substring(0, 5);

      verifiedKey = (keyCheckSum == calcCheckSum);
    }


    private int CreateCheckSumValue(string createdKey, string name) {
      int genValue = 0;

      for(int i = 0; i < createdKey.Length; ++i) {
        if(i < name.Length) { // creates a number based on the first sections. adds or takes depending on stuff.  Makes it mathematicaly harder to re-order the code.
          // sum the ascii values of the characters in the key
          if(!getPlusMinus(name.Substring(i, 1))) {
            genValue += Convert.ToInt32(Convert.ToChar(createdKey.Substring(i, 1)));
          } else {
            genValue -= Convert.ToInt32(Convert.ToChar(createdKey.Substring(i, 1)));
          }
        } else {
          if(i % 2 == 0) {
            genValue -= Convert.ToInt32(Convert.ToChar(createdKey.Substring(i, 1)));
          } else {
            genValue += Convert.ToInt32(Convert.ToChar(createdKey.Substring(i, 1)));
          }
        }
      }

      return Math.Abs(genValue);
    }


    private bool getPlusMinus(string letter) {
      return Convert.ToInt32(Convert.ToChar(letter)) - 65 < 12 ? true : false;
    }


  }


  // a stronger version would have 7 sets of 5 alpha/num.  2 of these set's numbers would be the hash.  If the hash number 1-3 the hash sets are in place 1 & 7, if 4-6 then in place 1 & 5. for place 5 & 7 the first set's value must be an alpha.

  //   1    2     3     4     5     6     7
  //26B41-XWES2-V422I-372GY-28628-9AFY9-162OC hash is 1&7 = 2641 & 1620 = 26411620 = 26411

  //5M3LO-7R9Q0-44OQ2-25J5I-27419-3M6NB-7K30C hash is 1&5 = 53 & 27419 = 5327419 = 53274

  //SUM7T-8811V-B74H1-8SP1R-30750-R5VA1-EO5G2 hash is 5&7 = 30750 & 52 = 3075052 = 30750

}
