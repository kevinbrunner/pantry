﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml;

using Pantry.Config;
using Pantry.Entities;
//Microsoft.Win32.RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(@"Software\xyz");


namespace Pantry.Helpers.Security {

  public class HashedEmail { // for FOAF and SIOC email searching... save this value in a hidden field beside the name in a blog or forum post.
    private HashedEmail() { }
    private string hashedEmail_;
    public HashedEmail(string emailAddress) {
      hashedEmail_ = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(emailAddress.ToLowerInvariant().Trim(), "sha1").ToLowerInvariant();
    }

    public string HashedAddress {
      get { return hashedEmail_; }
    }

  }


  public class SecureCard {
    public SecureCard() { }


    private bool isDecrypted = false;
    private bool isEncrypted = false;
    private string cardNameOn_;
    private string cardNumber_;
    private string expiryM_;
    private string expiryY_;
    private bool useCardToken_;
    private string encryptedData_;
    private string salt_;
    private XmlDocument xmlCardData;
    private int cardEncrtypPassword_;


    /// <summary>
    /// encrypts the string you send
    /// </summary>
    /// <param name="encryptedData"></param>
    public SecureCard(string encryptedData, string salt, int cardEncrtypPassword) {
      // constructor for use with encrypted data
      encryptedData_ = encryptedData;
      salt_ = salt;
      cardEncrtypPassword_ = cardEncrtypPassword;
      DecryptData();
    }


    /// <summary>
    /// Encrypts credit card data
    /// </summary>
    /// <param name="CardNameOn"></param>
    /// <param name="CardNumber"></param>
    /// <param name="Expiry Mounth"></param>
    /// <param name="Expiry Year"></param>
    /// <param name="CardID"></param>

    public SecureCard(string nameOnCard, string cardNumber, string expiryM, string expiryY, string salt, int cardEncrtypPassword) {
      cardNameOn_ = nameOnCard;
      cardNumber_ = cardNumber;
      expiryM_ = expiryM;
      expiryY_ = expiryY;
      salt_ = salt;
      cardEncrtypPassword_ = cardEncrtypPassword;
      EncryptData();
    }


    private void CreateXml() {
      xmlCardData = new XmlDocument();
      XmlElement documentRoot = xmlCardData.CreateElement("CardDetails");
      XmlElement child;

      child = xmlCardData.CreateElement("NameOnCard");
      child.InnerXml = cardNameOn_;
      documentRoot.AppendChild(child);

      child = xmlCardData.CreateElement("CardNumber");
      child.InnerXml = cardNumber_;
      documentRoot.AppendChild(child);

      child = xmlCardData.CreateElement("ExpiryM");
      child.InnerXml = expiryM_;
      documentRoot.AppendChild(child);

      child = xmlCardData.CreateElement("ExpiryY");
      child.InnerXml = expiryY_;
      documentRoot.AppendChild(child);

      child = xmlCardData.CreateElement("UseCardToken");
      child.InnerXml = ""; //AppConfig.UseCardToken.ToString();
      documentRoot.AppendChild(child);

      xmlCardData.AppendChild(documentRoot);
    }


    private void ExtractXml() {
      cardNameOn_ = xmlCardData.GetElementsByTagName("NameOnCard").Item(0).InnerXml;
      cardNumber_ = xmlCardData.GetElementsByTagName("CardNumber").Item(0).InnerXml;
      expiryM_ = xmlCardData.GetElementsByTagName("ExpiryM").Item(0).InnerXml;
      expiryY_ = xmlCardData.GetElementsByTagName("ExpiryY").Item(0).InnerXml;
        useCardToken_ = Convert.ToBoolean(xmlCardData.GetElementsByTagName("UseCardToken").Item(0).InnerXml);
    }


    private void EncryptData() {
      int j = 0;
      if(!int.TryParse(expiryM_, out j)) {
        throw new SecureCardException("CreditCard Error: no expire month");
      }

      if(!int.TryParse(expiryY_, out j)) {
        throw new SecureCardException("CreditCard Error: no expire year");
      }


      if(cardNameOn_.Length < 3) {
        throw new Exception("CreditCard Error: no name on card");
      }

      try {
        CreateXml();
        encryptedData_ = StringEncryptor.Encrypt(xmlCardData.OuterXml, salt_, cardEncrtypPassword_);
        isEncrypted = true;
        isDecrypted = true;
      }
      catch {
        throw new SecureCardException("CreditCard Error: Unable to encrypt data.");
      }
    }


    private void DecryptData() {
      try {
        xmlCardData = new XmlDocument();
        xmlCardData.InnerXml = StringEncryptor.Decrypt(encryptedData_, salt_, cardEncrtypPassword_);
        ExtractXml();
        isDecrypted = true;
      }
      catch {
        throw new SecureCardException("CreditCard Error: Unable to decrypt data.");
      }
    }


    public string NameOnCard {
      get {
        if(isDecrypted) {
          return cardNameOn_;
        } else {
          throw new SecureCardException("CreditCard Error: Data not decrypted.");
        }
      }
    }


    public string CardNumber {
      get {
        if(isDecrypted) {
          return cardNumber_;
        } else {
          throw new SecureCardException("CreditCard Error: Data not decrypted.");
        }
      }
    }


    //public string CardNumberX {
    //  get {
    //    if(isDecrypted) {
    //      string cNumber = cardNumber_;
    //      if(cNumber.Length < 6) {
    //        cNumber = "Card Error";
    //      } else {
    //        cNumber = cardNumber_.Substring(0, 2) + cardNumber_.Substring(cardNumber_.Length - 4, 4);
    //      }
    //      return cNumber;
    //    } else {
    //      throw new SecureCardException("CreditCard Error: Data not decrypted.");
    //    }
    //  }
    //}


    public string FirstSix {
      get {
        if(isDecrypted) {
          return cardNumber_.Substring(0, 6);
        } else {
          throw new SecureCardException("CreditCard Error: Data not decrypted.");
        }
      }
    }


    public string LastFour {
      get {
        if(isDecrypted) {
          return cardNumber_.Substring(cardNumber_.Length - 4, 4);
        } else {
          throw new SecureCardException("CreditCard Error: Data not decrypted.");
        }
      }
    }


    public string ExpiryM {
      get {
        if(isDecrypted) {
          return expiryM_;
        } else {
          throw new SecureCardException("CreditCard Error: Data not decrypted.");
        }
      }
    }


    public string ExpiryY {
      get {
        if(isDecrypted) {
          return expiryY_;
        } else {
          throw new SecureCardException("CreditCard Error: Data not decrypted.");
        }
      }
    }


    public string EncryptedData {
      get {
        if(isEncrypted) {
          return encryptedData_;
        } else {
          throw new SecureCardException("CreditCard Error: Data not decrypted.");
        }
      }
    }


  }


  public class SecureCardException : Exception {
    public SecureCardException(string message)
      : base(message) {
    }
  }


  public static class StringEncryptor {

    public static string defaultSalt = "TYqeAOeiCZ0Pqu8mxVulwA==";
    public static string tempSalt = "AFqfAOeiCZ0pQu8mxVulwB==";

    public static string Encrypt(string sourceData) {
      return Encrypt(sourceData, defaultSalt);
    }

    public static string Encrypt(string sourceData, bool useTempSalt, string salt = "", int cardEncrtypPassword = 0) {
      if(useTempSalt) {
        salt = tempSalt;
      }

      return Encrypt(sourceData, salt);
    }

    public static string Encrypt(string sourceData, string salt, int cardEncrtypPassword = 1) {
      string returnValue = "";
      var key = new byte[16];
      var iv = new byte[16];

      //string WhichCardEncrtypPassword = "";

      //if(cardEncrtypPassword.ToString() == AppConfig.CardEncrtypPassword0.Substring(0, 1)) {
      //  WhichCardEncrtypPassword = SiteConfig.CardEncrtypPassword0;
      //}

      //if(cardEncrtypPassword.ToString() == AppConfig.CardEncrtypPassword1.Substring(0, 1)) {
      //  WhichCardEncrtypPassword = AppConfig.CardEncrtypPassword1;
      //}

      //if(String.IsNullOrWhiteSpace(WhichCardEncrtypPassword)) {
      //  throw new StringEncryptorException("Bad encrypt password");
      //}

      //int saltScrew = WhichCardEncrtypPassword.Length;

      //while(saltScrew > salt.Length) {
      //  saltScrew = saltScrew / 2;
      //}

      //string newSalt = salt.Substring(saltScrew) + salt.Substring(0, saltScrew);

      salt = salt.Substring(10) + salt.Substring(0, 10);
      string _password = ""; //AppConfig.CardEncrtypPassword;


      try {
        var saltBytes = Encoding.Default.GetBytes(salt);
        GetKeyAndIVFromPasswordAndSalt(_password, saltBytes, SymmetricAlgorithm.Create("AES"), ref key, ref iv);

        returnValue = Convert.ToBase64String(Encrypt(sourceData, key, iv));
      }
      catch {
        throw new StringEncryptorException("Unable to encrypt data.");
      }

      return returnValue;
    }


    public static string Decrypt(string sourceData) {
      return Decrypt(sourceData, defaultSalt);
    }


    public static string Decrypt(string sourceData, bool useTempSale, string salt = "") {
      if(useTempSale) {
        salt = tempSalt;
      }

      return Decrypt(sourceData, salt, 1);
    }


    public static string Decrypt(string sourceData, string salt, int cardEncrtypPassword = 1) {
      string returnValue = "";
      var key = new byte[16];
      var iv = new byte[16];

      salt = salt.Substring(10) + salt.Substring(0, 10);
      string _password = ""; // AppConfig.CardEncrtypPassword;

      if(cardEncrtypPassword == 1) {
        _password = ""; //AppConfig.CardEncrtypPassword1;
      }

      try {
        var saltBytes = Encoding.Default.GetBytes(salt);
        GetKeyAndIVFromPasswordAndSalt(_password, saltBytes, SymmetricAlgorithm.Create("AES"), ref key, ref iv);

        byte[] encryptedDataBytes = Convert.FromBase64String(sourceData);

        returnValue = Decrypt(encryptedDataBytes, key, iv);
      }
      catch {
        throw new StringEncryptorException("CreditCard Error: Unable to encrypt data.");
      }

      return returnValue;
    }


    private static void GetKeyAndIVFromPasswordAndSalt(string password, byte[] salt, SymmetricAlgorithm symmetricAlgorithm, ref byte[] key, ref byte[] iv) {
      var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, salt);
      key = rfc2898DeriveBytes.GetBytes(symmetricAlgorithm.KeySize / 8);
      iv = rfc2898DeriveBytes.GetBytes(symmetricAlgorithm.BlockSize / 8);
    }


    public static byte[] Encrypt(string clearText, byte[] key, byte[] iv) {
      var clearTextBytes = System.Text.Encoding.Default.GetBytes(clearText);
      var rijndael = new RijndaelManaged();
      var outputStream = new MemoryStream();

      var inputStream = new CryptoStream(outputStream, rijndael.CreateEncryptor(key, iv), CryptoStreamMode.Write);

      inputStream.Write(clearTextBytes, 0, clearText.Length);
      inputStream.FlushFinalBlock();

      return outputStream.ToArray();
    }


    public static string Decrypt(byte[] cipherText, byte[] key, byte[] iv) {
      var rijndael = new RijndaelManaged();
      var transform = rijndael.CreateDecryptor(key, iv);
      var outputStream = new MemoryStream(cipherText);

      var inputStream = new CryptoStream(outputStream, transform, CryptoStreamMode.Read);

      return new StreamReader(inputStream).ReadToEnd();
    }


  }


  public class StringEncryptorException : Exception {
    public StringEncryptorException(string message)
      : base(message) {
    }
  }



  public class PasswordStrength {

    //private PasswordStrength() { }
    //private bool passedCheck;

    //public PasswordStrength(string pw) {
    //  passedCheck = PassedCheck(pw);
    //}

    //public bool HashedAddress {
    //  get { return passedCheck; }
    //}


    public static Entities.PasswordStrength Check(string passWord) {
      int passwordScore = passWord.Length;

      if(passwordScore > 5) {
        int alphas = 0;
        int uppers = 0;
        int digits = 0;
        int puncts = 0;

        if(passwordScore < 13) {
          bool isPunct = true;
          byte[] pwAsciiBytes = System.Text.Encoding.ASCII.GetBytes(passWord);

          foreach(byte charVal in pwAsciiBytes) {
            int charInt = (int)charVal;
            isPunct = true;

            if(charInt > 96 && charInt < 123) { // lowercase
              alphas += 1;
              isPunct = false;
            }

            if(charInt > 64 && charInt < 91) { // uppercase
              uppers += 1;
              isPunct = false;
            }

            if(charInt > 47 && charInt < 58) { // numbers
              digits += 1;
              isPunct = false;
            }

            if(isPunct) {
              puncts += 2;
            }
          }

          if(alphas > 1) { alphas = 2; }
          if(uppers > 1) { uppers = 2; }
          if(digits > 1) { digits = 2; }
          if(puncts > 3) { puncts = 4; }

          passwordScore += alphas + uppers + digits + puncts;

        }

      }

      Entities.PasswordStrength theStrength = Entities.PasswordStrength.Fail;

      if(passwordScore >= 8) {
        theStrength = Entities.PasswordStrength.Poor;
      }

      if(passwordScore >= 10) {
        theStrength = Entities.PasswordStrength.Okay;
      }

      if(passwordScore == 12) {
        theStrength = Entities.PasswordStrength.Good;
      }

      if(passwordScore >= 13) {
        theStrength = Entities.PasswordStrength.Better;
      }

      return theStrength;
    }


  }


  public static class Tools {
    public static string CreateSalt() {
      Byte[] Salt = new byte[64];
      RandomNumberGenerator.Create().GetBytes(Salt);

      return Salt.HashBytesToHexString();

      //RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
      //byte[] buff = new byte[32]; // could have been 16 or 24
      //rng.GetBytes(buff);

      //return Convert.ToBase64String(buff);
    }


  }


  public static class SecureStringTools {
    public static string GetRandomString(int length, RandomStringType type) {
      Random random = new Random((int)DateTime.Now.Ticks);
      return getRandomString(random, length, type);
    }


    public static string GetRandomString(int length, int randomSeedValue, RandomStringType type) {
      Random random = new Random(randomSeedValue * (int)DateTime.Now.Ticks);
      return getRandomString(random, length, type);
    }


    private static string getRandomString(Random random, int length, RandomStringType type) {
      string chars = String.Empty;

      switch(type) {
        case RandomStringType.Alphanumeric:
          chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
          break;

        case RandomStringType.AlphanumericAndSymbols:
          chars = "abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*+=?|";
          break;

        case RandomStringType.AlphaOnly:
          chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
          break;


        case RandomStringType.NumericOnly:
          chars = "0123456789";
          break;

        case RandomStringType.CouponSafe:
          chars = "ZBCDF3GHJ2K5LMN7PQY6RST4VW89X";
          break;

      }

      char[] stringChars = new char[length];

      for(int i = 0; i < stringChars.Length; i++) {
        stringChars[i] = chars[random.Next(chars.Length)];
      }

      return new String(stringChars);

    }
  }

}
