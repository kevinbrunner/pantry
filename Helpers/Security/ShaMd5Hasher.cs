﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;


namespace Pantry.Helpers.Security {
  public static class ShaMd5Hasher {
    //private static SHA1Managed hasher1 = new SHA1Managed();
    //private static SHA256Managed hasher256 = new SHA256Managed();
    //private static SHA384Managed hasher384 = new SHA384Managed();
    //private static MD5CryptoServiceProvider hasherMD5 = new MD5CryptoServiceProvider();
    private static SHA512Managed hasher512 = new SHA512Managed();
    private static MD5 hasherMD5 = new MD5CryptoServiceProvider();


    /// <summary>
    /// password hasher
    /// </summary>
    /// <param name="stringToHash"></param>
    /// <returns>hashed password</returns>
    public static string HashSha512(string stringToHash) {
      // convert password to byte array
      byte[] hashedStringBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(stringToHash);

      // generate hash from byte array of password
      byte[] hashedString = hasher512.ComputeHash(hashedStringBytes);

      // convert hash to string
      return Convert.ToBase64String(hashedString, 0, hashedString.Length);
    }


    public static string HashMD5(string plainText) {
      // convert password to byte array
      byte[] plainTextBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(plainText);

      // generate hash from byte array of password
      byte[] plainTextHash = hasherMD5.ComputeHash(plainTextBytes);

      // convert hash to string
      return Convert.ToBase64String(plainTextHash, 0, plainTextHash.Length);
    }

    private static MD5 hash = new MD5CryptoServiceProvider();


    public static string MD5FileNameHasher(string value) {
      //value = "the quick brown fox jumped over the fence"; value = (value + value).Replace(" ", "");

      value = value.ToLower();

      // Use input string to calculate MD5 hash
      MD5 md5 = System.Security.Cryptography.MD5.Create();
      byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(value);
      byte[] hashBytes = md5.ComputeHash(inputBytes);

      // Convert the byte array to hexadecimal string
      StringBuilder sb = new StringBuilder();
      for(int i = 0; i < hashBytes.Length; i++) {
        sb.Append(hashBytes[i].ToString("X2"));
      }

      value = sb.ToString();
      Regex alpha = new Regex(@"[^a-zA-Z0-9_\-]");
      value = alpha.Replace(value, "");

      return value;

    }


    public static string FileNameHasher(string value) {
      //value = "the quick brown fox jumped over the fence"; value = (value + value).Replace(" ", "");

      value = value.ToLower();

      Regex alpha;

      //alpha = new Regex(@"[^a-zA-Z0-9_\-]");
      //value = alpha.Replace(value, "");

      alpha = new Regex(@"[^aeiouy-]");
      alpha = new Regex(@"[^aeiou]");
      value = alpha.Replace(value, "");

      return value;

    }


    public static string ComputeMD5HexHash(string plainText) {
      // Convert plain text into a byte array.
      // byte[] plainTextBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(plainText);
      byte[] plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);

      //HashAlgorithm hash = new MD5CryptoServiceProvider();

      // Compute hash value of our plain text.
      byte[] hashBytes = hash.ComputeHash(plainTextBytes);

      // Convert result into a HEX string.
      StringBuilder temp = new StringBuilder();
      for(int i = 0; i < hashBytes.Length; i++) {
        temp.Append(hashBytes[i].ToString("X2"));
      }

      string hashValue = temp.ToString();

      // Return the result.
      return hashValue;
    }

    /// <summary>
    /// Creates an MD5 check sum for the file.
    /// </summary>
    /// <param name="file"></param>
    /// <returns></returns>
    public static string FileCreateCheckSum(string file) {
      MD5 md5 = MD5.Create();
      using(FileStream fs = System.IO.File.OpenRead(file)) {
        byte[] checksum = md5.ComputeHash(fs);
        return (BitConverter.ToString(checksum).Replace("-", String.Empty));
      }
    }


    /// <summary>
    /// Encrypts a string to using MD5 algorithm
    /// </summary>
    /// <param name="val"></param>
    /// <returns>string representation of the MD5 encryption</returns>
    public static string MD5String(this string val) {
      MD5 md5Hasher = MD5.Create();
      byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(val));

      StringBuilder sBuilder = new StringBuilder();
      for(int i = 0; i < data.Length; i++) {
        sBuilder.Append(data[i].ToString("x2"));
      }
      return sBuilder.ToString();
    }


    /// <summary>
    /// Verifies the string against the encrypted value for equality
    /// </summary>
    /// <param name="val"></param>
    /// <param name="hash">The encrypted value of the string</param>
    /// <returns>true is the given string is equal to the string encrypted</returns>
    public static bool VerifyMD5String(this string val, string hash) {
      string hashOfInput = MD5String(val);
      StringComparer comparer = StringComparer.OrdinalIgnoreCase;
      return 0 == comparer.Compare(hashOfInput, hash) ? true : false;
    }


    /// <summary>
    /// Calculates a files CheckSum
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public static string GetMD5HashFromFile(string fileName) {
      FileStream file = new FileStream(fileName, FileMode.Open);
      MD5 md5 = new MD5CryptoServiceProvider();
      byte[] retVal = md5.ComputeHash(file);
      file.Close();

      StringBuilder sb = new StringBuilder();
      for(int i = 0; i < retVal.Length; i++) {
        sb.Append(retVal[i].ToString("x2"));
      }
      return sb.ToString();
    }


  }
}
