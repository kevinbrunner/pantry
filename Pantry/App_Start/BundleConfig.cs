﻿using System.Web;
using System.Web.Optimization;

//using dotless;

namespace Pantry.Web {
  public class BundleConfig {
    // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
    public static void RegisterBundles(BundleCollection bundles) {
      //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
      //            "~/Scripts/jquery-{version}.js"));

      //bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
      //            "~/Scripts/jquery-ui-{version}.js"));

      bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                  "~/Scripts/jquery.unobtrusive*",
                  "~/Scripts/jquery.validate*"
                  ));

      // Use the development version of Modernizr to develop with and learn from. Then, when you're
      // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
      bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                  "~/Content/Scripts/modernizr-*"));

      bundles.Add(new StyleBundle("~/Styles/bootstrap").Include(
        "~/Content/Styles/bootstrap.css",
        "~/Content/Styles/font-awesome.css",
        "~/Content/Styles/ionicons.css",
        "~/Content/Styles/morris/morris.css",
        "~/Content/Styles/jvectormap/jquery-jvectormap-1.2.2.css",
        "~/Content/Styles/datepicker/datepicker3.css",
        "~/Content/Styles/daterangepicker/daterangepicker-bs3.css",
        "~/Content/Styles/bootstrap-wysihtml5/bootstrap3-wysihtml5.css",
        "~/Content/Styles/AdminLTE.css"
      ));

      bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/Styles/default.css"));

      bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
        "~/Content/themes/base/jquery.ui.core.css",
        "~/Content/themes/base/jquery.ui.resizable.css",
        "~/Content/themes/base/jquery.ui.selectable.css",
        "~/Content/themes/base/jquery.ui.accordion.css",
        "~/Content/themes/base/jquery.ui.autocomplete.css",
        "~/Content/themes/base/jquery.ui.button.css",
        "~/Content/themes/base/jquery.ui.dialog.css",
        "~/Content/themes/base/jquery.ui.slider.css",
        "~/Content/themes/base/jquery.ui.tabs.css",
        "~/Content/themes/base/jquery.ui.datepicker.css",
        "~/Content/themes/base/jquery.ui.progressbar.css",
        "~/Content/themes/base/jquery.ui.theme.css"
      ));

      bundles.Add(new ScriptBundle("~/Scripts/bootstrap").Include(
        "~/Content/Scripts/jquery-2.0.2.js",
        "~/Content/Scripts/jquery.unobtrusive*",
        "~/Content/Scripts/jquery.validate*",
        "~/Content/Scripts/jquery-ui-1.10.3.js",
        "~/Content/Scripts/jquery.extra.js",
        "~/Content/Scripts/jsrender.js",
        "~/Content/Scripts/bootstrap.js",
        "~/Content/Scripts/raphael-min.js",
        "~/Content/Scripts/plugins/morris/morris.js",
        "~/Content/Scripts/plugins/sparkline/jquery.sparkline.js",
        "~/Content/Scripts/plugins/jvectormap/jquery-jvectormap-1.2.2.js",
        "~/Content/Scripts/plugins/jvectormap/jquery-jvectormap-world-mill-en.js",
        "~/Content/Scripts/plugins/jqueryKnob/jquery.knob.js",
        "~/Content/Scripts/plugins/daterangepicker/daterangepicker.js",
        "~/Content/Scripts/plugins/datepicker/bootstrap-datepicker.js",
        "~/Content/Scripts/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js",
        "~/Content/Scripts/plugins/iCheck/icheck.js",
        "~/Content/Scripts/AdminLTE/app.js",
        "~/Content/Scripts/AdminLTE/dashboard.js",
        //"~/Content/Scripts/AdminLTE/demo.js",
        "~/Content/Scripts/Default.js",
        "~/Content/Scripts/spin.js"
      ));

      bundles.Add(new ScriptBundle("~/Scripts/angularjs").Include(
        "~/Content/Scripts/angular.min.js",
        "~/Content/Scripts/DefaultAngular.js"
      ));

    }


  }
}