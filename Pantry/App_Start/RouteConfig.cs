﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Pantry.Web {
  public class RouteConfig {
    public static void RegisterRoutes(RouteCollection routes) {
      routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

      routes.MapRoute(
        name: "Merge",
        url: "Client/Merge/{client1}/{client2}",
        defaults: new { controller = "Client", action = "Merge" }
       );

      routes.MapRoute(
        name: "DeleteClient",
        url: "Client/DeleteMember",
        defaults: new { controller = "Client", action = "DeleteMember" }
       );

      routes.MapRoute(
        name: "DefaultClient",
        url: "Client/{id}",
        defaults: new { controller = "Client", action = "Index" }
       );

      routes.MapRoute(
          name: "Default",
          url: "{controller}/{action}/{id}",
          defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
      );
    }
  }
}