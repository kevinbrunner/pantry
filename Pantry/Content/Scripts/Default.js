﻿/// <reference path="../jquery/jquery.js" />
/// <reference path="../jquery/jquery.extra.js" />

// add here for jslint checking
/*jslint bitwise: true, undef: true, browser: true, continue: true, debug: true, sloppy: true, eqeq: true, vars: true, evil: true, white: true, nomen: true, plusplus: true, regexp: true, maxerr: 100, indent: 2 */
//var Modular = {}, pantry = {}, jwColors = {}, jQuery = {};

data = { 'd': [{}], 'i': [{}] };
var membership = {};

var loadingMsgTimer = '';
//var domain = domain || '';
var jwColors = {};

// Digital Juice Default Javascript Library.
var pantry = pantry || {};

pantry.Globals = {
  Domain: "",
  ContentID: 0,
  VisitorId: '',
  IsLoggedIn: false,
  IsAdmin: false,
  AjaxPostTimeout: 1000000 // to be set to 2000 on launch
}

pantry.clearCursorWait = function () {
  document.body.style.cursor = 'auto';
};


//pantry.getParameterByName = function (name, location_, default_, getHash) {
//  if (location_ == "" || !location_) {
//    location_ = window.location.href;
//  }
//  if (!default_) { default_ = ""; }
//  var regexS = "";
//  if (getHash) {
//    regexS = "[\\#&]" + name + "=([^&?]*)";
//  }
//  else {
//    regexS = "[\\?&]" + name + "=([^&#]*)";
//  }
//  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
//  var regex = new RegExp(regexS);
//  var results = regex.exec(location_);
//  if (results == null) {
//    return default_;
//  }
//  else {
//    return decodeURIComponent(results[1].replace(/\+/g, " "));
//  }
//};

//pantry.getHashStringParameterByName = function (name, default_) { return pantry.getParameterByName(name, default_, true); };
//pantry.getQueryStringParameterByName = function (name, default_) { return pantry.getParameterByName(name, default_, false); };

pantry.qAndLoad = function (qDel, qAdd, isHash) {
  var isQuery = "";
  if (!isHash) { isQuery = "?"; } else { isQuery = "#"; }
  if (qDel.indexOf("=") < 0) { qDel = qDel + "="; }
  var newHashStr = [];
  var qDeli = qDel.length;
  var urlLoc = window.location.href.substring(location.href.indexOf(isQuery) + 1); // query string or hash
  var i;
  if (isQuery == "?" && urlLoc.indexOf("#") > 0) {
    urlLoc = urlLoc.substring(0, urlLoc.indexOf("#"));
  }
  if (urlLoc.length > 0) {
    newHashStr = urlLoc.split('&');
    if (newHashStr.length > 0) {
      for (i = 0; i < newHashStr.length; i++) {
        if (newHashStr[i].substring(0, qDeli) == qDel) {
          newHashStr.splice(i, 1);
          --i;
        }
      }
    }
  }

  if (qAdd.length > 0) { newHashStr.push(qDel + qAdd); }

  urlLoc = window.location.href;
  if (urlLoc.indexOf(isQuery) > 0) {
    urlLoc = urlLoc.substring(0, urlLoc.indexOf(isQuery));
  }

  if (newHashStr.length > 0) {
    isQuery = isQuery + newHashStr.join("&");
  }

  if (isQuery.length > 1) {
    urlLoc = urlLoc + isQuery;
  }
  else {
    urlLoc = urlLoc + isQuery + qDel;
  }

  if (window.location.href != urlLoc) {
    window.location.href = urlLoc;
  }

  return false;
};


pantry.RegXEmail = function () { return /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~])+)*))@((([a-z]|\d)|(([a-z]|\d)([a-z]|\d|-|\.|_|~)*([a-z]|\d)))\.)+(([a-z])|(([a-z])([a-z]|\d|-|\.|_|~)*([a-z])))\.?$/ }; //{ return /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,6}|[0-9]{1,3})(\]?)$/; };
pantry.RegXAllow = function () { return new RegExp(/^([a-zA-Z0-9\s_\-']{1,20})$/); };
pantry.RegXIllegal = function () { return new RegExp(/[a-zA-Z0-9_\-]/g); };
pantry.RegXWhiteSpaces = function () { return RegExp(/ +/g); };
pantry.CreditCardsRest = function () { return RegExp(/^(?!000)\d{3}$/g); };
pantry.CreditCardAmx = function () { return RegExp(/^(?!000)\d{4}$/g); };

pantry.ValById = function (nodeID, val) {
  if (val) {
    return document.getElementById(nodeID).value = val;
  }
  else {
    return document.getElementById(nodeID).value;
  }
};

pantry.GetIdVal = function (nodeID) {
  return document.getElementById(nodeID).value;
};

pantry.MakeEmpty = function (nodeID) {
  return document.getElementById(nodeID).value = "";
};

pantry.GetById = function (nodeID) {
  return document.getElementById(nodeID);
};

pantry.isSafari = function () { return (navigator.userAgent.toLowerCase().indexOf('safari') != -1) ? true : false; };
pantry.gebtn = function (parEl, child) { return parEl.getElementsByTagName(child); };
//pantry.gebtnInId = function (id, child) { return document.getElementById(id).getElementsByTagName(child); };

pantry.isUndefined = function (x) { return x == null && x !== null; }
pantry.isEmpty = function (x) { typeof x === "undefined" || x === null }
pantry.isUndefinedOrNullOrEmpty = function (x) { return (x == null && x !== null) || (typeof x === "undefined" || x === null) }


pantry.IsTouchDevice = function () {
  return !!('ontouchstart' in window) // works on most browsers 
      || !!('onmsgesturechange' in window); // works on ie10
};

String.prototype.trim = function () {
  return (this.replace(/^[\s\xA0]+/, "").replace(/[\s\xA0]+$/, ""));
};

String.prototype.removeWhite = function () {
  return (this.replace(/[\t\f\n\r]/, ""));
};

String.prototype.startsWith = function (str) {
  return (this.match("^" + str) == str);
};

String.prototype.endsWith = function (str) {
  return (this.match(str + "$") == str);
};


var toggleLoadingMsg = function (action) {

  if (action == 'delay') { loadingMsgTimer = setTimeout('toggleLoadingMsg(\'show\')', 350); }

  if (action == 'show') {
    clearTimeout(loadingMsgTimer);
    if (document.getElementById('ajax_loading')) {
      document.getElementById('ajax_loading').style.display = 'block';
      document.getElementById('ajax_loading').style.opacity = '1';
      //document.body.style.cursor = 'wait';
    }
  }

  if (action == 'hide') {
    clearTimeout(loadingMsgTimer);
    document.getElementById('ajax_loading').style.display = 'none';
    setTimeout('pantry.clearCursorWait()', 5000);
  }

};


var searchPress = {
  Init: function () {
    if (jQuery("#SearchInput")) {
      jQuery("#SearchInput").keypress(function (e) {
        if (e.which == 13) {
          searchPress.ClickMeSearch();
        }
      }).focus(function () {
        if (this.value === "Search") {
          this.value = "";
        }
      }).blur(function () {
        if (this.value === "") {
          this.value = "Search";
        }
      });
    }

    jQuery("#SearchGlass").click(function (event) {
      searchPress.ClickMeSearch();
      event.stopPropagation();
    });
  },

  ClickMeSearch: function () {
    var searchTerm = jQuery("#SearchInput").val().trim();
    searchTerm = searchTerm.split(' ').join('+');
    searchTerm = searchTerm.split(',').join('+');
    searchTerm = searchTerm.split('~').join('');

    pantry.TrackAction("Searched", "HeaderSearch", searchTerm);
    if (searchTerm.length > 0) {
      window.location.href = pantry.Globals.Domain + "Search?t=" + searchTerm;
    }
    else {
      window.location.href = pantry.Globals.Domain + "Search";
    }
  }

};



function guidInJS() {
  'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = Math.random() * 16 | 0;
    var v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

String.prototype.startsWith = function (str) { return (this.match("^" + str) == str); };
String.prototype.endsWith = function (str) { return (this.match(str + "$") == str); };
//String.prototype.stripHTML = function () { return this.replace(/<.*?>/g, ''); }; // var str_no_html = str.stripHTML();
//String.prototype.convertHTMLspecialChars = function () { var str = this.replace(/&/g, '&amp;'); str = str.replace(/</g, '&lt;'); str = str.replace(/>/g, '&gt;'); str = str.replace(/"/g, '&quot;'); return str; }; // var str_hsc = str.convertHTMLspecialChars();

// JS to Elmah Error handling
//var logErrorUrl = "/error/LogJavaScriptError";
//function JuicyWorldLogError(ex, stack) { if (ex == null) { return; } if (logErrorUrl == null) { alert('logErrorUrl must be defined.'); return; } var url = ex.fileName != null ? ex.fileName : document.location; if (stack == null && ex.stack != null) { stack = ex.stack; } var out = ex.message != null ? ex.name + ": " + ex.message : ex; out += ": at document path '" + url + "'."; if (stack != null) { out += "\n  at " + stack.join("\n  at "); } jQuery.ajax({ type: 'POST', url: logErrorUrl, data: { message: out} }); }
//Function.prototype.trace = function () { var trace = []; var current = this; while (current) { trace.push(current.signature()); current = current.caller; } return trace; };
//Function.prototype.signature = function () { var signature = { name: this.getName(), params: [], toString: function () { var params = this.params.length > 0 ? "'" + this.params.join("', '") + "'" : ""; return this.name + "(" + params + ")"; } }; thisArgs = this.arguments; if (thisArgs) { var x; for (x = 0; x < thisArgs.length; x++) { signature.params.push(thisArgs[x]); } } return signature; };
//Function.prototype.getName = function () { if (this.name) { return this.name; } var definition = this.toString().split("\n")[0]; var exp = /^function ([^\s(]+).+/; if (exp.test(definition)) { return definition.split("\n")[0].replace(exp, "$1") || "anonymous"; } return "anonymous"; };
//window.onerror = function (msg, url, line) { JuicyWorldLogError(msg, arguments.callee.trace()); };



pantry.AjaxPost = function () {
  jQuery.ajax({
    type: "POST",
    url: "/Members/Ajax/SetAdminViewMode",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    data: JSON.stringify({ "key": "value" }),
    timeout: 5000, //pantry.Globals.AjaxPostTimeout,
    success: function (data) {
      if (data.status === "success") {
        window.location.reload();
      }
      else {
        alert(Error);
      }
    }
    , error: function (e) {
      alert("An error occured:" + e.toString());
    }
  }, "json");
}


pantry.ObjectInspector = function (oObject, sSeparator, sText) {

  if (typeof sText == 'undefined') { sText = ''; }
  if (typeof sSeparator == 'undefined') { sSeparator = ','; }

  if (sText.length > 64) { return '[MAX LEN!]'; }
  var r = [];

  for (var obj in oObject) {
    var tOf = typeof oObject[obj];

    if (tOf == 'number') { tOf = 'n'; }
    else if (tOf == 'string') { tOf = 's'; }
    else if (tOf == 'boolean') { tOf = 'b'; }
    else if (tOf == 'function') { tOf = 'fnct'; }
    else if (tOf == 'null') { tOf = 'N'; }
    else if (tOf == 'undefined') { tOf = 'undef'; }

    r.push(sText + obj + '[' + tOf + ']=' + (tOf == 'object' ? 'obj:' + pantry.ObjectInspector(oObject[obj], sText + ';') : oObject[obj]));
  }

  return r.join(sText + sSeparator);
}

//jQuery("#SearchQ2").keyup(function (event) {
//  if (event.which == 13) {
//    Search()
//  }
//});

//jQuery("#searchBtn2").click(function () {
//  Search()
//});

function Search() {
  Modular.PopupSpinner();

  jQuery("#EditWrapper").hide();
  jQuery("#SearchResultsWrapper").hide();
  jQuery.ajax({
    type: "POST",
    url: "/Search/Click",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    data: JSON.stringify({ "q": document.getElementById("SearchQ2").value }),
    timeout: pantry.Globals.AjaxPostTimeout,
    success: function (data) {
      Modular.CloseSpinner();
      console.log("test");
      switch (data.SearchType) {
        case "social":
          break;
        case "phone":
          break;
        case "multi":
          break;
      }

      if (!data.IsSuccess) {
        Modular.CloseSpinner();
        alert("An error occured:" + e.toString());
      }
      else {
        jQuery("#FamiliesServedCount").html(data.FamiliesCount);
        jQuery("#FamilyMembersServedCount").html(data.FamilyMembersCount);
        if (data.SearchReturn.SearchDataum.length > 0) {
          jQuery("#SearchResults").html(
            jQuery("#SearchTemplate").render(data.SearchReturn.SearchDataum)
          );
          mergeCount = 0;
          jQuery("#SearchResults .mergeCheck").click(function () {
            if (jQuery(this).prop("checked") === true) {
              mergeCount += 1;
            } else {
              mergeCount -= 1;
            }

            jQuery("#SearchResults .mergeCheck").next().next().hide();

            if (mergeCount === 2) {
              jQuery("#SearchResults .mergeCheck:checked").next().next().show();
              jQuery("#SearchResults .mergeCheck:checked").each(function () {
                mergeLink += "/" + jQuery(this).attr("rel");
              })
              mergeLink = "/Client/Merge" + mergeLink;
            }

            if (mergeCount > 2) {
              mergeLink = "";
              alert("Sorry, you may only merge 2 at a time...")
            }

          });

          jQuery("#SearchResults .mergeButton").click(function () {
            location.href = mergeLink;
          });

        } else {
          jQuery("#SearchResults").html("We found no matches!");
        }
        jQuery("#SearchResultsWrapper").show(200);


      }
    }
    , error: function (e) {
      Modular.CloseSpinner();
      alert("An error occured:" + e.toString());
    }
  }, "json");
};


function ClickLink(id) {
  console.log("clicked");
  Modular.PopupSpinner(50, "SearchResults");
  window.location.href = "/Client/Index/" + id;
}


function SaveUpdate(whatClicked) {
  document.getElementById("ButtonClicked").value = whatClicked

  Modular.PopupSpinner(50, "SubmitButtons");

  var d = jQuery("#InputFamilyHeadCreateDate").val();
  var toReplace = false;
  if (Object.prototype.toString.call(d) === "[object Date]") {
    if (isNaN(d.getTime())) {
      toReplace = true;
    }
  }
  else {
    toReplace = true;
  }
  if (toReplace) {
    var today = new Date();
    document.getElementById("InputFamilyHeadCreateDate").value = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
  }

  var zipCheck = jQuery('#InputFamilyHeadZip');
  if (zipCheck.length != 0) {
    if (zipCheck.val().length != 0) {
      document.getElementById("InputFamilyHeadZip").value = CityToZip(document.getElementById("InputFamilyHeadZip").value, document.getElementById("InputFamilyHeadCity").value)
    }
  }
  var form = document.getElementById("Form1");
  form.submit();

};


jQuery("#NewClient").click(
  function () {
    window.location.href = "/Client/Index";
  });


function ChangeNewButton() {
  if (jQuery("#AddEditBox").html().indexOf("Adding") >= 0) {
    jQuery("#NewClient").html("Clear");
  }
}


function ZipToCity(zip, city) {
  if (city === "" && zip !== "") {
    switch (zip) {
      case "32784":
        city = "UMATILLA";
        break;

      case "32767":
      case "32760":
        city = "PAISLEY";
        break;

      case "32726":
      case "32736":
        city = "EUSTIS";
        break;

      case "32102":
        city = "ASTOR";
        break;

      case "32702":
        city = "ALTOONA";
        break;

      case "32735":
        city = "GRAND ISLAND";
        break;

      case "32720":
        city = "PINE LAKES";
        break;

    }
  }

  return city;
}

function CityToZip(zip, city) {
  if (zip === "" && city !== "") {
    city = city.toLowerCase();
    switch (city) {
      case "umatilla":
        zip = "32784";
        city = "Umatilla";
        break;

      case "paisley":
        zip = "32767";
        city = "Paisley";
        break;

      case "eustis":
        zip = "32736";
        city = "Eustis";
        break;

      case "astor":
        city = "32102";
        break;

      case "altoona":
        city = "32702";
        break;

      case "grand island":
        zip = "32735";
        city = "Grand Island";
        break;

      case "pine lakes":
        zip = "32720";
        city = "Pine Lakes";
        break;

    }
  }

  return zip;
}


function CheckDate(str) {
  var matches = str.match(/(\d{1,2})[- \/](\d{1,2})[- \/](\d{2,4})/);
  if (!matches) {
    return false;
  }
  // parse each piece and see if it makes a valid date object
  var month = parseInt(matches[1], 10);
  var day = parseInt(matches[2], 10);
  var year = parseInt(matches[3], 10);
  if (year < 2000) {
    year = 2000 + year;
  }
  var date = new Date(year, month - 1, day);
  if (!date || !date.getTime()) {
    return false;
  }
  // make sure we have no funny rollovers that the date object sometimes accepts
  // month > 12, day > what's allowed for the month
  if (date.getMonth() + 1 !== month) {
    return false;
  }
  if (date.getFullYear() !== year) {
    alert(date.getFullYear() + ' ' + year);
    return false;
  }
  if (
      date.getDate() !== day) {
    return false;
  }
  return (true);
}


function DeleteMember(memberID) {

  var answer = prompt("Type 'yes' to delete", "no");

  if (answer.toLowerCase() === "yes") {
    Modular.PopupSpinner();
    jQuery.ajax({
      type: "POST",
      url: "/Client/DeleteMember",
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      data: JSON.stringify({ "memberID": memberID }),
      timeout: pantry.Globals.AjaxPostTimeout,
      success: function (data) {
        Modular.CloseSpinner();
        jQuery("#Member" + memberID.toString()).hide(500);
        jQuery("#Member" + memberID.toString()).remove();
      }
      , error: function () {
        Modular.CloseSpinner();
        alert("We had a system error!");
      }
    });
  } else {
    alert("not deleted!");
  }

}


function DeleteClient() {

  var familyID = jQuery("#FamilyWeb_FamilyHead_FamilyID").val();
  
  Modular.PopupSpinner();
  jQuery.ajax({
    type: "POST",
    url: "/Client/DeleteClient",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    data: JSON.stringify({ "familyID": familyID }),
    timeout: pantry.Globals.AjaxPostTimeout,
    success: function (data) {
      window.location.href = "/Client";
    }
    , error: function () {
      Modular.CloseSpinner();
      alert("We had a system error!");
    }
  });


}

