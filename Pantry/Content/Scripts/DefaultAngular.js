﻿/// <reference path="../jquery/jquery.js" />
/// <reference path="../jquery/jquery.extra.js" />

// add here for jslint checking
/*jslint bitwise: true, undef: true, browser: true, continue: true, debug: true, sloppy: true, eqeq: true, vars: true, evil: true, white: true, nomen: true, plusplus: true, regexp: true, maxerr: 100, indent: 2 */
//var Modular = {}, pantry = {}, jwColors = {}, jQuery = {};

data = { 'd': [{}], 'i': [{}] };
var membership = {};

var loadingMsgTimer = '';
//var domain = domain || '';
var jwColors = {};

// Digital Juice Default Javascript Library.
var pantry = pantry || {};

pantry.Globals = {
  Domain: "",
  ContentID: 0,
  VisitorId: '',
  IsLoggedIn: false,
  IsAdmin: false,
  AjaxPostTimeout: 1000000 // to be set to 2000 on launch
}

pantry.clearCursorWait = function () {
  document.body.style.cursor = 'auto';
};


//pantry.getParameterByName = function (name, location_, default_, getHash) {
//  if (location_ == "" || !location_) {
//    location_ = window.location.href;
//  }
//  if (!default_) { default_ = ""; }
//  var regexS = "";
//  if (getHash) {
//    regexS = "[\\#&]" + name + "=([^&?]*)";
//  }
//  else {
//    regexS = "[\\?&]" + name + "=([^&#]*)";
//  }
//  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
//  var regex = new RegExp(regexS);
//  var results = regex.exec(location_);
//  if (results == null) {
//    return default_;
//  }
//  else {
//    return decodeURIComponent(results[1].replace(/\+/g, " "));
//  }
//};

//pantry.getHashStringParameterByName = function (name, default_) { return pantry.getParameterByName(name, default_, true); };
//pantry.getQueryStringParameterByName = function (name, default_) { return pantry.getParameterByName(name, default_, false); };
pantry.RegXEmail = function () { return /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~])+)*))@((([a-z]|\d)|(([a-z]|\d)([a-z]|\d|-|\.|_|~)*([a-z]|\d)))\.)+(([a-z])|(([a-z])([a-z]|\d|-|\.|_|~)*([a-z])))\.?$/ }; //{ return /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,6}|[0-9]{1,3})(\]?)$/; };
pantry.RegXAllow = function () { return new RegExp(/^([a-zA-Z0-9\s_\-']{1,20})$/); };
pantry.RegXIllegal = function () { return new RegExp(/[a-zA-Z0-9_\-]/g); };
pantry.RegXWhiteSpaces = function () { return RegExp(/ +/g); };
pantry.CreditCardsRest = function () { return RegExp(/^(?!000)\d{3}$/g); };
pantry.CreditCardAmx = function () { return RegExp(/^(?!000)\d{4}$/g); };

pantry.ValById = function (nodeID, val) {
  if (val) {
    return document.getElementById(nodeID).value = val;
  }
  else {
    return document.getElementById(nodeID).value;
  }
};

pantry.GetIdVal = function (nodeID) {
  return document.getElementById(nodeID).value;
};

pantry.MakeEmpty = function (nodeID) {
  return document.getElementById(nodeID).value = "";
};

pantry.GetById = function (nodeID) {
  return document.getElementById(nodeID);
};

pantry.isSafari = function () { return (navigator.userAgent.toLowerCase().indexOf('safari') != -1) ? true : false; };
pantry.gebtn = function (parEl, child) { return parEl.getElementsByTagName(child); };
//pantry.gebtnInId = function (id, child) { return document.getElementById(id).getElementsByTagName(child); };

pantry.isUndefined = function (x) { return x == null && x !== null; }
pantry.isEmpty = function (x) { typeof x === "undefined" || x === null }
pantry.isUndefinedOrNullOrEmpty = function (x) { return (x == null && x !== null) || (typeof x === "undefined" || x === null) }


pantry.IsTouchDevice = function () {
  return !!('ontouchstart' in window) // works on most browsers 
      || !!('onmsgesturechange' in window); // works on ie10
};

String.prototype.trim = function () {
  return (this.replace(/^[\s\xA0]+/, "").replace(/[\s\xA0]+$/, ""));
};

String.prototype.removeWhite = function () {
  return (this.replace(/[\t\f\n\r]/, ""));
};

String.prototype.startsWith = function (str) {
  return (this.match("^" + str) == str);
};

String.prototype.endsWith = function (str) {
  return (this.match(str + "$") == str);
};


var toggleLoadingMsg = function (action) {

  if (action == 'delay') { loadingMsgTimer = setTimeout('toggleLoadingMsg(\'show\')', 350); }

  if (action == 'show') {
    clearTimeout(loadingMsgTimer);
    if (document.getElementById('ajax_loading')) {
      document.getElementById('ajax_loading').style.display = 'block';
      document.getElementById('ajax_loading').style.opacity = '1';
      //document.body.style.cursor = 'wait';
    }
  }

  if (action == 'hide') {
    clearTimeout(loadingMsgTimer);
    document.getElementById('ajax_loading').style.display = 'none';
    setTimeout('pantry.clearCursorWait()', 5000);
  }

};


function guidInJS() {
  'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = Math.random() * 16 | 0;
    var v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

String.prototype.startsWith = function (str) { return (this.match("^" + str) == str); };
String.prototype.endsWith = function (str) { return (this.match(str + "$") == str); };
//String.prototype.stripHTML = function () { return this.replace(/<.*?>/g, ''); }; // var str_no_html = str.stripHTML();
//String.prototype.convertHTMLspecialChars = function () { var str = this.replace(/&/g, '&amp;'); str = str.replace(/</g, '&lt;'); str = str.replace(/>/g, '&gt;'); str = str.replace(/"/g, '&quot;'); return str; }; // var str_hsc = str.convertHTMLspecialChars();




pantry.ObjectInspector = function (oObject, sSeparator, sText) {

  if (typeof sText == 'undefined') { sText = ''; }
  if (typeof sSeparator == 'undefined') { sSeparator = ','; }

  if (sText.length > 64) { return '[MAX LEN!]'; }
  var r = [];

  for (var obj in oObject) {
    var tOf = typeof oObject[obj];

    if (tOf == 'number') { tOf = 'n'; }
    else if (tOf == 'string') { tOf = 's'; }
    else if (tOf == 'boolean') { tOf = 'b'; }
    else if (tOf == 'function') { tOf = 'fnct'; }
    else if (tOf == 'null') { tOf = 'N'; }
    else if (tOf == 'undefined') { tOf = 'undef'; }

    r.push(sText + obj + '[' + tOf + ']=' + (tOf == 'object' ? 'obj:' + pantry.ObjectInspector(oObject[obj], sText + ';') : oObject[obj]));
  }

  return r.join(sText + sSeparator);
}

