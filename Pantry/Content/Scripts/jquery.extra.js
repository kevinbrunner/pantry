﻿/// <reference path="jquery.js" />
/// <reference path="../Common/default.js" />

// add here for jslint checking
/*jslint bitwise: true, undef: true, browser: true, continue: true, debug: true, sloppy: true, eqeq: true, vars: true, evil: true, white: true, nomen: true, plusplus: true, regexp: true, maxerr: 100, indent: 2 */
//var Modular = {}, jwd = {}, jwColors = {}, jQuery = {};

// DumpVar
//if (typeof (DumpVar) == 'undefined') { DumpVar = {}; } DumpVar.dump = function (obj) { return (new DumpVar()).dump(obj); }; DumpVar.EXPORT = ['DumpVar']; DumpVar.VERSION = '0.02'; DumpVar.DumpVar = function () { return DumpVar.DumpVar.apply(new DumpVar(), arguments); }; DumpVar.ESC = { "\t": "\\t", "\n": "\\n", "\f": "\\f" }; DumpVar.nodeTypes = { 1: "ELEMENT_NODE", 2: "ATTRIBUTE_NODE", 3: "TEXT_NODE", 4: "CData_SECTION_NODE", 5: "ENTITY_REFERENCE_NODE", 6: "ENTITY_NODE", 7: "PROCESSING_INSTRUCTION_NODE", 8: "COMMENT_NODE", 9: "DOCUMENT_NODE", 10: "DOCUMENT_TYPE_NODE", 11: "DOCUMENT_FRAGMENT_NODE", 12: "NOTATION_NODE" }; DumpVar = function () { if (arguments.length > 1) { return this._dump(arguments); } else if (arguments.length == 1) { return DumpVar._dump(arguments[0]); } else { return "()"; } }; DumpVar._dump = function (obj) { var out; switch (this._typeof(obj)) { case 'object': var pairs = []; for (var prop in obj) { if (obj.hasOwnProperty(prop)) { pairs.push(prop + ': ' + this._dump(obj[prop])); } } out = '{' + this._format_list(pairs) + '}'; break; case 'string': for (var prop1 in DumpVar.ESC) { if (DumpVar.ESC.hasOwnProperty(prop1)) { obj = obj.replace(prop1, DumpVar.ESC[prop1]); } } if (obj.match(/^[\x00-\x7f]*$/)) { out = '"' + obj + '"'; } else { out = "unescape('" + escape(obj) + "')"; } break; case 'array': var elems = []; for (var i = 0; i < obj.length; i++) { elems.push(this._dump(obj[i])); } out = '[' + this._format_list(elems) + ']'; break; case 'date': var utc_string = obj.toUTCString().replace(/GMT/, 'UTC'); out = 'new Date("' + utc_string + '")'; break; case 'element': out = this._dump_dom(obj); break; default: out = obj; } out = String(out).replace(/\n/g, '\n    '); out = out.replace(/\n    (.*)$/, "\n$1"); return out; }; DumpVar._format_list = function (list) { if (!list.length) { return ''; } var nl = list.toString().length > 60 ? '\n' : ' '; return nl + list.join(',' + nl) + nl; }; DumpVar._typeof = function (obj) { if (Array.prototype.isPrototypeOf(obj)) { return 'array'; } if (Date.prototype.isPrototypeOf(obj)) { return 'date'; } if (typeof (obj.nodeType) != 'undefined') { return 'element'; } return typeof (obj); }; DumpVar._dump_dom = function (obj) { return '"' + DumpVar.nodeTypes[obj.nodeType] + '"'; };



/*
 * jQuery BBQ: Back Button & Query Library - v1.3pre - 8/26/2010
 * http://benalman.com/projects/jquery-bbq-plugin/
 *
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function ($, r) { var h, n = Array.prototype.slice, t = decodeURIComponent, a = $.param, j, c, m, y, b = $.bbq = $.bbq || {}, s, x, k, e = $.event.special, d = "hashchange", B = "querystring", F = "fragment", z = "elemUrlAttr", l = "href", w = "src", p = /^.*\?|#.*$/g, u, H, g, i, C, E = {}; function G(I) { return typeof I === "string" } function D(J) { var I = n.call(arguments, 1); return function () { return J.apply(this, I.concat(n.call(arguments))) } } function o(I) { return I.replace(H, "$2") } function q(I) { return I.replace(/(?:^[^?#]*\?([^#]*).*$)?.*/, "$1") } function f(K, P, I, L, J) { var R, O, N, Q, M; if (L !== h) { N = I.match(K ? H : /^([^#?]*)\??([^#]*)(#?.*)/); M = N[3] || ""; if (J === 2 && G(L)) { O = L.replace(K ? u : p, "") } else { Q = m(N[2]); L = G(L) ? m[K ? F : B](L) : L; O = J === 2 ? L : J === 1 ? $.extend({}, L, Q) : $.extend({}, Q, L); O = j(O); if (K) { O = O.replace(g, t) } } R = N[1] + (K ? C : O || !N[1] ? "?" : "") + O + M } else { R = P(I !== h ? I : location.href) } return R } a[B] = D(f, 0, q); a[F] = c = D(f, 1, o); a.sorted = j = function (J, K) { var I = [], L = {}; $.each(a(J, K).split("&"), function (P, M) { var O = M.replace(/(?:%5B|=).*$/, ""), N = L[O]; if (!N) { N = L[O] = []; I.push(O) } N.push(M) }); return $.map(I.sort(), function (M) { return L[M] }).join("&") }; c.noEscape = function (J) { J = J || ""; var I = $.map(J.split(""), encodeURIComponent); g = new RegExp(I.join("|"), "g") }; c.noEscape(",/"); c.ajaxCrawlable = function (I) { if (I !== h) { if (I) { u = /^.*(?:#!|#)/; H = /^([^#]*)(?:#!|#)?(.*)$/; C = "#!" } else { u = /^.*#/; H = /^([^#]*)#?(.*)$/; C = "#" } i = !!I } return i }; c.ajaxCrawlable(0); $.deparam = m = function (L, I) { var K = {}, J = { "true": !0, "false": !1, "null": null }; $.each(L.replace(/\+/g, " ").split("&"), function (O, T) { var N = T.split("="), S = t(N[0]), M, R = K, P = 0, U = S.split("]["), Q = U.length - 1; if (/\[/.test(U[0]) && /\]$/.test(U[Q])) { U[Q] = U[Q].replace(/\]$/, ""); U = U.shift().split("[").concat(U); Q = U.length - 1 } else { Q = 0 } if (N.length === 2) { M = t(N[1]); if (I) { M = M && !isNaN(M) ? +M : M === "undefined" ? h : J[M] !== h ? J[M] : M } if (Q) { for (; P <= Q; P++) { S = U[P] === "" ? R.length : U[P]; R = R[S] = P < Q ? R[S] || (U[P + 1] && isNaN(U[P + 1]) ? {} : []) : M } } else { if ($.isArray(K[S])) { K[S].push(M) } else { if (K[S] !== h) { K[S] = [K[S], M] } else { K[S] = M } } } } else { if (S) { K[S] = I ? h : "" } } }); return K }; function A(K, I, J) { if (I === h || typeof I === "boolean") { J = I; I = a[K ? F : B]() } else { I = G(I) ? I.replace(K ? u : p, "") : I } return m(I, J) } m[B] = D(A, 0); m[F] = y = D(A, 1); $[z] || ($[z] = function (I) { return $.extend(E, I) })({ a: l, base: l, iframe: w, img: w, input: w, form: "action", link: l, script: w }); k = $[z]; function v(L, J, K, I) { if (!G(K) && typeof K !== "object") { I = K; K = J; J = h } return this.each(function () { var O = $(this), M = J || k()[(this.nodeName || "").toLowerCase()] || "", N = M && O.attr(M) || ""; O.attr(M, a[L](N, K, I)) }) } $.fn[B] = D(v, B); $.fn[F] = D(v, F); b.pushState = s = function (L, I) { if (G(L) && /^#/.test(L) && I === h) { I = 2 } var K = L !== h, J = c(location.href, K ? L : {}, K ? I : 2); location.href = J }; b.getState = x = function (I, J) { return I === h || typeof I === "boolean" ? y(I) : y(J)[I] }; b.removeState = function (I) { var J = {}; if (I !== h) { J = x(); $.each($.isArray(I) ? I : arguments, function (L, K) { delete J[K] }) } s(J, 2) }; e[d] = $.extend(e[d], { add: function (I) { var K; function J(M) { var L = M[F] = c(); M.getState = function (N, O) { return N === h || typeof N === "boolean" ? m(L, N) : m(L, O)[N] }; K.apply(this, arguments) } if ($.isFunction(I)) { K = I; return J } else { K = I.handler; I.handler = J } } }) })(jQuery, this);

/*
 * jQuery hashchange event - v1.3 - 7/21/2010
 * http://benalman.com/projects/jquery-hashchange-plugin/
 *
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
//(function ($, e, b) { var c = "hashchange", h = document, f, g = $.event.special, i = h.documentMode, d = "on" + c in e && (i === b || i > 7); function a(j) { j = j || location.href; return "#" + j.replace(/^[^#]*#?(.*)$/, "$1") } $.fn[c] = function (j) { return j ? this.bind(c, j) : this.trigger(c) }; $.fn[c].delay = 50; g[c] = $.extend(g[c], { setup: function () { if (d) { return false } $(f.start) }, teardown: function () { if (d) { return false } $(f.stop) } }); f = (function () { var j = {}, p, m = a(), k = function (q) { return q }, l = k, o = k; j.start = function () { p || n() }; j.stop = function () { p && clearTimeout(p); p = b }; function n() { var r = a(), q = o(m); if (r !== m) { l(m = r, q); $(e).trigger(c) } else { if (q !== m) { location.href = location.href.replace(/#.*/, "") + q } } p = setTimeout(n, $.fn[c].delay) } $.browser.msie && !d && (function () { var q, r; j.start = function () { if (!q) { r = $.fn[c].src; r = r && r + a(); q = $('<iframe tabindex="-1" title="empty"/>').hide().one("load", function () { r || l(a()); n() }).attr("src", r || "javascript:0").insertAfter("body")[0].contentWindow; h.onpropertychange = function () { try { if (event.propertyName === "title") { q.document.title = h.title } } catch (s) { } } } }; j.stop = k; o = function () { return a(q.location.href) }; l = function (v, s) { var u = q.document, t = $.fn[c].domain; if (v !== s) { u.title = h.title; u.open(); t && u.write('<script>document.domain="' + t + '"<\/script>'); u.close(); q.location.hash = v } } })(); return j })() })(jQuery, this);


/*
* Notifier.js https://github.com/Srirangan/notifer.js 
*/
(function ($) { var config = window.NotifierjsConfig = { defaultTimeOut: 5000, position: ["top", "right"], container: $("<div></div>") }; $(document).ready(function () { config.container.css("position", "fixed"); config.container.css("z-index", 9999); config.container.css(config.position[0], "12px"); config.container.css(config.position[1], "12px"); $("body").append(config.container); }); function getNotificationElement() { return $("<div>").addClass("notificationStyles"); } var Notifier = window.Notifier = {}; Notifier.notify = function (message, title, iconUrl, timeOut) { var notificationElement = getNotificationElement(); timeOut = timeOut || config.defaultTimeOut; if (iconUrl) { var iconElement = $("<img/>", { src: iconUrl, css: { width: 36, height: 36, display: "inline-block", verticalAlign: "middle" } }); notificationElement.append(iconElement); } var textElement = $("<div/>").css({ display: 'inline-block', verticalAlign: 'middle', padding: '0 12px' }); if (title) { var titleElement = $("<div/>"); titleElement.append(document.createTextNode(title)); titleElement.css("font-weight", "bold"); textElement.append(titleElement); } if (message) { var messageElement = $("<div/>"); messageElement.append(document.createTextNode(message)); textElement.append(messageElement); } notificationElement.delay(timeOut).fadeOut(function () { notificationElement.remove(); }); notificationElement.bind("click", function () { notificationElement.hide(); }); notificationElement.append(textElement); config.container.prepend(notificationElement); }; Notifier.info = function (message, title) { Notifier.notify(message, title, ""); }; Notifier.warning = function (message, title) { Notifier.notify(message, title, ""); }; Notifier.error = function (message, title) { Notifier.notify(message, title, "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAABFCSURBVHjavFsJjBVVFr311+6mu2kQbFYVUVEURVREbSSgLbigIxoxKCqLJhqX0cQYE2McMjHuMm7jZMYlLgnRCePoqCi448a4owiCCoMIyGbv3f//qppzqt77/X5Rf2saXnL7VdWvenXP3d69r15bK4YPl0g0KlFFYcdeH4t5vSYrEvGP4/FuSiTEIqljQa/PSRKLDbVisRHoRwue5xhiWSKuK67jiDjOGslk1rmZzCb04qbTPqVSIugd9Pqcxw6vabJtbwz2mmyM4fX6POQ4JoFmmcdkDuT1wfvUb3XXXy+QkhCQ1xvH6RdeOAT3nY/zRlADAFfq3y19rx6PAHhA4TpOhxuJLMd9S/H8v+KXXLIO3IpHFJRx3LRwYZZHNw+PJv9BJLGwi/rBkpq+V1Hnk0+Oh9angRpBDaK1TKqqEqmtFQsEK+h+Dy0AZDU3iwsCuEpcb/QoErknvWjRcmh9KWhJcvbsFTnvLLWFKNLSAuhpswwBtD3++BQAvhmmPy3LGMw/ctBBYh1wgFj19b7GFeAcEASvmWRP89+2TdyNG8XZsEEklWrA1Qb8/icIeAnM/96q+fPf9sZRfLg9xcAYEPR97e/BOBAkmnE0Hh8D0LcjBlxIf4/W1Ej0iCMkwtgyaJBvqvThzk7ffLXQtMvoFvwtmfQEyHvcLVvE+eUXsVevFgcW4vhx4J/oF0AYK22eG76fEweCPYRrxgLPAoLSozlq6RZoFaC/gq7w7oXQ4sceK/ExY/xn29tF1q0T6ejwBUBBEHBFhQ+OQdG0CP4OINLV5VkAx/Puwf1Wnz4SPewwiY4aJemVK8X58ktxu7ouxHMX4v1Pg64GdRbkVrlZzqUwF3BDBOHuLowGXHvYct2x/CV+5JGSPP54Bi+RH38U2bHDFwCBEGxlpe//BKSJ2iVILQDeqy2Bwmht9cegUGgRHKN/f4mPwCRy8MGS+vxz6frqK/JGBYxFfx2eXi5F+A8iibn5wIdYhaIbQAu9ADJ4sFSfcYZECGT9ehH4rAeeoMgwNJc1aU06+msN85o2f20l5v281tIisnmzL9x+/cQaOlSSsLQ4LKJ96VJJbdw4Fk9/AL7+CPqLm0fbYccxMaWU59g4f0p8iUslNN5nLN77668+Y+ypRRN4MSJACoFaLnYvGy2CAv7pJxHElwgsoXraNGmHS7R/8gn5W0hrAM0J4T302LMA15BM2LE6fwY0m5qrxUsr9ttP5NNPReCT0tbmmzqBGz5XlKh5gmdfyv3aRWgRW7eKrF0rMnq0VCHoRmEZTa+/zmB4heJ9jhTB5RazAMPs/4E/s5nN9TvnHElSI2+9JbJqla91+jd9WoPSgMxek05itM+bMcBMdgqNo+/H7CC//eb1yeOOk7rp02XnSy/xN1qpDb7nuwE8QYwRNxAs3IDpoL8aNI/m2v/ssyXBqL5kicjHH4swaQlqNAyo7qltTZwZGPE1qXQ3S+Zz7CngoGBItIYVK0TefFMSmGqpIPJKnhXvhbBJxDvIDXLmAyeBHuN5v8ZGD7wL8C6irze9BcFq6ts3VwCFgAcpTBDwdcE0mGMlprUwNnzzjbhvvCEJjFF3+ukax2MKQ7iSmXnPq672UkRSRPWKKkAvg+r7TpggNQMGeJq3vv1WLDCWzeXNqE5i8nPHHSI7d4ps394d7Mw0VGsvKCCC1wLSx8OGiVx1lQhmHC/WcKYhYP27FhgTLcQFF9NnHDHBhVt2InlyHGc8iqSn0Ge8Ygmkewc8ROdqAbBAyRXA30FTK5HG7n/iieJgunFhahaYs1Q1KEEhkMl77hFh2rv//n6Q4nxuRnLTXfIJQAuB4OfN88dm/XDCCX7w++67XCHp53HNpRAwZgWU1gkFpHbtqoe2hwHwv3MEAB54HJ2LyB1RObgBfhroPqa7Q2bMEPnsM3Hfflss+Jul5mfLrADZY26WRx7xhcBGNxgyRKSpyZ+69Fxvug3BawEEtU/wc+fmpstsFOymTf7Ua4JX5TNdk0LgjFRx0knSBNdwMpmxAPwpgK8zLYBuEJ2DGwOapzD+hn7EgMmTpYq19quvepLPCkkLQScrZPbJJ30hmFUip8pDDvGTJLqDqXn2pgB0GqzBa80HW10d8tAGPwZxFqJbqHUDTwgkuIjz++8SRdZoQSGtyBsAegjoGUetPWgBRPjHUUFB/TgL/ZQYNFgH5u0PPxSHSY6eQ/kga3etQZr7M8/4TIc1MowA6gmDTJPow2ScvSZ9TiHOnx8OXjcmW5ddJjJ+fFaImiePP/IJnsl7XwTQGNwH+KYA1yxt/hpv9AoEi4D53w8aWX/aaZKAqdnvvusxbenyl6WrihfCMpfzLvqC6wUDB4qgUPLSWQSmbAQ3fZ89Sme55prC4HVj0nXUUb7f//CDJ8CsYrSi6H54d3TkSGles4aga3H9WdMKIjnTguNMwo+NUQxeC9PJfPGFV366RgblEV8E/7ZeeUXkwANLK7wZxGgJyNhyNK57lM9y7bWlgdcNJi533SVy5pnismxWq0qayLsNDDWw0iiCPfA1At8kV5l/1gVcFRHRz+aPdciqbAQZkp4vc1JImHMEiYensXLaoYeKXHmlrzlqneBJFOJNN5UH3lzpufNOrcCcdJfXiMEB9YUFKs3PNvBCAIY0cLGBN9VCsjYCjIOov5v2mTzghRal35PGpGbWrO55nee33toz8EoA7rp14tIFQnglhgyw1EJZyv8bHNMCnG5psMYfVQHGYhgsHaL97KDQvvPRRz1fS+NUdvHFfnbHpKmn4MnPsmWSwXTn5OFVWwExJYENQhilsObGAIf+jws1MNPMhg1iY+52wgbkkt2LL0oKfueg71Ej4HHjRO6+288e9wT81KndkT8fv8BiA1M1ZjXHwJoTA0AT2VciYGSYQnJqobZDyBuYAebBB8VlOdzTxhyil8GH8gssxFQFbMrlJ3YHQX86iIGmcoBKzAAZzKGuCTboAlqyqAjTF10kzqJF/vy+j5qLtNwE7wT4CuOXmIjN8fGe4uhpUCUFwymNOKYod9cuyWzbljNoqAUo4kpt+rbbxGGFuA/BewBC+MnHr4cJBVocCR6wVoOGmi4witJMoOLL4CabqWQB0MHrXLvP3HKLFxyzCx17CXz6jDN2C3il8ElMNpRLjKoeGNWdCjvOQA4aQ1Zoo3rzAkUQZMi5a/iY/cEHkp45U5zXXtsr4CncFMDnc8dCfHrE2QAzgbIAnh/gzwJ+UOjLnkteNupqJ48pFSMbBUj6/vvFRQXW2+DT9Pke8mVaAWcgZQHVpgtUU0IUgPeldQ9eknn/fek86yzJ3Hdf74Dnsjd9fg/Be8SFnFhMW4AngJjjZUOOv47Gz0X8pJznS6q5yhoJ3GMZ5KKIinLlphda6oYb8kb3MNM3j+2gADi1G6WwayRCrTzJoLIyH7QDg9lhg4ZQZPJkScAVeqPFH35YIuedJ04yWZqZh/BpHhOjAt/qWYDrB7xWatTmBgSYiFNA+2GWoHs+E+ciyhtv7FGGl5M0oiy3UC8wM00vX15yAAwDT2weRt8CWp3sqrDrbuFJiqUv40AB7QclbPZRgK8heH7t6cXGL82J22+XyPjxuwGzS+BR/05sXQjUKgZsygZBXFnHvh3JAr/GBgd1ikiXfQzg++4F8P4HvJjEGxul+pVXJAZ3sPMIoZBLOGolqW3zZr0itMr7MKI+fK6nADq5KYG+hhKzHAug2ffbW+BNS0AVWXnjjWIdcUT5FsCyGcrtQjLkxTxtAWpshEd3GaNkJxcYkRA5BQCbRPADAN7ay+B1S0yaJP1eeEGSF1zgBUa7RD75+a6DAdDPAT7R3xrNcuwD/mmDhKIDB4aaf9AqEgA/aB+Cz84MRx0lfRcskOiYMaGuGsZ3BJjafO1nsQYF8Db/tCCvjw0ZUtS0qqZPl6F7CL7rnXdK2YkSHhYOO0z63X23JKdMERs5jF3EDaLA1Mz9RgbWoAC4u2JNGxIYl8vInDIKDDr85Zf3CHwn0tvtMOMOjNOjlTDwVwXwg557TmrnzCksAGZ/wNTmJ2drzJ0kwRUJ74dWTBWR/fYrOOiGSy+VTub8PdBgB8BvQXqbhkk2PfCApL79tucTxODBkkLwLsQrsRCTiTGfAJ7ln12rVkmcyYeaDcJo+/PPy1YwX64A2gF+E8Bn1DhtqB22nHuutC5axI1PZQtg+6OPyi5YUV4BAAOx7ORXJANjNtGamTseneRUp6trBD+K8vMSt6W6ebLAFHwqDbOqGDVKYv37F2W2DeA3qqrOzNwyeE9q9WqpPPlkL/6U2rY99JD87/rrC6flNTXi1tdLM3es+b6/oJAA/ClR5IJ0S4tUjxghKe7ACNs0xRegdG777DNPELWnnipRfvzI01oBfoNR0gaLmvTOndIJV4hhrk9wc2WRleLfAH6DKpTykQvtVxx+uOwAeJb5aLeBVhYTAG84FQ+MqEAK6rS1iaNM0w3sstJ9Fz93IX+omTgxNDC2APxPqqTNu3TFVBzW1LpsmVQee6wk+VE1T9saAB+WEXr1CRc/MP21cFOVr/2bd6s1Zoa/YwdoVsfWrVJz5JGeFegFyDABsM5u+fhjaf/qK6k6+miJ84Opas0Av9YAX6i09dbu2tsljXK6ipsujXF02wLwPxfRvC58kqNHyw7uHPG/GHEf4dpSBcAbD/I2IGKOTcC0001NeQWgme/ghgjblv7IEdiaAH51CPiCjHOc9etl54sveq7AHWB6c8VmgP+pBPCkxLBh0tHaKik/+nM36b2h1ebM/G6G5F7OyzQ31ycxGPfn2yGuEDxuX7NGOn/4QaJIPb/LA94tYYEj09EhXRBE9YQJkhg0SH4F+B9DwIcVQjR9Zn4t3EThu/QfVGzbPZ9YXDjQNui0se6YY6QTGnZgopZR/2sKnkvIcdgqk5UnwOq+etw4qb/8clkL8JJHWOZ5BIKvOPRQ2fX113q4icG5vxwBiNqI/Bijci3MsZ3TCSKqFQCcj4KCkCKLLcEtrUELK0QWqr1KBM+m77/X+wSuEX9Dd/4Ks4TplgM8wQFbYNoV/Mqq1gzKoUye43LvyUfkKQnemrgRwgf/RDHw/l7h0tp8xhXEgdmtcIM+I0dKB/fvK3copv1CLlDICvIF2yDR7BmnWsCbAv+04ll6SwBslzH5wwvmtcISqpBedu3YIRnMDuUIoFwXKCYA7mVKMNfnNhm/PVEq+FJdIGgJ13k5PZKLaCIhSaSuYZVjuSZcjgvpCo/v5n+ntfuJjp7r55cDqCdLt4+oqeVhVGFjGBwrME1xuZnpLNfcS43+hbSf1xWYl6DuiCaTktqyRZv8SgX+vbLL6sXS49b9LzMcCCkw5+sUaogU3cL4h6ZeEQCrOph7EsVNF4FzV5nfnpZS/mVmLwhAtzGgP4PO9RRUWSnxAQMkDYugIHQd0dMYEIGmkwAeQ5/evl2c7n0IL4cVN/tcACdwqxzaf6dPb0R3E2haNsBgaorW1XmrzF3NzV6ebxVbP8C9zCKTSL+juJelstOZo9wloAfw3qXqvXu24r7H3+yVRmf41dZ7sMdTKkWmIrpOQsI0weE/NagXJbktFxZCrXpfo1WO76rNl7QWFxrmZ2yP1DvQf4Is7702kTfninzImNg7Xx7LsIAZuVlvRGFKqN4k1sMs5uPni4w4WeT0/UVOrBY5Bg8nSxRqV4vIN7+JrHgfgv2PyM9qIkirSSCtzvW1VGCicBaX+L+U5QhAA48aFDP63QQQuB6dLDL8YJGhg0WGgctI1L/Hsn0Q7laRX1C+bEIo32QAygTABgWQyTfzliKEsmLAjFwLMIFrwBWFBGD0mvRYYR+hCgmgQ2ndNq5n9DiLJfuFv3djgJKobWihkLDiSghhWwjCJoSwOkhfSy/2Afd6+78AAwAu4RZEgEv6AAAAAABJRU5ErkJggg%3D%3D"); }; Notifier.success = function (message, title) { Notifier.notify(message, title, ""); }; }(jQuery));


/**
* hoverIntent r6 // 2011.02.26 // jQuery 1.5.1+  http://cherne.net/brian/resources/jquery.hoverIntent.html
*/
(function ($) { $.fn.hoverIntent = function (f, g) { var cfg = { sensitivity: 7, interval: 100, timeout: 0 }; cfg = $.extend(cfg, g ? { over: f, out: g } : f); var cX, cY, pX, pY; var track = function (ev) { cX = ev.pageX; cY = ev.pageY }; var compare = function (ev, ob) { ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t); if ((Math.abs(pX - cX) + Math.abs(pY - cY)) < cfg.sensitivity) { $(ob).unbind("mousemove", track); ob.hoverIntent_s = 1; return cfg.over.apply(ob, [ev]) } else { pX = cX; pY = cY; ob.hoverIntent_t = setTimeout(function () { compare(ev, ob) }, cfg.interval) } }; var delay = function (ev, ob) { ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t); ob.hoverIntent_s = 0; return cfg.out.apply(ob, [ev]) }; var handleHover = function (e) { var ev = jQuery.extend({}, e); var ob = this; if (ob.hoverIntent_t) { ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t) } if (e.type == "mouseenter") { pX = ev.pageX; pY = ev.pageY; $(ob).bind("mousemove", track); if (ob.hoverIntent_s != 1) { ob.hoverIntent_t = setTimeout(function () { compare(ev, ob) }, cfg.interval) } } else { $(ob).unbind("mousemove", track); if (ob.hoverIntent_s == 1) { ob.hoverIntent_t = setTimeout(function () { delay(ev, ob) }, cfg.timeout) } } }; return this.bind('mouseenter', handleHover).bind('mouseleave', handleHover) } })(jQuery);



//spin JS - Canvas loading with fallback
//fgnass.github.com/spin.js#v1.2.7
!function (e, t, n) { function o(e, n) { var r = t.createElement(e || "div"), i; for (i in n) r[i] = n[i]; return r } function u(e) { for (var t = 1, n = arguments.length; t < n; t++) e.appendChild(arguments[t]); return e } function f(e, t, n, r) { var o = ["opacity", t, ~~(e * 100), n, r].join("-"), u = .01 + n / r * 100, f = Math.max(1 - (1 - e) / t * (100 - u), e), l = s.substring(0, s.indexOf("Animation")).toLowerCase(), c = l && "-" + l + "-" || ""; return i[o] || (a.insertRule("@" + c + "keyframes " + o + "{" + "0%{opacity:" + f + "}" + u + "%{opacity:" + e + "}" + (u + .01) + "%{opacity:1}" + (u + t) % 100 + "%{opacity:" + e + "}" + "100%{opacity:" + f + "}" + "}", a.cssRules.length), i[o] = 1), o } function l(e, t) { var i = e.style, s, o; if (i[t] !== n) return t; t = t.charAt(0).toUpperCase() + t.slice(1); for (o = 0; o < r.length; o++) { s = r[o] + t; if (i[s] !== n) return s } } function c(e, t) { for (var n in t) e.style[l(e, n) || n] = t[n]; return e } function h(e) { for (var t = 1; t < arguments.length; t++) { var r = arguments[t]; for (var i in r) e[i] === n && (e[i] = r[i]) } return e } function p(e) { var t = { x: e.offsetLeft, y: e.offsetTop }; while (e = e.offsetParent) t.x += e.offsetLeft, t.y += e.offsetTop; return t } var r = ["webkit", "Moz", "ms", "O"], i = {}, s, a = function () { var e = o("style", { type: "text/css" }); return u(t.getElementsByTagName("head")[0], e), e.sheet || e.styleSheet }(), d = { lines: 12, length: 7, width: 5, radius: 10, rotate: 0, corners: 1, color: "#000", speed: 1, trail: 100, opacity: .25, fps: 20, zIndex: 2e9, className: "spinner", top: "auto", left: "auto", position: "relative" }, v = function m(e) { if (!this.spin) return new m(e); this.opts = h(e || {}, m.defaults, d) }; v.defaults = {}, h(v.prototype, { spin: function (e) { this.stop(); var t = this, n = t.opts, r = t.el = c(o(0, { className: n.className }), { position: n.position, width: 0, zIndex: n.zIndex }), i = n.radius + n.length + n.width, u, a; e && (e.insertBefore(r, e.firstChild || null), a = p(e), u = p(r), c(r, { left: (n.left == "auto" ? a.x - u.x + (e.offsetWidth >> 1) : parseInt(n.left, 10) + i) + "px", top: (n.top == "auto" ? a.y - u.y + (e.offsetHeight >> 1) : parseInt(n.top, 10) + i) + "px" })), r.setAttribute("aria-role", "progressbar"), t.lines(r, t.opts); if (!s) { var f = 0, l = n.fps, h = l / n.speed, d = (1 - n.opacity) / (h * n.trail / 100), v = h / n.lines; (function m() { f++; for (var e = n.lines; e; e--) { var i = Math.max(1 - (f + e * v) % h * d, n.opacity); t.opacity(r, n.lines - e, i, n) } t.timeout = t.el && setTimeout(m, ~~(1e3 / l)) })() } return t }, stop: function () { var e = this.el; return e && (clearTimeout(this.timeout), e.parentNode && e.parentNode.removeChild(e), this.el = n), this }, lines: function (e, t) { function i(e, r) { return c(o(), { position: "absolute", width: t.length + t.width + "px", height: t.width + "px", background: e, boxShadow: r, transformOrigin: "left", transform: "rotate(" + ~~(360 / t.lines * n + t.rotate) + "deg) translate(" + t.radius + "px" + ",0)", borderRadius: (t.corners * t.width >> 1) + "px" }) } var n = 0, r; for (; n < t.lines; n++) r = c(o(), { position: "absolute", top: 1 + ~(t.width / 2) + "px", transform: t.hwaccel ? "translate3d(0,0,0)" : "", opacity: t.opacity, animation: s && f(t.opacity, t.trail, n, t.lines) + " " + 1 / t.speed + "s linear infinite" }), t.shadow && u(r, c(i("#000", "0 0 4px #000"), { top: "2px" })), u(e, u(r, i(t.color, "0 0 1px rgba(0,0,0,.1)"))); return e }, opacity: function (e, t, n) { t < e.childNodes.length && (e.childNodes[t].style.opacity = n) } }), function () { function e(e, t) { return o("<" + e + ' xmlns="urn:schemas-microsoft.com:vml" class="spin-vml">', t) } var t = c(o("group"), { behavior: "url(#default#VML)" }); !l(t, "transform") && t.adj ? (a.addRule(".spin-vml", "behavior:url(#default#VML)"), v.prototype.lines = function (t, n) { function s() { return c(e("group", { coordsize: i + " " + i, coordorigin: -r + " " + -r }), { width: i, height: i }) } function l(t, i, o) { u(a, u(c(s(), { rotation: 360 / n.lines * t + "deg", left: ~~i }), u(c(e("roundrect", { arcsize: n.corners }), { width: r, height: n.width, left: n.radius, top: -n.width >> 1, filter: o }), e("fill", { color: n.color, opacity: n.opacity }), e("stroke", { opacity: 0 })))) } var r = n.length + n.width, i = 2 * r, o = -(n.width + n.length) * 2 + "px", a = c(s(), { position: "absolute", top: o, left: o }), f; if (n.shadow) for (f = 1; f <= n.lines; f++) l(f, -2, "progid:DXImageTransform.Microsoft.Blur(pixelradius=2,makeshadow=1,shadowopacity=.3)"); for (f = 1; f <= n.lines; f++) l(f); return u(t, a) }, v.prototype.opacity = function (e, t, n, r) { var i = e.firstChild; r = r.shadow && r.lines || 0, i && t + r < i.childNodes.length && (i = i.childNodes[t + r], i = i && i.firstChild, i = i && i.firstChild, i && (i.opacity = n)) }) : s = l(t, "animation") }(), typeof define == "function" && define.amd ? define(function () { return v }) : e.Spinner = v }(window, document);

/*
    var opts = {
      lines: 13, // The number of lines to draw
      length: 2, // The length of each line
      width: 2, // The line thickness
      radius: 4, // The radius of the inner circle
      corners: 1, // Corner roundness (0..1)
      rotate: 0, // The rotation offset
      color: '#000', // #rgb or #rrggbb
      speed: 0.8, // Rounds per second
      trail: 60, // Afterglow percentage
      shadow: false, // Whether to render a shadow
      hwaccel: false, // Whether to use hardware acceleration
      className: 'spinner', // The CSS class to assign to the spinner
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      top: 'auto', // Top position relative to parent in px
      left: 'auto' // Left position relative to parent in px
    };
    var target = document.getElementById('foo');
    var spinner = new Spinner(opts).spin(target);
*/

/* jquery plugin for spin.js

You can now create a spinner using any of the variants below:

$("#el").spin(); // Produces default Spinner using the text color of #el.
$("#el").spin("small"); // Produces a 'small' Spinner using the text color of #el.
$("#el").spin("large", "white"); // Produces a 'large' Spinner in white (or any valid CSS color).
$("#el").spin({ ... }); // Produces a Spinner using your custom settings.

$("#el").spin(false); // Kills the spinner.

*/
(function ($) { $.fn.spin = function (opts, color) { var presets = { "tiny": { lines: 8, length: 2, width: 2, radius: 3 }, "small": { lines: 8, length: 4, width: 3, radius: 5 }, "large": { lines: 10, length: 8, width: 4, radius: 8 } }; if (Spinner) { return this.each(function () { var $this = $(this); var data = $this.data(); if (data.spinner) { data.spinner.stop(); delete data.spinner; } if (opts !== false) { if (typeof opts === "string") { if (opts in presets) { opts = presets[opts]; } else { opts = {}; } if (color) { opts.color = color; } } data.spinner = new Spinner($.extend({ color: $this.css('color') }, opts)).spin(this); } }); } else { throw "Spinner class not available."; } }; })(jQuery);




//animated PNG frames
// jQuery PNG Animation
// by Josh Pyles (Pixelmatrix Design LLC)
// 
// Licensed under MIT License - http://www.opensource.org/licenses/mit-license.php
//
// Usage:
// $("#container").animatePNG("/Images/animation.png", 32, 32, 10, {fps: 10, horizontal: false});
// Creates a div that is animated inside of #container and returns the animating div
//
// $(…).animatePNG(imageURL, w, h, frames, options);
// ---------------------------------------------------------
// imageURL (string): URL to the image to be animated (can be relative)
// w (integer): width (in pixels) of the image frame
// h (integer): height (in pixels) of the image frame
// frames (integer): number of frames in the animation
// options (object):
// - fps (integer): number of frames per second to render
// - horizontal (boolean): true if the frames go horizontal (left to right), or false if they go vertical (top to bottom)
//

(function (e) { e.fn.animatePNG = function (t, n, r, i, s) { function l() { if (a.o.horizontal) { a.x = a.x - a.w > a.imgW ? a.x - a.w : 0; return a.x } else { return 0 } } function c() { if (a.o.horizontal) { return 0 } else { a.y = a.y - a.h > a.imgH ? a.y - a.h : 0; return a.y } } function h() { a.container.css("background-position", l() + "px " + c() + "px") } function p() { a.t = setInterval(h, a.fpms); a.container.data("animation", a) } var o = e.extend({}, e.fn.animatePNG.defaults, s); var u = e(this); var a = { o: e.meta ? e.extend({}, o, u.data()) : o, container: e("<div class='animation'></div>"), imgW: null, imgH: null, fpms: null, t: null, x: 0, y: 0, h: r, w: n, imageURL: t, frames: i }; a.imgW = a.o.horizontal ? 0 - a.frames * a.w : 0; a.imgH = a.o.horizontal ? 0 : 0 - a.frames * a.h; a.fpms = Math.round(1e3 / a.o.fps); var f = new Image; f.src = a.imageURL; a.container.css({ "background-color": "transparent", "background-image": "url('" + a.imageURL + "')", "background-position": "0px 0px", "background-repeat": "no-repeat", width: a.w, height: a.h }); u.append(a.container); p(); return a.container }; e.fn.animatePNG.stop = function (e) { var t = e.data("animation"); window.clearInterval(t.t) }; e.fn.animatePNG.defaults = { fps: 20, horizontal: true } })(jQuery)







//Cookie plugin Copyright (c) 2006 Klaus Hartl (stilbuero.de) Dual licensed under the MIT and GPL licenses: http://www.opensource.org/licenses/mit-license.php http://www.gnu.org/licenses/gpl.html
jQuery.cookie = function (name, value, options) { if (typeof value != 'undefined') { options = options || {}; if (value === null) { value = ''; options.expires = -1; } var expires = ''; if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) { var date; if (typeof options.expires == 'number') { date = new Date(); date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000)); } else { date = options.expires; } expires = '; expires=' + date.toUTCString(); } var path = options.path ? '; path=' + (options.path) : ''; var domain = options.domain ? '; domain=' + (options.domain) : ''; var secure = options.secure ? '; secure' : ''; document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join(''); } else { var cookieValue = null; if (document.cookie && document.cookie !== '') { var cookies = document.cookie.split(';'); for (var i = 0; i < cookies.length; i++) { var cookie = jQuery.trim(cookies[i]); if (cookie.substring(0, name.length + 1) == (name + '=')) { cookieValue = decodeURIComponent(cookie.substring(name.length + 1)); break; } } } return cookieValue; } };

// TextBox Active Stuff Or Change Style ( on click, blur and focus )
; (function ($) { $.fn.SetActiveFocusBlur = function () { return this.each(function (i) { var $this = $(this); var attrName = $(this).attr('type'); if ('__text.password'.indexOf(attrName) > 0) { $this.focus(function (e) { $($this).addClass("ActiveTextBoxCss"); }); $this.blur(function (e) { $($this).removeClass("ActiveTextBoxCss"); }); } }); }; }(jQuery));



///////////////////////////////////
// sets activetext input boxes to have an "active" or "yellowish" border
// kb
///////////////////////////////////
; (function ($) {
  $.fn.SetPasswordActiveFocusBlur = function () {
    return this.each(function (i) {
      var $this = $(this);
      var attrName = $(this).attr('type');
      if ('__text.password'.indexOf(attrName) > 0) {
        $this.focus(function (e) {
          jQuery(this).css({ "font-weight": "bold", "color": "#FFFFFF" });
          jQuery(".NewPassWordOuterContainer").css({ "border-color": "#00FF00" });
        });
        $this.blur(function (e) {
          jQuery(this).css({ "font-weight": "normal", "color": "#6F7F97" });
          jQuery(".NewPassWordOuterContainer").css({ "border-color": "#3B3E4A" });
        });
      }
    });
  };
})(jQuery);



/*! JsRender v1.0.0-beta: http://github.com/BorisMoore/jsrender and http://jsviews.com/jsviews
informal pre V1.0 commit counter: 55 */
(function (n, t, i) { "use strict"; function ot(n) { return n } function tr(n) { return n } function kt(n) { s._dbgMode = n; pt = n ? "Unavailable (nested view): use #getIndex()" : ""; g("dbg", hi.dbg = it.dbg = n ? tr : ot) } function dt(n) { return { getTgt: n, map: function (t) { var r, i = this; i.src !== t && (i.src && i.unmap(), typeof t == "object" && (r = n.apply(i, arguments), i.src = t, i.tgt = r)) } } } function st(n) { this.name = (u.link ? "JsViews" : "JsRender") + " Error"; this.message = n || this.name } function f(n, t) { var i; n = n || {}; for (i in t) n[i] = t[i]; return n } function tt(n) { return typeof n == "function" } function gt(n, t, i) { return (!o.rTag || n) && (p = n ? n.charAt(0) : p, w = n ? n.charAt(1) : w, h = t ? t.charAt(0) : h, v = t ? t.charAt(1) : v, nt = i || nt, n = "\\" + p + "(\\" + nt + ")?\\" + w, t = "\\" + h + "\\" + v, y = "(?:(?:(\\w+(?=[\\/\\s\\" + h + "]))|(?:(\\w+)?(:)|(>)|!--((?:[^-]|-(?!-))*)--|(\\*)))\\s*((?:[^\\" + h + "]|\\" + h + "(?!\\" + v + "))*?)", o.rTag = y + ")", y = new RegExp(n + y + "(\\/)?|(?:\\/(\\w+)))" + t, "g"), yt = new RegExp("<.*>|([^\\\\]|^)[{}]|" + n + ".*" + t)), [p, w, h, v, nt] } function ir(n, t) { t || (t = n, n = i); var e, f, o, u, r = this, s = !t || t === "root"; if (n) { if (u = r.type === t ? r : i, !u) if (e = r.views, r._.useKey) { for (f in e) if (u = e[f].get(n, t)) break } else for (f = 0, o = e.length; !u && f < o; f++) u = e[f].get(n, t) } else if (s) while (r.parent.parent) u = r = r.parent; else while (r && !u) u = r.type === t ? r : i, r = r.parent; return u } function ni() { var n = this.get("item"); return n ? n.index : i } function ti() { return this.index } function rr(t) { var u, e = this, o = e.linkCtx, r = (e.ctx || {})[t]; return r === i && o && o.ctx && (r = o.ctx[t]), r === i && (r = hi[t]), r && tt(r) && !r._wrp && (u = function () { return r.apply(!this || this === n ? e : this, arguments) }, u._wrp = 1, f(u, r)), u || r } function ur(n, t, u) { var f, e, c, s = +u === u && u, h = t.linkCtx; if (s && (u = (s = t.tmpl.bnds[s - 1])(t.data, t, r)), e = u.args[0], n || s) { f = h && h.tag; f || (f = { _: { inline: !h, bnd: s }, tagName: ":", cvt: n, flow: !0, tagCtx: u, _is: "tag" }, h && (h.tag = f, f.linkCtx = h, u.ctx = a(u.ctx, h.view.ctx)), o._lnk(f)); for (c in u.props) ft.test(c) && (f[c] = u.props[c]); u.view = t; f.ctx = u.ctx || {}; delete u.ctx; t._.tag = f; e = ht(f, f.convert || n !== "true" && n)[0]; e = s && t._.onRender ? t._.onRender(e, t, s) : e; t._.tag = i } return e != i ? e : "" } function ht(n, t) { var r = n.tagCtx, u = r.view, i = r.args; return t = t && ("" + t === t ? u.getRsc("converters", t) || c("Unknown converter: '" + t + "'") : t), i = !i.length && !r.index && n.autoBind ? [u.data] : t ? i.slice() : i, t && (t.depends && (n.depends = o.getDeps(n.depends, n, t.depends, t)), i[0] = t.apply(n, i)), i } function fr(n, t) { for (var f, e, u = this; f === i && u;) e = u.tmpl[n], f = e && e[t], u = u.parent; return f || r[n][t] } function er(n, t, u, s, h) { var lt, l, wt, at, et, y, vt, k, v, ot, rt, bt, d, yt, pt, nt, p, st, tt, kt, g = "", ct = +s === s && s, w = t.linkCtx || 0, ut = t.ctx, dt = u || t.tmpl; for (n._is === "tag" && (l = n, n = l.tagName, s = l.tagCtxs), l = l || w.tag, ct && (s = (bt = dt.bnds[ct - 1])(t.data, t, r)), vt = s.length, y = 0; y < vt; y++) { if (y || u && l || (d = t.getRsc("tags", n) || c("Unknown tag: {{" + n + "}}")), v = s[y], w.tag || (rt = v.tmpl, rt = v.content = rt && dt.tmpls[rt - 1], f(v, { tmpl: (l ? l : d).template || rt, render: fi, index: y, view: t, ctx: a(v.ctx, ut) })), (u = v.props.tmpl) && (u = "" + u === u ? t.getRsc("templates", u) || e(u) : u, v.tmpl = u), l || (d._ctr ? (l = new d._ctr, yt = !!l.init, l.attr = l.attr || d.attr || i) : o._lnk(l = { render: d.render }), l._ = { inline: !w }, w && (w.attr = l.attr = w.attr || l.attr, w.tag = l, l.linkCtx = w), (l._.bnd = bt || w.fn) ? l._.arrVws = {} : l.dataBoundOnly && c("{^{" + n + "}} tag must be data-bound"), l.tagName = n, l.parent = et = ut && ut.tag, l._is = "tag", l._def = d, l.tagCtxs = s), !y) for (st in tt = v.props) ft.test(st) && (l[st] = tt[st]); v.tag = l; l.map && l.tagCtxs && (v.map = l.tagCtxs[y].map); l.flow || (ot = v.ctx = v.ctx || {}, wt = l.parents = ot.parentTags = ut && a(ot.parentTags, ut.parentTags) || {}, et && (wt[et.tagName] = et), ot.tag = l) } for (t._.tag = l, l.rendering = {}, y = 0; y < vt; y++) v = l.tagCtx = l.tagCtxs[y], tt = v.props, p = ht(l, l.convert), (pt = tt.map || l).map && (p.length || tt.map) && (nt = v.map = f(v.map || { unmap: pt.unmap }, tt), nt.src !== p[0] && (nt.src && nt.unmap(), pt.map.apply(nt, p)), p = [nt.tgt]), l.ctx = v.ctx, !y && yt && (kt = l.template, l.init(v, w, l.ctx), yt = i, l.template !== kt && (l._.tmpl = l.template)), k = i, lt = l.render, (lt = l.render) && (k = lt.apply(l, p)), p = p.length ? p : [t], k = k !== i ? k : v.render(p[0], !0) || (h ? i : ""), g = g ? g + (k || "") : k; return delete l.rendering, l.tagCtx = l.tagCtxs[0], l.ctx = l.tagCtx.ctx, l._.inline && (at = l.attr) && at !== b && (g = at === "text" ? it.html(g) : ""), ct && t._.onRender ? t._.onRender(g, t, ct) : g } function d(n, t, i, r, u, f, e, o) { var a, h, c, s = this, v = t === "array", l = { key: 0, useKey: v ? 0 : 1, id: "" + nr++, onRender: o, bnds: {} }; s.data = r; s.tmpl = u; s.content = e; s.views = v ? [] : {}; s.parent = i; s.type = t; s._ = l; s.linked = !!o; i ? (a = i.views, h = i._, h.useKey ? (a[l.key = "_" + h.useKey++] = s, s.index = pt, s.getIndex = ni, c = h.tag, l.bnd = v && (!c || !!c._.bnd && c)) : a.splice(l.key = s.index = f, 0, s), s.ctx = n || i.ctx) : s.ctx = n } function or(n) { var i, r, t, u, f, e; for (i in k) if (u = k[i], (f = u.compile) && (r = n[i + "s"])) for (t in r) r[t] = f(t, r[t], n, i, u), (e = o.onStoreItem) && e(i, t, r[t], f) } function sr(n, t, r) { var f, u; return tt(t) ? t = { depends: t.depends, render: t } : ((u = t.template) !== i && (t.template = "" + u === u ? e[u] || e(u) : u), t.init !== !1 && (f = t._ctr = function () { }, (f.prototype = t).constructor = f)), r && (t._parentTmpl = r), t } function ii(r, u, f, o, s, h) { function v(u) { if ("" + u === u || u.nodeType > 0) { try { l = u.nodeType > 0 ? u : !yt.test(u) && t && t(n.document).find(u)[0] } catch (c) { } return l && (u = e[r = r || l.getAttribute(et)], u || (r = r || "_" + gi++, l.setAttribute(et, r), u = e[r] = ii(r, l.innerHTML, f, o, s, h)), l = i), u } } var c, l; return u = u || "", c = v(u), h = h || (u.markup ? u : {}), h.tmplName = r, f && (h._parentTmpl = f), !c && u.markup && (c = v(u.markup)) && c.fn && (c.debug !== u.debug || c.allowCode !== u.allowCode) && (c = c.markup), c !== i ? (r && !f && (bt[r] = function () { return u.render.apply(u, arguments) }), c.fn || u.fn ? c.fn && (u = r && r !== c.tmplName ? a(h, c) : c) : (u = ri(c, h), lt(c.replace(yi, "\\$&"), u)), or(h), u) : void 0 } function ri(n, t) { var i, e = s.wrapMap || {}, r = f({ markup: n, tmpls: [], links: {}, tags: {}, bnds: [], _is: "template", render: ui }, t); return t.htmlTag || (i = bi.exec(n), r.htmlTag = i ? i[1].toLowerCase() : ""), i = e[r.htmlTag], i && i !== e.div && (r.markup = u.trim(r.markup)), r } function hr(n, t) { function u(e, s, h) { var v, c, l, a; if (e && "" + e !== e && !e.nodeType && !e.markup) { for (l in e) u(l, e[l], s); return r } return s === i && (s = e, e = i), e && "" + e !== e && (h = s, s = e, e = i), a = h ? h[f] = h[f] || {} : u, c = t.compile, e ? s === null ? delete a[e] : a[e] = c ? s = c(e, s, h, n, t) : s : s = c(i, s), c && s && (s._is = n), (v = o.onStoreItem) && v(n, e, s, c), s } var f = n + "s"; r[f] = u; k[n] = t } function cr(n, t) { var i = this.jquery && (this[0] || c('Unknown template: "' + this.selector + '"')), r = i.getAttribute(et); return ui.call(r ? e[r] : e(i), n, t) } function ct(n, t, i) { if (s._dbgMode) try { return n.fn(t, i, r) } catch (u) { return c(u, i) } return n.fn(t, i, r) } function ui(n, t, i, r, f, e) { var o = this; return !r && o.fn._nvw && !u.isArray(n) ? ct(o, n, { tmpl: o }) : fi.call(o, n, t, i, r, f, e) } function fi(n, t, r, f, o, s) { var y, ut, g, l, nt, tt, it, p, v, rt, w, ft, h, et, c = this, k = ""; if (!!t === t && (r = t, t = i), o === !0 && (it = !0, o = 0), c.tag ? (p = c, c = c.tag, rt = c._, ft = c.tagName, h = rt.tmpl || p.tmpl, et = c.attr && c.attr !== b, t = a(t, c.ctx), v = p.content, p.props.link === !1 && (t = t || {}, t.link = !1), f = f || p.view, n = arguments.length ? n : f) : h = c, h && (!f && n && n._is === "view" && (f = n), f && (v = v || f.content, s = s || f._.onRender, n === f && (n = f.data), t = a(t, f.ctx)), f && f.data !== i || ((t = t || {}).root = n), h.fn || (h = e[h] || e(h)), h)) { if (s = (t && t.link) !== !1 && !et && s, w = s, s === !0 && (w = i, s = f._.onRender), t = h.helpers ? a(h.helpers, t) : t, u.isArray(n) && !r) for (l = it ? f : o !== i && f || new d(t, "array", f, n, h, o, v, s), y = 0, ut = n.length; y < ut; y++) g = n[y], nt = new d(t, "item", l, g, h, (o || 0) + y, v, s), tt = ct(h, g, nt), k += l._.onRender ? l._.onRender(tt, nt) : tt; else (f || !h.fn._nvw) && (l = it ? f : new d(t, ft || "data", f, n, h, o, v, s), rt && !c.flow && (l.tag = c)), k += ct(h, n, l); return w ? w(k, l) : k } return "" } function c(n, t, i) { var r = s.onError(n, t, i); if ("" + n === n) throw new o.Err(r); return !t.linkCtx && t.linked ? it.html(r) : r } function l(n) { c("Syntax error\n" + n) } function lt(n, t, i, r) { function k(t) { t -= f; t && h.push(n.substr(f, t).replace(ut, "\\n")) } function c(t) { t && l('Unmatched or missing tag: "{{/' + t + '}}" in template:\n' + n) } function d(e, o, v, y, p, d, nt, tt, it, rt, et, ot) { d && (p = ":", y = b); rt = rt || i; var st = (o || i) && [], ct = "", lt = "", at = "", vt = "", yt = "", pt = "", wt = "", bt = "", ht = !rt && !p && !nt; v = v || (it = it || "#data", p); k(ot); f = ot + e.length; tt ? g && h.push(["*", "\n" + it.replace(vi, "$1") + "\n"]) : v ? (v === "else" && (wi.test(it) && l('for "{{else if expr}}" use "{{else expr}}"'), st = u[7], u[8] = n.substring(u[8], ot), u = s.pop(), h = u[2], ht = !0), it && si(it.replace(ut, " "), st, t).replace(pi, function (n, t, i, r, u, f, e, o) { return e ? (lt += f + ",", vt += "'" + o + "',") : i ? (at += r + f + ",", pt += r + "'" + o + "',") : t ? wt += f : (u === "trigger" && (bt += f), ct += r + f + ",", yt += r + "'" + o + "',", w = w || ft.test(u)), "" }).slice(0, -1), a = [v, y || !!r || w || "", ht && [], ei(vt, yt, pt), ei(lt, ct, at), wt, bt, st || 0], h.push(a), ht && (s.push(u), u = a, u[8] = f)) : et && (c(et !== u[0] && u[0] !== "else" && et), u[8] = n.substring(u[8], ot), u = s.pop()); c(!u && et); h = u[2] } var o, a, w, g = t && t.allowCode, e = [], f = 0, s = [], h = e, u = [, , e]; return i && (n = p + n + v), c(s[0] && s[0][2].pop()[0]), n.replace(y, d), k(n.length), (f = e[e.length - 1]) && c("" + f !== f && +f[8] === f[8] && f[0]), i ? (o = at(e, n, i), o.paths = e[0][7]) : o = at(e, t), o._nvw && (o._nvw = !/[~#]/.test(n)), o } function ei(n, t, i) { return [n.slice(0, -1), t.slice(0, -1), i.slice(0, -1)] } function oi(n, t) { return "\n\t" + (t ? t + ":{" : "") + "args:[" + n[0] + "]" + (n[1] || !t ? ",\n\tprops:{" + n[1] + "}" : "") + (n[2] ? ",\n\tctx:{" + n[2] + "}" : "") } function si(n, t, i) { function p(p, b, k, d, g, nt, tt, it, rt, ut, ft, et, ot, st, ht, ct, at, vt, yt, pt) { function dt(n, i, r, f, e, s, h, l) { if (r && (t && (u === "linkTo" && (o = t._jsvto = t._jsvto || [], o.push(g)), (!u || c) && t.push(g.slice(i.length))), r !== ".")) { var a = (f ? 'view.hlp("' + f + '")' : e ? "view" : "data") + (l ? (s ? "." + s : f ? "" : e ? "" : "." + r) + (h || "") : (l = f ? "" : e ? s || "" : r, "")); return a = a + (l ? "." + l : ""), i + (a.slice(0, 9) === "view.data" ? a.slice(5) : a) } return n } nt = nt || ""; k = k || b || et; g = g || rt; ut = ut || at || ""; var bt, kt, wt; if (!tt || e || f) return t && ct && !e && !f && (!u || c || o) && (bt = y[r], pt.length - 1 > yt - bt && (bt = pt.slice(bt, yt + 1), ct = w + ":" + bt + h, wt = v[ct], wt || (v[ct] = 1, v[ct] = wt = lt(ct, i || t, !0), wt.paths.push({ _jsvOb: wt })), wt !== 1 && (o || t).push({ _jsvOb: wt }))), e ? (e = !ot, e ? p : '"') : f ? (f = !st, f ? p : '"') : (k ? (r++, y[r] = yt++, k) : "") + (vt ? r ? "" : (s = pt.slice(s, yt), u ? (u = c = o = !1, "\b") : "\b,") + s + (s = yt + p.length, "\b") : it ? (r && l(n), u = g, c = d, s = yt + p.length, g + ":") : g ? g.split("^").join(".").replace(li, dt) + (ut ? (a[++r] = !0, g.charAt(0) !== "." && (y[r] = yt), kt ? "" : ut) : nt) : nt ? nt : ht ? (a[r--] = !1, ht) + (ut ? (a[++r] = !0, ut) : "") : ft ? (a[r] || l(n), ",") : b ? "" : (e = ot, f = st, '"')); l(n) } var u, o, c, f, e, s = 0, v = i ? i.links : t && (t.links = t.links || {}), a = {}, y = { 0: -1 }, r = 0; return (n + (i ? " " : "")).replace(/\)\^/g, ").").replace(ai, p) } function at(n, i, r) { var y, f, e, c, d, ht, ct, bt, lt, g, rt, p, o, ft, et, v, nt, w, tt, vt, k, yt, pt, ot, s, a, st, wt, h = 0, u = "", it = {}, kt = n.length; for ("" + i === i ? (v = r ? 'data-link="' + i.replace(ut, " ").slice(1, -1) + '"' : i, i = 0) : (v = i.tmplName || "unnamed", i.allowCode && (it.allowCode = !0), i.debug && (it.debug = !0), p = i.bnds, et = i.tmpls), y = 0; y < kt; y++) if (f = n[y], "" + f === f) u += '\n+"' + f + '"'; else if (e = f[0], e === "*") u += ";\n" + f[1] + "\nret=ret"; else { if (c = f[1], tt = f[2], d = oi(f[3], "params") + "}," + oi(ft = f[4]), a = f[5], wt = f[6], vt = f[8], (pt = e === "else") || (h = 0, p && (o = f[7]) && (h = p.push(o))), (ot = e === ":") ? c && (e = c === b ? ">" : c + e) : (tt && (nt = ri(vt, it), nt.tmplName = v + "/" + e, at(tt, nt), et.push(nt)), pt || (w = e, yt = u, u = ""), k = n[y + 1], k = k && k[0] === "else"), st = a ? ";\ntry{\nret+=" : "\n+", ot && (o || wt || c && c !== b)) { if (s = "return {" + d + "};", a && (s = "try {\n" + s + "\n}catch(e){return {error: j._err(e,view," + a + ")}}\n"), s = new Function("data,view,j,u", " // " + v + " " + h + " " + e + "\n" + s), s.paths = o, s._tag = e, r) return s; rt = 1 } if (u += ot ? (r ? (a ? "\ntry{\n" : "") + "return " : st) + (rt ? (rt = 0, g = lt = !0, 'c("' + c + '",view,' + (o ? (p[h - 1] = s, h) : "{" + d + "}") + ")") : e === ">" ? (ct = !0, "h(" + ft[0] + ")") : (bt = !0, "((v=" + ft[0] + ')!=null?v:"")')) : (g = ht = !0, "\n{view:view,tmpl:" + (tt ? et.length : "0") + "," + d + "},"), w && !k) { if (u = "[" + u.slice(0, -1) + "]", (r || o) && (u = new Function("data,view,j,u", " // " + v + " " + h + " " + w + "\nreturn " + u + ";"), o && ((p[h - 1] = u).paths = o), u._tag = e, r)) return u; u = yt + st + 't("' + w + '",view,this,' + (h || u) + ")"; o = 0; w = 0 } a && (g = !0, u += ";\n}catch(e){ret" + (r ? "urn " : "+=") + "j._err(e,view," + a + ");}\n" + (r ? "" : "ret=ret")) } u = "// " + v + "\nj=j||" + (t ? "jQuery." : "jsviews.") + "views;var v" + (ht ? ",t=j._tag" : "") + (lt ? ",c=j._cnvt" : "") + (ct ? ",h=j.converters.html" : "") + (r ? ";\n" : ',ret=""\n') + (it.debug ? "debugger;" : "") + u + (r ? "\n" : ";\nreturn ret;"); try { u = new Function("data,view,j,u", u) } catch (dt) { l("Compiled template code:\n\n" + u + '\n: "' + dt.message + '"') } return i && (i.fn = u), g || (u._nvw = !0), u } function a(n, t) { return n && n !== t ? t ? f(f({}, t), n) : n : t && f({}, t) } function lr(n) { return wt[n] || (wt[n] = "&#" + n.charCodeAt(0) + ";") } function ar(n) { var i, t, r = []; if (typeof n == "object") for (i in n) t = n[i], t && t.toJSON && !t.toJSON() || tt(t) || r.push({ key: i, prop: n[i] }); return r } function ci(n) { return n != null ? ki.test(n) && ("" + n).replace(di, lr) || n : "" } if ((!t || !t.views) && !n.jsviews) { var u, rt, y, yt, pt, p = "{", w = "{", h = "}", v = "}", nt = "^", li = /^(!*?)(?:null|true|false|\d[\d.]*|([\w$]+|\.|~([\w$]+)|#(view|([\w$]+))?)([\w$.^]*?)(?:[.[^]([\w$]+)\]?)?)$/g, ai = /(\()(?=\s*\()|(?:([([])\s*)?(?:(\^?)(!*?[#~]?[\w$.^]+)?\s*((\+\+|--)|\+|-|&&|\|\||===|!==|==|!=|<=|>=|[<>%*:?\/]|(=))\s*|(!*?[#~]?[\w$.^]+)([([])?)|(,\s*)|(\(?)\\?(?:(')|("))|(?:\s*(([)\]])(?=\s*\.|\s*\^|\s*$)|[)\]])([([]?))|(\s+)/g, ut = /[ \t]*(\r\n|\n|\r)/g, vi = /\\(['"])/g, yi = /['"\\]/g, pi = /(?:\x08|^)(onerror:)?(?:(~?)(([\w$]+):)?([^\x08]+))\x08(,)?([^\x08]+)/gi, wi = /^if\s/, bi = /<(\w+)[>\s]/, ki = /[\x00`><\"'&]/, ft = /^on[A-Z]|^convert(Back)?$/, di = /[\x00`><"'&]/g, gi = 0, nr = 0, wt = { "&": "&amp;", "<": "&lt;", ">": "&gt;", "\x00": "&#0;", "'": "&#39;", '"': "&#34;", "`": "&#96;" }, b = "html", et = "data-jsv-tmpl", bt = {}, k = { template: { compile: ii }, tag: { compile: sr }, helper: {}, converter: {} }, r = { jsviews: "v1.0.0-beta", settings: function (n) { f(s, n); kt(s._dbgMode); s.jsv && s.jsv() }, sub: { View: d, Err: st, tmplFn: lt, cvt: ht, parse: si, extend: f, syntaxErr: l, DataMap: dt, _lnk: ot }, _cnvt: ur, _tag: er, _err: c }; (st.prototype = new Error).constructor = st; ni.depends = function () { return [this.get("item"), "index"] }; ti.depends = function () { return ["index"] }; d.prototype = { get: ir, getIndex: ti, getRsc: fr, hlp: rr, _is: "view" }; for (rt in k) hr(rt, k[rt]); var vt, e = r.templates, it = r.converters, hi = r.helpers, g = r.tags, o = r.sub, s = r.settings; t ? (u = t, u.fn.render = cr, (vt = u.observable) && (f(o, vt.sub), delete vt.sub)) : (u = n.jsviews = {}, u.isArray = Array && Array.isArray || function (n) { return Object.prototype.toString.call(n) === "[object Array]" }); u.render = bt; u.views = r; u.templates = e = r.templates; s({ debugMode: kt, delimiters: gt, onError: function (n, t, r) { return t && (n = r === i ? "{Error: " + n + "}" : tt(r) ? r(n, t) : r), n }, _dbgMode: !0 }); g({ "else": function () { }, "if": { render: function (n) { var t = this; return t.rendering.done || !n && (arguments.length || !t.tagCtx.index) ? "" : (t.rendering.done = !0, t.selected = t.tagCtx.index, t.tagCtx.render(t.tagCtx.view, !0)) }, onUpdate: function (n, t, i) { for (var r, f, u = 0; (r = this.tagCtxs[u]) && r.args.length; u++) if (r = r.args[0], f = !r != !i[u].args[0], !this.convert && !!r || f) return f; return !1 }, flow: !0 }, "for": { render: function (n) { var f, t = this, r = t.tagCtx, e = "", o = 0; return t.rendering.done || ((f = !arguments.length) && (n = r.view.data), n !== i && (e += r.render(n, f), o += u.isArray(n) ? n.length : 1), (t.rendering.done = o) && (t.selected = r.index)), e }, flow: !0, autoBind: !0 }, include: { flow: !0, autoBind: !0 }, "*": { render: ot, flow: !0 } }); g({ props: f(f({}, g.for), dt(ar)) }); g.props.autoBind = !0; it({ html: ci, attr: ci, url: function (n) { return n != i ? encodeURI("" + n) : n === null ? n : "" } }); gt() } })(this, this.jQuery);
/*
//# sourceMappingURL=jsrender.min.js.map
*/



///////////////////////////////////
//////// Modular ////////
///////////////////////////////////
var Modular = {
  contentData: "",
  spinner: false,
  redirectPage: "",
  hiddenfieldForm: "body",
  hiddenHtmlID: "",
  hiddenfieldData: "",
  spinnerImage: '<img src="/Content/Images/SpinnerWhite.gif" alt="Please Wait" style="z-index:11004;" width="100" height="100"  />',
  close: "",
  content: "",
  spinnerTimeOut: 0,
  forceTop: false,
  isWindowOpen: false,
  fireSpinnerOnClose: false,

  init: function () {
    jQuery(document).bind("keydown", function (e) {
      if (e.keyCode === 27 && document.getElementById("jwmOverlay")) {
        if (this.fireSpinnerOnClose) {
          this.PopupSpinner(200);
        }
        Modular.ClosePopup(Modular.redirectPage);
      }
    });
  } (),

  CreateOverlayAndWrap: function () {

    this.close = jQuery('<div id="jwmClose"><img id="fancycloseImg" src="/Content/Images/FancyClose.png" alt="Click To Close" style="z-index:1104;"  /></div>');
    this.content = jQuery('<div id="jwmContent"></div>');

    jQuery('body').append(jQuery('<div id="jwmOverlay"></div>'));
    jQuery('body').append(jQuery('<div id="jwmWrap"></div>').append(this.content, this.close));

  },

  PopupContent: function (cData, rPage) {
    this.CreateOverlayAndWrap();
    this.content.addClass("jwmPopupContentCss");
    this.content.append(cData);
    this.redirectPage = rPage;
    this.PositionOfPopup(false);

  },

  PopupContentUsingJSON: function (jsonString, rPage) {
    this.contentData = jsonString.Header + jsonString.Body;
    this.PopupContent(contentData, rPage);
  },

  PopupSpinnerAndContent: function (cData) {
    this.CreateOverlayAndWrap();
    this.content.append(cData + this.spinnerImage);
    this.PositionOfPopup(false);
  },

  PopupSpinner: function (timeOut, element) {
    if (!timeOut || timeOut < 0) {
      timeOut = 0;
    }
    if (timeOut > 10000) {
      timeOut = 10000;
    }
    this.Destroy();
    Modular.spinnerTimeOut = setTimeout(function () {
      Modular.CreateOverlayAndWrap();
      Modular.close.css({ 'display': 'none' });
      //Modular.content.append(Modular.spinnerImage);
      if (element) {
      } else {
        element = '#jwmContent';
      }

      Modular.LargeSpinner(element);
      Modular.PositionOfPopup(true);
    }, timeOut);

  },

  PopupSpinnerWait: function () {
    this.CreateOverlayAndWrap();
    this.close.css({ 'display': 'none' });
    //this.content.append(this.spinnerImage);
    Modular.LargeSpinner('#jwmContent');
    this.PositionOfPopup(true);
  },

  PopupUseHiddenHTML: function (thisID, rPage, destroy, fireSpinner) {

    if (fireSpinner) {
      this.fireSpinnerOnClose = true;
    }

    this.contentData = document.getElementById(thisID);
    this.contentData.style.display = "block";
    //this.contentData = jQuery("#" + thisID).show(); //.append(this.contentData);
    this.hiddenfieldData = this.contentData;
    this.hiddenHtmlID = thisID;

    this.PopupContent(this.contentData, rPage);
    Modular.isWindowOpen = true;
  },

  PopupDefault: function () {
    this.contentData = '<div id="jwmPopupWindow" >';
    this.contentData += '<ul>';
    this.contentData += '<li class="jwmPopupMessageTitleText">';
    this.contentData += 'Please click to continue...';
    this.contentData += '</li>';
    this.contentData += '</ul>';
    this.contentData += '</div>';

    this.PopupContent(this.contentData, '');
  },

  PositionOfPopup: function (spinner) {
    jQuery(document).ready(function () {
      windowWidth = document.documentElement.clientWidth;
      windowHeight = document.documentElement.clientHeight;

      popupHeight = jQuery("#jwmContent").height();
      popupWidth = jQuery("#jwmContent").width();
      jQuery("#jwmContent").css({
        "position": "fixed",
        "top": windowHeight / 2 - popupHeight / 2,
        "left": windowWidth / 2 - popupWidth / 2
      });

      jQuery("#jwmClose").css({
        "position": "fixed",
        "top": windowHeight / 2 - popupHeight / 2,
        "left": (windowWidth / 2 - popupWidth / 2 + jQuery("#jwmContent").width()) + 13
      });

      // Wire up to the window resize event
      jQuery(window).resize(function () {
        // Call the function
        Modular.CenteringPopup();
      });

      // And call it so it runs immediately
      Modular.CenteringPopup();

      if (spinner === false) {
        jQuery("#jwmClose, #jwmOverlay").click(function () {
          Modular.ClosePopup(Modular.redirectPage);
        });

      }

    });
  },

  CloseSpinner: function () {
    if (Modular.spinnerTimeOut) {
      clearTimeout(Modular.spinnerTimeOut);
    }
    if (document.getElementById("jwmOverlay")) {
      document.getElementById("jwmWrap").parentNode.removeChild(document.getElementById("jwmWrap"));
      document.getElementById("jwmOverlay").parentNode.removeChild(document.getElementById("jwmOverlay"));
    }
  },

  Destroy: function () {
    if (Modular.spinnerTimeOut) {
      clearTimeout(Modular.spinnerTimeOut);
    }
    if (document.getElementById("jwmOverlay")) {
      document.getElementById("jwmOverlay").parentNode.removeChild(document.getElementById("jwmOverlay"));
    }
    if (document.getElementById("jwmWrap")) {
      document.getElementById("jwmWrap").parentNode.removeChild(document.getElementById("jwmWrap"));
    }
  },

  ResetHiddenHTML: function () {
    var popup = document.getElementById('this.hiddenHtmlID');
    popup.style.display = "none";
    document.getElementByTag("body")[0].appendChild(popup.parentNode.removeChild(popup));

    Modular.Destroy();

  },

  ClosePopup: function (rPage) {

    if (typeof AvatarUpdate == 'undefined' || !AvatarUpdate.isUpdatingStart) {
      jQuery("#" + this.hiddenHtmlID).hide();
      jQuery('body').append(jQuery("#" + this.hiddenHtmlID));
      if (this.fireSpinnerOnClose) {
        this.PopupSpinner(200);
      } else {
        this.CloseSpinner();
      }
      jQuery(document).unbind("keypress");
      if (rPage) {
        Modular.redirectPage = rPage;
        setTimeout("Modular.ReloadPage()", 50);
      }
      Modular.isWindowOpen = false;
    }
    else {
      Notifier.success('Image updation is in progress');
    }
  },

  MiniSpinner: function (element) {
    jQuery(element).spin({
      lines: 11, // The number of lines to draw
      length: 3, // The length of each line
      width: 2, // The line thickness
      radius: 2, // The radius of the inner circle
      corners: 1, // Corner roundness (0..1)
      rotate: 0, // The rotation offset
      color: '#ffffff', // #rgb or #rrggbb
      speed: 1, // Rounds per second
      trail: 60, // Afterglow percentage
      shadow: false, // Whether to render a shadow
      hwaccel: false, // Whether to use hardware acceleration
      className: 'spinner', // The CSS class to assign to the spinner
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      top: '5', // Top position relative to parent in px
      left: '5' // Left position relative to parent in px
    });
  },

  MediumSpinner: function (element) {
    jQuery(element).spin({
      lines: 13, // The number of lines to draw
      length: 7, // The length of each line
      width: 4, // The line thickness
      radius: 10, // The radius of the inner circle
      corners: 1, // Corner roundness (0..1)
      rotate: 0, // The rotation offset
      color: '#ffffff', // #rgb or #rrggbb
      speed: 1, // Rounds per second
      trail: 60, // Afterglow percentage
      shadow: false, // Whether to render a shadow
      hwaccel: false, // Whether to use hardware acceleration
      className: 'spinner', // The CSS class to assign to the spinner
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      top: '5', // Top position relative to parent in px
      left: '5' // Left position relative to parent in px
    });
  },

  LargeSpinner: function (element) {

    if (element.substring(0, 1) === "#") {
      element = element.substring(1);
    }

    var spinnerOpts = {
      lines: 13, // The number of lines to draw
      length: 28, // The length of each line
      width: 12, // The line thickness
      radius: 35, // The radius of the inner circle
      corners: 1, // Corner roundness (0..1)
      rotate: 0, // The rotation offset
      direction: 1, // 1: clockwise, -1: counterclockwise
      color: '#000', // #rgb or #rrggbb or array of colors
      speed: 0.9, // Rounds per second
      trail: 60, // Afterglow percentage
      shadow: false, // Whether to render a shadow
      hwaccel: false, // Whether to use hardware acceleration
      className: 'spinner', // The CSS class to assign to the spinner
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      top: '50%', // Top position relative to parent
      left: '50%' // Left position relative to parent
    };
    var target = document.getElementById(element);
    var spinner = new Spinner(spinnerOpts).spin(target);

    //var spinner = new Spinner().spin();
    //target.appendChild(spinner.el);



    //jQuery(element).spin({
    //  lines: 15, // The number of lines to draw
    //  length: 15, // The length of each line
    //  width: 4, // The line thickness
    //  radius: 20, // The radius of the inner circle
    //  corners: 1, // Corner roundness (0..1)
    //  rotate: 0, // The rotation offset
    //  color: '#ffffff', // #rgb or #rrggbb
    //  speed: 1, // Rounds per second
    //  trail: 60, // Afterglow percentage
    //  shadow: false, // Whether to render a shadow
    //  hwaccel: false, // Whether to use hardware acceleration
    //  className: 'spinner', // The CSS class to assign to the spinner
    //  zIndex: 2e9, // The z-index (defaults to 2000000000)
    //  top: 'inherit', // Top position relative to parent in px
    //  left: 'inherit' // Left position relative to parent in px
    //});
  },


  ReloadPage: function () {
    if (Modular.redirectPage == "reload") {
      location.reload();
    }
    else {
      if (Modular.redirectPage === '/') {
        Modular.redirectPage = '';
      }
      window.location.href = jwd.Globals.Domain + Modular.redirectPage;
    }
  },

  CenteringPopup: function () {
    if (!this.forceTop) {
        // Get x and y co-ordinates
        var xPosition = (jQuery(window).width() - this.content[0].clientWidth) / 2;                //this.content.outerWidth(true);       IE-9 issue.
        var yPosition = (jQuery(window).height() - this.content[0].clientHeight) / 2;              ////this.content.outerHeight(true);

      // Check the element doesn't off the screen (do this so if the window
      // is smaller than the content, the content stays flush with the left of
      // the window, rather than going beyond it).
      if (xPosition >= jQuery('body').offset().left) {
        // Set the left position
        this.content.css({ 'left': xPosition + 'px' });
        this.close.css({ 'left': xPosition + jQuery("#jwmContent").width() + 15 + 'px' });
      }
      else {
        // Go back to normal
        this.content.css({ 'left': 'auto' });
        this.close.css({ 'left': 'auto' });
      }

      // Same check for the y co-ordinates
      if (yPosition >= jQuery('body').offset().top) {
        // Set the left position
        this.content.css({ 'top': yPosition + 'px' });
        this.close.css({ 'top': yPosition - 10 + 'px' });
      }
      else {
        // Go back to normal
        this.content.css({ 'top': 'auto' });
        this.close.css({ 'top': 'auto' });
      }
    }
  },

  PopupPartialHTML: function (cData) {
    this.forceTop = true;
    this.contentData = cData;
    this.PopupContent(this.contentData, '');

    //this.content.css({ 'top': '0', 'overflow-y': 'scroll' });

    var popupheight = jQuery(".jwmPopupContentCss").height();

    if (popupheight > jQuery(window).height() - 50) {
      var maxH = (jQuery(window).height() - 50).toString() + 'px';
      this.content.css({ 'overflow-y': 'auto', 'max-height': maxH, 'top': '25px' });
      var left = (parseInt(this.close.css('left'), 10) + 10).toString() + 'px';
      this.close.css({ 'top': '0px', 'left': left });
    }
    else {
      var maxH = ((jQuery(window).height() - popupheight) / 2).toString() + 'px';
      this.content.css({ 'top': maxH });
      this.close.css({ 'top': maxH, 'left': left });
    }


  }

};


/*    json2.js    2012-10-08    Public Domain.    NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.    See http://www.JSON.org/js.html    This code should be minified before deployment.    See http://javascript.crockford.com/jsmin.html    USE YOUR OWN COPY. IT IS EXTREMELY UNWISE TO LOAD CODE FROM SERVERS YOU DO    NOT CONTROL.    This file creates a global JSON object containing two methods: stringify    and parse.        JSON.stringify(value, replacer, space)            value       any JavaScript value, usually an object or array.            replacer    an optional parameter that determines how object                        values are stringified for objects. It can be a                        function or an array of strings.            space       an optional parameter that specifies the indentation                        of nested structures. If it is omitted, the text will                        be packed without extra whitespace. If it is a number,                        it will specify the number of spaces to indent at each                        level. If it is a string (such as '\t' or '&nbsp;'),                        it contains the characters used to indent at each level.            This method produces a JSON text from a JavaScript value.            When an object value is found, if the object contains a toJSON            method, its toJSON method will be called and the result will be            stringified. A toJSON method does not serialize: it returns the            value represented by the name/value pair that should be serialized,            or undefined if nothing should be serialized. The toJSON method            will be passed the key associated with the value, and this will be            bound to the value            For example, this would serialize Dates as ISO strings.                Date.prototype.toJSON = function (key) {                    function f(n) {                        // Format integers to have at least two digits.                        return n < 10 ? '0' + n : n;                    }                    return this.getUTCFullYear()   + '-' +                         f(this.getUTCMonth() + 1) + '-' +                         f(this.getUTCDate())      + 'T' +                         f(this.getUTCHours())     + ':' +                         f(this.getUTCMinutes())   + ':' +                         f(this.getUTCSeconds())   + 'Z';                };            You can provide an optional replacer method. It will be passed the            key and value of each member, with this bound to the containing            object. The value that is returned from your method will be            serialized. If your method returns undefined, then the member will            be excluded from the serialization.            If the replacer parameter is an array of strings, then it will be            used to select the members to be serialized. It filters the results            such that only members with keys listed in the replacer array are            stringified.            Values that do not have JSON representations, such as undefined or            functions, will not be serialized. Such values in objects will be            dropped; in arrays they will be replaced with null. You can use            a replacer function to replace those with JSON values.            JSON.stringify(undefined) returns undefined.            The optional space parameter produces a stringification of the            value that is filled with line breaks and indentation to make it            easier to read.            If the space parameter is a non-empty string, then that string will            be used for indentation. If the space parameter is a number, then            the indentation will be that many spaces.            Example:            text = JSON.stringify(['e', {pluribus: 'unum'}]);            // text is '["e",{"pluribus":"unum"}]'            text = JSON.stringify(['e', {pluribus: 'unum'}], null, '\t');            // text is '[\n\t"e",\n\t{\n\t\t"pluribus": "unum"\n\t}\n]'            text = JSON.stringify([new Date()], function (key, value) {                return this[key] instanceof Date ?                    'Date(' + this[key] + ')' : value;            });            // text is '["Date(---current time---)"]'        JSON.parse(text, reviver)            This method parses a JSON text to produce an object or array.            It can throw a SyntaxError exception.            The optional reviver parameter is a function that can filter and            transform the results. It receives each of the keys and values,            and its return value is used instead of the original value.            If it returns what it received, then the structure is not modified.            If it returns undefined then the member is deleted.            Example:            // Parse the text. Values that look like ISO date strings will            // be converted to Date objects.            myData = JSON.parse(text, function (key, value) {                var a;                if (typeof value === 'string') {                    a =/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);                    if (a) {                        return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4],                            +a[5], +a[6]));                    }                }                return value;            });            myData = JSON.parse('["Date(09/09/2001)"]', function (key, value) {                var d;                if (typeof value === 'string' &&                        value.slice(0, 5) === 'Date(' &&                        value.slice(-1) === ')') {                    d = new Date(value.slice(5, -1));                    if (d) {                        return d;                    }                }                return value;            });    This is a reference implementation. You are free to copy, modify, or    redistribute.*//*jslint evil: true, regexp: true *//*members "", "\b", "\t", "\n", "\f", "\r", "\"", JSON, "\\", apply,    call, charCodeAt, getUTCDate, getUTCFullYear, getUTCHours,    getUTCMinutes, getUTCMonth, getUTCSeconds, hasOwnProperty, join,    lastIndex, length, parse, prototype, push, replace, slice, stringify,    test, toJSON, toString, valueOf*/// Create a JSON object only if one does not already exist. We create the// methods in a closure to avoid creating global variables.
if (typeof JSON !== 'object') { JSON = {}; } (function () { 'use strict'; function f(n) { return n < 10 ? '0' + n : n; } if (typeof Date.prototype.toJSON !== 'function') { Date.prototype.toJSON = function (key) { return isFinite(this.valueOf()) ? this.getUTCFullYear() + '-' + f(this.getUTCMonth() + 1) + '-' + f(this.getUTCDate()) + 'T' + f(this.getUTCHours()) + ':' + f(this.getUTCMinutes()) + ':' + f(this.getUTCSeconds()) + 'Z' : null; }; String.prototype.toJSON = Number.prototype.toJSON = Boolean.prototype.toJSON = function (key) { return this.valueOf(); }; } var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g, escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g, gap, indent, meta = { '\b': '\\b', '\t': '\\t', '\n': '\\n', '\f': '\\f', '\r': '\\r', '"': '\\"', '\\': '\\\\' }, rep; function quote(string) { escapable.lastIndex = 0; return escapable.test(string) ? '"' + string.replace(escapable, function (a) { var c = meta[a]; return typeof c === 'string' ? c : '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4); }) + '"' : '"' + string + '"'; } function str(key, holder) { var i, k, v, length, mind = gap, partial, value = holder[key]; if (value && typeof value === 'object' && typeof value.toJSON === 'function') { value = value.toJSON(key); } if (typeof rep === 'function') { value = rep.call(holder, key, value); } switch (typeof value) { case 'string': return quote(value); case 'number': return isFinite(value) ? String(value) : 'null'; case 'boolean': case 'null': return String(value); case 'object': if (!value) { return 'null'; } gap += indent; partial = []; if (Object.prototype.toString.apply(value) === '[object Array]') { length = value.length; for (i = 0; i < length; i += 1) { partial[i] = str(i, value) || 'null'; } v = partial.length === 0 ? '[]' : gap ? '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']' : '[' + partial.join(',') + ']'; gap = mind; return v; } if (rep && typeof rep === 'object') { length = rep.length; for (i = 0; i < length; i += 1) { if (typeof rep[i] === 'string') { k = rep[i]; v = str(k, value); if (v) { partial.push(quote(k) + (gap ? ': ' : ':') + v); } } } } else { for (k in value) { if (Object.prototype.hasOwnProperty.call(value, k)) { v = str(k, value); if (v) { partial.push(quote(k) + (gap ? ': ' : ':') + v); } } } } v = partial.length === 0 ? '{}' : gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}' : '{' + partial.join(',') + '}'; gap = mind; return v; } } if (typeof JSON.stringify !== 'function') { JSON.stringify = function (value, replacer, space) { var i; gap = ''; indent = ''; if (typeof space === 'number') { for (i = 0; i < space; i += 1) { indent += ' '; } } else if (typeof space === 'string') { indent = space; } rep = replacer; if (replacer && typeof replacer !== 'function' && (typeof replacer !== 'object' || typeof replacer.length !== 'number')) { throw new Error('JSON.stringify'); } return str('', { '': value }); }; } if (typeof JSON.parse !== 'function') { JSON.parse = function (text, reviver) { var j; function walk(holder, key) { var k, v, value = holder[key]; if (value && typeof value === 'object') { for (k in value) { if (Object.prototype.hasOwnProperty.call(value, k)) { v = walk(value, k); if (v !== undefined) { value[k] = v; } else { delete value[k]; } } } } return reviver.call(holder, key, value); } text = String(text); cx.lastIndex = 0; if (cx.test(text)) { text = text.replace(cx, function (a) { return '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4); }); } if (/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) { j = eval('(' + text + ')'); return typeof reviver === 'function' ? walk({ '': j }, '') : j; } throw new SyntaxError('JSON.parse'); }; } }());

/*
 * jQuery mousehold plugin - fires an event while the mouse is clicked down.
 * Additionally, the function, when executed, is passed a single
 * argument representing the count of times the event has been fired during
 * this session of the mouse hold.
 *
 * @author Remy Sharp (leftlogic.com)
 * @date 2006-12-15
 * @example $("img").mousehold(200, function(i){  })
 * @desc Repeats firing the passed function while the mouse is clicked down
 *
 * @name mousehold
 * @type jQuery
 * @param Number timeout The frequency to repeat the event in milliseconds
 * @param Function fn A function to execute
 * @cat Plugin
 * 
 * Modified by Jimmy Lam (jimmykhlam.com) on 2011-11-26 to include passing
 * the origEv event from original mousedown.
 */

jQuery.fn.mousehold = function(timeout, f) {
  if (timeout && typeof timeout == 'function') {
    f = timeout;
    timeout = 100;
  }
  if (f && typeof f == 'function') {
    var timer = 0;
    var fireStep = 0;
    return this.each(function() {
      var origEv;					   //var for original event
      jQuery(this).mousedown(function(ev) {
        origEv = ev;			           //rem original event
        fireStep = 1;
        var ctr = 0;
        var t = this;
        timer = setInterval(function() {
          ctr++;
          f.call(t, ctr, ev);
          fireStep = 2;
        }, timeout);
      })

      clearMousehold = function() {
        clearInterval(timer);
        if (fireStep == 1) { f.call(this, 1, origEv); } //pass original event
        fireStep = 0;
      }
      
      jQuery(this).mouseout(clearMousehold);
      jQuery(this).mouseup(clearMousehold);
    })
  }
}

var j$ = jQuery;
