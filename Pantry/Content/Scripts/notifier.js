(function ($) {

  var config = window.NotifierjsConfig = {
    defaultTimeOut: 5000,
    position: ["top", "right"],
    container: $("<div></div>")
  };

  $(document).ready(function () {
    config.container.css("position", "fixed");
    config.container.css("z-index", 1000000);
    config.container.css(config.position[0], "12px");
    config.container.css(config.position[1], "12px");
    $("body").append(config.container);
  });

  function getNotificationElement() {
    return $("<div>").addClass("notificationStyles");
  }

  var Notifier = window.Notifier = {};

  Notifier.notify = function (message, title, iconUrl, timeOut) {
    var notificationElement = getNotificationElement();

    timeOut = timeOut || config.defaultTimeOut;

    if (iconUrl) {
      var iconElement = $("<img/>", {
        src: iconUrl,
        css: {
          width: 36,
          height: 36,
          display: "inline-block",
          verticalAlign: "middle"
        }
      });
      notificationElement.append(iconElement);
    }

    var textElement = $("<div/>").css({
      display: 'inline-block',
      verticalAlign: 'middle',
      padding: '0 12px'
    });

    if (title) {
      var titleElement = $("<div/>");
      titleElement.append(document.createTextNode(title));
      titleElement.css("font-weight", "bold");
      textElement.append(titleElement);
    }

    if (message) {
      var messageElement = $("<div/>");
      messageElement.append(document.createTextNode(message));
      textElement.append(messageElement);
    }

    notificationElement.delay(timeOut).fadeOut(function () {
      notificationElement.remove();
    });
    notificationElement.bind("click", function () {
      notificationElement.hide();
    });

    notificationElement.append(textElement);
    config.container.prepend(notificationElement);
  };

  Notifier.info = function (message, title) {
    Notifier.notify(message, title, "");
  };
  Notifier.warning = function (message, title) {
    Notifier.notify(message, title, "");
  };
  Notifier.error = function (message, title) {
    Notifier.notify(message, title, "");
  };
  Notifier.success = function (message, title) {
    Notifier.notify(message, title, "");
  };

} (jQuery));