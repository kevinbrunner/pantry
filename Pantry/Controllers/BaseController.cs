﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using Pantry.Entities;
using Pantry.Web.Mvc;

using fastJSON;


namespace Pantry.Web.Controllers {
  public class BaseController : PantryController {

    //protected override void OnActionExecuting(ActionExecutingContext filterContext)
    //{
    //  base.OnActionExecuting();
    //
    //  if (SessionFactory.CurrentAdminUser == null)
    //    filterContext.Result = new RedirectResult(Url.Action("AdminLogin", "Admin"));
    //}


    //protected override void Initialize(RequestContext requestContext) {
    protected override void Initialize(System.Web.Routing.RequestContext requestContext) {

      //always on TOP!
      base.Initialize(requestContext);

      this.ViewGlobals.ReturnUrl = Request.QueryString["ReturnUrl"] ?? "";
      this.ViewGlobals.PathAndQuery = Request.Url.PathAndQuery.ToLower();
      this.ViewGlobals.Domain = Request.Url.Authority.ToLower();


      //if ((this.ViewGlobals.Domain.IndexOf("fbcucrisis.azurewebsites.net") >= 0)) { // || (this.PageContext.Domain.IndexOf("test.digitaljuice.com") >= 0)) {

      //  bool validAuth = false;

      //  HttpCookie authCookie = Request.Cookies["PantryRestrict"];
      //  if (authCookie != null) {
      //    if (authCookie.Value == Config.SiteConfig.RestrictedCookieValue) {
      //      validAuth = true;
      //    }
      //  }

      //  HttpCookie cookie = new HttpCookie("PantryRestrict");
      //  cookie.Expires = DateTime.Now.AddDays(30);
      //  cookie.Value = Config.SiteConfig.RestrictedCookieValue;
      //  if (Request.Cookies["PantryRestrict"] != null) {
      //    Response.SetCookie(cookie);
      //  }
      //  else {
      //    Response.Cookies.Add(cookie);
      //  }
      //  if (!validAuth) {
      //    //RedirectToAction("Restricted", "Index", new { area = "" });

      //    requestContext.HttpContext.Response.Clear();
      //    requestContext.HttpContext.Response.Redirect(Url.Action("Index", "Restricted"));
      //    requestContext.HttpContext.Response.End();
      //  }
      //}

      this.ViewGlobals.PageTitle = "no page title set here!!!";
      this.ViewGlobals.Account = new Account();
      this.ViewGlobals.Account.FullName = "Lynda or John";
      this.ViewGlobals.Account.Avatar = "/Content/Images/avatar3.png";
      this.ViewGlobals.Account.Avatar2 = "/Content/Images/avatar5.png";
    }


    protected override void OnActionExecuting(ActionExecutingContext filterContext) {
      base.OnActionExecuting(filterContext);

      if ((this.ViewGlobals.Domain.IndexOf("fbcucrisis.azurewebsites.net") >= 0)) { // || (this.PageContext.Domain.IndexOf("test.digitaljuice.com") >= 0)) {

        bool validAuth = false;

        HttpCookie authCookie = Request.Cookies["PantryRestrict"];
        if (authCookie != null) {
          if (authCookie.Value == Config.SiteConfig.RestrictedCookieValue) {
            validAuth = true;
          }
        }

        HttpCookie cookie = new HttpCookie("PantryRestrict");
        cookie.Expires = DateTime.Now.AddDays(30);
        cookie.Value = Config.SiteConfig.RestrictedCookieValue;
        if (Request.Cookies["PantryRestrict"] != null) {
          Response.SetCookie(cookie);
        }
        else {
          Response.Cookies.Add(cookie);
        }
        if (!validAuth) {
          //RedirectToAction("Index", "Restricted", new { area = "" });

          filterContext.Result = RedirectToAction("Index", "Restricted", new { area = "" });

          //requestContext.HttpContext.Response.Clear();
          //requestContext.HttpContext.Response.Redirect(Url.Action("Index", "Restricted"));
          //requestContext.HttpContext.Response.End();
        }
      }
      if (filterContext.ActionDescriptor.ActionName == "About")
        filterContext.Result = RedirectToAction("Index");
    }


  }
}
