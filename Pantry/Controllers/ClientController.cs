﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Pantry.Config;
using Pantry.Entities;
using Pantry.Helpers;
using Pantry.Services;
using Pantry.Utilities;
using Pantry.Web;

namespace Pantry.Web.Controllers {
  public class ClientController : BaseController {

    FamilyService familyService = new FamilyService();
    LoggingService loggingService = new LoggingService();
    ReportsService reportsService  = new ReportsService();
    NotesService notesService = new NotesService();

    public ClientController() {
      this.ViewGlobals.PageType = "Client Access";
      this.ViewGlobals.PageSubType = "Control Panel";
      this.ViewGlobals.BreadCrumbs.Add(new BreadCrumb("Client Access"));
      this.ViewGlobals.PageLayout = @"~/Views/Shared/_LayoutEdit.cshtml";
    }


    protected override void Initialize(System.Web.Routing.RequestContext requestContext) {

      base.Initialize(requestContext);

      string ReturnUrl = Request.QueryString["ReturnUrl"] ?? "";
      string PathAndQuery = Request.Url.PathAndQuery.ToLower();
      string Domain = Request.Url.Authority.ToLower();

    }


    [HttpGet]
    //public ActionResult Index(string id = "", string id2 = "", string id3 = "") {
    public ActionResult Index(int id = 0) {
      this.ViewGlobals.PageLayout = @"~/Views/Shared/_LayoutEdit.cshtml";
      this.ViewGlobals.BreadCrumbs.Add(new BreadCrumb("Client Access"));

      Models.ClientMain pageModel = new Models.ClientMain();
      int pb = 0;
      if(pb > 0) {
        pageModel.IsPostBackSuccessful = true;
      }

      int familyID = 0;

      string id2 = id.ToString();
      if(int.TryParse(id2, out familyID)&& familyID > 0) {

        FamilyData familyData = familyService.GetFamily(familyID);

        if(familyData.FamilyHead != null) {
          pageModel.FamilyWeb.FamilyHead = familyData.FamilyHead;
          if(familyData.FamilyHead != null && familyData.FamilyHead.FamilyID > 0) {
            pageModel.FamilyWeb.Count = 1;
          }
        }

        int familyFamilyMembersCount = 0;
        int familyPickUpAssociates = 0;

        if(familyData.FamilyMembers != null && familyData.FamilyMembers.Count > 0) {
          pageModel.FamilyWeb.FamilyMembers = familyData.FamilyMembers.Where(w => w.FamilyRelationship != FamilyRelationship.PickUp).ToList();
          familyFamilyMembersCount = familyData.FamilyMembers.Count;
          pageModel.FamilyWeb.Count += familyData.FamilyMembers.Count;
        }
        if(familyData.PickUpAssociates != null && familyData.PickUpAssociates.Count > 0) {
          pageModel.FamilyWeb.PickUpAssociates = familyData.PickUpAssociates;
          familyPickUpAssociates = pageModel.FamilyWeb.PickUpAssociates.Count;
        }
        for(int i = familyFamilyMembersCount; i < 10; i++) {
          pageModel.FamilyWeb.FamilyMembers.Add(new FamilyMember() { IsActive = true });
        }
        for(int i = familyPickUpAssociates; i < 3; i++) {
          pageModel.FamilyWeb.PickUpAssociates.Add(new FamilyMember());
        }
        pageModel.FamilyWeb.LastVisits = familyData.LastVisits;

        pageModel.Notes = notesService.GetNotes(familyID, NoteType.All).ToList();
      } else {
        pageModel.FamilyWeb = new FamilyData();
        pageModel.FamilyWeb.FamilyHead = new FamilyHead();
        pageModel.FamilyWeb.FamilyMembers = new List<FamilyMember>();
        for(int i = 0; i < 10; i++) {
          pageModel.FamilyWeb.FamilyMembers.Add(new FamilyMember());
        }
        for(int i = 0; i < 3; i++) {
          pageModel.FamilyWeb.PickUpAssociates.Add(new FamilyMember(FamilyRelationship.PickUp));
        }
        pageModel.FamilyWeb.LastVisits = new List<LastVisit>();

        pageModel.Notes = new List<Note>();
      }

      VisitCounts visitCounts = reportsService.GetVisitCounts(DateTime.Now.Date, DateTime.Now.Date.AddDays(1).AddSeconds(-1));
      this.ViewGlobals.FamiliesCount = visitCounts.FamiliesCount;
      this.ViewGlobals.FamilyMembersCount = visitCounts.FamilyMembersCount;

      this.ViewGlobals.ServiceLevel = Config.ThisVisit.ServiceLevel;

      return View(pageModel);
    }


    [HttpPost]
    public ActionResult Index(Models.ClientMain pageModel) {
      this.ViewGlobals.PageLayout = @"~/Views/Shared/_LayoutEdit.cshtml";
      this.ViewGlobals.BreadCrumbs.Add(new BreadCrumb("Client Access"));
      pageModel.PageErrors = new List<string>();
      pageModel.PageWarnings = new List<string>();

      if(pageModel.ButtonClicked == "SaveNote") {
        pageModel.NewNote.CreateDate = DateTime.Now;
        pageModel.NewNote.TransferDate = new DateTime(1970, 1, 1);
        pageModel.NewNote.FamilyID = pageModel.FamilyWeb.FamilyHead.FamilyID;
        notesService.SaveNote(pageModel.NewNote);

        return RedirectToAction("Edit", "Home", new { id = pageModel.FamilyWeb.FamilyHead.FamilyID });
      }

      if(pageModel.ButtonClicked == "ApplyBreadProduceVisit") {
        pageModel.FamilyWeb.FamilyHead.VisitType = VisitType.BreadAndProduce;
        pageModel.ButtonClicked = "ApplyVisitOnly";
      } else if(pageModel.ButtonClicked == "ApplyGroceryVisit") {
        pageModel.FamilyWeb.FamilyHead.VisitType = VisitType.Grocery;
        pageModel.ButtonClicked = "ApplyVisitOnly";
      }

      if(pageModel.ButtonClicked == "ApplyVisitOnly") {
        pageModel.FamilyWeb.FamilyHead.LastVisitDate = DateTime.Now;
        loggingService.SaveFamilyVisit(pageModel.FamilyWeb.FamilyHead, pageModel.FamilyWeb.FamilyMembers, Config.ThisVisit.ServiceLevel, pageModel.FamilyWeb.FamilyHead.VisitType, true, this.ViewGlobals.PantryID);
      } else {

        if(pageModel.FamilyWeb.FamilyHead.FirstName.IsEmpty()) {
          pageModel.PageErrors.Add("First Name required");
        }

        if(pageModel.FamilyWeb.FamilyHead.LastName.IsEmpty()) {
          pageModel.PageErrors.Add("Last Name required");
        }

        if(pageModel.FamilyWeb.FamilyHead.Social.IsEmpty()) {
          pageModel.PageErrors.Add("Social Last 4 Digits are required");
        }


        switch(pageModel.FamilyWeb.FamilyHead.FamilyType) {
          case FamilyType.Homeless: {
              if(pageModel.FamilyWeb.FamilyHead.Address1.IsEmpty()) {
                pageModel.FamilyWeb.FamilyHead.Address1 = "STATE ROAD 19";
              }

              if(pageModel.FamilyWeb.FamilyHead.City.IsEmpty()) {
                pageModel.FamilyWeb.FamilyHead.City = "UMATILLA";
              }

              if(pageModel.FamilyWeb.FamilyHead.Zip.IsEmpty()) {
                pageModel.FamilyWeb.FamilyHead.Zip = "32784";
              }

              pageModel.FamilyWeb.FamilyHead.Zip = pageModel.FamilyWeb.FamilyHead.Zip.SetAndTrim();
              if(pageModel.FamilyWeb.FamilyHead.Zip.IsEmpty()) {
                pageModel.FamilyWeb.FamilyHead.Zip = "32784";
              } else if(!Config.AppConfig.AllowedZips.Contains(pageModel.FamilyWeb.FamilyHead.Zip)) {
                pageModel.PageWarnings.Add("Zip out of Area");
              }
            }
            break;

          case FamilyType.RecoveryVillage: {
              if(pageModel.FamilyWeb.FamilyHead.Address1.IsEmpty()) {
                pageModel.FamilyWeb.FamilyHead.Address1 = "633 UMATILLA BLVD";
              }

              if(pageModel.FamilyWeb.FamilyHead.City.IsEmpty()) {
                pageModel.FamilyWeb.FamilyHead.City = "UMATILLA";
              }

              if(pageModel.FamilyWeb.FamilyHead.Zip.IsEmpty()) {
                pageModel.FamilyWeb.FamilyHead.Zip = "32784";
              }

              pageModel.FamilyWeb.FamilyHead.Zip = pageModel.FamilyWeb.FamilyHead.Zip.SetAndTrim();
              if(pageModel.FamilyWeb.FamilyHead.Zip.IsEmpty()) {
                pageModel.FamilyWeb.FamilyHead.Zip = "32784";
              } else if(!Config.AppConfig.AllowedZips.Contains(pageModel.FamilyWeb.FamilyHead.Zip)) {
                pageModel.PageWarnings.Add("Zip out of Area");
              }
            }
            break;

          default: {
              if(pageModel.FamilyWeb.FamilyHead.Address1.IsEmpty()) {
                pageModel.PageErrors.Add("An Address required");
              }

              if(pageModel.FamilyWeb.FamilyHead.City.IsEmpty()) {
                pageModel.PageErrors.Add("A City is required");
              }

              if(pageModel.FamilyWeb.FamilyHead.Zip.IsEmpty()) {
                pageModel.PageErrors.Add("Zip is required");
              }

              pageModel.FamilyWeb.FamilyHead.Zip = pageModel.FamilyWeb.FamilyHead.Zip.SetAndTrim();
              if(!Config.AppConfig.AllowedZips.Contains(pageModel.FamilyWeb.FamilyHead.Zip)) {
                pageModel.PageWarnings.Add("Zip out of Area");
              }
            }
            break;
        }

        if(pageModel.ButtonClicked == "ApplyVisit" && pageModel.FamilyWeb.FamilyHead.FamilyID == 0) {
          pageModel.ButtonClicked = "ApplyVisitUpdate";
        }

        if(pageModel.PageErrors.Count == 0 && (pageModel.ButtonClicked == "ApplyUpdate" || pageModel.ButtonClicked == "ApplyVisitUpdate")) {
          for(int i = 0; i < pageModel.FamilyWeb.PickUpAssociates.Count; i++) {
            pageModel.FamilyWeb.PickUpAssociates[i].FamilyRelationship = FamilyRelationship.PickUp;
          }
          pageModel.FamilyWeb.FamilyHead.FamilyID = familyService.SaveFamily(pageModel.FamilyWeb.FamilyHead, pageModel.FamilyWeb.FamilyMembers, pageModel.FamilyWeb.PickUpAssociates);
        }

        if(pageModel.PageErrors.Count == 0 && (pageModel.ButtonClicked == "ApplyVisit" || pageModel.ButtonClicked == "ApplyVisitUpdate")) {
          pageModel.FamilyWeb.FamilyHead.LastVisitDate = DateTime.Now;
          loggingService.SaveFamilyVisit(pageModel.FamilyWeb.FamilyHead, pageModel.FamilyWeb.FamilyMembers, Config.ThisVisit.ServiceLevel, pageModel.FamilyWeb.FamilyHead.VisitType, false, this.ViewGlobals.PantryID);
        }
      } // if(pageModel.ButtonClicked != "ApplyVisitOnly")

      FamilyData familyData = null;

      if(pageModel.FamilyWeb.FamilyHead.FamilyID > 0) {
        familyData = familyService.GetFamily(pageModel.FamilyWeb.FamilyHead.FamilyID);
      }

      if(familyData.FamilyHead != null) {
        pageModel.FamilyWeb.FamilyHead = familyData.FamilyHead;
        pageModel.FamilyWeb.Count = 1;
      }

      int familyFamilyMembersCount = 0;

      if(familyData.FamilyMembers != null && familyData.FamilyMembers.Count > 0) {
        pageModel.FamilyWeb.FamilyMembers = familyData.FamilyMembers.Where(w => w.FamilyRelationship != FamilyRelationship.PickUp).ToList();
        familyFamilyMembersCount = familyData.FamilyMembers.Count;
        pageModel.FamilyWeb.Count += familyFamilyMembersCount;
      }

      for(int i = familyFamilyMembersCount; i < 10; i++) {
        pageModel.FamilyWeb.FamilyMembers.Add(new FamilyMember());
      }

      int familyPickUpAssociatesCount = 0;
      pageModel.FamilyWeb.PickUpAssociates = new List<FamilyMember>();
      if(familyData.PickUpAssociates != null && familyData.PickUpAssociates.Count > 0) {
        pageModel.FamilyWeb.PickUpAssociates = familyData.PickUpAssociates.Where(w => w.FamilyRelationship == FamilyRelationship.PickUp).ToList();
        familyPickUpAssociatesCount = familyData.PickUpAssociates.Count;
      }
      for(int i = familyPickUpAssociatesCount; i < 3; i++) {
        pageModel.FamilyWeb.PickUpAssociates.Add(new FamilyMember(FamilyRelationship.PickUp));
      }

      pageModel.FamilyWeb.LastVisits = familyData.LastVisits;

      pageModel.Notes = notesService.GetNotes(pageModel.FamilyWeb.FamilyHead.FamilyID, NoteType.All).ToList();

      VisitCounts visitCounts = reportsService.GetVisitCounts(DateTime.Now.Date, DateTime.Now.Date.AddDays(1).AddSeconds(-1));
      this.ViewGlobals.FamiliesCount = visitCounts.FamiliesCount;
      this.ViewGlobals.FamilyMembersCount = visitCounts.FamilyMembersCount;

      return Redirect("/Client/" + pageModel.FamilyWeb.FamilyHead.FamilyID);

      return View(pageModel);
    }


    [HttpGet]
    public ActionResult Merge(string client1 = "", string client2 = "") {

      return View();
    }


    [HttpPost]
    public FastJsonResult DeleteMember(int memberID) {

      familyService.DeleteMember(memberID);

      FastJsonResult r = new FastJsonResult { Data = new { IsSuccess = true } };

      return r;
    }


    [HttpPost]
    public FastJsonResult DeleteClient(int familyID) {

      familyService.DeleteClient(familyID);

      FastJsonResult r = new FastJsonResult { Data = new { IsSuccess = true } };

      return r;
    }


  }
}

