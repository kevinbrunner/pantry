﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Pantry.Config;
using Pantry.Entities;
using Pantry.Helpers;
using Pantry.Services;
using Pantry.Utilities;
using Pantry.Web;

namespace Pantry.Web.Controllers {
  public class HomeController : BaseController {

    FamilyService familyService = new FamilyService();
    LoggingService loggingService = new LoggingService();
    NotesService notesService = new NotesService();
    ReportsService reportsService = new ReportsService();

    public HomeController() {
      this.ViewGlobals.PageType = "Client Access";
      this.ViewGlobals.PageSubType = "Control Panel";
      this.ViewGlobals.BreadCrumbs.Add(new BreadCrumb("Home", "/"));
    }


    [HttpGet]
    public ActionResult Index() {
      this.ViewGlobals.PageLayout = @"~/Views/Shared/_Layout.cshtml";
      this.ViewGlobals.BreadCrumbs.Add(new BreadCrumb("Client Access"));

      VisitCounts visitCounts = reportsService.GetVisitCounts(DateTime.Now.Date, DateTime.Now.Date.AddDays(1).AddSeconds(-1));

      this.ViewGlobals.FamiliesCount = visitCounts.FamiliesCount;
      this.ViewGlobals.FamilyMembersCount = visitCounts.FamilyMembersCount;

      this.ViewGlobals.ServiceLevel = Config.ThisVisit.ServiceLevel;

      return View();
    }


    [HttpGet]
    public ActionResult Edit(string id = "", int pb = 0) {
      return RedirectToAction("Index", "Client");



      this.ViewGlobals.PageLayout = @"~/Views/Shared/_LayoutEdit.cshtml";
      this.ViewGlobals.BreadCrumbs.Add(new BreadCrumb("Client Access"));

      Models.ClientMain pageModel = new Models.ClientMain();

      if(pb > 0) {
        pageModel.IsPostBackSuccessful = true;
      }

      int familyID = 0;

      if(int.TryParse(id, out familyID)) {

        FamilyData familyData = familyService.GetFamily(familyID);

        if(familyData.FamilyHead != null) {
          pageModel.FamilyWeb.FamilyHead = familyData.FamilyHead;
        }

        int familyFamilyMembersCount = 0;
        int familyPickUpAssociates = 0;

        if(familyData.FamilyMembers != null && familyData.FamilyMembers.Count > 0) {
          pageModel.FamilyWeb.FamilyMembers = familyData.FamilyMembers.Where(w => w.FamilyRelationship != FamilyRelationship.PickUp).ToList();
          pageModel.FamilyWeb.PickUpAssociates = familyData.FamilyMembers.Where(w => w.FamilyRelationship == FamilyRelationship.PickUp).ToList();
          familyFamilyMembersCount = familyData.FamilyMembers.Count;
          familyPickUpAssociates = pageModel.FamilyWeb.PickUpAssociates.Count;
        }
        for(int i = familyFamilyMembersCount; i < 10; i++) {
          pageModel.FamilyWeb.FamilyMembers.Add(new FamilyMember());
        }
        for(int i = familyPickUpAssociates; i < 3; i++) {
          pageModel.FamilyWeb.PickUpAssociates.Add(new FamilyMember());
        }
        pageModel.FamilyWeb.LastVisits = familyData.LastVisits;

        pageModel.Notes = notesService.GetNotes(familyID, NoteType.All).ToList();
      } else {
        pageModel.FamilyWeb = new FamilyData();
        pageModel.FamilyWeb.FamilyHead = new FamilyHead();
        pageModel.FamilyWeb.FamilyMembers = new List<FamilyMember>();
        for(int i = 0; i < 10; i++) {
          pageModel.FamilyWeb.FamilyMembers.Add(new FamilyMember());
        }
        for(int i = 0; i < 3; i++) {
          pageModel.FamilyWeb.PickUpAssociates.Add(new FamilyMember(FamilyRelationship.PickUp));
        }
        pageModel.FamilyWeb.LastVisits = new List<LastVisit>();

        pageModel.Notes = new List<Note>();
      }

      VisitCounts visitCounts = reportsService.GetVisitCounts(DateTime.Now.Date, DateTime.Now.Date.AddDays(1).AddSeconds(-1));
      this.ViewGlobals.FamiliesCount = visitCounts.FamiliesCount;
      this.ViewGlobals.FamilyMembersCount = visitCounts.FamilyMembersCount;

      this.ViewGlobals.ServiceLevel = Config.ThisVisit.ServiceLevel;

      return View(pageModel);
    }


    [HttpPost]
    public ActionResult Index(Models.ClientMain pageModel) {
      this.ViewGlobals.PageLayout = @"~/Views/Shared/_LayoutEdit.cshtml";
      this.ViewGlobals.BreadCrumbs.Add(new BreadCrumb("Client Access"));
      pageModel.PageErrors = new List<string>();
      pageModel.PageWarnings = new List<string>();

      if(pageModel.FamilyWeb.FamilyHead.FirstName.IsEmpty()) {
        pageModel.PageErrors.Add("First Name required");
      }

      if(pageModel.FamilyWeb.FamilyHead.LastName.IsEmpty()) {
        pageModel.PageErrors.Add("Last Name required");
      }

      if(pageModel.FamilyWeb.FamilyHead.Social.IsEmpty()) {
        pageModel.PageErrors.Add("Social Last 4 Digits are required");
      }

      if(pageModel.FamilyWeb.FamilyHead.Address1.IsEmpty()) {
        pageModel.PageErrors.Add("An Address required");
      }

      if(pageModel.FamilyWeb.FamilyHead.City.IsEmpty()) {
        pageModel.PageErrors.Add("A City is required");
      }

      if(pageModel.FamilyWeb.FamilyHead.Zip.IsEmpty()) {
        pageModel.PageErrors.Add("Zip is required");
      }

      pageModel.FamilyWeb.FamilyHead.Zip = pageModel.FamilyWeb.FamilyHead.Zip.SetAndTrim();
      if(!Config.AppConfig.AllowedZips.Contains(pageModel.FamilyWeb.FamilyHead.Zip)) {
        pageModel.PageWarnings.Add("Zip out of Area");
      }

      if(pageModel.ButtonClicked == "ApplyVisit" && pageModel.FamilyWeb.FamilyHead.FamilyID == 0) {
        pageModel.ButtonClicked = "ApplyVisitUpdate";
      }

      if(pageModel.PageErrors.Count == 0 && (pageModel.ButtonClicked == "ApplyUpdate" || pageModel.ButtonClicked == "ApplyVisitUpdate")) {
        for(int i = 0; i < pageModel.FamilyWeb.PickUpAssociates.Count; i++) {
          pageModel.FamilyWeb.PickUpAssociates[i].FamilyRelationship = FamilyRelationship.PickUp;
        }
        pageModel.FamilyWeb.FamilyHead.FamilyID = familyService.SaveFamily(pageModel.FamilyWeb.FamilyHead, pageModel.FamilyWeb.FamilyMembers, pageModel.FamilyWeb.PickUpAssociates);
      }

      if(pageModel.PageErrors.Count == 0 && (pageModel.ButtonClicked == "ApplyVisit" || pageModel.ButtonClicked == "ApplyVisitUpdate")) {
        pageModel.FamilyWeb.FamilyHead.LastVisitDate = DateTime.Now;
        loggingService.SaveFamilyVisit(pageModel.FamilyWeb.FamilyHead, pageModel.FamilyWeb.FamilyMembers, Config.ThisVisit.ServiceLevel, pageModel.FamilyWeb.FamilyHead.VisitType, false, this.ViewGlobals.PantryID);
      }

      FamilyData familyData = null;

      if(pageModel.FamilyWeb.FamilyHead.FamilyID > 0) {
        familyData = familyService.GetFamily(pageModel.FamilyWeb.FamilyHead.FamilyID);
      }

      if(familyData.FamilyHead != null) {
        pageModel.FamilyWeb.FamilyHead = familyData.FamilyHead;
      }

      int familyFamilyMembersCount = 0;

      if(familyData.FamilyMembers != null && familyData.FamilyMembers.Count > 0) {
        pageModel.FamilyWeb.FamilyMembers = familyData.FamilyMembers.Where(w => w.FamilyRelationship != FamilyRelationship.PickUp).ToList();
        familyFamilyMembersCount = familyData.FamilyMembers.Count;
      }
      for(int i = familyFamilyMembersCount; i < 10; i++) {
        pageModel.FamilyWeb.FamilyMembers.Add(new FamilyMember());
      }


      int familyPickUpAssociatesCount = 0;
      if(familyData.PickUpAssociates != null && familyData.PickUpAssociates.Count > 0) {
        pageModel.FamilyWeb.PickUpAssociates = familyData.FamilyMembers.Where(w => w.FamilyRelationship == FamilyRelationship.PickUp).ToList();
        familyPickUpAssociatesCount = familyData.PickUpAssociates.Count;
      }
      for(int i = familyPickUpAssociatesCount; i < 3; i++) {
        pageModel.FamilyWeb.PickUpAssociates.Add(new FamilyMember(FamilyRelationship.PickUp));
      }

      pageModel.FamilyWeb.LastVisits = familyData.LastVisits;

      pageModel.Notes = notesService.GetNotes(pageModel.FamilyWeb.FamilyHead.FamilyID, NoteType.All).ToList();

      VisitCounts visitCounts = reportsService.GetVisitCounts(DateTime.Now.Date, DateTime.Now.Date.AddDays(1).AddSeconds(-1));
      this.ViewGlobals.FamiliesCount = visitCounts.FamiliesCount;
      this.ViewGlobals.FamilyMembersCount = visitCounts.FamilyMembersCount;

      return RedirectToAction("Edit", "Home", new { id = pageModel.FamilyWeb.FamilyHead.FamilyID, pb = 1 });

      //return View(pageModel);
    }


    [HttpPost]
    public ActionResult Edit(Models.ClientMain pageModel) {
      this.ViewGlobals.BreadCrumbs.Add(new BreadCrumb("Client Access"));
      pageModel.PageErrors = new List<string>();
      pageModel.PageWarnings = new List<string>();

      if(pageModel.ButtonClicked == "SaveNote") {
        pageModel.NewNote.CreateDate = DateTime.Now;
        pageModel.NewNote.TransferDate = new DateTime(1970, 1, 1);
        pageModel.NewNote.FamilyID = pageModel.FamilyWeb.FamilyHead.FamilyID;
        notesService.SaveNote(pageModel.NewNote);

        return RedirectToAction("Edit", "Home", new { id = pageModel.FamilyWeb.FamilyHead.FamilyID });
      }
      
      if(pageModel.FamilyWeb.FamilyHead.FirstName.IsEmpty()) {
        pageModel.PageErrors.Add("First Name required");
      }

      if(pageModel.FamilyWeb.FamilyHead.LastName.IsEmpty()) {
        pageModel.PageErrors.Add("Last Name required");
      }

      if(pageModel.FamilyWeb.FamilyHead.Social.IsEmpty()) {
        pageModel.PageErrors.Add("Social Last 4 Digits are required");
      }

      if(pageModel.FamilyWeb.FamilyHead.FamilyType == FamilyType.Homeless) {
        if(pageModel.FamilyWeb.FamilyHead.Address1.IsEmpty()) {
          pageModel.FamilyWeb.FamilyHead.Address1 = "STATE ROAD 19";
        }

        if(pageModel.FamilyWeb.FamilyHead.City.IsEmpty()) {
          pageModel.FamilyWeb.FamilyHead.City = "UMATILLA";
        }

        if(pageModel.FamilyWeb.FamilyHead.Zip.IsEmpty()) {
          pageModel.FamilyWeb.FamilyHead.Zip = "32784";
        }

        pageModel.FamilyWeb.FamilyHead.Zip = pageModel.FamilyWeb.FamilyHead.Zip.SetAndTrim();
        if(pageModel.FamilyWeb.FamilyHead.Zip.IsEmpty()) {
          pageModel.FamilyWeb.FamilyHead.Zip = "";
        } else  if(!Config.AppConfig.AllowedZips.Contains(pageModel.FamilyWeb.FamilyHead.Zip)) {
          pageModel.PageWarnings.Add("Zip out of Area");
        }

      } else {
        if(pageModel.FamilyWeb.FamilyHead.Address1.IsEmpty()) {
          pageModel.PageErrors.Add("An Address required");
        }

        if(pageModel.FamilyWeb.FamilyHead.City.IsEmpty()) {
          pageModel.PageErrors.Add("A City is required");
        }

        if(pageModel.FamilyWeb.FamilyHead.Zip.IsEmpty()) {
          pageModel.PageErrors.Add("Zip is required");
        }

        pageModel.FamilyWeb.FamilyHead.Zip = pageModel.FamilyWeb.FamilyHead.Zip.SetAndTrim();
        if(!Config.AppConfig.AllowedZips.Contains(pageModel.FamilyWeb.FamilyHead.Zip)) {
          pageModel.PageWarnings.Add("Zip out of Area");
        }
      }

      if(pageModel.ButtonClicked == "ApplyVisit" && pageModel.FamilyWeb.FamilyHead.FamilyID == 0) {
        pageModel.ButtonClicked = "ApplyVisitUpdate";
      }

      if(pageModel.PageErrors.Count == 0 && (pageModel.ButtonClicked == "ApplyUpdate" || pageModel.ButtonClicked == "ApplyVisitUpdate")) {
        for(int i = 0; i < pageModel.FamilyWeb.PickUpAssociates.Count; i++) {
          pageModel.FamilyWeb.PickUpAssociates[i].FamilyRelationship = FamilyRelationship.PickUp;
        }
        pageModel.FamilyWeb.FamilyHead.FamilyID = familyService.SaveFamily(pageModel.FamilyWeb.FamilyHead, pageModel.FamilyWeb.FamilyMembers, pageModel.FamilyWeb.PickUpAssociates);
      }

      if(pageModel.PageErrors.Count == 0 && (pageModel.ButtonClicked == "ApplyVisit" || pageModel.ButtonClicked == "ApplyVisitUpdate")) {
        pageModel.FamilyWeb.FamilyHead.LastVisitDate = DateTime.Now;
        loggingService.SaveFamilyVisit(pageModel.FamilyWeb.FamilyHead, pageModel.FamilyWeb.FamilyMembers, Config.ThisVisit.ServiceLevel, pageModel.FamilyWeb.FamilyHead.VisitType, false, this.ViewGlobals.PantryID);
      }

      FamilyData familyData = null;

      if(pageModel.FamilyWeb.FamilyHead.FamilyID > 0) {
        familyData = familyService.GetFamily(pageModel.FamilyWeb.FamilyHead.FamilyID);
      }

      if(familyData.FamilyHead != null) {
        pageModel.FamilyWeb.FamilyHead = familyData.FamilyHead;
      }

      int familyFamilyMembersCount = 0;

      if(familyData.FamilyMembers != null && familyData.FamilyMembers.Count > 0) {
        pageModel.FamilyWeb.FamilyMembers = familyData.FamilyMembers.Where(w => w.FamilyRelationship != FamilyRelationship.PickUp).ToList();
        familyFamilyMembersCount = familyData.FamilyMembers.Count;
      }
      for(int i = familyFamilyMembersCount; i < 10; i++) {
        pageModel.FamilyWeb.FamilyMembers.Add(new FamilyMember());
      }


      int familyPickUpAssociatesCount = 0;
      pageModel.FamilyWeb.PickUpAssociates = new List<FamilyMember>();
      if(familyData.PickUpAssociates != null && familyData.PickUpAssociates.Count > 0) {
        pageModel.FamilyWeb.PickUpAssociates = familyData.FamilyMembers.Where(w => w.FamilyRelationship == FamilyRelationship.PickUp).ToList();
        familyPickUpAssociatesCount = familyData.PickUpAssociates.Count;
      }
      for(int i = familyPickUpAssociatesCount; i < 3; i++) {
        pageModel.FamilyWeb.PickUpAssociates.Add(new FamilyMember(FamilyRelationship.PickUp));
      }

      pageModel.FamilyWeb.LastVisits = familyData.LastVisits;

      pageModel.Notes = notesService.GetNotes(pageModel.FamilyWeb.FamilyHead.FamilyID, NoteType.All).ToList();

      VisitCounts visitCounts = reportsService.GetVisitCounts(DateTime.Now.Date, DateTime.Now.Date.AddDays(1).AddSeconds(-1));
      this.ViewGlobals.FamiliesCount = visitCounts.FamiliesCount;
      this.ViewGlobals.FamilyMembersCount = visitCounts.FamilyMembersCount;

      return View(pageModel);
    }


    public ActionResult About() {
      ViewBag.Message = "Your app description page.";

      return View();
    }


    public ActionResult Contact() {
      ViewBag.Message = "Your contact page.";

      return View();
    }
  }
}

