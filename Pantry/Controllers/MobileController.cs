﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Pantry.Config;
using Pantry.Entities;
using Pantry.Helpers;
using Pantry.Services;
using Pantry.Utilities;
using Pantry.Web;

namespace Pantry.Web.Controllers {
  public class MobileController : BaseController {

    FamilyService familyService = new FamilyService();
    LoggingService loggingService = new LoggingService();
    NotesService notesService = new NotesService();

    public MobileController() {
      this.ViewGlobals.PageType = "Client Access";
      this.ViewGlobals.PageSubType = "Control Panel";
      this.ViewGlobals.BreadCrumbs.Add(new BreadCrumb("Home", "/"));
    }


    [HttpGet]
    public ActionResult Index() {
      this.ViewGlobals.PageLayout = @"~/Views/Shared/_LayoutMobile.cshtml";
      this.ViewGlobals.BreadCrumbs.Add(new BreadCrumb("Client Access"));

      
      return View();
    }


    [HttpPost]
    public FastJsonResult Search(string id) {


      return new FastJsonResult() { Data = new { success = true } };
    }


    [HttpPost]
    public FastJsonResult GetFamily(string id) {


      return new FastJsonResult() { Data = new { success = true } };
    }


    [HttpPost]
    public FastJsonResult UpdateAndApply(int familyID, List<int> active, List<int> inactive) {


      return new FastJsonResult() { Data = new { success = true } };
    }



  }
}

