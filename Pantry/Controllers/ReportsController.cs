﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Pantry.Config;
using Pantry.Entities;
using Pantry.Helpers;
using Pantry.Services;
using Pantry.Utilities;
using Pantry.Web;

namespace Pantry.Web.Controllers {
  public class ReportsController : BaseController {

    FamilyService familyService = new FamilyService();
    NotesService notesService = new NotesService();
    public ReportsService ReportsService = new ReportsService();

    public ReportsController() {
      this.ViewGlobals.PageType = "Reports";
      this.ViewGlobals.PageSubType = "Control Panel";
      this.ViewGlobals.BreadCrumbs.Add(new BreadCrumb("Home", "/"));
    }


    [HttpGet]
    public ActionResult Index() {
      this.ViewGlobals.PageLayout = @"~/Views/Shared/_Layout.cshtml";
      this.ViewGlobals.BreadCrumbs.Add(new BreadCrumb("Client Access"));

      VisitCounts visitCounts = ReportsService.GetVisitCounts(DateTime.Now.Date, DateTime.Now.Date.AddDays(1).AddSeconds(-1));

      this.ViewGlobals.FamiliesCount = visitCounts.FamiliesCount;
      this.ViewGlobals.FamilyMembersCount = visitCounts.FamilyMembersCount;

      this.ViewGlobals.ServiceLevel = Config.ThisVisit.ServiceLevel;
      Models.Reports pageModel= new Models.Reports();
      pageModel.StartDate = DateTime.Now.AddDays(-1);
      pageModel.EndDate = DateTime.Now.Date.AddDays(1);


      return View(pageModel);
    }


    [HttpPost]
    public ActionResult Index(Models.Reports pageModel) {
      this.ViewGlobals.PageLayout = @"~/Views/Shared/_LayoutEdit.cshtml";
      this.ViewGlobals.BreadCrumbs.Add(new BreadCrumb("Client Access"));
      pageModel.PageErrors = new List<string>();
      pageModel.PageWarnings = new List<string>();

      pageModel.EndDate = pageModel.EndDate.AddDays(1).AddSeconds(-1);




      return View(pageModel);
    
    }



  }
}

