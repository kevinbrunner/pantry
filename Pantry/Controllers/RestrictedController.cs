﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Pantry.Config;
using Pantry.Entities;
using Pantry.Helpers;
using Pantry.Services;
using Pantry.Utilities;
using Pantry.Web;

namespace Pantry.Web.Controllers {
  public class RestrictedController : BaseController
  {

    FamilyService familyService = new FamilyService();
    LoggingService loggingService = new LoggingService();

    public RestrictedController() {
      this.ViewGlobals.PageType = "Client Access";
      this.ViewGlobals.PageSubType = "Control Panel";
      this.ViewGlobals.BreadCrumbs.Add(new BreadCrumb("Home", "/"));
    }

    [HttpGet]
    public ActionResult Index() {
      Models.Restricted pageModel = new Models.Restricted();

      return View(pageModel);
    }

    [HttpPost]
    public ActionResult Index(Models.Restricted pageModel)    {

      if (pageModel.Password == Config.SiteConfig.RestrictedPassword)      {

        HttpCookie cookie = new HttpCookie("PantryAuth");
        cookie.Expires = DateTime.Now.AddDays(30);
        cookie.Value = Config.SiteConfig.RestrictedCookieValue;
        if (Request.Cookies["PantryAuth"] != null)
        {
          Response.SetCookie(cookie);
        } else {
          Response.Cookies.Add(cookie);
        }

        return Redirect("/");
      }

      return RedirectToAction("Index", "Restricted"); // Redirect("/Restricted");
    }


  }
}
