﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

using Pantry.Config;
using Pantry.Entities;
using Pantry.Helpers;
using Pantry.Services;
using Pantry.Utilities;
using Pantry.Web.Models;

namespace Pantry.Web.Controllers {
  public class SearchController : BaseController {

    public SearchService SearchService = new SearchService();
    public LoggingService LoggingService = new LoggingService();
    public ReportsService ReportsService = new ReportsService();

    public SearchController() {
    }

    [HttpGet]
    public ActionResult Index() {
      return RedirectToAction("Index", "Home");

      return View();
    }


    [HttpPost]
    public FastJsonResult Click(string q = "") {

      List<SearchData> searchData = new List<SearchData>();

      Models.SearchReturn searchReturn = new SearchReturn();

      List<string> searchWords = q.Split(new char[] { ' ', '\n', '\r', ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

      string searchType = "multi";
      bool isSuccess = true;

      try {
        if(searchWords.Count == 1) {
          string checkWord = Regex.Replace(searchWords[0], "[^0-9.]", "");

          if(checkWord.Length == 4 && searchWords[0].Substring(0, 1).ToLower() == "p") {
            searchData.AddRange(this.SearchService.SearchForPhone(checkWord));
            searchType = "phone";
          } else if(checkWord.Length == 4) {
            searchData.AddRange(this.SearchService.SearchForSocial(checkWord));
            searchType = "social";
          }

          if(checkWord.Length == 7 || checkWord.Length == 10) {
            searchData.AddRange(this.SearchService.SearchForPhone(checkWord));
            searchType = "phone";
          }
        }

        if(searchData.Count == 0 && searchWords.Count > 0) {
          searchData = this.SearchService.SearchOnWords(searchWords).ToList();
        }

        if(searchData.Count > 0 && searchData[0] == null) {
          searchData = new List<SearchData>();
        }

        foreach(var s in searchData) {
          if(s.Phone.IsNotEmpty() && s.Phone.IndexOf(".") < 0 && s.Phone.Length == 10) {
            s.Phone = s.Phone.Substring(0, 3) + "." + s.Phone.Substring(3, 3) + "." + s.Phone.Substring(6);
          }
          if(s.LastVisit.IsNotEmpty()) {
            s.LastVisit = String.Format("{0:MMM dd yyyy}", Convert.ToDateTime(s.LastVisit));
          }
        }

        searchReturn.SearchDataum = searchData;

        VisitCounts visitCounts = ReportsService.GetVisitCounts(DateTime.Now.Date, DateTime.Now.Date.AddDays(1).AddSeconds(-1));
        searchReturn.FamiliesCount = visitCounts.FamiliesCount;
        searchReturn.FamilyMembersCount = visitCounts.FamilyMembersCount;

      }
      catch {
        isSuccess = false;
      }

      FastJsonResult r = new FastJsonResult { Data = new { SearchReturn = searchReturn, SearchType = searchType, IsSuccess = isSuccess } };

      return r;
    }



  }
}
