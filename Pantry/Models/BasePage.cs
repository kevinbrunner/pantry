﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pantry.Web.Models {
  public class BasePage {
    public BasePage() {
      this.PageErrors = new List<string>();
      this.PageWarnings = new List<string>();
    }

    public List<string> PageErrors { get; set; }

    public List<string> PageWarnings { get; set; }

    public bool IsPostBackSuccessful { get; set; }



  }
}