﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Pantry.Config;
using Pantry.Entities;


namespace Pantry.Web.Models {
  public class ClientMain : BasePage {
    public ClientMain() {
      this.FamilyWeb = new FamilyData();
      this.FamilyWeb.FamilyHead = new FamilyHead();
      this.LastVisits = new List<LastVisit>();
      this.Notes = new List<Note>();
      this.NewNote = new Note();
      this.ShowSideBar = -1;
    }

    public string SearchTerms { get; set; }

    public FamilyData FamilyWeb { get; set; }

    //public IEnumerable<SelectListItem> Months {
    //  get {
    //    return DateTimeFormatInfo
    //           .InvariantInfo
    //           .MonthNames
    //           .Where(m => !String.IsNullOrWhiteSpace(m))
    //           .Select((monthName, index) => new SelectListItem {
    //             Value = (index + 1).ToString(),
    //             Text = monthName
    //           });
    //  }
    //}

    public VisitType VisitType { get; set; }

    public string ButtonClicked { get; set; }

    public List<LastVisit> LastVisits { get; set; }

    public List<Note> Notes { get; set; }

    public Note NewNote { get; set; }

    public int ShowSideBar { get; set; }


  }


}
