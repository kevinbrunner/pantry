﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Pantry.Config;
using Pantry.Entities;


namespace Pantry.Web.Models {
  public class Reports : BasePage {
    public Reports() {
      this.ShowSideBar = -1;
      this.ServiceDays = new List<ServiceDay>();
    }

   
    public VisitType VisitType { get; set; }

    public string ButtonClicked { get; set; }

    public DateTime StartDate { get; set; }

    public DateTime EndDate { get; set; }
    
    public int ShowSideBar { get; set; }

    public List<ServiceDay> ServiceDays { get; set; }


  }


}
