using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Pantry.Config;
using Pantry.Entities;


namespace Pantry.Web.Models {
  public class Restricted {
    public Restricted() {
      this.Password = "";
    }
    public string Password { get; set; }

  }
}
