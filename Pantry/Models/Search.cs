﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Pantry.Entities;

namespace Pantry.Web.Models {
  public class Search : BasePage {
    public Search() { }

    public string SearchTerm { get; set; }

  }

  public class SearchReturn {
    public SearchReturn() {
      this.SearchDataum = new List<SearchData>();
      this.FamiliesCount = 0;
      this.FamilyMembersCount = 0;
    }

    public SearchReturn(int totalCount, int familyCount, List<SearchData> searchDatum) {
      this.FamilyMembersCount = totalCount;
      this.FamiliesCount = familyCount;
      this.SearchDataum = searchDatum;
    }

    public int FamilyMembersCount { get; set; }
    public int FamiliesCount { get; set; }
    public List<SearchData> SearchDataum { get; set; }
  }



}
