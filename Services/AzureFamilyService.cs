﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pantry.Config;
using Pantry.DataAccess;
using Pantry.Entities;
using Pantry.Helpers;
using Pantry.Utilities;


namespace Pantry.Services {
  public class AzureFamilyService {

    public AzureFamilyRepository FamilyRepo = new AzureFamilyRepository();
    public SearchRepository SearchRepo = new SearchRepository();

    public void SaveFamilyHead(FamilyHead family) {

      List<string> sTerms = new List<string>();
      sTerms.Add(family.Address1);
      sTerms.Add(family.Cell);
      sTerms.Add(family.City);
      sTerms.Add(family.FirstName);
      sTerms.Add(family.LastName);
      sTerms.Add(family.Phone);
      sTerms.Add(family.Social);

      FamilyRepo.SaveFamilyHead(family);
      SearchRepo.SaveSearchTerms(family.FamilyID, sTerms);

    }


    public void SaveFamilyMembers(List<FamilyMember> members) {

      FamilyRepo.SaveFamilyMembers(members, 0);
    }


    public int SaveFamily(FamilyHead familyHead, List<FamilyMember> members, List<FamilyMember> familyPickUpAssociates) {

      members.AddRange(familyPickUpAssociates);

      if(familyHead.FirstName.IsNotEmpty()) {

        List<string> sTerms = new List<string>();
        sTerms.Add(familyHead.Address1);
        sTerms.Add(familyHead.Cell);
        sTerms.Add(familyHead.City);
        sTerms.Add(familyHead.FirstName);
        sTerms.Add(familyHead.LastName);
        sTerms.Add(familyHead.Phone);
        sTerms.Add(familyHead.Social);

        FamilyRepo.SaveFamilyHead(familyHead);
        FamilyRepo.SaveFamilyMembers(members, familyHead.FamilyID);

        SearchRepo.SaveSearchTerms(familyHead.FamilyID, sTerms);
      } else {
        familyHead.FamilyID = 0;
      }

      return familyHead.FamilyID;
    }


    public FamilyHead GetFamilyHead(int familyHeadID) {
      FamilyHead familyHead = FamilyRepo.GetFamilyHead(familyHeadID);

      return familyHead;
    }


    public IEnumerable<FamilyMember> GetFamilyMembers(int familyID) {
      IEnumerable<FamilyMember> familyMembers = FamilyRepo.GetFamilyMembers(familyID);

      return familyMembers;
    }


    public FamilyData GetFamily(int familyID) {
      FamilyData family = FamilyRepo.GetFamily(familyID);

      return family;
    }


    public IEnumerable<FamilyHead> GetAllHeads() {

      return FamilyRepo.GetAllHeads(); 
    }


  }
}
