﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Pantry.Config;
using Pantry.DataAccess;
using Pantry.Entities;
using Pantry.Helpers;
using Pantry.Utilities;


namespace Pantry.Services {
  public class FamilyService {

    public FamilyRepository FamilyRepo = new FamilyRepository();
    public SearchRepository SearchRepo = new SearchRepository();

    public void SaveFamilyHead(FamilyHead familyHead) {

      familyHead.Phone = familyHead.Phone.SetAndTrim();
      familyHead.Phone = Regex.Replace(familyHead.Phone, "[^0-9]", "");
      if(familyHead.Phone.Length == 7) {
        familyHead.Phone = "352" + familyHead.Phone;
      }
      familyHead.Social = familyHead.Social.SetAndTrim();
      familyHead.Social = Regex.Replace(familyHead.Social, "[^0-9]", "");

      List<string> sTerms = new List<string>();
      if(familyHead.FirstName.IsNotEmpty()) {
        sTerms.Add(familyHead.FirstName);
      }
      if(familyHead.LastName.IsNotEmpty()) {
        sTerms.Add(familyHead.LastName);
      }
      if(familyHead.Address1.IsNotEmpty()) {
        sTerms.Add(familyHead.Address1);
      }
      if(familyHead.Phone.IsNotEmpty()) {
        sTerms.Add(familyHead.Phone);
      }
      if(familyHead.Social.IsNotEmpty()) {
        sTerms.Add(familyHead.Social);
      }
      FamilyRepo.SaveFamilyHead(familyHead);
      SearchRepo.SaveSearchTerms(familyHead.FamilyID, sTerms);

    }


    public void SaveFamilyMembers(List<FamilyMember> members) {

      foreach(FamilyMember member in members) {
        member.Social.SetAndTrim(4);
        member.FirstName = member.FirstName.SetAndTrim();
        member.LastName = member.LastName.SetAndTrim();
      }

      FamilyRepo.SaveFamilyMembers(members, 0);
    }


    public int SaveFamily(FamilyHead familyHead, List<FamilyMember> members, List<FamilyMember> familyPickUpAssociates) {

      members.AddRange(familyPickUpAssociates);

      if(familyHead.FirstName.IsNotEmpty()) {

        familyHead.FoodStampsReason = familyHead.FoodStampsReason.SetAndTrim();
        familyHead.Interviewer = familyHead.Interviewer.SetAndTrim();
        familyHead.State = familyHead.State.SetAndTrim();
        familyHead.Year = familyHead.Year.SetAndTrim();
        familyHead.Email = familyHead.Email.SetAndTrim();
        familyHead.CriticalNote = familyHead.CriticalNote.SetAndTrim();
        familyHead.Cell = familyHead.Cell.SetAndTrim();
        
        familyHead.FirstName = familyHead.FirstName.SetAndTrim();
        familyHead.LastName = familyHead.LastName.SetAndTrim();
        familyHead.Address1 = familyHead.Address1.SetAndTrim().ToUpper();
        familyHead.Address2 = familyHead.Address2.SetAndTrim().ToUpper();
        familyHead.City = familyHead.City.SetAndTrim().ToUpper();

        familyHead.Zip = Regex.Replace(familyHead.Zip.SetAndTrim(), "[^0-9]", "");

        familyHead.Phone = Regex.Replace(familyHead.Phone.SetAndTrim(), "[^0-9]", "");
        if(familyHead.Phone.Length == 7) {
          familyHead.Phone = "352" + familyHead.Phone;
        }
        if(familyHead.Phone.IsNotEmpty()) {
          familyHead.Phone = familyHead.Phone.Substring(0, 3) + "." + familyHead.Phone.Substring(3, 3) + "." + familyHead.Phone.Substring(6);
        }

        familyHead.Social = Regex.Replace(familyHead.Social.SetAndTrim(), "[^0-9]", "");

        List<string> sTerms = new List<string>();
        if(familyHead.FirstName.IsNotEmpty()) {
          sTerms.Add(familyHead.FirstName);
        }
        if(familyHead.LastName.IsNotEmpty()) {
          sTerms.Add(familyHead.LastName);
        }
        if(familyHead.Address1.IsNotEmpty()) {
          sTerms.Add(familyHead.Address1);
        }
        if(familyHead.Phone.IsNotEmpty()) {
          sTerms.Add(familyHead.Phone);
        }
        if(familyHead.Social.IsNotEmpty()) {
          sTerms.Add(familyHead.Social);
        }

        FamilyRepo.SaveFamilyHead(familyHead);
        FamilyRepo.SaveFamilyMembers(members, familyHead.FamilyID);

        SearchRepo.SaveSearchTerms(familyHead.FamilyID, sTerms);
      } else {
        familyHead.FamilyID = 0;
      }

      return familyHead.FamilyID;
    }


    public FamilyHead GetFamilyHead(int familyHeadID) {
      FamilyHead familyHead = FamilyRepo.GetFamilyHead(familyHeadID);

      familyHead.Phone = familyHead.Phone.SetAndTrim();
      familyHead.Phone = Regex.Replace(familyHead.Phone, "[^0-9]", "");
      if(familyHead.Phone.IndexOf(".") < 0 && familyHead.Phone.Length == 10) {
        familyHead.Phone = familyHead.Phone.Substring(0, 3) + "." + familyHead.Phone.Substring(3, 3) + "." + familyHead.Phone.Substring(6);
      } else if(familyHead.Phone.IndexOf(".") < 0 && familyHead.Phone.Length == 7) {
        familyHead.Phone = "352." + familyHead.Phone.Substring(0, 3) + "." + familyHead.Phone.Substring(3);
      }

      return familyHead;
    }


    public IEnumerable<FamilyMember> GetFamilyMembers(int familyID) {
      IEnumerable<FamilyMember> familyMembers = FamilyRepo.GetFamilyMembers(familyID);

      return familyMembers;
    }


    public FamilyData GetFamily(int familyID) {
      FamilyData family = FamilyRepo.GetFamily(familyID);

      family.FamilyHead.Phone = Regex.Replace(family.FamilyHead.Phone, "[^0-9]", "");
      if(family.FamilyHead.Phone.IndexOf(".") < 0) {
        if(family.FamilyHead.Phone.IsNotEmpty() && family.FamilyHead.Phone.Length == 10) {
          family.FamilyHead.Phone = family.FamilyHead.Phone.Substring(0, 3) + "." + family.FamilyHead.Phone.Substring(3, 3) + "." + family.FamilyHead.Phone.Substring(6);
        }
        if(family.FamilyHead.Phone.IsNotEmpty() && family.FamilyHead.Phone.Length == 7) {
          family.FamilyHead.Phone = "352." + family.FamilyHead.Phone.Substring(0, 3) + "." + family.FamilyHead.Phone.Substring(3);
        }
      }

      return family;
    }


    public IEnumerable<FamilyHead> GetAllHeads() {

      return FamilyRepo.GetAllHeads();
    }


    public void DeleteMember(int memberID) {
      FamilyRepo.DeleteMember(memberID);
    }


    public void DeleteClient(int familyID) {
      FamilyRepo.DeleteMember(familyID);
    }



  }
}
