﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pantry.Config;
using Pantry.DataAccess;
using Pantry.Entities;
using Pantry.Helpers;
using Pantry.Utilities;


namespace Pantry.Services {
  public class LoggingService {

    public LoggingRepository LogRepo = new LoggingRepository();


    public void SaveFamilyVisit(FamilyHead familyHead, List<FamilyMember> familyMembers, ServiceLevel serviceLevel, VisitType visitType, bool overwriteToday, int pantryID) {
      VisitLog visitLog = new VisitLog();
      visitLog.FamilyID = familyHead.FamilyID;
      visitLog.NumberOfMembers = familyMembers.Where(w => w.IsActive && w.FirstName.IsNotEmpty()).Count() + 1;
      visitLog.ServiceLevel = serviceLevel;
      visitLog.VisitType = visitType;
      visitLog.PantryID = pantryID;

      LogRepo.SaveVisitLog(visitLog, overwriteToday);
    }




  }
}
