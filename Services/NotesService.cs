﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pantry.Config;
using Pantry.DataAccess;
using Pantry.Entities;
using Pantry.Helpers;
using Pantry.Utilities;


namespace Pantry.Services {
  public class NotesService {

    public NotesRepository NotesRepo = new NotesRepository();

    public void SaveNote(Note note) {

      NotesRepo.SaveNote(note);

    }


    public IEnumerable<Note> GetNotes(int familyID, NoteType noteType) {

      return NotesRepo.GetNotes(familyID, noteType);
    }




  }
}
