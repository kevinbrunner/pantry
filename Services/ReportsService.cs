﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pantry.Config;
using Pantry.DataAccess;
using Pantry.Entities;
using Pantry.Helpers;
using Pantry.Utilities;


namespace Pantry.Services {
  public class ReportsService {

    public ReportsRepository ReportsRepo = new ReportsRepository();



    public VisitCounts GetVisitCounts(DateTime startDate, DateTime endDate) {
      if(startDate > endDate) {
        throw new Exception("date mixup");
      }

      return ReportsRepo.GetVisitCount(startDate, endDate);
    }


    public List<VisitCounts> GetVisitReport(DateTime startDate, DateTime endDate) {

      startDate = startDate.Date;
      endDate = endDate.Date.AddDays(1).AddSeconds(-1);

      int daysToAdd = 0;
      switch(startDate.DayOfWeek) {
        case DayOfWeek.Sunday:
          daysToAdd = 3;
          break;
        case DayOfWeek.Monday:
          daysToAdd = 2;
          break;
        case DayOfWeek.Tuesday:
          daysToAdd = 1;
          break;
        case DayOfWeek.Thursday:
          daysToAdd = 2;
          break;
        case DayOfWeek.Friday:
          daysToAdd = 1;
          break;
      }

      startDate = startDate.AddDays(daysToAdd);

      List<VisitCounts> visitReport = new List<VisitCounts>();

      if(startDate < endDate) {
        DateTime thisDate = startDate;
        do {
          if(thisDate.DayOfWeek == DayOfWeek.Wednesday) {
            visitReport.Add(GetVisitCounts(startDate, startDate.AddDays(1)));
            thisDate = thisDate.AddDays(3);

          } else if(thisDate.DayOfWeek == DayOfWeek.Saturday) {
            visitReport.Add(GetVisitCounts(startDate, startDate.AddDays(1)));
            thisDate = thisDate.AddDays(4);
          }
        }
        while(thisDate < endDate);
      }

      return visitReport;
    }


    public DailyDetailedReport GetDetailedReport(DateTime thisDate) {

      IEnumerable<DailyDetailedReport> r = ReportsRepo.GetDetailedReport(thisDate.Date, thisDate.Date.AddDays(1).AddSeconds(-1));

      if(r != null && r.Count() > 0) {
        return r.First();
      }

      return new DailyDetailedReport();
    }


    public List<List<DailyDetailedReport>> GetDetailedReport(DateTime startDate, DateTime endDate) {

      startDate = startDate.Date;
      endDate = endDate.Date.AddDays(1).AddSeconds(-1);

      int daysToAdd = 0;
      switch(startDate.DayOfWeek) {
        case DayOfWeek.Sunday:
          daysToAdd = 3;
          break;
        case DayOfWeek.Monday:
          daysToAdd = 2;
          break;
        case DayOfWeek.Tuesday:
          daysToAdd = 1;
          break;
        case DayOfWeek.Thursday:
          daysToAdd = 2;
          break;
        case DayOfWeek.Friday:
          daysToAdd = 1;
          break;
      }

      startDate = startDate.AddDays(daysToAdd);

      List<List<DailyDetailedReport>> visitReport = new List<List<DailyDetailedReport>>();

      if(startDate < endDate) {
        DateTime thisDate = startDate;
        do {
          if(thisDate.DayOfWeek == DayOfWeek.Wednesday) {
            visitReport.Add(ReportsRepo.GetDetailedReport(startDate, startDate.AddDays(1)).ToList());
            thisDate = thisDate.AddDays(3);

          } else if(thisDate.DayOfWeek == DayOfWeek.Saturday) {
            visitReport.Add(ReportsRepo.GetDetailedReport(startDate, startDate.AddDays(1)).ToList());
            thisDate = thisDate.AddDays(4);
          }
        }
        while(thisDate < endDate);
      }

      return visitReport;
    }



  }
}
