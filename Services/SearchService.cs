﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Pantry.DataAccess;
using Pantry.Entities;

namespace Pantry.Services {
  public class SearchService {

    private SearchRepository SearchRepo = new SearchRepository();

    public IEnumerable<SearchData> SearchOnWords(List<string> searchTerms) {

      if(searchTerms.Count == 0) {
        return new List<SearchData>();
      } else {
        return this.SearchRepo.SearchOnTerms(searchTerms);
      }
    }


    public IEnumerable<SearchData> SearchForSocial(string social) {
      return this.SearchRepo.SearchForSocial(social);
    }


    public IEnumerable<SearchData> SearchForPhone(string phone) {
      phone = Regex.Replace(phone, "[^0-9]", "");

      return this.SearchRepo.SearchForPhone(phone);
    }


  }
}
