USE [Pantry]
GO

/****** Object:  Table [dbo].[Family]    Script Date: 9/27/2014 11:27:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Family](
	[FamilyID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](64) NULL,
	[LastName] [varchar](64) NULL,
	[Social] [varchar](4) NULL,
	[Address1] [varchar](64) NULL,
	[Address2] [varchar](64) NULL,
	[City] [varchar](64) NULL,
	[State] [varchar](2) NULL,
	[Zip] [varchar](10) NULL,
	[Phone] [varchar](16) NULL,
	[Cell] [varchar](16) NULL,
	[Updated] [datetime2](7) NULL,
	[CreateDate] [datetime2](7) NULL,
	[LastVisitDate] [datetime2](7) NULL,
	[IsFoodStamps] [bit] NULL,
	[FoodStampsReason] [varchar](128) NULL,
	[Birthday] [date] NULL,
	[VisitType] [varchar](32) NULL,
	[Year] [varchar](4) NULL,
	[FamilyType] [varchar](32) NULL,
 CONSTRAINT [PK_Family] PRIMARY KEY CLUSTERED 
(
	[FamilyID] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Family] ADD  CONSTRAINT [DF_Family_IsFoodStamps]  DEFAULT ((1)) FOR [IsFoodStamps]
GO

ALTER TABLE [dbo].[Family] ADD  CONSTRAINT [DF_Family_FoodStampsReason]  DEFAULT ('') FOR [FoodStampsReason]
GO

ALTER TABLE [dbo].[Family] ADD  CONSTRAINT [DF_Family_Year]  DEFAULT ('') FOR [Year]
GO

