USE [Pantry]
GO

/****** Object:  Table [dbo].[FamilyMembers]    Script Date: 10/31/2014 11:10:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FamilyMembers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[FamilyID] [int] NOT NULL,
	[FirstName] [varchar](64) NULL,
	[LastName] [varchar](64) NULL,
	[Birthday] [date] NULL,
	[Updated] [datetime2](7) NULL,
	[CreateDate] [datetime2](7) NULL,
	[IsActive] [bit] NULL,
	[FamilyRelationship] [varchar](16) NULL,
	[Year] [varchar](4) NULL,
	[Social] [varchar](4) NULL,
 CONSTRAINT [PK_FamilyMembers] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[FamilyMembers] ADD  CONSTRAINT [DF_FamilyMembers_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO

ALTER TABLE [dbo].[FamilyMembers] ADD  CONSTRAINT [DF_FamilyMembers_FamilyRelationship]  DEFAULT ('Child') FOR [FamilyRelationship]
GO

ALTER TABLE [dbo].[FamilyMembers] ADD  CONSTRAINT [DF_FamilyMembers_Year]  DEFAULT ('') FOR [Year]
GO

ALTER TABLE [dbo].[FamilyMembers] ADD  CONSTRAINT [DF_FamilyMembers_Social]  DEFAULT ('') FOR [Social]
GO


