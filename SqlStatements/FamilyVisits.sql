USE [Pantry]
GO

/****** Object:  Table [dbo].[FamilyVisits]    Script Date: 9/27/2014 11:47:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FamilyVisits](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[FamilyID] [int] NULL,
	[CreateDate] [datetime2](7) NULL,
	[NumberOfMembers] [int] NULL,
	[VisitType] [varchar](32) NULL,
	[ServiceLevel] [varchar](32) NULL,
	[PantryID] [int] NULL,
 CONSTRAINT [PK_FamilyVisits] PRIMARY KEY CLUSTERED 
(
	[id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[FamilyVisits] ADD  CONSTRAINT [DF_FamilyVisits_VisitType]  DEFAULT ('Grocery') FOR [VisitType]
GO

ALTER TABLE [dbo].[FamilyVisits] ADD  CONSTRAINT [DF_FamilyVisits_ServiceLevel]  DEFAULT ('Medium') FOR [ServiceLevel]
GO

