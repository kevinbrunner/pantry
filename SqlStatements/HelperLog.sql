USE [Pantry]
GO

/****** Object:  Table [dbo].[HelperLog]    Script Date: 9/26/2014 11:40:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[HelperLog](
	[HelperLogID] [int] IDENTITY(1,1) NOT NULL,
	[HelperID] [int] NULL,
	[CreateDate] [datetime2](7) NULL,
	[Location] [varchar](16) NULL,
	[HoursWorked] [decimal](5, 2) NULL,
 CONSTRAINT [PK_HelperLog] PRIMARY KEY CLUSTERED 
(
	[HelperLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

