USE [Pantry]
GO

/****** Object:  Table [dbo].[Notes]    Script Date: 10/31/2014 11:10:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Notes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[FamilyID] [int] NULL,
	[CreateDate] [datetime2](7) NULL,
	[EnteredBy] [varchar](32) NULL,
	[Title] [varchar](64) NULL,
	[Body] [text] NULL,
	[TransferredTo] [varchar](32) NULL,
	[TransferDate] [datetime2](7) NULL,
	[NoteType] [varchar](16) NULL,
 CONSTRAINT [PK_Notes] PRIMARY KEY CLUSTERED 
(
	[id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Notes] ADD  CONSTRAINT [DF_Notes_NoteType]  DEFAULT ('Normal') FOR [NoteType]
GO


