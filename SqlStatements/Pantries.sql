USE [Pantry]
GO

/****** Object:  Table [dbo].[Pantries]    Script Date: 9/27/2014 11:54:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Pantries](
	[PantryID] [int] IDENTITY(1,1) NOT NULL,
	[PantryName] [varchar](64) NULL,
	[PantryShort] [varchar](16) NULL,
	[Address1] [varchar](64) NULL,
	[Address2] [varchar](64) NULL,
	[City] [varchar](64) NULL,
	[State] [varchar](2) NULL,
	[Zip] [varchar](10) NULL,
	[Contact1] [varchar](32) NULL,
	[Phone1] [varchar](10) NULL,
	[Email1] [varchar](64) NULL,
	[Contact2] [varchar](32) NULL,
	[Phone2] [varchar](10) NULL,
	[Email2] [varchar](64) NULL,
 CONSTRAINT [PK_Pantries] PRIMARY KEY CLUSTERED 
(
	[PantryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

