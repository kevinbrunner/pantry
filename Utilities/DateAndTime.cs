﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pantry.Utilities {
  public class DateAndTime {
    public static string FormatElapsedTime(TimeSpan ts) {

      string time = String.Format("{0} Milliseconds", Math.Floor((decimal)ts.Milliseconds));

      if(ts.TotalMilliseconds > 999 && ts.TotalSeconds < 60) {
        time = String.Format("{0} Seconds, {1} Milliseconds", Math.Floor((decimal)ts.Seconds), Math.Floor((decimal)ts.Milliseconds));
      }

      if(ts.TotalSeconds > 60 && ts.TotalMinutes < 60) {
        time = String.Format("{0} Minutes, {1} Seconds", Math.Floor((decimal)ts.Minutes), Math.Floor((decimal)ts.Seconds));
      }

      if(ts.TotalMinutes > 60 && ts.TotalHours < 24) {
        time = String.Format("{0} Hours, {1} Minutes, {2} Seconds", Math.Floor((decimal)ts.Hours), Math.Floor((decimal)ts.Minutes), Math.Floor((decimal)ts.Seconds));
      }

      if(ts.TotalHours > 24) {
        time = String.Format("{0} Days, {1} Hours, {2} Minutes, {3} Seconds", Math.Floor((decimal)ts.Days), Math.Floor((decimal)ts.Hours), Math.Floor((decimal)ts.Minutes), Math.Floor((decimal)ts.Seconds));
      }

      return time;

    }

  }


}