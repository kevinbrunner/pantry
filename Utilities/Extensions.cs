﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Pantry.Utilities {
  public static partial class StringExtensions {

    public static string ToDbQuote(this object enumString) {
      return "'" + enumString.ToString() + "'";
    }


    public static string ToStringLongDateTime(this DateTime now) {
      return String.Format("{0:MM/dd/yyyy HH:mm:ss:fff}", now);
    }


    public static DateTime ToHourFloor(this DateTime now) {
      return new DateTime(now.Year, now.Month, now.Day).AddHours(now.Hour);
    }


    public static bool IsEmpty(this string value) {
      return String.IsNullOrWhiteSpace(value);
    }


    public static bool IsNotEmpty(this string value) {
      return !String.IsNullOrWhiteSpace(value);
    }


    public static string SetAndTrim(this string value, int maxLength = 0) {
      value = value ?? "";
      if(maxLength > 0) {
        value = value.PadRight(maxLength + 1).Substring(0, maxLength);
      }
      return value.Trim();
    }


    public static string Left(string s, int count) {
      return s.Substring(0, count);
    }


    public static string Right(string s, int count) {
      return s.Substring(s.Length - count, count);
    }


    public static string Mid(string s, int index, int count) {
      return s.Substring(index, count);
    }


    public static int ToInteger(string s) {
      int integerValue = 0;
      int.TryParse(s, out integerValue);
      return integerValue;
    }


    public static bool IsInteger(string s) {
      Regex regularExpression = new Regex("^-[0-9]+$|^[0-9]+$");
      return regularExpression.Match(s).Success;
    }


    public static bool IsAlphaNum(string s) {
      Regex regularExpression = new Regex("^[a-zA-Z0-9]+$");
      return regularExpression.Match(s).Success;
    }


    public static void IsValidGuidID(this string guidID) {
      if(!guidID.ValidateGuidID()) {
        throw new Exception("InvalidID");
      }
    }
    
    
    //public static bool IsValidEmail(this string emailAddress) {
    //  return ValidateEmail(emailAddress);
    //}


    public static bool ValidateEmail(string emailAddress) {
      Regex e2 = new Regex(@"^((([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~])+(\.([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~])+)*))@((([a-zA-Z]|\d)|(([a-zA-Z]|\d)([a-zA-Z]|\d|-|\.|_|~)*([a-zA-Z]|\d)))\.)+(([a-zA-Z])|(([a-zA-Z])([a-zA-Z]|\d|-|\.|_|~)*([a-zA-Z])))\.?$");
      return e2.IsMatch(emailAddress);
    }



    public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector) {
      HashSet<TKey> seenKeys = new HashSet<TKey>();
      foreach(TSource element in source) {
        if(seenKeys.Add(keySelector(element))) {
          yield return element;
        }
      }

    }


    public static bool ValidateGuidID(this string guidID) {
      bool isValid = true;

      if(guidID != null && (guidID.Length == 36 || guidID.Length == 38)) {
        Guid g = new Guid();
        if(!Guid.TryParse(guidID, out g)) {
          isValid = false;
        }
      } else {
        isValid = false;
      }

      return isValid;
    }


    public static string ToProper(this string inputWord, bool firstWordOnly = false) {

      string properWord = "";

      if(!String.IsNullOrWhiteSpace(inputWord)) {
        List<string> words = inputWord.Split(new char[] { ' ' }).ToList();
        int wordsCount = words.Count;
        if(firstWordOnly) {
          wordsCount = 1;
        }
        for(int i = 0; i < wordsCount; i++) {
          words[i] = words[i].Substring(0, 1).ToUpper() + words[i].Substring(1, words[i].Length - 1).ToLower();
        }
        properWord = String.Join(" ", words);
      }

      return properWord;
    }


    public static List<string> GetAllCombinations(this List<string> list) {
      List<string> requiredList = new List<string>();
      for(int i = 0; i < list.Count; i++) {
        var thisWord = list[i];
        var partWord = thisWord;
        GetCombinations(requiredList, list, thisWord, i);
        int k = i + 1;
        while(k < list.Count) {

          partWord += " " + list[k];
          GetCombinations(requiredList, list, partWord, k + 1);
          k++;
        }
      }

      return requiredList;
    }


    private static void GetCombinations(List<string> requiredList, List<string> list, string thisWord, int j) {
      for(int i = j; i < list.Count; i++) {
        var nextWord = list[i];
        List<string> tempList = new List<string>();
        if(thisWord != nextWord) {
          var singleWord = thisWord + " " + nextWord;// tempList.Aggregate((a, b) => a + " " + b);// string.Concat(tempList, +" " + tempList);          
          tempList.Add(singleWord);
        } else {
          tempList.Add(thisWord);
        }

        requiredList.AddRange(tempList.Where(s => !requiredList.Contains(s)));
      }
    }

    /// <summary>
    /// Encodes to Base64
    /// Test Coverage: Included
    /// </summary>
    /// <param name="val"></param>
    /// <returns>Base 64 Encoded string</returns>
    public static string Base64StringEncode(this string val) {
      byte[] toEncodeAsBytes = ASCIIEncoding.ASCII.GetBytes(val);
      string returnValue = Convert.ToBase64String(toEncodeAsBytes);
      return returnValue;
    }

    /// <summary>
    /// Decodes a Base64 encoded string
    /// Test Coverage: Included
    /// </summary>
    /// <param name="val"></param>
    /// <returns>Base 64 decoded string</returns>
    public static string Base64StringDecode(this string val) {
      byte[] encodedDataAsBytes = Convert.FromBase64String(val);
      string returnValue = ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
      return returnValue;
    }


    /// <summary>
    /// Left pads the passed string using the passed pad string for the total number of spaces. 
    /// It will not cut-off the pad even if it causes the string to exceed the total width.
    /// Test Coverage: Included
    /// </summary>
    /// <param name="val"></param>
    /// <param name="pad">The pad string</param>
    /// <param name="totalWidth">The total width of the resulting string</param>
    /// <returns>Copy of string with the padding applied</returns>
    public static string PadLeft(this string val, string pad, int totalWidth) {
      return PadLeft(val, pad, totalWidth, false);
    }


    /// <summary>
    /// Left pads the passed string using the passed pad string for the total number of spaces. 
    /// </summary>
    /// <param name="val"></param>
    /// <param name="pad">The pad string</param>
    /// <param name="totalWidth">The total width of the resulting string</param>
    /// <param name="cutOff">True to cut off the characters if exceeds the specified width</param>
    /// <returns>Copy of string with the padding applied</returns>
    public static string PadLeft(this string val, string pad, int totalWidth, bool cutOff) {
      if(val.Length >= totalWidth)
        return val;

      int padCount = pad.Length;
      string paddedString = val;

      while(paddedString.Length < totalWidth) {
        paddedString += pad;
      }

      if(cutOff)
        paddedString = paddedString.Substring(0, totalWidth);
      return paddedString;
    }


    /// <summary>
    /// Right pads the passed string using the passed pad string for the total number of spaces. 
    /// It will not cut-off the pad even if it causes the string to exceed the total width.
    /// Test Coverage: Included
    /// </summary>
    /// <param name="val"></param>
    /// <param name="pad">The pad string</param>
    /// <param name="totalWidth">The total width of the resulting string</param>
    /// <returns>Copy of string with the padding applied</returns>
    public static string PadRight(this string val, string pad, int totalWidth) {
      return PadRight(val, pad, totalWidth, false);
    }


    /// <summary>
    /// Right pads the passed string using the passed pad string for the total number of spaces. 
    /// </summary>
    /// <param name="val"></param>
    /// <param name="pad">The pad string</param>
    /// <param name="totalWidth">The total width of the resulting string</param>
    /// <param name="cutOff">True to cut off the characters if exceeds the specified width</param>
    /// <returns>Copy of string with the padding applied</returns>
    public static string PadRight(this string val, string pad, int totalWidth, bool cutOff) {
      if(val.Length >= totalWidth)
        return val;

      string paddedString = String.Empty;

      while(paddedString.Length < totalWidth - val.Length) {
        paddedString += pad;
      }

      if(cutOff)
        paddedString = paddedString.Substring(0, totalWidth - val.Length);
      paddedString += val;
      return paddedString;
    }


    /// <summary>
    /// Removes a non numeric character from a string
    /// Test Coverage: Included
    /// </summary>
    /// <param name="s"></param>
    /// <returns>Copy of the string after removing non numeric characters</returns>
    public static string RemoveNonNumeric(this string s) {
      StringBuilder sb = new StringBuilder();
      for(int i = 0; i < s.Length; i++) {
        if(Char.IsNumber(s[i])) {
          sb.Append(s[i]);
        }
      }
      return sb.ToString();
    }


    /// <summary>
    /// Removes numeric characters from a given string
    /// Test Coverage: Included
    /// </summary>
    /// <param name="s"></param>
    /// <returns>Copy of the string after removing the numeric characters</returns>
    public static string RemoveNumeric(this string s) {
      StringBuilder sb = new StringBuilder();
      for(int i = 0; i < s.Length; i++) {
        if(!Char.IsNumber(s[i])) {
          sb.Append(s[i]);
        }
      }
      return sb.ToString();
    }


    /// <summary>
    /// Reverses a string
    /// Test Coverage: Included
    /// </summary>
    /// <param name="val"></param>
    /// <returns>Copy of the reversed string</returns>
    public static string Reverse(this string val) {
      char[] reverse = new char[val.Length];
      for(int i = 0, k = val.Length - 1; i < val.Length; i++, k--) {
        if(char.IsSurrogate(val[k])) {
          reverse[i + 1] = val[k--];
          reverse[i++] = val[k];
        } else {
          reverse[i] = val[k];
        }
      }
      return new string(reverse);
    }


    /// <summary>
    /// Removes multiple spaces between words
    /// Test Coverage: Included
    /// </summary>
    /// <param name="val"></param>
    /// <returns>Returns a copy of the string after removing the extra spaces</returns>
    public static string TrimIntraWords(this string val) {
      Regex regEx = new Regex(@"[\s]+");
      return regEx.Replace(val, " ");
    }


    /// <summary>
    /// Converts an list of string to CSV string representation.
    /// Test Coverage: Included
    /// </summary>
    /// <param name="val"></param>
    /// <param name="insertSpaces">True to add spaces after each comma</param>
    /// <returns>CSV representation of the data</returns>
    public static string ToCSV(this IEnumerable<string> val, bool insertSpaces = false) {
      if(insertSpaces) {
        return String.Join(", ", val.ToArray());
      } else {
        return String.Join(",", val.ToArray());
      }
    }


    /// <summary>
    /// Converts an list of characters to CSV string representation.
    /// Test Coverage: Included
    /// </summary>
    /// <param name="val"></param>
    /// <param name="insertSpaces">True to add spaces after each comma</param>
    /// <returns>CSV representation of the data</returns>
    public static string ToCSV(this IEnumerable<char> val, bool insertSpaces = false) {
      List<string> casted = new List<string>();
      foreach(var item in val) {
        casted.Add(item.ToString());
      }

      if(insertSpaces) {
        return String.Join(", ", casted.ToArray());
      } else {
        return String.Join(",", casted.ToArray());
      }
    }


    /// <summary>
    /// Converts CSV to list of string.
    /// Test Coverage: Included
    /// </summary>
    /// <param name="val"></param>
    /// <returns>IEnumerable collection of string</returns>
    public static IEnumerable<string> ListFromCSV(this string val) {
      string[] split = val.Split(',');
      foreach(string item in split) {
        item.Trim();
      }
      return new List<string>(split);
    }


    /// <summary>
    /// Converts an list of characters to Pipes Separate Value string representation.
    /// Test Coverage: Included
    /// </summary>
    /// <param name="val"></param>
    /// <param name="insertSpaces">True to add spaces after each comma</param>
    /// <returns>Pipes Separate Values representation of the data</returns>
    public static string ToPSV(this IEnumerable<string> val, bool insertSpaces = false) {
      if(insertSpaces) {
        return String.Join("| ", val.ToArray());
      } else {
        return String.Join("|", val.ToArray());
      }
    }


    /// <summary>
    /// Converts Pipes Separate Values to list of string.
    /// Test Coverage: Included
    /// </summary>
    /// <param name="val"></param>
    /// <returns>IEnumerable collection of string</returns>
    public static IEnumerable<string> ListFromPSV(this string val) {
      string[] split = val.Split('|');
      foreach(string item in split) {
        item.Trim();
      }
      return new List<string>(split);
    }


    public static string ConvertNumberToWord(long numberVal) {
      string[] powers = new string[] { "thousand ", "million ", "billion " };

      string[] ones = new string[] { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };

      string[] tens = new string[] { "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

      string wordValue = "";

      if(numberVal == 0) { return "zero"; }
      if(numberVal < 0) {
        wordValue = "negative ";
        numberVal = -numberVal;
      }

      long[] partStack = new long[] { 0, 0, 0, 0 };
      int partNdx = 0;

      while(numberVal > 0) {
        partStack[partNdx++] = numberVal % 1000;
        numberVal /= 1000;
      }

      for(int i = 3; i >= 0; i--) {
        long part = partStack[i];

        if(part >= 100) {
          wordValue += ones[part / 100 - 1] + " hundred ";
          part %= 100;
        }

        if(part >= 20) {
          if((part % 10) != 0) {
            wordValue += tens[part / 10 - 2] + " " + ones[part % 10 - 1] + " ";
          } else {
            wordValue += tens[part / 10 - 2] + " ";
          }
        } else if(part > 0) {
          wordValue += ones[part - 1] + " ";
        }

        if(part != 0 && i > 0) {
          wordValue += powers[i - 1];
        }
      }

      return wordValue.Trim();
    }


    /// <summary>
    /// Convert to Ordinal number
    /// Test Coverage: Included
    /// </summary>
    /// <param name="val"></param>
    /// <returns>String representation of the Ordinal number</returns>
    public static string ToOrdinal(this int val) {
      if(val <= 0) { throw new ArgumentException("Cardinal must be positive."); }

      int lastTwoDigits = val % 100;
      int lastDigit = lastTwoDigits % 10;
      string suffix;
      switch(lastDigit) {
        case 1:
          suffix = "st";
          break;

        case 2:
          suffix = "nd";
          break;

        case 3:
          suffix = "rd";
          break;

        default:
          suffix = "th";
          break;
      }

      if(11 <= lastTwoDigits && lastTwoDigits <= 13) {
        suffix = "th";
      }

      return string.Format("{0}{1}", val, suffix);
    }


  }


  public static class DateTimeExtensions {
    public static bool IsCreditCardExpired(this DateTime expirationDate, int daysBack = 0) {
      bool isExpired = false;

      //card expiration date is set to one month past the date on card.
      //so if card date says 2/2012, then expiration date is 3/1/2012

      if((expirationDate.AddDays((daysBack * -1)) - DateTime.Now).Days < 1) {
        isExpired = true;
      }

      return isExpired;
    }


  }
}
