﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Pantry.Utilities {
  public class StringTools {
    public static string ReverseString(string s) {
      char[] arr = s.ToCharArray();
      Array.Reverse(arr);
      return new string(arr);
    }
  }

  /// <summary>
  /// Summary description for ValidationUtils
  /// </summary>
  public static class Validations {

    public static bool IsGuid(string checkGuid) {
      bool isValid = false;

      if(!String.IsNullOrWhiteSpace(checkGuid)) {
        //modified for JW invalid guids
        //Regex guidPattern = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);

        Regex guidPattern = new Regex(@"^(\{){0,1}[0-9a-zA-Z]{8}\-[0-9a-zA-Z]{4}\-[0-9a-zA-Z]{4}\-[0-9a-zA-Z]{4}\-[0-9a-zA-Z]{12}(\}){0,1}$", RegexOptions.Compiled);
        isValid = guidPattern.IsMatch(checkGuid);
      }

      return isValid;
    }


    public static bool IsNumeric(string checkNumeric) {
      Regex numericPattern = new Regex(@"^\d+$", RegexOptions.Compiled);
      return numericPattern.IsMatch(checkNumeric);
    }



    /// <summary>
    /// checks for valid characters in an email address
    /// </summary>
    /// <param name="EmailAddress"></param>
    /// <returns>false if this is a match</returns>
    public static bool IsValidEmail(string emailAddress) {
      if(String.IsNullOrWhiteSpace(emailAddress)) // || emailAddress.IndexOf(" ") > -1)
      {
        return false;
      }
      //string emailregex = @"[-a-zA-Z0-9!#$%&'*+/=?^_'{|}~]+(?:\.[-a-zA-Z0-9!#$%&'*+/=?^_'{|}~]+)*@([a-zA-Z0-9](?:[-a-zA-Z0-9]*[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[-a-zA-Z0-9]*[a-zA-Z0-9])?)+|\[(?:\d{1,3}(?:\.\d{1,3}){3}|IPv6:[0-9A-Fa-f:]{4,39})\])";
      //Regex emailtest = new Regex(emailregex);

      Regex regex = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,6}|[0-9]{1,3})(\]?)$");
      return regex.IsMatch(emailAddress);
    }

    /// <summary>
    /// checks for valid alpah, numeric, and safe symbols? (good for passwords except for the spaces)
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static bool IsAlphaNumericSafeSymbols(string str) {
      if(String.IsNullOrEmpty(str)) {
        return false;
      }


      Regex regexAlphaNum = new Regex("[a-zA-Z0-9!@#$%^&*()-_ ]");
      return regexAlphaNum.IsMatch(str);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="password"></param>
    /// <returns></returns>
    public static bool IsValidPassword(string password) {
      //changed by JP on 1/18/2012 to remove char limitations on passwords

      bool isValid = false;

      if(!String.IsNullOrWhiteSpace(password)) // && password.IndexOf(" ") == -1)
      {
        //if (IsAlphaNumeric(password) && password.Length > 7 && password.Length < 21)
        if(password.Length > 5) {
          isValid = true;
        }
      }

      return isValid;
    }

    /// <summary>
    /// checks for alpha and numeric characters in a string
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static bool IsAlphaNumeric(string str) {
      if(String.IsNullOrWhiteSpace(str)) {
        return false;
      }

      Regex regexAlphaNum = new Regex("[^a-zA-Z0-9]");
      return !regexAlphaNum.IsMatch(str);
    }


    public static bool IsValidReturnPath(string path) {
      bool isValid = false;

      path = path.ToLower();

      if(!String.IsNullOrEmpty(path) && path.IndexOf("/account/signout") == -1 && path.IndexOf("/home/signout") == -1 && path.IndexOf("%2faccount%2fsignout") == -1 && (path.IndexOf("/") == 0 || path.IndexOf("%2f") == 0)) {
        isValid = true;
      }

      return isValid;
    }


  }

}
