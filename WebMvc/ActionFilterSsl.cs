﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

using Pantry.Helpers;

namespace Pantry.Web.Mvc {
  //supported uri scheme values
  public enum UriScheme {
    Http,
    Https
  }


  public class RequireSSL : ActionFilterAttribute {
    private UriScheme TargetUriScheme { get; set; }
    public int? HttpPort { get; set; }
    public int? HttpsPort { get; set; }

    public override void OnActionExecuting(ActionExecutingContext filterContext) {
      HttpRequestBase reqst = filterContext.HttpContext.Request;
      HttpResponseBase respns = filterContext.HttpContext.Response;

      bool isSecure = false;

      if(reqst.IsSecureConnection) {
        isSecure = true;
      }

      string http_x_forwarded_port = filterContext.HttpContext.Request.ServerVariables["HTTP_X_FORWARDED_PORT"] + "";
      if(String.IsNullOrWhiteSpace(http_x_forwarded_port)) {
        http_x_forwarded_port = "empty";
      }

      if(http_x_forwarded_port == "443") {
        isSecure = true;
      }

      if(filterContext.HttpContext.Request.Url.Port == 44300 || filterContext.HttpContext.Request.Url.Port == 44301) {
        isSecure = true;
      }

      if(!isSecure && !reqst.IsLocal) {
        UriBuilder uriBuilder = null;
        TargetUriScheme = UriScheme.Https;

        if(reqst.IsLocal) {
          uriBuilder = new UriBuilder(reqst.Url);
        } else {
          uriBuilder = new UriBuilder(reqst.Url) { Scheme = Uri.UriSchemeHttps, Port = 443 };
        }

        string url = "";

        url = uriBuilder.Uri.ToString();

        respns.Redirect(url);
      }

      base.OnActionExecuting(filterContext);
    }
  }


  public class DropSSL : ActionFilterAttribute {
    private UriScheme TargetUriScheme { get; set; }
    public int? HttpPort { get; set; }
    public int? HttpsPort { get; set; }

    public override void OnActionExecuting(ActionExecutingContext filterContext) {
      HttpRequestBase reqst = filterContext.HttpContext.Request;
      HttpResponseBase respns = filterContext.HttpContext.Response;

      bool isSecure = false;

      if(reqst.IsSecureConnection) {
        isSecure = true;
      }

      string http_x_forwarded_port = filterContext.HttpContext.Request.ServerVariables["HTTP_X_FORWARDED_PORT"] + "";
      if(String.IsNullOrWhiteSpace(http_x_forwarded_port)) {
        http_x_forwarded_port = "empty";
      }

      if(http_x_forwarded_port == "443") {
        isSecure = true;
      }

      if(filterContext.HttpContext.Request.Url.Port == 44300 || filterContext.HttpContext.Request.Url.Port == 44301) {
        isSecure = true;
      }

      if(isSecure) {
        UriBuilder uriBuilder = null;
        TargetUriScheme = UriScheme.Http;

        if(reqst.IsLocal) {
          uriBuilder = new UriBuilder(reqst.Url);
        } else {
          uriBuilder = new UriBuilder(reqst.Url) { Scheme = Uri.UriSchemeHttp, Port = 80 };
        }

        string url = "";

        url = uriBuilder.Uri.ToString();

        respns.Redirect(url);
      }

      base.OnActionExecuting(filterContext);
    }
  }

  
  public class RequireHttpsAttribute : System.Web.Mvc.RequireHttpsAttribute {

    public override void OnAuthorization(AuthorizationContext filterContext) {

      if(filterContext == null) {
        throw new ArgumentNullException("filterContext");
      }

      if(filterContext.HttpContext.Request.IsSecureConnection || filterContext.HttpContext.Request.ServerVariables["HTTP_X_FORWARDED_PORT"] == "443") {
        return;
      }


      string host = filterContext.HttpContext.Request.Url.Host.ToString().ToLower();

      Logging.Log2Text("ssl-host.txt", host);

      if(host.Contains("localhost")) {
        return;
      }

      if(host != "api.digitaljuice.com" && host != "subscriptions.digitaljuice.com") {
        //if (host == "localhost" ) {
        //if (host.IndexOf("digitaljuice.com") == -1) {
        return;
      }

      HandleNonHttpsRequest(filterContext);

    }
  }


  public class RequireHttpAttribute : RequireHttpsAttribute {

    public override void OnAuthorization(AuthorizationContext filterContext) {

      if(filterContext == null) {
        throw new ArgumentNullException("filterContext");
      }

      string host = filterContext.HttpContext.Request.Url.Host.ToString();
      string port = filterContext.HttpContext.Request.Url.Port.ToString();

      Logging.Log2Text("ssl-host.txt", host);
      Logging.Log2Text("ssl-port.txt", port);

      if(filterContext.HttpContext.Request.IsSecureConnection || filterContext.HttpContext.Request.ServerVariables["HTTP_X_FORWARDED_PORT"] == "443") {

        if(port != "80" || port != "443") {
          host = host + ":" + port;
        }

        string pathAndQuery = filterContext.HttpContext.Request.Url.PathAndQuery.ToString();
        filterContext.Result = new RedirectResult("http://" + host + pathAndQuery);
      }


    }
  }

}
