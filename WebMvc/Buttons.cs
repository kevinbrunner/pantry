﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Razor;

using Pantry.Utilities;


namespace Pantry.Web.Mvc.Extensions {

  public enum FormButtonType {
    Anchor,
    Button,
    Submit
  }

  public class FormButton {
    public FormButton() {
      this.ID = "";
      this.Text = "";
      this.CssClass = "";
      this.ButtonType = FormButtonType.Button;
      this.TabIndex = 0;
      this.IsDisabled = false;
      this.HRef = "";
      this.WidthPix = 0;
    }

    public FormButton(string id, string text, string cssClass, FormButtonType buttonType, int tabIndex = 0, bool isDisabled = false, string hRef = "", string cssStyle = "", int widthPx = 0) {
      this.ID = id;
      this.Text = text;
      this.CssClass = cssClass;
      this.ButtonType = buttonType;
      this.TabIndex = tabIndex;
      this.IsDisabled = isDisabled;
      this.HRef = hRef;
      this.CssStyle = cssStyle;
      this.WidthPix = widthPx;
    }

    public string ID { get; set; }
    public string Text { get; set; }
    public string CssClass { get; set; }
    public FormButtonType ButtonType { get; set; }
    public int TabIndex { get; set; }
    public bool IsDisabled { get; set; }
    public string HRef { get; set; }
    public string CssStyle { get; set; }
    public int WidthPix { get; set; }
  }


  public static class MvcHtmlHelperExtensions {

    public static MvcHtmlString FormButtons(this HtmlHelper helper, List<FormButton> formButtons, int width, int marginPx, string cssClass = "", string cssStyle = "", string id = "") {

      string ulID=id;
      if(ulID == "") { ulID = "FormButtons"; }

      width = width + 1; // just to be safe... dont know if it matters or now, but please leave in.
      marginPx = marginPx < 0 ? 0 : marginPx;
      StringBuilder LiList = new StringBuilder();
      TagBuilder tagBuilder = new TagBuilder("tag");
      TagBuilder liBuilder = new TagBuilder("li");

      bool isNotFirst = false;

      foreach(FormButton fb in formButtons) {
        liBuilder = new TagBuilder("li");
        if(isNotFirst) {
          liBuilder.MergeAttribute("style", "margin-left:" + marginPx.ToString() + "px;");
        }

        if (fb.WidthPix > 0) {
          if (fb.CssStyle.IsEmpty()) {
            fb.CssStyle = "width: " + fb.WidthPix.ToString() + "px; ";
          } else {
            fb.CssStyle = fb.CssStyle + "; width: " + fb.WidthPix.ToString() + "px; ";
          }
        }
      
          switch(fb.ButtonType) {
            case FormButtonType.Anchor:
              tagBuilder = new TagBuilder("a");
              tagBuilder.MergeAttribute("href", fb.HRef);
              tagBuilder.InnerHtml = fb.Text;
              break;
            case FormButtonType.Button:
              tagBuilder = new TagBuilder("input");
              tagBuilder.MergeAttribute("value", fb.Text);
              tagBuilder.MergeAttribute("type", "button");
              break;
            case FormButtonType.Submit:
              tagBuilder = new TagBuilder("input");
              tagBuilder.MergeAttribute("value", fb.Text);
              tagBuilder.MergeAttribute("type", "submit");
              break;
          }

        tagBuilder.MergeAttribute("name", fb.ID);
        tagBuilder.MergeAttribute("id", fb.ID);

        if(fb.TabIndex > 0) {
          tagBuilder.MergeAttribute("tabindex", fb.TabIndex.ToString());
        }

        if(fb.IsDisabled) {
          tagBuilder.MergeAttribute("disabled", "disabled");
        }

        if(!String.IsNullOrEmpty(fb.CssStyle)) {
          tagBuilder.MergeAttribute("style", fb.CssStyle);
        }

        tagBuilder.AddCssClass(fb.CssClass);

        switch(fb.ButtonType) {
          case FormButtonType.Anchor:
            liBuilder.InnerHtml = tagBuilder.ToString();
            break;
          default:
            liBuilder.InnerHtml = tagBuilder.ToString(TagRenderMode.SelfClosing);
            break;
        }

        LiList.Append(liBuilder.ToString());
        isNotFirst = true;
      }

      TagBuilder ulBuilder = new TagBuilder("ul");
      ulBuilder.MergeAttribute("style", "width: " + width.ToString() + "px; " + cssStyle);
      ulBuilder.MergeAttribute("class", "FormButtons " + cssClass);
      ulBuilder.MergeAttribute("id", ulID);
      ulBuilder.InnerHtml = LiList.ToString();

      return new MvcHtmlString(ulBuilder.ToString());

    }


  }

}
