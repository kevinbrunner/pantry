﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Security;

using Pantry.Config;
using Pantry.Utilities;


namespace Pantry.Web.Mvc {
  public class JCookies {
    private static int rememberMeMinutes = 43200;
    private static int dontRememberMeMinutes = 720;
    private static int rememberMeMinutes_ = 720;

    public static void SetVistorCookie(string visitorID) {

      if (visitorID.IsEmpty()) {
        visitorID = Guid.NewGuid().ToString();
      }

      HttpCookie vistorCookieNew = new HttpCookie(SiteConfig.CookieName);

      vistorCookieNew.Path = "/";
      vistorCookieNew.Expires = DateTime.Now.AddDays(SiteConfig.CookieAge);
      vistorCookieNew["id"] = visitorID;

      try {
        HttpContext.Current.Response.SetCookie(vistorCookieNew);
      }
      catch (Exception e) {
      }

    }


    public static void SetAuthorizationCookie(string accountID, bool rememberMe = false) {
      rememberMeMinutes_ = rememberMeMinutes;

      if (!rememberMe) {
        rememberMeMinutes_ = dontRememberMeMinutes;
      }

      FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
        1,
        accountID,
        DateTime.Now,
        DateTime.Now.AddMinutes(rememberMeMinutes),
        rememberMeMinutes_ == rememberMeMinutes,
        "",
        FormsAuthentication.FormsCookiePath
      );

      HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket));
      authCookie.Path = "/";
      authCookie.Expires = DateTime.Now.AddMinutes(rememberMeMinutes);


      if (HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName] != null) {
        HttpContext.Current.Response.SetCookie(authCookie);
      } else {
        HttpContext.Current.Response.Cookies.Add(authCookie);
      }

      SetVistorCookie(accountID);

      if (rememberMe) {
        rememberMeMinutes_ = rememberMeMinutes;
      } else {
        rememberMeMinutes_ = dontRememberMeMinutes;
      }

      HttpCookie rememberMeCookie = new HttpCookie("jCookieData");
      rememberMeCookie.Path = "/";
      rememberMeCookie.Expires = DateTime.Now.AddYears(3);
      rememberMeCookie["rememberMe"] = rememberMeMinutes_.ToString();

      try {
        HttpContext.Current.Response.SetCookie(rememberMeCookie);
      }
      catch (Exception e) {
      }
    }


    public static bool IsRememberMe() {
      bool rememberMe = false;
      HttpCookie rememberMeCookie = HttpContext.Current.Request.Cookies["jCookieData"];
      if (rememberMeCookie != null) {
        int rMe = 0;
        if (int.TryParse(rememberMeCookie["rememberMe"], out rMe)) {
          if (rMe > 1440) {
            rememberMe = true;
          }
        }
      }

      return rememberMe;
    }

    
    public static void SetJuicyAuthCookie(string accountID) {

      HttpCookie juicyAuthCookie = new HttpCookie("juicyAuthCookie");
      juicyAuthCookie.Expires = DateTime.Now.AddDays(SiteConfig.CookieAge);

      juicyAuthCookie.Path = "/";
      juicyAuthCookie["id"] = accountID;

      if (HttpContext.Current.Request.Cookies["juicyAuthCookie"] != null) {
        HttpContext.Current.Response.SetCookie(juicyAuthCookie);
      } else {
        HttpContext.Current.Response.Cookies.Add(juicyAuthCookie);
      }

    }


    public static void ClearJuicyAuthCookie() {

      HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];

      if (authCookie != null) {
        authCookie.Path = "/";
        authCookie.Expires = DateTime.Now.AddYears(-1);
        authCookie.Value = "";

        HttpContext.Current.Response.SetCookie(authCookie);
      }

    }



    public static void SignOut() {

      HttpContext.Current.Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
      FormsAuthentication.SignOut();
    }


  }
}