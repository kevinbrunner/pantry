﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

using Pantry.Web.Mvc;


namespace Pantry.Web.Mvc.Extensions {

  public static class ExtensionsHelper {

    public static ViewGlobals GetViewGlobals(this HtmlHelper html) {

      PantryController controller = html.ViewContext.Controller as PantryController;

      if(controller != null) {
        return controller.ViewGlobals;
      }

      return null;

    }


    public static PantryController GetController(this HtmlHelper html) {

      PantryController controller = html.ViewContext.Controller as PantryController;

      return controller;
    }


  }
}
