﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Routing;

namespace Pantry.Web.Mvc.Extensions {

  public static class MvcHtmlChecks {

    public static MvcHtmlString MyCheckBoxFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool>> expression) {
      return htmlHelper.MyCheckBoxFor(expression, label: null);
    }


    public static MvcHtmlString MyCheckBoxFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool>> expression, IDictionary<string, object> htmlAttributes = null) {
      //Derive property name for checkbox name
      UnaryExpression propExpression = expression.Body as UnaryExpression;

      string propertyName = ((MemberExpression)propExpression.Operand).Member.Name;

     TagBuilder builder = new TagBuilder("span");

      builder.MergeAttribute("class", "juicycheckbox");
      builder.MergeAttributes<string, object>(htmlAttributes);

      builder.InnerHtml = string.Format("<input type=\"hidden\" id=\"{0}\" name=\"{0}\" value=\"{1}\" />", propertyName, expression.Compile().Invoke(htmlHelper.ViewData.Model).ToString().ToLower());
      //builder.InnerHtml = string.Format("<label for=\"{0}\" class=\"labelCheck\" ><input name=\"{0}\" id=\"{0}\" type=\"checkbox\" checked=\"checked\" />{1}</label>", propertyName, expression.Compile().Invoke(htmlHelper.ViewData.Model).ToString().ToLower()); //, ...Make this my Single Click credit card.

      return new MvcHtmlString(builder.ToString());
    }


    public static MvcHtmlString FormattedCheckBoxFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool>> expression, string htmlText, IDictionary<string, object> htmlAttributes = null) {
      //Derive property name for checkbox name
      string propertyName = ((MemberExpression)expression.Body).Member.Name;

      TagBuilder builder = new TagBuilder("label");

      builder.MergeAttribute("class", "labelCheck");
      builder.MergeAttribute("for", propertyName);
      builder.MergeAttribute("id", propertyName + "Label");
      //builder.MergeAttributes<string, object>(htmlAttributes);

      var a = expression.Compile().Invoke(htmlHelper.ViewData.Model).ToString().ToLower();
      var isChecked = expression.Compile().Invoke(htmlHelper.ViewData.Model).ToString().ToLower() == "on" ? "checked=\"checked\" " : "";

      builder.InnerHtml = string.Format("<input name=\"{0}\" id=\"{0}\" type=\"checkbox\" {1}/>{2}", propertyName, isChecked, htmlText);

      return new MvcHtmlString(builder.ToString());
    }


    public static MvcHtmlString FormattedCheckBoxFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool>> expression, object htmlAttributes) {
      if(htmlAttributes == null) {
        return htmlHelper.MyCheckBoxFor(expression, new RouteValueDictionary());
      }
      else {
        return htmlHelper.MyCheckBoxFor(expression, new RouteValueDictionary(htmlAttributes));
      }
    }


    public static MvcHtmlString MyCheckBoxFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool>> expression, object htmlAttributes) {
      if(htmlAttributes == null) {
        return htmlHelper.MyCheckBoxFor(expression, new RouteValueDictionary());
      }
      else {
        return htmlHelper.MyCheckBoxFor(expression, new RouteValueDictionary(htmlAttributes));
      }
    }


    public static MvcHtmlString MyCheckBoxFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool>> expression, string label) {

      //Derive property name for checkbox name
      UnaryExpression propExpression = expression.Body as UnaryExpression;

      string propertyName = ((MemberExpression)propExpression.Operand).Member.Name;

      TagBuilder builder = new TagBuilder("span");
      builder.MergeAttribute("class", "juicycheckbox");

      builder.InnerHtml = string.Format("<input type=\"hidden\" id=\"{0}\" name=\"{0}\" value=\"{1}\" />", propertyName, expression.Compile().Invoke(htmlHelper.ViewData.Model).ToString().ToLower());

      return new MvcHtmlString(builder.ToString() + label ?? String.Empty);

    }

  }



}