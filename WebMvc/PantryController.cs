﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

using Pantry.Config;
//using Pantry.Entities;
using Pantry.Helpers;

using fastJSON;


namespace Pantry.Web.Mvc {

  public class PantryController : PantryJsonController {

    public static string visitorID = "";
    public HttpCookie vistorCookie;
    public ViewGlobals ViewGlobals { get; protected set; }
    public string ViewBasePath { get; set; }
    public string ControllerName { get; set; }
    public string AreaPath { get; set; }
    public string ActionName { get; set; }

    public PantryController() {
      ViewGlobals = new ViewGlobals(this);
    }


    protected override void Initialize(RequestContext requestContext) {
      //always on TOP!
      base.Initialize(requestContext);

      this.ViewGlobals.PageTitle = "no page title set here!!!";

    }


    //protected override void OnActionExecuted(ActionExecutedContext ctx) {
    //  base.OnActionExecuted(ctx);
    //  //this.PageContext.PageTitle += "Controller ActionExecutedContext ctx - ";
    //  //ctx.HttpContext.Trace.Write("Log: OnActionExecuted", "After " + ctx.ActionDescriptor.ActionName);

    //  //HistoryTracking.SaveTracking(this.Account.AccountID, this.PageContext.PageTitle , HttpContext.Current.Request.RawUrl.ToLower());

    //}


    //protected override void OnActionExecuting(ActionExecutingContext ctx) {
    //  base.OnActionExecuting(ctx);
    //  //this.PageContext.PageTitle += "Controller OnActionExecuting - ";
    //}


    public string ControllerPath {
      get { return this.ControllerName + "/"; }
    }


    public string AreaControllerPath {
      get {
        return this.AreaPath + this.ControllerPath;
      }
    }


    //protected override void OnException(ExceptionContext filterContext) {
    //  // Bail if we can't do anything; app will crash.
    //  if(filterContext == null) {
    //    return;
    //  }

    //  // since we're handling this, log to elmah
    //  var ex = filterContext.Exception ?? new Exception("No further information exists.");
    //  //LogException(ex);

    //  filterContext.ExceptionHandled = true;
    //  var data = new ErrorPresentation {
    //    ErrorMessage = HttpUtility.HtmlEncode(ex.Message),
    //    TheException = ex,
    //    ShowMessage = !(filterContext.Exception == null),
    //    ShowLink = false
    //  };
    //  filterContext.Result = View("ErrorPage", data);
    //}




  }


  public class ImageResult : ActionResult {
    public ImageResult() { }

    public ImageResult(MemoryStream imageStream, string contentType) {
      if(imageStream == null) {
        throw new ArgumentNullException("imageStream");
      }
      if(contentType == null) {
        throw new ArgumentNullException("contentType");
      }

      this.ImageStream = imageStream;
      this.ContentType = contentType;
    }

    public ImageResult(MemoryStream imageStream, ImageContentTypes contentType = ImageContentTypes.Jpg) {
      if(imageStream == null) {
        throw new ArgumentNullException("imageStream");
      }

      this.ImageStream = imageStream;
      this.ContentType = ContentMimeType.MimeType(contentType.ToString().ToLower());
    }

    public MemoryStream ImageStream { get; private set; }
    public string ContentType { get; private set; }

    public override void ExecuteResult(ControllerContext context) {
      if(context == null) {
        throw new ArgumentNullException("context");
      }

      HttpResponseBase response = context.HttpContext.Response;

      response.ContentType = this.ContentType;
      response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
      response.Cache.SetValidUntilExpires(false);
      response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
      response.Cache.SetCacheability(HttpCacheability.NoCache);
      response.Cache.SetNoStore();

      byte[] buffer = new byte[4096];
      while(true) {
        int read = this.ImageStream.Read(buffer, 0, buffer.Length);
        if(read == 0) {
          break;
        }

        response.OutputStream.Write(buffer, 0, read);
      }

      response.End();
    }
  }


  public enum ImageContentTypes {
    Png,
    Jpg,
    Gif
  }


  public static class ControllerExtensions {
    public static ImageResult Image(this Controller controller, MemoryStream imageStream, string contentType) {
      return new ImageResult(imageStream, contentType);
    }

    public static ImageResult Image(this Controller controller, byte[] imageBytes, string contentType) {
      return new ImageResult(new MemoryStream(imageBytes), contentType);
    }

    public static ImageResult Image(this Controller controller, MemoryStream imageStream, ImageContentTypes contentType = ImageContentTypes.Jpg) {
      return new ImageResult(imageStream, contentType);
    }

    public static ImageResult Image(this Controller controller, byte[] imageBytes, ImageContentTypes contentType = ImageContentTypes.Jpg) {
      return new ImageResult(new MemoryStream(imageBytes), contentType);
    }
  }


  public abstract class PantryJsonController : System.Web.Mvc.Controller {
    private string IdentityName = null;

    public string IdentityID {
      get {

        if(this.HttpContext != null && this.HttpContext.User != null && this.HttpContext.User.Identity != null && string.IsNullOrEmpty(IdentityName)) {
          IdentityName = this.HttpContext.User.Identity.Name;
        }

        return IdentityName;

      }
      set { IdentityName = value; }
    }


    protected string RenderPartialViewToString() {
      return RenderPartialViewToString(null, null);
    }


    protected string RenderPartialViewToString(string viewName) {
      return RenderPartialViewToString(viewName, null);
    }


    protected string RenderPartialViewToString(object model) {
      return RenderPartialViewToString(null, model);
    }


    protected string RenderPartialViewToString(string viewName, object model) {
      if(string.IsNullOrEmpty(viewName)) {
        viewName = ControllerContext.RouteData.GetRequiredString("action");
      }

      ViewData.Model = model;

      using(StringWriter sw = new StringWriter()) {
        ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
        ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
        viewResult.View.Render(viewContext, sw);

        return sw.GetStringBuilder().ToString();
      }
    }


    public class FastJsonResult : JsonResult {

      public override void ExecuteResult(ControllerContext context) {
        if(context == null) {
          throw new ArgumentNullException("context");
        }

        if(this.JsonRequestBehavior == JsonRequestBehavior.DenyGet && String.Equals(context.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase)) {
          throw new InvalidOperationException("JSON GET is not allowed");
        }

        HttpResponseBase response = context.HttpContext.Response;
        response.ContentType = String.IsNullOrEmpty(this.ContentType) ? "application/json" : this.ContentType;

        if(this.ContentEncoding != null) {
          response.ContentEncoding = this.ContentEncoding;
        }

        if(this.Data == null) {
          return;
        }

        string jsonString = fastJSON.JSON.ToJSON(this.Data, new fastJSON.JSONParameters() { UseExtensions = false, UsingGlobalTypes = false, UseUTCDateTime = false, EnableAnonymousTypes = true });

        response.Write(jsonString);
      }
    }


    public RedirectToRouteResult GoHome() {
      return RedirectToAction("Index", "Home");
    }

  

  }
}
