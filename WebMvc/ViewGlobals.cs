﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Pantry.Config;
using Pantry.Entities;

namespace Pantry.Web.Mvc {

  public class ViewGlobals {

    public PantryController Controller { get; set; }

    public ViewGlobals(PantryController controller) {
      this.Controller = controller;
      this.PageTitle = "";
      this.PageSubType = "";
      this.PageType = "";
      this.NameOnAccount = "Guest";
      this.BreadCrumbs = new List<Pantry.Entities.BreadCrumb>();
      this.PageLayout = "";
      this.MetaKeywords = "";
      this.MetaDescription = "";
      this.JavaScripts = new List<string>();
      this.StyleSheets = new List<string>();

      this.Account = new Pantry.Entities.Account();
      this.VisitorID = "";

      this.ReturnUrl = "";
      this.PathAndQuery = "";
      this.Domain = "";
    }


    public string PageType { get; set; }
    public string PageSubType { get; set; }
    public string PageTitle { get; set; }
    public string NameOnAccount { get; set; }
    public string NameFirstL { get; set; }
    public List<string> JavaScripts { get; set; }
    public List<string> StyleSheets { get; set; }

    public List<Pantry.Entities.BreadCrumb> BreadCrumbs { get; set; }
    public string PageLayout { get; set; }
    public string MetaKeywords { get; set; }
    public string MetaDescription { get; set; }

    public Pantry.Entities.Account Account { get; set; }

    public string VisitorID { get; set; }

    public string ReturnUrl { get; set; }
    public string PathAndQuery { get; set; }

    public string Domain { get; set; }

    public string UserAgent { get; set; }

    public int FamilyMembersCount { get; set; }
    public int FamiliesCount { get; set; }

    public ServiceLevel ServiceLevel { get; set; }

    private int pantryID = 1;
    public int PantryID {
      get {
        switch(this.Domain) { 
          case "fbcucrisis.azurewebsites.net":
            pantryID = 1;
            break;

          default:
            pantryID = 1;
            break;

        }
        return pantryID;
      }
    }

    public Pantry.Entities.DeviceType DeviceType {
      get {
        Pantry.Entities.DeviceType deviceType = Pantry.Entities.DeviceType.Desktop;
        string userAgent = this.UserAgent.ToLowerInvariant();
        if(userAgent.Contains("blackberry")) {
          deviceType = Pantry.Entities.DeviceType.Phone;
        }

        if(userAgent.Contains("iphone")) {
          deviceType = Pantry.Entities.DeviceType.Phone;
        }

        if(userAgent.Contains("ipod")) {
          deviceType = Pantry.Entities.DeviceType.Phone;
        }

        if(userAgent.Contains("trident")) { // Windows
          if(userAgent.Contains("phone")) {
            deviceType = Pantry.Entities.DeviceType.Phone;
            if(userAgent.Contains("phone os 7.0")) {
              deviceType = Pantry.Entities.DeviceType.Phone;
            } else if(userAgent.Contains("phone os 7.5")) {
              deviceType = Pantry.Entities.DeviceType.Phone;
            } else if(userAgent.Contains("arm;") || userAgent.Contains("phone os 8")) {
              deviceType = Pantry.Entities.DeviceType.Phone;
            }
          } else if(userAgent.Contains("arm;")) { // surface
            deviceType = Pantry.Entities.DeviceType.Tablet;
          } else { // surface
            deviceType = Pantry.Entities.DeviceType.Tablet;
          }
        }

        if(userAgent.Contains("ipad")) {
          deviceType = Pantry.Entities.DeviceType.Tablet;
        }

        if(userAgent.Contains("android")) {
          if(userAgent.Contains("mobile")) {
            deviceType = Pantry.Entities.DeviceType.Phone;
          } else {
            deviceType = Pantry.Entities.DeviceType.Tablet;
          }
        }

        return deviceType;
      }
    }

    
  }


}
